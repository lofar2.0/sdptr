#! /usr/bin/python3

import subprocess
import time

p1 = subprocess.Popen('git describe --tags --always', stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
tag = p1.communicate()[0].decode('utf-8')

if tag.startswith("v"):
    print(f"version tag: {tag}")
    tag_str = f"{tag}"
else:
    tag_str = time.strftime("%Y-%m-%dT%H.%M.%S", time.gmtime())
    p2 = subprocess.Popen('git tag -a {} -m "build tag"'.format(tag_str), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    print(p2.communicate()[0].decode('utf-8'))

with open('version.txt', 'w') as fd:
    fd.write(tag_str)

# replace VERSION info in config.h file.
with open('config.h', 'r') as fd:
    config_file = fd.read()

ptr = config_file.find('#define VERSION')
ptr1 = config_file.find('"', ptr) + 1
ptr2 = config_file.find('"', ptr1)

old_version = config_file[ptr1:ptr2]
config_file = config_file.replace(old_version, tag_str.strip())

with open('config.h', 'w') as fd:
    fd.write(config_file)
