/* *************************************************************************
 * Copyright 2023
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *********************************************************************** */

/* *************************************************************************
 * Author:
 * . Leon Hiemstra
 * . Pieter Donker
 * Purpose:
 * . opc-ua to ucp translator
 * Description:
 * . holds information about this program
 * *********************************************************************** */

// You need to compile with _REENTRANT defined since this uses threads
#ifndef _REENTRANT
#define _REENTRANT 1
#endif

#include <iostream>
#include <exception>
#include <csignal>

#include "tr.h"
#include "sdptr.h"
#include "constants.h"
#include "tools/loguru.h"
#include "tools/util.h"

using namespace std;

// Everything addressed with TR_...
extern int debug;

extern Serverdat SD;

#define FPGA_MASK 1
#define SOFTWARE_VERSION 2
#define START_TIME 3
#define TOD_SYNC_STATUS 4
#define TOD 5
#define TOD_PPS_DELTA 6
#define PPS_MONITOR_DELTA 7
#define FPGA_COMMUNICATION_ERROR 8
#define SDP_CONFIG_FIRST_FPGA_NR 13
#define SDP_CONFIG_NOF_FPGAS 14
#define SDP_CONFIG_NOF_BEAMSETS 15
#define SDP_CONFIG_NOF_FILTERBANKS 16
#define UCP_DISABLE_COMMUNICATION 20
#define UCP_RESET_COUNTERS 21
#define UCP_NOF_READS 30
#define UCP_NOF_READ_RETRIES 31
#define UCP_NOF_READ_FAILURES 32
#define UCP_NOF_WRITES 40
#define UCP_NOF_WRITE_RETRIES 41
#define UCP_NOF_WRITE_FAILURES 42


TranslatorMap::TranslatorMap()
{
    translatorMap = new CPointMap();

    translatorMap->add_register("TR_fpga_mask_R", FPGA_MASK, SD.n_fpgas, 1, "RO", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_fpga_mask_RW", FPGA_MASK, SD.n_fpgas, 1, "RW", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_software_version_R", SOFTWARE_VERSION, 1, 1, "RO", REG_FORMAT_STRING);
    translatorMap->add_register("TR_start_time_R", START_TIME, 1, 1, "RO", REG_FORMAT_INT64);
    translatorMap->add_register("TR_tod_sync_status_R", TOD_SYNC_STATUS, 1, 1, "RO", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_tod_R", TOD, 1, 2, "RO", REG_FORMAT_INT64);
    translatorMap->add_register("TR_tod_pps_delta_R", TOD_PPS_DELTA, 1, 1, "RO", REG_FORMAT_DOUBLE);
    translatorMap->add_register("TR_pps_monitor_delta_R", PPS_MONITOR_DELTA, 1, 1, "RO", REG_FORMAT_DOUBLE);
    translatorMap->add_register("TR_fpga_communication_error_R", FPGA_COMMUNICATION_ERROR, SD.n_fpgas, 1, "RO", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_sdp_config_first_fpga_nr_R", SDP_CONFIG_FIRST_FPGA_NR, 1, 1, "RO", REG_FORMAT_UINT32);
    translatorMap->add_register("TR_sdp_config_nof_fpgas_R", SDP_CONFIG_NOF_FPGAS, 1, 1, "RO", REG_FORMAT_UINT32);
    translatorMap->add_register("TR_sdp_config_nof_beamsets_R", SDP_CONFIG_NOF_BEAMSETS, 1, 1, "RO", REG_FORMAT_UINT32);
    translatorMap->add_register("TR_sdp_config_nof_filterbanks_R", SDP_CONFIG_NOF_FILTERBANKS, 1, 1, "RO", REG_FORMAT_UINT32);

    translatorMap->add_register("TR_ucp_disable_communication_R", UCP_DISABLE_COMMUNICATION, SD.n_fpgas, 1, "RO", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_ucp_disable_communication_RW", UCP_DISABLE_COMMUNICATION, SD.n_fpgas, 1, "RW", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_ucp_reset_counters_R", UCP_RESET_COUNTERS, SD.n_fpgas, 1, "RO", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_ucp_reset_counters_RW", UCP_RESET_COUNTERS, SD.n_fpgas, 1, "RW", REG_FORMAT_BOOLEAN);
    translatorMap->add_register("TR_ucp_nof_reads_R", UCP_NOF_READS, SD.n_fpgas, 1, "RO", REG_FORMAT_UINT64);
    translatorMap->add_register("TR_ucp_nof_read_retries_R", UCP_NOF_READ_RETRIES, SD.n_fpgas, 1, "RO", REG_FORMAT_UINT64);
    translatorMap->add_register("TR_ucp_nof_read_failures_R", UCP_NOF_READ_FAILURES, SD.n_fpgas, 1, "RO", REG_FORMAT_UINT64);
    translatorMap->add_register("TR_ucp_nof_writes_R", UCP_NOF_WRITES, SD.n_fpgas, 1, "RO", REG_FORMAT_UINT64);
    translatorMap->add_register("TR_ucp_nof_write_retries_R", UCP_NOF_WRITE_RETRIES, SD.n_fpgas, 1, "RO", REG_FORMAT_UINT64);
    translatorMap->add_register("TR_ucp_nof_write_failures_R", UCP_NOF_WRITE_FAILURES, SD.n_fpgas, 1, "RO", REG_FORMAT_UINT64);
}

TranslatorMap::~TranslatorMap()
{
    my_delete(translatorMap);
}

/* translator() is only called by read() and write() and is used set/get the opc-ua request.
  data: char buffer for send/receive data, buffer size is calculated in ua_server.cpp
  cmd: ['R' | 'W'] read or write action.
  pointname: opcua pointname
1) Check if read/write can be done on the requested pointname.
2) convert pointname to a cmd_id, cmd_id is a numeric id for the pointname without the access part.
3) set or get requested point
4) return data and/or status
*/
bool TranslatorMap::translator(char *data, const char cmd, const string &pointname)
{
    bool retval = true;

    if (cmd == 'R' && !translatorMap->canRead(pointname))
    {
        return false;
    }
    else if (cmd == 'W' && !translatorMap->canWrite(pointname))
    {
        return false;
    }

    uint32_t n_nodes = translatorMap->getNodesSize(pointname);

    uint32_t cmd_id = translatorMap->getCommandId(pointname);

    switch (cmd_id)
    {
    case FPGA_MASK:
    {
        if (cmd == 'R')
        {
            bool *ptr_out = (bool *)data;
            vector<bool> masked = SD.fpga_map->get_all_masked_nodes();
            for (auto val : masked)
            {
                *ptr_out = val;
                ptr_out++;
            }
            // if real number of nodes to serve is less than OPCUA_FIXED_N_NODES,
            // add mask is false for nodes not available
            if (n_nodes < OPCUA_FIXED_N_NODES) {
                for (int i = 0; i < OPCUA_FIXED_N_NODES - (int)n_nodes; i++) {
                    *ptr_out = false;
                    ptr_out++;
                }
            }
        }
        else
        {
            bool *ptr_in = (bool *)data;
            vector<bool> masked;
            for (uint i = 0; i < n_nodes; i++)
            {
                masked.push_back(*ptr_in);
                ptr_in++;
            }
            SD.fpga_map->set_all_masked_nodes(masked);
        }
    }
    break;

    case SOFTWARE_VERSION:
        strcpy(data, SDPTR_VERSION);
        break;

    case TOD:
    {
        struct timespec ts;
        int64_t *ptr_out = (int64_t *)data;
        clock_gettime(CLOCK_REALTIME, (struct timespec *)&ts);
        ptr_out[0] = (int64_t)ts.tv_sec;
        ptr_out[1] = (int64_t)ts.tv_nsec;
    }
    break;

    case TOD_PPS_DELTA:
    {
        double pps_delta = SD.tod_pps_delta;
        memcpy(data, &pps_delta, sizeof(pps_delta));
    }
    break;

    case TOD_SYNC_STATUS:
    {
        /* check response from PTP/NTP,
           also if PTP activ -> 'NTPSynchronized=yes':

        > timedatectl show

        Timezone=Etc/UTC
        LocalRTC=no
        CanNTP=yes
        NTP=no
        NTPSynchronized=yes
        TimeUSec=Mon 2022-08-15 08:19:23 UTC
        */
        bool sync_status = true;
        string cmd = "timedatectl show";
        string valid_kv = "NTPSynchronized=yes";
        string response = exec(cmd.c_str());
        LOG_F(INFO, "%s", response.c_str());
        // sync_status is false if 'valid_kv' not found in response.
        if (response.find(valid_kv) == string::npos)
        {
            sync_status = false;
        }
        bool *ptr_out = (bool *)data;
        *ptr_out = sync_status;
    }
    break;

    case PPS_MONITOR_DELTA:
    {
        double pps_delta = SD.pps_monitor_delta;
        memcpy(data, &pps_delta, sizeof(pps_delta));
    }
    break;

    case START_TIME:
    {
        uint32_t *ptr_out = (uint32_t *)data;
        *ptr_out = (uint32_t)SD.start_time;
    }
    break;

    case FPGA_COMMUNICATION_ERROR:
    {
        bool *ptr_out = (bool *)data;
        vector<bool> offline = SD.fpga_map->get_all_offline_nodes();
        for (auto val : offline)
        {
            *ptr_out = val;
            ptr_out++;
        }
        // if real number of nodes to serve is less than OPCUA_FIXED_N_NODES,
        // add communication error for nodes not available
        if (n_nodes < OPCUA_FIXED_N_NODES) {
            for (int i = 0; i < OPCUA_FIXED_N_NODES - (int)n_nodes; i++) {
                *ptr_out = true;
                ptr_out++;
            }
        }
    }
    break;

    case SDP_CONFIG_FIRST_FPGA_NR:
    {
        uint32_t *ptr_out = (uint32_t *)data;
        *ptr_out = (uint32_t)SD.first_fpga_nr;
    }
    break;

    case SDP_CONFIG_NOF_FPGAS:
    {
        uint32_t *ptr_out = (uint32_t *)data;
        *ptr_out = (uint32_t)SD.n_fpgas;
    }
    break;

    case SDP_CONFIG_NOF_BEAMSETS:
    {
        uint32_t *ptr_out = (uint32_t *)data;
        *ptr_out = (uint32_t)SD.n_beamsets;
    }
    break;

    case SDP_CONFIG_NOF_FILTERBANKS:
    {
        uint32_t *ptr_out = (uint32_t *)data;
        *ptr_out = (uint32_t)SD.n_filterbanks;
    }
    break;

    case UCP_DISABLE_COMMUNICATION:
    {
        if (cmd == 'R')
        {
            bool *ptr_out = (bool *)data;
            vector<bool> state = SD.fpga_map->getAllUcpCommunicationStatus();
            for (auto val : state)
            {
                *ptr_out = val;
                ptr_out++;
            }
        }
        else
        {
            bool *ptr_in = (bool *)data;
            vector<bool> state;
            for (uint i = 0; i < n_nodes; i++)
            {
                state.push_back(*ptr_in);
                ptr_in++;
            }
            SD.fpga_map->setAllUcpSetCommunicationState(state);
        }
    }
    break;

    case UCP_RESET_COUNTERS:
    {
        if (cmd == 'R')
        {
            bool *ptr_out = (bool *)data;
            for (uint i = 0; i < n_nodes; i++)
            {
                *ptr_out = false;
                ptr_out++;
            }
        }
        else
        {
            bool *ptr_in = (bool *)data;
            vector<bool> reset;
            for (uint i = 0; i < n_nodes; i++)
            {
                reset.push_back(*ptr_in);
                ptr_in++;
            }
            SD.fpga_map->setAllUcpResetCounters(reset);
        }
    }
    break;

    case UCP_NOF_READS:
    {
        uint64_t *ptr_out = (uint64_t *)data;
        vector<uint64_t> counts = SD.fpga_map->getAllUcpNofReads();
        for (auto val : counts)
        {
            *ptr_out = val;
            ptr_out++;
        }
    }
    break;

    case UCP_NOF_READ_RETRIES:
    {
        uint64_t *ptr_out = (uint64_t *)data;
        vector<uint64_t> counts = SD.fpga_map->getAllUcpNofReadRetries();
        for (auto val : counts)
        {
            *ptr_out = val;
            ptr_out++;
        }
    }
    break;

    case UCP_NOF_READ_FAILURES:
    {
        uint64_t *ptr_out = (uint64_t *)data;
        vector<uint64_t> counts = SD.fpga_map->getAllUcpNofReadFailures();
        for (auto val : counts)
        {
            *ptr_out = val;
            ptr_out++;
        }
    }
    break;

    case UCP_NOF_WRITES:
    {
        uint64_t *ptr_out = (uint64_t *)data;
        vector<uint64_t> counts = SD.fpga_map->getAllUcpNofWrites();
        for (auto val : counts)
        {
            *ptr_out = val;
            ptr_out++;
        }
    }
    break;

    case UCP_NOF_WRITE_RETRIES:
    {
        uint64_t *ptr_out = (uint64_t *)data;
        vector<uint64_t> counts = SD.fpga_map->getAllUcpNofWriteRetries();
        for (auto val : counts)
        {
            *ptr_out = val;
            ptr_out++;
        }
    }
    break;

    case UCP_NOF_WRITE_FAILURES:
    {
        uint64_t *ptr_out = (uint64_t *)data;
        vector<uint64_t> counts = SD.fpga_map->getAllUcpNofWriteFailures();
        for (auto val : counts)
        {
            *ptr_out = val;
            ptr_out++;
        }
    }
    break;

    default:
        retval = false;
        break;
    }
    return retval;
}

/* read and write are called by the ua_server */
bool TranslatorMap::read(char *data, const string &pointname)
{
    return translator(data, 'R', pointname);
}

bool TranslatorMap::write(char *data, const string &pointname)
{
    return translator(data, 'W', pointname);
}
