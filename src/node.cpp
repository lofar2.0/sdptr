/* *************************************************************************
 * Copyright 2023
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . This is the class that does the communication with the nodes, using several
#   threads with workers.
* *********************************************************************** */

// You need to compile with _REENTRANT defined since this uses threads
#ifndef _REENTRANT
#define _REENTRANT 1
#endif

#include <unistd.h>
#include <stdexcept>
#include <cstring>
#include <cstdio>

#include "tools/util.h"
#include "tools/loguru.h"
#include "node.h"

using namespace std;

extern int debug;
volatile bool WorkersRunning = true;

cCmd::cCmd() { init(); }
void cCmd::init()
{
    action = ' ';
    cmd_id = 0;
    nvalues = 0;
    format = REG_FORMAT_UNKNOWN;
    data = nullptr;
}
cCmd::~cCmd() {}

cReply::cReply() { init(); }
void cReply::init()
{
    action = ' ';
    cmd_id = 0;
    retval = 0;
    nvalues = 0;
    format = REG_FORMAT_UNKNOWN;
}
cReply::~cReply() {}

void Node::worker()
{
    string thread_name = "node " + to_string(GlobalNr) + " thread";
    loguru::set_thread_name(thread_name.c_str());

    LOG_F(DEBUG, "start worker for client: Uniboard=%d, IP=%s, localnr=%d, globalnr=%d", UniboardNr, IpAddr.c_str(), LocalNr, GlobalNr);

    /* As long as the worker is running handle request from the cmd_queue
     * it calls the read or write function in the Periph_fpga class.
     * and returns the response in the reply_queue
     * WorkersRunning is set to false on a Ctrl-C (kill)
     */
    while (WorkersRunning)
    {
        cCmd p;

        if (cmd_queue.Pop(p) == false)
        {
            continue;
        }
        LOG_F(DEBUG, "worker cmd: action=%d, cmd_id=%d, nvalues=%zu, format=%d", p.action, p.cmd_id, p.nvalues, p.format);

        cReply r;
        r.action = p.action;
        try
        {
            switch (p.action)
            {
            case 'S':
                LOG_F(WARNING, "Received stop request, shutdown now!!");
                WorkersRunning = false;
                r.format = p.format;
                continue; // shutdown worker thread
                break;
            case 'M':
                r.retval = periph_fpga->monitor();
                r.format = p.format;
                MonitorDone = true;
                continue; // monitor points done, do not fill reply_queue
                break;
            case 'r':
                // read last writen value
                r.retval = periph_fpga->read_saved(p.data, p.cmd_id);
                r.cmd_id = p.cmd_id;
                r.nvalues = p.nvalues;
                r.format = p.format;
                break;
            case 'R':
                // read
                r.retval = periph_fpga->read(p.data, p.cmd_id);
                r.cmd_id = p.cmd_id;
                r.nvalues = p.nvalues;
                r.format = p.format;
                break;
            case 'W':
                // write
                r.retval = periph_fpga->write(p.data, p.cmd_id);
                r.cmd_id = p.cmd_id;
                r.nvalues = p.nvalues;
                r.format = p.format;
                break;
            default:
                throw runtime_error("Node::nothing to do");
                break;
            }
        }
        catch (exception &e)
        {
            r.retval = 0;
            LOG_F(ERROR, "exception: %s", e.what());
        }
        catch (...)
        {
            r.retval = 0;
            LOG_F(ERROR, "'...' exception");
        }
        // LOG_F(DEBUG, "worker response: action=%d, cmd_id=%d, nvalues=%d, format=%d, retval=%d", r.action, r.cmd_id, r.nvalues, r.format, r.retval);

        reply_queue.Push(r);
    }
    LOG_F(DEBUG, "leaving worker for client: Uniboard=%d, IP=%s, localnr=%d, globalnr=%d", UniboardNr, IpAddr.c_str(), LocalNr, GlobalNr);
}

Node::Node(const string &ipaddr, const uint udpport, const uint unb, const uint localnr, const uint n_filterbanks, const uint n_beamsets) : IpAddr(ipaddr),
                                                                                                                                            UdpPort(udpport),
                                                                                                                                            UniboardNr(unb),
                                                                                                                                            LocalNr(localnr),
                                                                                                                                            GlobalNr(localnr + FPGAS_PER_BOARD * unb),
                                                                                                                                            nFilterbanks(n_filterbanks),
                                                                                                                                            nBeamsets(n_beamsets),
                                                                                                                                            MonitorDone(true)
{
    periph_fpga = new Periph_fpga(GlobalNr, IpAddr, UdpPort, nFilterbanks, nBeamsets);

    LOG_F(INFO, "node %d, Uniboard=%d, IP=%s, UdpPort=%d, localnr=%d enabled", GlobalNr, UniboardNr, IpAddr.c_str(), UdpPort, LocalNr);

    WorkersRunning = true;
    worker_thread = new thread(&Node::worker, this);
}

Node::~Node()
{
    WorkersRunning = false;

    LOG_F(DEBUG, "Send stop command to worker node %d", GlobalNr);
    cCmd p;
    p.action = 'S';
    cmd_queue.Push(p);

    if (worker_thread != nullptr)
    {
        LOG_F(DEBUG, "Joining worker thread node %d", GlobalNr);
        worker_thread->join();
        LOG_F(DEBUG, "Delete worker thread node %d", GlobalNr);
        my_delete(worker_thread);
    }
    if (periph_fpga != nullptr)
    {
        LOG_F(DEBUG, "Delete periph_fpga node %d", GlobalNr);
        my_delete(periph_fpga);
    }
}

bool Node::monitor(void)
{
    // if last monitor request not finished, abort this request, else issue a new monitor request
    if (MonitorDone || cmd_queue.Empty())
    {
        cCmd p;
        p.action = 'M';
        cmd_queue.Push(p);
        MonitorDone = false;
    }
    return true;
}

bool Node::isOnline(void)
{
    return periph_fpga->isOnline();
}

bool Node::isMasked()
{
    return periph_fpga->isMasked();
}

void Node::setMasked(bool mask)
{
    periph_fpga->setMasked(mask);
}

double Node::monitorPpsOffsetTime(void)
{
    return periph_fpga->monitorPpsOffsetTime();
}

struct timespec Node::monitorStartTime(void)
{
    return periph_fpga->monitorStartTime();
}

void Node::ucpSetCommunicationState(bool state)
{
    periph_fpga->ucpSetCommunicationState(state);
}

bool Node::UcpCommunicationStatus(void)
{
    return periph_fpga->UcpCommunicationStatus();
}

void Node::ucpResetCounters(bool reset)
{
    periph_fpga->ucpResetCounters(reset);
}


uint64_t Node::ucpNofReads(void)
{
    return periph_fpga->ucpNofReads();
}

uint64_t Node::ucpNofReadRetries(void)
{
    return periph_fpga->ucpNofReadRetries();
}

uint64_t Node::ucpNofReadFailures(void)
{
    return periph_fpga->ucpNofReadFailures();
}

uint64_t Node::ucpNofWrites(void)
{
    return periph_fpga->ucpNofWrites();
}

uint64_t Node::ucpNofWriteRetries(void)
{
    return periph_fpga->ucpNofWriteRetries();
}

uint64_t Node::ucpNofWriteFailures(void)
{
    return periph_fpga->ucpNofWriteFailures();
}

bool Node::exec_cmd(const char cmd, const uint32_t cmd_id,
                    char *data, const size_t nvalues, const uint32_t format)
{
    cCmd p;
    p.action = cmd;
    p.cmd_id = cmd_id;
    p.nvalues = nvalues;
    p.format = format;
    p.data = data;
    LOG_F(DEBUG, "exec_cmd: p.cmd_id=%d, p.nvalues=%zu, p.format=%u", p.cmd_id, p.nvalues, p.format);

    cmd_queue.Push(p);
    return true;
}

/* exec_reply()
 *
 * returns:
 *   0 = no response yet
 *   1 = succes valid value
 *  -1 = no succes failure or timeout
 */
int Node::exec_reply(const uint32_t cmd_id)
{
    cReply r;

    if (reply_queue.Pop(r, false) == false)
    {
        return 0; // queue empty
    }
    if (r.cmd_id != cmd_id)
    {
        return 0; // wrong message
    }

    LOG_F(DEBUG, "exec_reply: received %zu bytes from worker, retval=%u, datatype=%u", r.nvalues, r.retval, r.format);

    if (r.retval)
    {
        return 1; // valid response
    }
    else
    {
        LOG_F(DEBUG, "exec_reply, retval false, data not copied");
        return -1; // not valid response
    }
}
