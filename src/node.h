/* *************************************************************************
 * Copyright 2023
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . This is the class that does the communication with the nodes, using several
#   threads with workers.
* *********************************************************************** */

#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include <array>

#include "io/ucp.h"
#include "registers.h"
#include "periph/fpga.h"
#include "tools/message_queue.h"

/*
 * cCmd class used for sending messages to the worker thread.
 * init() is used to clear all values.
 */
class cCmd
{
public:
    cCmd();
    ~cCmd();
    void init();
    char action;     // Requested cmd: 'R', 'W', 'S' or 'M' task. (Read, Write, Monitor or Stop).
    uint32_t cmd_id; // fpga register address to access.
    size_t nvalues;    // Number of values of type format in data.
    uint32_t format;      // data format in data, see REG_FORMAT_* in register.h for all available types
    char *data;      // data array.
};

/*
 * cReply class used for return value from the worker thread.
 * init() is used to clear all values.
 */
class cReply
{
public:
    cReply();
    ~cReply();
    void init();
    char action; // processed cmd: 'R', 'W', 'S' or 'M' task. (Read, Write, Monitor or Stop)
    uint32_t cmd_id;
    uint retval; // return value of executed cmd, true = success, false = failure.
    size_t nvalues; // Number of values of type format in data.
    uint32_t format;  // data format in data, see REG_FORMAT_* in register.h
};

// #define GLOBALNODE_to_UNB(n)   (n >> 2)
// #define GLOBALNODE_to_NODE(n)  (n & 0x3)

class Node
{
private:
    std::string IpAddr;
    uint UdpPort;
    uint UniboardNr;
    uint LocalNr;
    uint GlobalNr;
    uint nFilterbanks;
    uint nBeamsets;
    bool MonitorDone;

    std::thread *worker_thread;
    CMessageQueue<cCmd> cmd_queue;
    CMessageQueue<cReply> reply_queue;

    Periph_fpga *periph_fpga;

    void print_node_nr(void)
    {
        std::cout << "Uniboard=" << UniboardNr << " localnr=" << LocalNr << " globalnr=" << GlobalNr << std::endl;
    }

public:
    Node(const std::string &ipaddr, const uint udpport, const uint unb, const uint localnr, const uint nof_filterbanks, const uint nof_beamsets);
    ~Node();

    uint GetLocalNr();
    uint GetUniboardNr();
    const std::string GetIP();
    uint GetGlobalNr();
    uint GetNr();
    bool isOnline(void);
    bool isMasked();
    void setMasked(bool mask);
    double monitorPpsOffsetTime(void);
    struct timespec monitorStartTime(void);

    void ucpSetCommunicationState(bool state);
    bool UcpCommunicationStatus(void);
    
    void ucpResetCounters(bool reset);
    
    uint64_t ucpNofReads(void);
    uint64_t ucpNofReadRetries(void);
    uint64_t ucpNofReadFailures(void);
    uint64_t ucpNofWrites(void);
    uint64_t ucpNofWriteRetries(void);
    uint64_t ucpNofWriteFailures(void);

    bool monitor(void);

    void worker();
    bool exec_cmd(const char cmd, const uint32_t cmd_id, char *data, const size_t nvalues, const uint32_t format);
    int exec_reply(const uint32_t cmd_id);
};

inline uint Node::GetLocalNr() { return LocalNr; }
inline uint Node::GetUniboardNr() { return UniboardNr; }
inline const std::string Node::GetIP() { return IpAddr; }
inline uint Node::GetGlobalNr() { return GlobalNr; }
inline uint Node::GetNr() { return LocalNr; }

#endif /* NODE_H */
