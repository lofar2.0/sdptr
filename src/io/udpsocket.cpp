/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . UDP communication
* *********************************************************************** */

// You need to compile with _REENTRANT defined since this uses threads
#ifndef _REENTRANT
#define _REENTRANT 1
#endif

#include <cstring>
#include <iostream>
#include <stdexcept>

#include <sys/time.h>
#include <strings.h>
#include <unistd.h>
#include <arpa/inet.h> // htons, inet_pton

#include "udpsocket.h"
#include "../tools/loguru.h"

using namespace std;

udpsocket::udpsocket(const string& ipaddr, uint16_t port, int timeo)
{
    dport = port;
    sock = socket (PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sock < 0) {
        LOG_F(ERROR, "socket creation: %s", strerror(errno));
        throw runtime_error("udpsocket: socket creation error");
    }

    int optval = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0) {
        close(sock);
        LOG_F(ERROR, "set socket SO_REUSEADDR option: %s", strerror(errno));
        throw runtime_error("UDPSSocket: cannot set socket option SO_REUSEADDR");
    }

    // Give the socket a name.
    bzero(&sname, sizeof(sname));
    sname.sin_family = AF_INET;
    sname.sin_port = htons (dport);
    inet_pton(AF_INET, ipaddr.c_str(), &sname.sin_addr);

    // set receive timeout
    timeoutms = timeo;
    timeout.tv_sec = timeoutms / 1000;
    timeout.tv_usec = ( timeoutms % 1000 ) * 1000;
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) < 0) {
        LOG_F(ERROR, "set socket SO_RCVTIMEO option: %s", strerror(errno));
        throw runtime_error("udpsocket: socket set timeout error");
    }
}

udpsocket::~udpsocket()
{
    close(sock);
}

ssize_t udpsocket::rx(uint8_t *buf, size_t maxlen)
{
    // Receive a datagram from a tx source, non-blocking, see class Contructor
    ssize_t nrxbytes = recvfrom(sock, buf, maxlen, 0, NULL, NULL);
    return nrxbytes;
}

ssize_t udpsocket::tx(unsigned char* mes, size_t len)
{
    ssize_t ntxbytes = sendto(sock, mes, len, 0, (struct sockaddr *) &sname, sizeof(sname));

    if (ntxbytes < 0) {
        LOG_F(ERROR, "udpsocket::tx(): %s", strerror(errno));
        throw runtime_error("udpsocket::tx() Error");
    }
    return ntxbytes;
}
