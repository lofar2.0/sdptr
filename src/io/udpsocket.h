/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . UDP communication
* *********************************************************************** */

#ifndef __UDPSOCKET_H__
#define __UDPSOCKET_H__

#include <string>

#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>

#define UDP_TIMEOUT_IN_msec 250

class udpsocket {
private:
    struct sockaddr_in sname;
    int sock;
    uint16_t dport;
    int timeoutms;
    struct timeval timeout;
    
public:
    udpsocket(const std::string& ipaddr, uint16_t udp_port, int timeo=UDP_TIMEOUT_IN_msec);
    ~udpsocket();
    ssize_t rx(uint8_t* buf, size_t maxlen);
    ssize_t tx(unsigned char* mes, size_t len);
};

#endif // __UDPSOCKET_H__
