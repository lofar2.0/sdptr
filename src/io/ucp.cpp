/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . Uniboard protocol
* *********************************************************************** */

// You need to compile with _REENTRANT defined since this uses threads
#ifndef _REENTRANT
#define _REENTRANT 1
#endif

#include <cstring>
#include <cstdio>
#include <stdexcept>

#include <unistd.h>
#include <iostream>
#include <iomanip>
#include "ucp.h"
#include "../tools/util.h"
#include "../tools/loguru.h"

#define ERROR_READ       1
#define ERROR_SEQCNT     2
#define ERROR_STATUS     3
#define ERROR_PACKETLEN  4

using namespace std;

extern int debug;

UCP::UCP(const string& ipaddr, const uint16_t udp_port)
{
    commdev = new udpsocket(ipaddr, udp_port);
    seq_nr = 0;
}

UCP::~UCP()
{
    my_delete(commdev);
}

bool UCP::TransmitRead(const uint32_t opcode, const uint32_t addr, const uint nvalues)
{
    ssize_t pktlength = sizeof(struct ucp_cmd_struct);
    uint8_t *pkt = new uint8_t[pktlength];
    memset(pkt, 0, pktlength);
    UnbosCmdPacket *unb = (UnbosCmdPacket*)pkt;

    seq_nr++;
    unb->hdr.psn = seq_nr;
    unb->hdr.opcode = opcode;
    unb->hdr.nvalues = nvalues;
    unb->hdr.addr = addr;

    // print_cmd_packet(pkt, pktlength);
    ssize_t retval = commdev->tx(pkt, pktlength);
    my_delete_arr(pkt);
    return (retval == pktlength);
}


/*
 * Returns 0 if ok
 * Returns <0 if error
 */
int UCP::ReceiveReadAck(uint32_t *buf, const uint nvalues)
{
    int retstat;
    ssize_t pktlength = sizeof(struct ucp_reply_struct) + nvalues * sizeof(uint32_t);
    uint8_t *pkt = new uint8_t[pktlength];
    // memset(pkt, 0, pktlength);
    UnbosReadAckPacket *unb = (UnbosReadAckPacket*)pkt;

    while (true) {
        retstat = 0;
        ssize_t nbytes = commdev->rx(pkt, pktlength);
        // cout << "rx " << nbytes << " bytes from " << pktlength << " bytes" << endl;
        if (nbytes != pktlength) {
            retstat = -ERROR_READ;
            break;
        }
        else {
            if (unb->hdr.psn < seq_nr) {
                // nbytes = commdev->rx(pkt, pktlength);
                // old psn, try to catch next message
                continue;
            }
            //print_reply_packet(pkt,(int)nbytes);
            if (unb->hdr.psn != seq_nr) {
                cerr << "UCP::ReceiveReadAck seq_nr NOT EQUAL !"
                     << " packet seq=" << unb->hdr.psn
                     << " class seq=" << seq_nr << endl;
                retstat = -ERROR_SEQCNT;
            }
        }
        if (retstat == 0) {
            memcpy(buf, unb->data, (nvalues * sizeof(uint32_t)));
            break;
        }
    }
    my_delete_arr(pkt);
    return retstat;
}

void random_sleep(double tmax_in_sec) {
    usleep((int)((rand() * tmax_in_sec) / RAND_MAX * 1e6));
}

bool UCP::Read(const uint32_t opcode, const uint32_t addr, const uint nvalues, uint32_t *buf, const int max_retries)
{
    if (buf == nullptr || nvalues == 0) {
        throw runtime_error("UCP::Read buf=nullptr");
    }
    int retries = 0;
    int retstat = 0;
    uint nreceived = 0;

    //cout << "UCP::Read from addr=0x" << hex << addr << " span=" << dec << nvalues << endl;
    while (nreceived < nvalues) {
        uint _nvalues = nvalues - nreceived;
        if (_nvalues > UCP_BLOCK_SZ) { _nvalues = UCP_BLOCK_SZ; }

        uint32_t _addr = addr;
        if (opcode == UCP_OPCODE_MEMORY_READ) { _addr += (nreceived * sizeof(uint32_t)); }

        while (retries < max_retries) {
            TransmitRead(opcode, _addr, _nvalues);
            retstat = ReceiveReadAck(&(buf[nreceived]), _nvalues);
            if (retstat == 0) { break; }

            retries++;
            if (retries < max_retries) {
                if (retries == 1) { random_sleep(0.05); }
                else if (retries == 2) { random_sleep(0.5); }
            }
        }
        if (retries == max_retries) { break; }

        nreceived += _nvalues;
    }
    n_retries = retries;
    if (retries > 1) { LOG_F(ERROR, "UCP::Read() retries=%d %s", retries, (retstat == 0 ? " (recovered)" : " (failed)")); }
    if (retstat == 0) return true;
    return false;
}


bool UCP::TransmitWrite(const uint32_t opcode, const uint32_t addr, const uint nvalues, const uint32_t *buf)
{
    ssize_t pktlength = sizeof(struct ucp_cmd_struct) + nvalues * sizeof(uint32_t);
    uint8_t *pkt = new uint8_t[pktlength];
    // memset(pkt, 0, pktlength);
    UnbosCmdPacket *unb = (UnbosCmdPacket*)pkt;

    unb->hdr.psn = ++seq_nr;
    unb->hdr.opcode = opcode;
    unb->hdr.nvalues = nvalues;
    unb->hdr.addr = addr;
    memcpy(unb->data, buf, (nvalues * sizeof(uint32_t)));

    //print_cmd_packet(pkt, pktlength);
    ssize_t retval = commdev->tx(pkt, pktlength);
    my_delete_arr(pkt);
    return (retval == pktlength);
}

/*
 * Returns 0 if ok
 * Returns <0 if error
 */
int UCP::ReceiveWriteAck(void)
{
    int retstat = 0;
    UnbosWriteAckPacket ack;

    ssize_t nbytes = commdev->rx((uint8_t*)&ack, sizeof(ack));
    if (nbytes != sizeof(ack)) {
        retstat = -ERROR_READ;
    }
    else {
        //print_reply_packet((const uint8_t *)&ack,(int)nbytes);
        if (ack.hdr.psn != seq_nr) {
            cerr << "UCP::ReceiveWriteAck seq_nr NOT EQUAL !"
                 << " packet seq=" << ack.hdr.psn
                 << " class seq=" << seq_nr << endl;
            retstat = -ERROR_SEQCNT;
        }
    }
    return retstat;
}

/* Write
 * wait_for_ack true by default, but sometimes no response is expected
 * for example the reboot command is faster then sending the ack
 */
bool UCP::Write(const uint32_t opcode, const uint32_t addr, const uint nvalues,
                const uint32_t *buf, const bool wait_for_ack, const int max_retries)
{
    if (buf == nullptr) {
        throw runtime_error("UCP::Write buf=nullptr");
    }
    int retries = 0;
    int retstat = 0;
    uint nsent = 0;

    //cout << "UCP::Write to addr=0x" << hex << addr << " span=" << dec << nvalues << endl;

    while (nsent < nvalues) {
        uint _nvalues = nvalues - nsent;
        if (_nvalues > UCP_BLOCK_SZ) { _nvalues = UCP_BLOCK_SZ; }

        uint32_t _addr = addr;
        if (opcode == UCP_OPCODE_MEMORY_WRITE) { _addr += (nsent * sizeof(uint32_t)); }

        while (retries < max_retries) {
            TransmitWrite(opcode, _addr, _nvalues, &(buf[nsent]));
            if (!wait_for_ack) { return true; }

            retstat = ReceiveWriteAck();
            if (retstat == 0) { break; }
            retries++;
            if (retries == 1) { random_sleep(0.05); }
            else if (retries == 2) { random_sleep(0.5); }
        }
        if (retries == max_retries) { break; }
        nsent += _nvalues;
    }
    n_retries = retries;
    if (retries > 1) { LOG_F(ERROR, "UCP::Write() retries=%d %s", retries, (retstat == 0 ? " (recovered)" : " (failed)")); }
    if (retstat == 0) { return true; }
    return false;
}

void UCP::print_cmd_packet(const uint8_t *pkt, const int len)
{
    cout << "\x1b[31;1m" << "UCP::print_cmd_packet" << "\x1b[0m" << endl;
    UnbosCmdPacket *pkt_ptr=(UnbosCmdPacket *)pkt;
    cout << " hdr.psn=" << pkt_ptr->hdr.psn
         << " hdr.opcode=" << pkt_ptr->hdr.opcode
         << " hdr.nvalues=" << dec << pkt_ptr->hdr.nvalues
         << " hdr.addr=" << pkt_ptr->hdr.addr << endl;

    const uint8_t *bufptr = pkt;
    cout << "       ";
    for (int i=0; i<16; i++) {
        cout << setw(2) << right << hex << i << " ";
    }
    for (int i=0; i<len; i++) {
        if ((i % 16) == 0) {
            cout << endl << setfill('0') << setw(4) << right << hex << i << " : ";
        }
        cout << setfill('0') << setw(2) << right << hex << (int)*bufptr << " ";
        bufptr++;
    }
    cout << endl;
}

void UCP::print_reply_packet(const uint8_t *pkt, const int len)
{
    cout << "\x1b[32;1m" << "UCP::print_reply_packet" << "\x1b[0m" << endl;
    UnbosReadAckPacket *pkt_ptr = (UnbosReadAckPacket *)pkt;
    cout << " hdr.psn=" << pkt_ptr->hdr.psn
         << " hdr.addr=" << pkt_ptr->hdr.addr << endl;

    const uint8_t *bufptr = pkt;
    cout << "       ";
    for (int i=0; i<16; i++) {
        cout << setfill('0') << setw(2) << right << hex << i << " ";
    }
    for (int i=0; i<len; i++) {
        if ((i % 16) == 0) {
            cout << endl << setfill('0') << setw(4) << right << hex << i << " : ";
        }
        cout << setfill('0') << setw(2) << right << hex << (int)*bufptr << " ";
        bufptr++;
    }
    cout << endl;
}

bool UCP::readRegister(uint32_t addr, uint32_t nvalues, uint32_t *data_ptr, const int max_retries)
{
    if (data_ptr == nullptr) {
        throw runtime_error("UCP::readRegister data_ptr=nullptr");
    }
    return Read(UCP_OPCODE_MEMORY_READ, addr, nvalues, data_ptr, max_retries);
}

bool UCP::readFifoRegister(uint32_t addr, uint32_t nvalues, uint32_t *data_ptr, const int max_retries)
{
    if (data_ptr == nullptr) {
        throw runtime_error("UCP::readRegister data_ptr=nullptr");
    }
    return Read(UCP_OPCODE_FIFO_READ, addr, nvalues, data_ptr, max_retries);
}

bool UCP::writeRegister(uint32_t addr, uint32_t nvalues, const uint32_t *data_ptr, const bool wait_for_ack, const int max_retries)
{
    if (data_ptr == nullptr) {
        throw runtime_error("UCP::writeRegister data_ptr=nullptr");
    }
    return Write(UCP_OPCODE_MEMORY_WRITE, addr, nvalues, data_ptr, wait_for_ack, max_retries);
}

bool UCP::writeFifoRegister(uint32_t addr, uint32_t nvalues, const uint32_t *data_ptr, const bool wait_for_ack, const int max_retries)
{
    if (data_ptr == nullptr) {
        throw runtime_error("UCP::writeRegister data_ptr=nullptr");
    }
    return Write(UCP_OPCODE_FIFO_WRITE, addr, nvalues, data_ptr, wait_for_ack, max_retries);
}
