/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . Uniboard protocol
* *********************************************************************** */

#ifndef __UCP_H__
#define __UCP_H__

#include <string>

#include "udpsocket.h"


// constants
#define UCP_BLOCK_SZ 360

// status code
#define UCP_ST_OK           0
#define UCP_ST_REGISTER_ERR 1
#define UCP_ST_TIMEOUT_ERR  2
#define UCP_ST_FORMAT_ERR   3
#define UCP_ST_PACK_ERR     4
#define UCP_ST_UNPACK_ERR   5
#define UCP_ST_SIZE_ERR     6

// opcodes
#define UCP_OPCODE_MEMORY_READ  0x1
#define UCP_OPCODE_MEMORY_WRITE 0x2
#define UCP_OPCODE_MODIFY_AND   0x3
#define UCP_OPCODE_MODIFY_OR    0x4
#define UCP_OPCODE_MODIFY_XOR   0x5
#define UCP_OPCODE_FLASH_WRITE  0x6
#define UCP_OPCODE_FLASH_READ   0x7
#define UCP_OPCODE_FLASH_ERASE  0x8
#define UCP_OPCODE_FIFO_READ    0x9
#define UCP_OPCODE_FIFO_WRITE   0xA


struct ucp_cmd_struct {
  uint32_t psn;
  uint32_t opcode;
  uint32_t nvalues;
  uint32_t addr;
} __attribute__ ((__packed__));

typedef struct {
  struct ucp_cmd_struct hdr;
  uint32_t data[1];
} __attribute__ ((__packed__)) UnbosCmdPacket;



struct ucp_reply_struct {
  uint32_t psn;
  uint32_t addr;
} __attribute__ ((__packed__));

typedef struct {
  struct ucp_reply_struct hdr;
} __attribute__ ((__packed__)) UnbosWriteAckPacket;

typedef struct {
  struct ucp_reply_struct hdr;
  uint32_t data[1];
} __attribute__ ((__packed__)) UnbosReadAckPacket;



#define UNB_UDP_PORT 5000
class UCP 
{
protected:
  uint32_t seq_nr; 
  udpsocket *commdev;
  int32_t n_retries;

public:
  UCP(const std::string& ipaddr, const uint16_t udpport);
  ~UCP();
protected:
  // returns true on success.
  bool TransmitRead(const uint32_t opcode, const uint32_t addr, const uint nvalues);
  bool TransmitWrite(const uint32_t opcode, const uint32_t addr, const uint nvalues,
                     const uint32_t *buf);

  // receive read acknowledge packet, returns 0 on success
  // data is returned in buf[len].
  int ReceiveReadAck(uint32_t *buf, const uint nvalues);
  // receive write acknowledge packet, returns 0 on success
  int ReceiveWriteAck(void);

  void print_cmd_packet(const uint8_t *pkt, const int len);
  void print_reply_packet(const uint8_t *pkt, const int len);

public:
  bool Write(const uint32_t opcode, const uint32_t addr, const uint nvalues, const uint32_t *buf, const bool wait_for_ack=true, const int max_retries=3);
  bool Read(const uint32_t opcode, const uint32_t addr, const uint nvalues, uint32_t *buf, const int max_retries=3);
  bool writeRegister(uint32_t addr, uint32_t nvalues, const uint32_t *data_ptr, const bool wait_for_ack=true, const int max_retries=3);
  bool writeFifoRegister(uint32_t addr, uint32_t nvalues, const uint32_t *data_ptr, const bool wait_for_ack=true, const int max_retries=3);
  bool readRegister(uint32_t addr, uint32_t nvalues, uint32_t *data_ptr, const int max_retries=3);
  bool readFifoRegister(uint32_t addr, uint32_t nvalues, uint32_t *data_ptr, const int max_retries=3);
  int32_t getRetries(void);
};

inline int32_t UCP::getRetries(void) { return n_retries; }

#endif
