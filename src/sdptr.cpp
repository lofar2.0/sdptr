/* *************************************************************************
 * Copyright 2023
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *********************************************************************** */

/* *************************************************************************
 * Author:
 * . Leon Hiemstra
 * . Pieter Donker
 * Purpose:
 * . opc-ua to ucp translator
 * Description:
 * . main program
 * *********************************************************************** */

// You need to compile with _REENTRANT defined since this uses threads
#ifndef _REENTRANT
#define _REENTRANT 1
#endif

#include <iostream>
#include <exception>
#include <sstream>
#include <fstream>
#include <list>
#include <stdexcept>
#include <thread>
#include <chrono>

#include <cstring>
#include <cstdlib>
#include <csignal>
#include <sys/time.h>
#include <getopt.h>
#include <unistd.h>
#include <assert.h>

#include "sdptr.h"
#include "opcua/ua_server.h"

#include "tools/loguru.h"

using namespace std;

#define SECONDS_PER_TICK 1

// target offset is set to 100msec after pps to avoid problems with statistics output.
#define TARGET_PPS_MONITOR_OFFSET 0.1
#define TARGET_PPS_MONITOR_OFFSET_MIN (TARGET_PPS_MONITOR_OFFSET - 0.001)
#define TARGET_PPS_MONITOR_OFFSET_MAX (TARGET_PPS_MONITOR_OFFSET + 0.001)
#define MONITOR_DELAY_CHANGE_NS 1000000
#define NSEC_PER_SEC 1000000000

/* Global var */
bool nodaemon = false;
bool stub_mode = false;
volatile bool ServerRunning = true;
Serverdat SD;

struct timespec timespec_normalise(struct timespec ts)
{
    while(ts.tv_nsec >= NSEC_PER_SEC)
    {
        ++(ts.tv_sec);
        ts.tv_nsec -= NSEC_PER_SEC;
    }

    while(ts.tv_nsec <= -NSEC_PER_SEC)
    {
        --(ts.tv_sec);
        ts.tv_nsec += NSEC_PER_SEC;
    }

    if(ts.tv_nsec < 0)
    {
        --(ts.tv_sec);
        ts.tv_nsec = (NSEC_PER_SEC + ts.tv_nsec);
    }
    return ts;
}

bool timespec_gt(struct timespec ts1, struct timespec ts2)
{
	ts1 = timespec_normalise(ts1);
	ts2 = timespec_normalise(ts2);

	return (ts1.tv_sec > ts2.tv_sec || (ts1.tv_sec == ts2.tv_sec && ts1.tv_nsec > ts2.tv_nsec));
}

bool timespec_lt(struct timespec ts1, struct timespec ts2)
{
	ts1 = timespec_normalise(ts1);
	ts2 = timespec_normalise(ts2);

	return (ts1.tv_sec < ts2.tv_sec || (ts1.tv_sec == ts2.tv_sec && ts1.tv_nsec < ts2.tv_nsec));
}

struct timespec timespec_diff(struct timespec ts1, struct timespec ts2)
{
    struct timespec diff;

    ts1 = timespec_normalise(ts1);
    ts2 = timespec_normalise(ts2);

    if ((ts2.tv_nsec - ts1.tv_nsec) < 0) {
        diff.tv_sec = ts2.tv_sec - ts1.tv_sec - 1;
        diff.tv_nsec = ts2.tv_nsec - ts1.tv_nsec + NSEC_PER_SEC;
    }
    else {
        diff.tv_sec = ts2.tv_sec - ts1.tv_sec;
        diff.tv_nsec = ts2.tv_nsec - ts1.tv_nsec;
    }
    diff = timespec_normalise(diff);
    return diff;
}

double timespec_to_double(struct timespec ts)
{
	return ((double)(ts.tv_sec) + ((double)(ts.tv_nsec) / NSEC_PER_SEC));
}

void monitor_timer()
{
    struct timespec time_now;
    struct timespec time_next_run;
    struct timespec time_to_next_run;

    SD.uptime = 0;

    clock_gettime(CLOCK_REALTIME, &time_now);
    SD.start_time = time_now.tv_sec;
    time_next_run.tv_sec = time_now.tv_sec + 1;
    time_next_run.tv_nsec = 0L;

    vector<double> monitor_pps_offset_times;
    vector<struct timespec> monitor_start_times;
    double monitor_pps_offset_time;
    struct timespec monitor_start_time;
    loguru::set_thread_name("monitor thread");


    // keep running until program is stopped by user (see stopHandler())
    while (ServerRunning)
    {
        // wait for start next monitor run
        clock_gettime(CLOCK_REALTIME, &time_now);
        while (ServerRunning && (timespec_lt(time_now, time_next_run))) {
            this_thread::sleep_for(chrono::microseconds(1));
            clock_gettime(CLOCK_REALTIME, &time_now);
        }
        if (!ServerRunning) { continue; }

        // set time seconds for next monitor run
        time_next_run.tv_sec = time_now.tv_sec + 1;

        SD.uptime++;
        LOG_F(DEBUG, "t0 = %11.9f sec", (time_now.tv_sec + (time_now.tv_nsec / 1e9)));

        uint32_t uptime = SD.uptime;
        LOG_F(DEBUG, "uptime = %d seconds", uptime);

        // trigger the monitor on all perih/fpga's
        if (SD.fpga_map != nullptr) {
            LOG_F(DEBUG, "sdptr_monitor start");
            SD.fpga_map->monitor();
        }

        // wait for updated monitor results
        this_thread::sleep_for(chrono::milliseconds(50));

        // get min monitor_start_time_ns (cpu time) when received monitor_start_time in last monitor run, is used to set tod_pps_delay
        monitor_start_times = SD.fpga_map->get_all_monitor_start_times();
        monitor_start_time.tv_sec = time_now.tv_sec + 1;
        monitor_start_time.tv_nsec = time_now.tv_nsec;

        for (uint i = 0; i < monitor_start_times.size(); i++) {
            if (timespec_lt((monitor_start_times[i]), monitor_start_time)) {
                monitor_start_time = (monitor_start_times[i]);
            }
        }
        LOG_F(DEBUG, "monitor_start_time = %11.9f  sec", timespec_to_double(monitor_start_time));

        // if no firmware detected, continue while(ServerRunning)
        if (timespec_to_double(monitor_start_time) == 0.0) {
            continue;
        }

        // get min monitor_pps_offset_time, time between pps and monitor run in seconds.
        monitor_pps_offset_times = SD.fpga_map->get_all_monitor_pps_offset_times();
        monitor_pps_offset_time = 2.0;
        for (uint i = 0; i < monitor_pps_offset_times.size(); i++) {
            if (monitor_pps_offset_times[i] > 0) {
                if (monitor_pps_offset_times[i] < monitor_pps_offset_time) {
                    monitor_pps_offset_time = monitor_pps_offset_times[i];
                }
            }
        }
        LOG_F(DEBUG, "monitor_pps_offset_time = %11.9f sec", monitor_pps_offset_time);

        // measured system-time on hw-pps
        struct timespec ts_pps_time;
        ts_pps_time.tv_sec = monitor_start_time.tv_sec;
        ts_pps_time.tv_nsec = monitor_start_time.tv_nsec - (int64_t)(monitor_pps_offset_time * 1e9);
        ts_pps_time = timespec_normalise(ts_pps_time);

        // system-time on second change
        struct timespec ts_time_now_top;
        ts_time_now_top.tv_sec = time_now.tv_sec;
        ts_time_now_top.tv_nsec = 0L;

        double tod_pps_delta = 0.0;
        if (monitor_pps_offset_time != 2.0) {
            // save for TR_pps_monitor_delta_R
            SD.pps_monitor_delta = monitor_pps_offset_time;

            // calc tod_pps_delta, it can be a value from -1.3 .. 1.0
            // < 0.0 sdpfw pps is before tod second change
            // > 0.0 sdpfw pps is after tod second change
            tod_pps_delta = timespec_to_double(timespec_diff(ts_time_now_top, ts_pps_time));  // tod_pps_delta in seconds
        }

        /*  Correct and save for TR_tod_pps_delta_R.
            0.0  ..  0.5  // no change
           -0.5  ..  0.0  // no change
            0.5  ..  1.01 -> -0.5  ..  0.0
           -1.01 .. -0.5  ->  0.0  ..  0.5
             -   .. -1.01 // no change
        */
        SD.tod_pps_delta = tod_pps_delta;
        if ((abs(tod_pps_delta) > 0.5) && (abs(tod_pps_delta) < 1.01)) {
            SD.tod_pps_delta = (tod_pps_delta > 0) ? tod_pps_delta - 1.0 : tod_pps_delta + 1.0;
        }
        LOG_F(DEBUG, "tod_pps_delta = %11.9f sec", SD.tod_pps_delta);


        /* set time for next monitor run. */

        // if first run or sdpfw pps missing, do nothing and wait for next pps.
        if ((tod_pps_delta == 0.0) || (monitor_pps_offset_time >= 1.0)) {
            // empty
        }
        // preset monitor_delay on first valid monitor run
        else if (time_next_run.tv_nsec == 0) {
            if (tod_pps_delta < TARGET_PPS_MONITOR_OFFSET) {
                time_next_run.tv_nsec = (int64_t)((tod_pps_delta + 1.1) * 1e9);
                time_next_run = timespec_normalise(time_next_run);
            }
            else {
                time_next_run.tv_nsec = (int64_t)((1.1 - tod_pps_delta) * 1e9);
                time_next_run = timespec_normalise(time_next_run);
            }
        }
        // keep monitor running at TARGET_PPS_MONITOR_OFFSET.
        else if (monitor_pps_offset_time > TARGET_PPS_MONITOR_OFFSET_MAX) {
            time_next_run.tv_nsec -= MONITOR_DELAY_CHANGE_NS;
            time_next_run = timespec_normalise(time_next_run);
        }
        else if (monitor_pps_offset_time < TARGET_PPS_MONITOR_OFFSET_MIN) {
            time_next_run.tv_nsec += MONITOR_DELAY_CHANGE_NS;
            time_next_run = timespec_normalise(time_next_run);
        }

        // take a longer sleep to save processing power
        clock_gettime(CLOCK_REALTIME, &time_now);
        time_to_next_run = timespec_diff(time_now, time_next_run);
        int64_t sleep_time_msec = (int64_t)(time_to_next_run.tv_sec * 1000) + (int64_t)(time_to_next_run.tv_nsec / 1000000) - 10;
        if (sleep_time_msec > 0) {
            this_thread::sleep_for(chrono::milliseconds(sleep_time_msec));
        }
    }
}

void server_init()
{
    for (int i = 0; i < SD.n_fpgas; i++)
    {
        NODE_config nc;
        nc.gn = SD.first_fpga_nr + i;
        nc.board_nr = (int32_t)(nc.gn / FPGAS_PER_BOARD);
        nc.fpga_nr = (int32_t)(nc.gn % FPGAS_PER_BOARD);
        if (stub_mode == true)
        {
            // In stub mode there is only localhost, so use UDP port to distinguish nodes.
            nc.ipaddr = "localhost";
            nc.udp_port = 5000 + nc.gn;
        }
        else
        {
            // On HW each node has own IP address, so all nodes can use same UDP (source) port.
            nc.ipaddr = SD.ip_prefix + to_string(nc.board_nr) + "." + to_string(nc.fpga_nr + 1);
            nc.udp_port = 5000;
        }
        SD.node_config.push_back(nc);
    }

    for (auto nc : SD.node_config)
    {
        LOG_F(INFO, "GlobalNr=%d, BoardNr=%d, FpgaNr=%d, IP=%s", nc.gn, nc.board_nr, nc.fpga_nr, nc.ipaddr.c_str());
    }

    if (SD.fpga_map != NULL) { my_delete(SD.fpga_map); }

    SD.fpga_map = new FpgaMap();
    SD.fpga_map->add_nodes();

    ua_server_init(SD.ua_server_port);

    LOG_F(INFO, "done");
}

static void stopHandler(int signal)
{
    LOG_F(WARNING, "received ctrl-c: exit");
    ServerRunning = false;
    ua_server_stop();
}

static void usage()
{
    cout << "-h, --help        : This screen" << endl;
    cout << "-v, --version     : sdptr version" << endl;
    cout << "-d, --nodeamon    : With this flag, sdptr runs NOT as daemon" << endl;
    cout << "-p, --port        : this port number will be used for the opc-ua server" << endl;
    cout << "-f, --fpgas       : Specify number of FPGAs, defaults to 16" << endl;
    cout << "-g, --first_gn    : Specify first global number, defaults to 0" << endl;
    cout << "-o, --oversampled : Set this flag if the firmware design contains an oversampled filterbank" << endl;
    cout << "-b, --beamsets    : Specify number of beamsets, defaults to 1" << endl;
    cout << "-i, --ip_prefix   : Specify ip prefix, defaults to '10.99.' " << endl;
    cout << "-s, --stub_mode   : Talk to stub that represents the SDP firmware that runs on Uniboard2 FPGA nodes" << endl;
    cout << "-V, --verbosity   : debug level, -9=OFF, -3=FATAL, -2=ERROR, -1=WARNING, 0=INFO, 1=DEBUG, defaults to 0" << endl;
}

void parseOptions(int argc, char **argv)
{
    LOG_F(INFO, "parse options");
    static struct option long_options[] = {
        {"help", no_argument, 0, 'h'},
        {"version", no_argument, 0, 'v'},
        {"nodaemon", no_argument, 0, 'd'},
        {"port", required_argument, 0, 'p'},
        {"fpgas", required_argument, 0, 'f'},
        {"first_gn", required_argument, 0, 'g'},
        {"oversampled", no_argument, 0, 'o'},
        {"beamsets", required_argument, 0, 'b'},
        {"ip_prefix", required_argument, 0, 'i'},
        {"stub_mode", no_argument, 0, 's'},
        {"verbosity", required_argument, 0, 'V'},
        {0, 0, 0, 0}};

    optind = 0; // reset option parsing
    for (;;)
    {
        int option_index = 0;
        int c = getopt_long(argc, argv, "hvdp:f:g:ob:i:sV:", long_options, &option_index);

        if (c == -1)
        {
            break;
        }

        switch (c)
        {
        case 'h':
            // --help
            usage();
            exit(EXIT_FAILURE);
            break;
        case 'v':
            // --version
            LOG_F(INFO, "sdptr version: %s", SDPTR_VERSION);
            exit(EXIT_SUCCESS);
            break;
        case 'd':
            // --nodeamon
            nodaemon = true;
            break;
        case 'p':
            // --port
            SD.ua_server_port = stoi(optarg);
            break;
        case 'f':
            // --fpgas
            SD.n_fpgas = stoi(optarg);
            break;
        case 'g':
            // --first_gn
            SD.first_fpga_nr = stoi(optarg);
            break;
        case 'o':
            // --oversampled
            SD.n_filterbanks = 2; // The oversampled filterbank contains 2 filterbanks instead of 1.
            break;
        case 'b':
            // --beamsets
            SD.n_beamsets = stoi(optarg);
            break;
        case 'i':
            // --ip_prefix
            SD.ip_prefix = string(optarg);
            break;
        case 's':
            // --stub_mode
            stub_mode = true;
            break;
        case 'V':
            // --verbosity
            loguru::g_stderr_verbosity = stoi(optarg);
            break;
        default:
            assert(false);
            break;
        }
    }
}

int main(int argc, char **argv)
{
    // set default values
    SD.ua_server_port = 4840;
    SD.n_fpgas = 16;
    SD.first_fpga_nr = 0;
    SD.n_filterbanks = 1;
    SD.n_beamsets = 1;
    SD.ip_prefix = "10.99.";

    thread *monitor_thread;

    loguru::set_thread_name("main thread");

    parseOptions(argc, argv);

    LOG_F(INFO, "start of logger");

    LOG_F(INFO, "n_fpgas=%d, first_fpga_nr=%d, n_filterbanks=%d, n_beamsets=%d, ip_prefix=%s",
          SD.n_fpgas, SD.first_fpga_nr, SD.n_filterbanks, SD.n_beamsets, SD.ip_prefix.c_str());

    // If user pressed ctrl-c (SIGINT), stopHandler is called.
    signal(SIGINT, stopHandler);
    signal(SIGTERM, stopHandler);

    try
    {
        if (!nodaemon)
        {
            if (daemon(1, 0) < 0)
            {
                LOG_F(ERROR, "Error fork as daemon: %s", strerror(errno));
            }
        }

        SD.tr = new TranslatorMap();
        server_init();

        monitor_thread = new thread(monitor_timer);
        // ua_server_run() keeps running until SIGINT(Ctrl-c) or SIGTERM signal
        int ua_status = ua_server_run();
        LOG_F(INFO, "UA Server stop status = %d", ua_status);
        monitor_thread->join();
        my_delete(monitor_thread);
        my_delete(SD.fpga_map);
        my_delete(SD.tr);
        LOG_F(INFO, "Server Stopped");
        exit(EXIT_SUCCESS);
    }
    catch (exception &e)
    {
        LOG_F(ERROR, "Error server: %s", e.what());
        exit(EXIT_FAILURE);
    }
}
