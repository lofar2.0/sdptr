/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . class with fpga registers available for opc-ua server
* *********************************************************************** */

#ifndef FPGA_MAP_H
#define FPGA_MAP_H 1

#include <string>
#include <vector>
#include <list>

#include "node.h"
#include "tools/util.h"
#include "registers.h"


class FpgaMap
{
private:
    uint32_t nFilterbanks;
    uint32_t nBeamsets;
    uint32_t nFpgas;
    std::list<class Node*> FPGA;
    CPointMap *pointMap;
    std::vector<int> nodes;

public:
    FpgaMap();
    ~FpgaMap();

    bool monitor(void);
    bool write(char *data, const std::string& pointname);
    bool read(char *data, const std::string& pointname);
    bool point(char *data, const char cmd, const std::string& pointname);

    void add_nodes(void);
    Node * select_node(const int nr);
    std::vector<bool> get_all_enabled_nodes(void);
    std::vector<bool> get_all_masked_nodes(void);
    std::vector<bool> get_all_offline_nodes(void);
    void set_all_masked_nodes(std::vector<bool> masked);
    std::vector<double> get_all_monitor_pps_offset_times(void);
    std::vector<struct timespec> get_all_monitor_start_times(void);

    void setAllUcpSetCommunicationState(std::vector<bool> state);
    std::vector<bool> getAllUcpCommunicationStatus(void);
    void setAllUcpResetCounters(std::vector<bool> reset);
    std::vector<uint64_t> getAllUcpNofReads(void);
    std::vector<uint64_t> getAllUcpNofReadRetries(void);
    std::vector<uint64_t> getAllUcpNofReadFailures(void);
    std::vector<uint64_t> getAllUcpNofWrites(void);
    std::vector<uint64_t> getAllUcpNofWriteRetries(void);
    std::vector<uint64_t> getAllUcpNofWriteFailures(void);

    CPointMap * get_pointMap(void);
};
#endif

