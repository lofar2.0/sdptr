/* *************************************************************************
 * Copyright 2023
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *********************************************************************** */

/* *************************************************************************
 * Author:
 * . Leon Hiemstra
 * . Pieter Donker
 * Purpose:
 * . opc-ua to ucp translator
 * Description:
 * . class for fpga registers (peripherals) that need some more actions
 * *********************************************************************** */

// You need to compile with _REENTRANT defined since this uses threads
#ifndef _REENTRANT
#define _REENTRANT 1
#endif

#include <string>
#include <cstdint>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <fstream>

#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <exception>
#include <unistd.h>

#include <thread>
#include <chrono>

#include <sys/time.h>
#include <arpa/inet.h> // htons, inet_pton
#include <zlib.h>

#include "fpga.h"
#include "../sdptr.h"
#include "../tools/mmap.h"
#include "../tools/loguru.h"
#include "../tools/util.h"

using namespace std;

extern int debug;
extern Serverdat SD;

#define R_MEM 0 // read mem value
#define R_UCP 1 // read value from hardware

#define BIT_LO 0
#define BIT_HI 1

Periph_fpga::Periph_fpga(uint global_nr, string ipaddr, uint udpport, uint n_filterbanks, uint n_beamsets) : GlobalNr(global_nr),
                                                                                                             nFilterbanks(n_filterbanks),
                                                                                                             nBeamsets(n_beamsets),
                                                                                                             Masked(true),
                                                                                                             Online(false),
                                                                                                             ucp_block_comm(false),
                                                                                                             ucp_reads(0),
                                                                                                             ucp_read_retries(0),
                                                                                                             ucp_read_fails(0),
                                                                                                             ucp_writes(0),
                                                                                                             ucp_write_retries(0),
                                                                                                             ucp_write_fails(0),
                                                                                                             current_design_name("-"),
                                                                                                             current_hw_version(0),
                                                                                                             current_fw_version("-.-"),
                                                                                                             current_image(-1), // -1 = no image available, 0 = factory-image, 1 = user-image
                                                                                                             signal_input_sync_timeout(false),
                                                                                                             signal_input_bsn(0),
                                                                                                             signal_input_nof_blocks(0),
                                                                                                             signal_input_nof_samples(0),
                                                                                                             signal_input_mean{0.0},
                                                                                                             signal_input_rms{0.0},
                                                                                                             signal_input_std{0.0},
                                                                                                             xst_start_time(0),
                                                                                                             xst_input_bsn_at_sync(0),
                                                                                                             xst_output_sync_bsn(0),
                                                                                                             sst_offload_nof_packets(0),
                                                                                                             sst_offload_nof_valid(0),
                                                                                                             sst_offload_bsn(0),
                                                                                                             bst_offload_nof_packets{0},
                                                                                                             bst_offload_nof_valid{0},
                                                                                                             bst_offload_bsn{0},
                                                                                                             xst_offload_nof_packets(0),
                                                                                                             xst_offload_nof_valid(0),
                                                                                                             xst_offload_bsn(0),
                                                                                                             beamlet_output_nof_packets{0},
                                                                                                             beamlet_output_nof_valid{0},
                                                                                                             beamlet_output_bsn{0},
                                                                                                             beamlet_output_ready{0},
                                                                                                             beamlet_output_xon{0},
                                                                                                             beamlet_output_10gbe_tx_nof_frames(0),
                                                                                                             beamlet_output_10gbe_rx_nof_frames(0),
                                                                                                             jesd_csr_rbd_count{0},
                                                                                                             jesd_csr_dev_syncn{0},
                                                                                                             jesd_rx_err0{0},
                                                                                                             jesd_rx_err1{0},
                                                                                                             xst_processing_enable(false),
                                                                                                             monitor_pps_offset_time(0.0),
                                                                                                             pps_expected_cnt(0),
                                                                                                             pps_present(false),
                                                                                                             pps_capture_cnt(0),
                                                                                                             pps_error_cnt(0),
                                                                                                             xst_ring_rx_bsn{0},
                                                                                                             xst_ring_rx_nof_packets{0},
                                                                                                             xst_ring_rx_nof_valid{0},
                                                                                                             xst_ring_rx_latency{0},
                                                                                                             xst_rx_align_bsn{0},
                                                                                                             xst_rx_align_nof_packets{0},
                                                                                                             xst_rx_align_nof_valid{0},
                                                                                                             xst_rx_align_latency{0},
                                                                                                             xst_rx_align_nof_replaced_packets{0},
                                                                                                             xst_aligned_bsn(0),
                                                                                                             xst_aligned_nof_packets(0),
                                                                                                             xst_aligned_nof_valid(0),
                                                                                                             xst_aligned_latency(0),
                                                                                                             xst_ring_tx_bsn{0},
                                                                                                             xst_ring_tx_nof_packets{0},
                                                                                                             xst_ring_tx_nof_valid{0},
                                                                                                             xst_ring_tx_latency{0},
                                                                                                             bf_ring_rx_bsn{0},
                                                                                                             bf_ring_rx_nof_packets{0},
                                                                                                             bf_ring_rx_nof_valid{0},
                                                                                                             bf_ring_rx_latency{0},
                                                                                                             bf_rx_align_bsn{0},
                                                                                                             bf_rx_align_nof_packets{0},
                                                                                                             bf_rx_align_nof_valid{0},
                                                                                                             bf_rx_align_latency{0},
                                                                                                             bf_rx_align_nof_replaced_packets{0},
                                                                                                             bf_aligned_bsn{0},
                                                                                                             bf_aligned_nof_packets{0},
                                                                                                             bf_aligned_nof_valid{0},
                                                                                                             bf_aligned_latency{0},
                                                                                                             bf_ring_tx_bsn{0},
                                                                                                             bf_ring_tx_nof_packets{0},
                                                                                                             bf_ring_tx_nof_valid{0},
                                                                                                             bf_ring_tx_latency{0},
                                                                                                             flash_active(false),
                                                                                                             flash_addr(0),
                                                                                                             run_processing_enable(false)
{
    ucp = new UCP(ipaddr, udpport);
    mmap = new CMMap();

    monitor_start_time.tv_sec = 0;
    monitor_start_time.tv_nsec = 0;

    // flash settings
    Flash_fact_sector_start = 0;
    Flash_fact_sector_end = 159;
    Flash_user_sector_start = 160;
    Flash_user_sector_end = 319;
    Flash_select = 1; // default is user

    Flash_pages_per_sector = 1024;
    Flash_page_size_words = 64;
    Flash_page_size_bytes = Flash_page_size_words * 4;
    Flash_sector_size_words = Flash_pages_per_sector * Flash_page_size_words;
    Flash_sector_size_bytes = Flash_pages_per_sector * Flash_page_size_bytes;

#define FLASH_EPCS_REG_ADDR 0
#define FLASH_EPCS_REG_RDEN 1
#define FLASH_EPCS_REG_READ 2
#define FLASH_EPCS_REG_WRITE 3
#define FLASH_EPCS_REG_SECTOR_ERASE 4
#define FLASH_EPCS_REG_BUSY 5
#define FLASH_EPCS_REG_UNPROTECT 6

    // initialize arrays for reading  of _RW points
    pointmap = SD.fpga_map->get_pointMap();
    vector<string> regnames = pointmap->getRegnames("");
    for (auto m : regnames)
    {
        if (pointmap->canWrite(m, false) == true)
        {
            uint32_t size = pointmap->getFpgaPointSizeBytes(m);
            uint32_t cmd_id = pointmap->getCommandId(m);
            LOG_F(DEBUG, "init char array for cmd_id %d, size=%d", cmd_id, size);
            char *cmd_data_arr = new char[size];
            memset(cmd_data_arr, 0, size);
            rw_values[cmd_id] = cmd_data_arr;
        }
    }
}

Periph_fpga::~Periph_fpga()
{
    LOG_F(DEBUG, "Delete ucp node %d", GlobalNr);
    my_delete(ucp);

    LOG_F(DEBUG, "Delete mmap node %d", GlobalNr);
    my_delete(mmap);

    // delete all saved rw values
    for (auto it = rw_values.begin(); it != rw_values.end(); ++it)
    {
        char *value = it->second;
        my_delete_arr(value);
    }
    rw_values.clear();
}

/*
 * clear_fw_values(), this function clears all * values if communication is lost.
 */
bool Periph_fpga::clear_fw_values()
{
    current_design_name = "-";
    current_hw_version = 0;
    current_fw_version = "-.-";
    current_image = -1; // -1 = no image available, 0 = factory-image, 1 = user-image
    signal_input_sync_timeout = false;
    signal_input_bsn = 0;
    signal_input_nof_blocks = 0;
    signal_input_nof_samples = 0;
    xst_start_time = 0;
    xst_input_bsn_at_sync = 0;
    xst_output_sync_bsn = 0;
    sst_offload_nof_packets = 0;
    sst_offload_nof_valid = 0;
    sst_offload_bsn = 0;
    xst_offload_nof_packets = 0;
    xst_offload_nof_valid = 0;
    xst_offload_bsn = 0;
    pps_expected_cnt = 0;
    pps_present = false;
    pps_capture_cnt = 0;
    pps_error_cnt = 0;
    xst_aligned_bsn = 0;
    xst_aligned_nof_packets = 0;
    xst_aligned_nof_valid = 0;
    xst_aligned_latency = 0;
    beamlet_output_10gbe_tx_nof_frames = 0;
    beamlet_output_10gbe_rx_nof_frames = 0;

    monitor_start_time.tv_sec = 0;
    monitor_start_time.tv_nsec = 0;

    for (uint i = 0; i < C_N_pn_max; i++)
    {
        xst_ring_rx_bsn[i] = 0;
        xst_ring_rx_nof_packets[i] = 0;
        xst_ring_rx_nof_valid[i] = 0;
        xst_ring_rx_latency[i] = 0;
        xst_ring_tx_bsn[i] = 0;
        xst_ring_tx_nof_packets[i] = 0;
        xst_ring_tx_nof_valid[i] = 0;
        xst_ring_tx_latency[i] = 0;
    }

    for (uint i = 0; i < C_P_sq; i++)
    {
        xst_rx_align_bsn[i] = 0;
        xst_rx_align_nof_packets[i] = 0;
        xst_rx_align_nof_valid[i] = 0;
        xst_rx_align_latency[i] = 0;
        xst_rx_align_nof_replaced_packets[i] = 0;
    }

    for (uint i = 0; i < C_N_beamsets_sdp; i++)
    {
        bst_offload_nof_packets[i] = 0;
        bst_offload_nof_valid[i] = 0;
        bst_offload_bsn[i] = 0;
        beamlet_output_nof_packets[i] = 0;
        beamlet_output_nof_valid[i] = 0;
        beamlet_output_bsn[i] = 0;
        beamlet_output_ready[i] = 0;
        beamlet_output_xon[i] = 0;
        bf_aligned_nof_packets[i] = 0;
        bf_aligned_nof_valid[i] = 0;
        bf_aligned_latency[i] = 0;
        bf_ring_rx_bsn[i] = 0;
        bf_ring_rx_nof_packets[i] = 0;
        bf_ring_rx_nof_valid[i] = 0;
        bf_ring_rx_latency[i] = 0;
        bf_ring_tx_bsn[i] = 0;
        bf_ring_tx_nof_packets[i] = 0;
        bf_ring_tx_nof_valid[i] = 0;
        bf_ring_tx_latency[i] = 0;
    }

    for (uint i = 0; i < C_S_pn; i++)
    {
        jesd_csr_rbd_count[i] = 0;
        jesd_csr_dev_syncn[i] = 0;
        jesd_rx_err0[i] = 0;
        jesd_rx_err1[i] = 0;
        signal_input_mean[i] = 0.0;
        signal_input_rms[i] = 0.0;
        signal_input_std[i] = 0.0;
    }

    for (uint i = 0; i < (C_N_beamsets_sdp * C_P_sum); i++)
    {
        bf_rx_align_bsn[i] = 0;
        bf_rx_align_nof_packets[i] = 0;
        bf_rx_align_nof_valid[i] = 0;
        bf_rx_align_latency[i] = 0;
        bf_rx_align_nof_replaced_packets[i] = 0;
    }

    return true;
}

/*
 * read(), this function is called from Node::worker() thread.
 *
 * Register read is done if fpga is Online, even if not Masked.
 *   data: pointer to char array to give back the requested data
 *   cmd_id: id_nr of the requested CP/MP
 */
bool Periph_fpga::read(char *data, const uint32_t cmd_id)
{
    bool retval = false;

    if (cmd_id == GLOBAL_NODE_INDEX)
    {
        retval = read_global_node_index(data);
        return retval;
    }

    if (cmd_id == BOOT_IMAGE)
    {
        retval = read_boot_image(data);
        return retval;
    }

    if (ucp_block_comm)
    {
        return false;
    }

    if (!Online)
    {
        return false;
    } // Selected but not possible

    if (mmap->empty())
    {
        LOG_F(ERROR, "read(): empty mmap");
        return false;
    }

    LOG_F(DEBUG, "Online, read(%d)", cmd_id);
    switch (cmd_id)
    {
    case SYSTEM_INFO:
        retval = read_system_info(data);
        break;
    case STAMPS:
        retval = read_stamps(data);
        break;
    case NOTE:
        retval = true;
        break;
    case FIRMWARE_VERSION:
        retval = read_firmware_version(data);
        break;
    case HARDWARE_VERSION:
        retval = read_hardware_version(data);
        break;
    case TEMP:
        retval = read_fpga_temperature(data);
        break;
    case PPS_PRESENT:
        retval = read_pps_present(data, R_MEM);
        break;
    case PPS_EXPECTED_CNT:
        retval = read_pps_expected_cnt(data);
        break;
    case PPS_CAPTURE_CNT:
        retval = read_pps_capture_cnt(data, R_MEM);
        break;
    case PPS_ERROR_CNT:
        retval = read_pps_error_cnt(data);
        break;
    case TIME_SINCE_LAST_PPS:
        retval = read_time_since_last_pps(data);
        break;
    case MONITOR_PPS_OFFSET_TIME:
        retval = read_monitor_pps_offset_time(data, R_MEM);
        break;
    case PROCESSING_ENABLE:
        retval = read_all_from_port(data, cmd_id, "REG_BSN_SOURCE_V2", "dp_on");
        break;
    case CURRENT_BSN:
        retval = read_current_bsn(data);
        break;
    case SIGNAL_INPUT_BSN:
        retval = read_signal_input_bsn(data, R_MEM);
        break;
    case SIGNAL_INPUT_NOF_BLOCKS:
        retval = read_signal_input_nof_blocks(data, R_MEM);
        break;
    case SIGNAL_INPUT_NOF_SAMPLES:
        retval = read_signal_input_nof_samples(data, R_MEM);
        break;
    case SIGNAL_INPUT_MEAN:
        retval = read_signal_input_mean(data, R_MEM);
        break;
    case SIGNAL_INPUT_RMS:
        retval = read_signal_input_rms(data, R_MEM);
        break;
    case SIGNAL_INPUT_STD:
        retval = read_signal_input_std(data, R_MEM);
        break;
    case SIGNAL_INPUT_SAMPLES_DELAY:
        retval = read_all_from_port(data, cmd_id, "REG_DP_SHIFTRAM", "shift");
        break;
    case SIGNAL_INPUT_DATA_BUFFER:
        retval = read_signal_input_data_buffer(data);
        break;
    case SIGNAL_INPUT_HISTOGRAM:
        retval = read_all_from_port(data, cmd_id, "RAM_ST_HISTOGRAM", "data");
        break;
    case JESD204B_CSR_DEV_SYNCN:
        retval = read_jesd204b_csr_dev_syncn(data, R_MEM);
        break;
    case JESD204B_CSR_RBD_COUNT:
        retval = read_jesd204b_csr_rbd_count(data, R_MEM);
        break;
    case JESD204B_RX_ERR0:
        retval = read_jesd204b_rx_err0(data, R_MEM);
        break;
    case JESD204B_RX_ERR1:
        retval = read_jesd204b_rx_err1(data, R_MEM);
        break;
    case JESD_CTRL:
        retval = read_jesd_ctrl(data);
        break;
    case SUBBAND_WEIGHTS:
        retval = read_subband_weights(data, "RAM_EQUALIZER_GAINS" );
        break;
    case SUBBAND_WEIGHTS_CROSS:
        retval = read_subband_weights(data, "RAM_EQUALIZER_GAINS_CROSS");
        break;
    case SUBBAND_FIR_FILTER_COEFFICIENTS:
        retval = read_subband_fir_filter_coefficients(data);
        break;
    case SPECTRAL_INVERSION:
        retval = read_spectral_inversion(data);
        break;
    case SST_OFFLOAD_NOF_PACKETS:
        retval = read_sst_offload_nof_packets(data, R_MEM);
        break;
    case SST_OFFLOAD_NOF_VALID:
        retval = read_sst_offload_nof_valid(data, R_MEM);
        break;
    case SST_OFFLOAD_BSN:
        retval = read_sst_offload_bsn(data, R_MEM);
        break;
    case SST_OFFLOAD_WEIGHTED_SUBBANDS:
        retval = read_sst_offload_weighted_subbands(data);
        break;
    case SST_OFFLOAD_ENABLE:
        retval = read_all_from_port(data, cmd_id, "REG_STAT_ENABLE_SST", "enable");
        break;
    case SST_OFFLOAD_HDR_ETH_DESTINATION_MAC:
        retval = read_eth_mac(data, "REG_STAT_HDR_DAT_SST", "eth_destination_mac");
        break;
    case SST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS:
        retval = read_ip_address(data, "REG_STAT_HDR_DAT_SST", "ip_destination_address");
        break;
    case SST_OFFLOAD_HDR_UDP_DESTINATION_PORT:
        retval = read_udp_port(data, "REG_STAT_HDR_DAT_SST", "udp_destination_port");
        break;
    case BST_OFFLOAD_NOF_PACKETS:
        retval = read_bst_offload_nof_packets(data, R_MEM);
        break;
    case BST_OFFLOAD_NOF_VALID:
        retval = read_bst_offload_nof_valid(data, R_MEM);
        break;
    case BST_OFFLOAD_BSN:
        retval = read_bst_offload_bsn(data, R_MEM);
        break;
    case BST_OFFLOAD_ENABLE:
        retval = read_all_from_port(data, cmd_id, "REG_STAT_ENABLE_BST", "enable");
        break;
    case BST_OFFLOAD_HDR_ETH_DESTINATION_MAC:
        retval = read_eth_mac(data, "REG_STAT_HDR_DAT_BST", "eth_destination_mac");
        break;
    case BST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS:
        retval = read_ip_address(data, "REG_STAT_HDR_DAT_BST", "ip_destination_address");
        break;
    case BST_OFFLOAD_HDR_UDP_DESTINATION_PORT:
        retval = read_udp_port(data, "REG_STAT_HDR_DAT_BST", "udp_destination_port");
        break;
    case XST_INPUT_SYNC_AT_BSN:
        retval = read_xst_input_sync_at_bsn(data, R_MEM);
        break;
    case XST_OUTPUT_SYNC_BSN:
        retval = read_xst_output_sync_bsn(data, R_MEM);
        break;
    case XST_OFFLOAD_NOF_PACKETS:
        retval = read_xst_offload_nof_packets(data, R_MEM);
        break;
    case XST_OFFLOAD_NOF_VALID:
        retval = read_xst_offload_nof_valid(data, R_MEM);
        break;
    case XST_OFFLOAD_BSN:
        retval = read_xst_offload_bsn(data, R_MEM);
        break;
    case XST_RING_RX_BSN:
        retval = read_xst_ring_rx_bsn(data, R_MEM);
        break;
    case XST_RING_RX_NOF_PACKETS:
        retval = read_xst_ring_rx_nof_packets(data, R_MEM);
        break;
    case XST_RING_RX_NOF_VALID:
        retval = read_xst_ring_rx_nof_valid(data, R_MEM);
        break;
    case XST_RING_RX_LATENCY:
        retval = read_xst_ring_rx_latency(data, R_MEM);
        break;
    case XST_RX_ALIGN_BSN:
        retval = read_xst_rx_align_bsn(data, R_MEM);
        break;
    case XST_RX_ALIGN_NOF_PACKETS:
        retval = read_xst_rx_align_nof_packets(data, R_MEM);
        break;
    case XST_RX_ALIGN_NOF_VALID:
        retval = read_xst_rx_align_nof_valid(data, R_MEM);
        break;
    case XST_RX_ALIGN_LATENCY:
        retval = read_xst_rx_align_latency(data, R_MEM);
        break;
    case XST_ALIGNED_BSN:
        retval = read_xst_aligned_bsn(data, R_MEM);
        break;
    case XST_ALIGNED_NOF_PACKETS:
        retval = read_xst_aligned_nof_packets(data, R_MEM);
        break;
    case XST_ALIGNED_NOF_VALID:
        retval = read_xst_aligned_nof_valid(data, R_MEM);
        break;
    case XST_ALIGNED_LATENCY:
        retval = read_xst_aligned_latency(data, R_MEM);
        break;
    case XST_RING_TX_BSN:
        retval = read_xst_ring_tx_bsn(data, R_MEM);
        break;
    case XST_RING_TX_NOF_PACKETS:
        retval = read_xst_ring_tx_nof_packets(data, R_MEM);
        break;
    case XST_RING_TX_NOF_VALID:
        retval = read_xst_ring_tx_nof_valid(data, R_MEM);
        break;
    case XST_RING_TX_LATENCY:
        retval = read_xst_ring_tx_latency(data, R_MEM);
        break;
    case XST_RX_ALIGN_NOF_REPLACED_PACKETS:
        retval = read_xst_rx_align_nof_replaced_packets(data, R_MEM);
        break;
    case XST_OFFLOAD_ENABLE:
        retval = read_all_from_port(data, cmd_id, "REG_STAT_ENABLE_XST", "enable");
        break;
    case XST_OFFLOAD_NOF_CROSSLETS:
        retval = read_all_from_port(data, cmd_id, "REG_NOF_CROSSLETS", "nof_crosslets");
        break;
    case XST_OFFLOAD_HDR_ETH_DESTINATION_MAC:
        retval = read_eth_mac(data, "REG_STAT_HDR_DAT_XST", "eth_destination_mac");
        break;
    case XST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS:
        retval = read_ip_address(data, "REG_STAT_HDR_DAT_XST", "ip_destination_address");
        break;
    case XST_OFFLOAD_HDR_UDP_DESTINATION_PORT:
        retval = read_udp_port(data, "REG_STAT_HDR_DAT_XST", "udp_destination_port");
        break;
    case XST_START_TIME:
        retval = read_xst_start_time(data);
        break;
    case XST_PROCESSING_ENABLE:
        retval = read_xst_processing_enable(data);
        break;
    case XST_INTEGRATION_INTERVAL:
        retval = read_xst_integration_interval(data);
        break;
    case XST_SUBBAND_SELECT:
        retval = read_xst_subband_select(data);
        break;
    case XST_RING_NOF_TRANSPORT_HOPS:
        retval = read_all_from_port(data, cmd_id, "REG_RING_LANE_INFO_XST", "transport_nof_hops");
        break;
    case XST_RING_RX_CLEAR_TOTAL_COUNTS:
        retval = read_xst_ring_rx_clear_total_counts(data);
        break;
    case XST_RX_ALIGN_STREAM_ENABLE:
        retval = read_xst_rx_align_stream_enable(data);
        break;
    case XST_RING_RX_TOTAL_NOF_PACKETS_RECEIVED:
        retval = read_xst_ring_rx_total_nof_packets_received(data);
        break;
    case XST_RING_RX_TOTAL_NOF_PACKETS_DISCARDED:
        retval = read_xst_ring_rx_total_nof_packets_discarded(data);
        break;
    case XST_RING_RX_TOTAL_NOF_SYNC_RECEIVED:
        retval = read_xst_ring_rx_total_nof_sync_received(data);
        break;
    case XST_RING_RX_TOTAL_NOF_SYNC_DISCARDED:
        retval = read_xst_ring_rx_total_nof_sync_discarded(data);
        break;
    case BEAMLET_OUTPUT_ENABLE:
        retval = read_all_from_port(data, cmd_id, "REG_DP_XONOFF", "enable_stream");
        break;
    case BEAMLET_OUTPUT_NOF_PACKETS:
        retval = read_beamlet_output_nof_packets(data, R_MEM);
        break;
    case BEAMLET_OUTPUT_NOF_VALID:
        retval = read_beamlet_output_nof_valid(data, R_MEM);
        break;
    case BEAMLET_OUTPUT_BSN:
        retval = read_beamlet_output_bsn(data, R_MEM);
        break;
    case BEAMLET_OUTPUT_SCALE:
        retval = read_beamlet_output_scale(data);
        break;
    case BEAMLET_SUBBAND_SELECT:
        retval = read_beamlet_subband_select(data);
        break;
    case BEAMLET_OUTPUT_NOF_DESTINATIONS:
        retval = read_beamlet_output_nof_destinations(data);
        break;
    case BEAMLET_OUTPUT_NOF_DESTINATIONS_ACT:
        retval = read_beamlet_output_nof_destinations_act(data);
        break;
    case BEAMLET_OUTPUT_NOF_DESTINATIONS_MAX:
        retval = read_beamlet_output_nof_destinations_max(data);
        break;
    case BEAMLET_OUTPUT_NOF_BLOCKS_PER_PACKET:
        retval = read_beamlet_output_nof_blocks_per_packet(data);
        break;
    case BEAMLET_OUTPUT_HDR_ETH_DESTINATION_MAC:
        retval = read_eth_mac(data, "REG_HDR_DAT", "eth_destination_mac");
        break;
    case BEAMLET_OUTPUT_HDR_IP_DESTINATION_ADDRESS:
        retval = read_ip_address(data, "REG_HDR_DAT", "ip_destination_address");
        break;
    case BEAMLET_OUTPUT_HDR_UDP_DESTINATION_PORT:
        retval = read_udp_port(data, "REG_HDR_DAT", "udp_destination_port");
        break;
    case BEAMLET_OUTPUT_MULTIPLE_HDR_ETH_DESTINATION_MAC:
        retval = read_eth_mac(data, "REG_BDO_DESTINATIONS", "eth_destination_mac");
        break;
    case BEAMLET_OUTPUT_MULTIPLE_HDR_IP_DESTINATION_ADDRESS:
        retval = read_ip_address(data, "REG_BDO_DESTINATIONS", "ip_destination_address");
        break;
    case BEAMLET_OUTPUT_MULTIPLE_HDR_UDP_DESTINATION_PORT:
        retval = read_udp_port(data, "REG_BDO_DESTINATIONS", "udp_destination_port");
        break;
    case BEAMLET_OUTPUT_HDR_ETH_SOURCE_MAC:
        retval = read_eth_mac(data, "REG_HDR_DAT", "eth_source_mac");
        break;
    case BEAMLET_OUTPUT_HDR_IP_SOURCE_ADDRESS:
        retval = read_ip_address(data, "REG_HDR_DAT", "ip_source_address");
        break;
    case BEAMLET_OUTPUT_HDR_UDP_SOURCE_PORT:
        retval = read_udp_port(data, "REG_HDR_DAT", "udp_source_port");
        break;
    case BEAMLET_OUTPUT_READY:
        retval = read_beamlet_output_ready(data, R_MEM);
        break;
    case BEAMLET_OUTPUT_XON:
        retval = read_beamlet_output_xon(data, R_MEM);
        break;
    case BEAMLET_OUTPUT_10GBE_TX_NOF_FRAMES:
        retval = read_beamlet_output_10gbe_tx_nof_frames(data);
        break;
    case BEAMLET_OUTPUT_10GBE_RX_NOF_FRAMES:
        retval = read_beamlet_output_10gbe_rx_nof_frames(data);
        break;
    case BF_RING_RX_BSN:
        retval = read_bf_ring_rx_bsn(data, R_MEM);
        break;
    case BF_RING_RX_NOF_PACKETS:
        retval = read_bf_ring_rx_nof_packets(data, R_MEM);
        break;
    case BF_RING_RX_NOF_VALID:
        retval = read_bf_ring_rx_nof_valid(data, R_MEM);
        break;
    case BF_RING_RX_LATENCY:
        retval = read_bf_ring_rx_latency(data, R_MEM);
        break;
    case BF_RX_ALIGN_BSN:
        retval = read_bf_rx_align_bsn(data, R_MEM);
        break;
    case BF_RX_ALIGN_NOF_PACKETS:
        retval = read_bf_rx_align_nof_packets(data, R_MEM);
        break;
    case BF_RX_ALIGN_NOF_VALID:
        retval = read_bf_rx_align_nof_valid(data, R_MEM);
        break;
    case BF_RX_ALIGN_LATENCY:
        retval = read_bf_rx_align_latency(data, R_MEM);
        break;
    case BF_ALIGNED_BSN:
        retval = read_bf_aligned_bsn(data, R_MEM);
        break;
    case BF_ALIGNED_NOF_PACKETS:
        retval = read_bf_aligned_nof_packets(data, R_MEM);
        break;
    case BF_ALIGNED_NOF_VALID:
        retval = read_bf_aligned_nof_valid(data, R_MEM);
        break;
    case BF_ALIGNED_LATENCY:
        retval = read_bf_aligned_latency(data, R_MEM);
        break;
    case BF_RING_TX_BSN:
        retval = read_bf_ring_tx_bsn(data, R_MEM);
        break;
    case BF_RING_TX_NOF_PACKETS:
        retval = read_bf_ring_tx_nof_packets(data, R_MEM);
        break;
    case BF_RING_TX_NOF_VALID:
        retval = read_bf_ring_tx_nof_valid(data, R_MEM);
        break;
    case BF_RING_TX_LATENCY:
        retval = read_bf_ring_tx_latency(data, R_MEM);
        break;
    case BF_RX_ALIGN_NOF_REPLACED_PACKETS:
        retval = read_bf_rx_align_nof_replaced_packets(data, R_MEM);
        break;
    case BF_WEIGHTS_XX_YY:
        retval = read_bf_weights_xx_yy(data);
        break;
    case BF_WEIGHTS_XY_YX:
        retval = read_bf_weights_xy_yx(data);
        break;
    case BF_WEIGHTS:
        retval = read_bf_weights(data);
        break;
    case BF_WEIGHTS_XX_XY_YX_YY:
        retval = read_bf_weights_xx_xy_yx_yy(data);
        break;
    case BF_WEIGHTS_XX:
        retval = read_bf_weights_xx(data);
        break;
    case BF_WEIGHTS_XY:
        retval = read_bf_weights_xy(data);
        break;
    case BF_WEIGHTS_YX:
        retval = read_bf_weights_yx(data);
        break;
    case BF_WEIGHTS_YY:
        retval = read_bf_weights_yy(data);
        break;
    case BF_WEIGHTS_PP:
        retval = read_bf_weights_xx(data);
        break;
    case BF_RING_NOF_TRANSPORT_HOPS:
        retval = read_all_from_port(data, cmd_id, "REG_RING_LANE_INFO_BF", "transport_nof_hops");
        break;
    case BF_RING_RX_CLEAR_TOTAL_COUNTS:
        retval = read_bf_ring_rx_clear_total_counts(data);
        break;
    case BF_RX_ALIGN_STREAM_ENABLE:
        retval = read_bf_rx_align_stream_enable(data);
        break;
    case BF_RING_RX_TOTAL_NOF_PACKETS_RECEIVED:
        retval = read_bf_ring_rx_total_nof_packets_received(data);
        break;
    case BF_RING_RX_TOTAL_NOF_PACKETS_DISCARDED:
        retval = read_bf_ring_rx_total_nof_packets_discarded(data);
        break;
    case BF_RING_RX_TOTAL_NOF_SYNC_RECEIVED:
        retval = read_bf_ring_rx_total_nof_sync_received(data);
        break;
    case BF_RING_RX_TOTAL_NOF_SYNC_DISCARDED:
        retval = read_bf_ring_rx_total_nof_sync_discarded(data);
        break;
    case RING_NODE_OFFSET:
        retval = read_all_from_port(data, cmd_id, "REG_RING_INFO", "o_rn");
        break;
    case RING_NOF_NODES:
        retval = read_all_from_port(data, cmd_id, "REG_RING_INFO", "n_rn");
        break;
    case RING_USE_CABLE_TO_NEXT_RN:
        retval = read_all_from_port(data, cmd_id, "REG_RING_INFO", "use_cable_to_next_rn");
        break;
    case RING_USE_CABLE_TO_PREVIOUS_RN:
        retval = read_all_from_port(data, cmd_id, "REG_RING_INFO", "use_cable_to_previous_rn");
        break;
    case SDP_INFO_STATION_ID:
        retval = read_all_from_port(data, cmd_id, "REG_SDP_INFO", "station_id");
        break;
    case SDP_INFO_ANTENNA_FIELD_INDEX:
        retval = read_all_from_port(data, cmd_id, "REG_SDP_INFO", "antenna_field_index");
        break;
    case SDP_INFO_OBSERVATION_ID:
        retval = read_all_from_port(data, cmd_id, "REG_SDP_INFO", "observation_id");
        break;
    case SDP_INFO_NYQUIST_SAMPLING_ZONE_INDEX:
        retval = read_all_from_port(data, cmd_id, "REG_SDP_INFO", "nyquist_zone_index");
        break;
    case SDP_INFO_ANTENNA_BAND_INDEX:
        retval = read_all_from_port(data, cmd_id, "REG_SDP_INFO", "antenna_band_index");
        break;
    case SDP_INFO_F_ADC:
        retval = read_all_from_port(data, cmd_id, "REG_SDP_INFO", "f_adc");
        break;
    case SDP_INFO_FSUB_TYPE:
        retval = read_all_from_port(data, cmd_id, "REG_SDP_INFO", "fsub_type");
        break;
    case SDP_INFO_BLOCK_PERIOD:
        retval = read_all_from_port(data, cmd_id, "REG_SDP_INFO", "block_period");
        break;
    case DDR_GIGABYTES:
        retval = read_ddr_gigabytes(data);
        break;
    case DDR_CALIBRATED:
        retval = read_ddr_calibrated(data);
        break;
    case WG_ENABLE:
        retval = read_all_from_port(data, cmd_id, "REG_WG", "mode");
        break;
    case WG_AMPLITUDE:
        retval = read_wg_amplitude(data);
        break;
    case WG_PHASE:
        retval = read_wg_phase(data);
        break;
    case WG_FREQUENCY:
        retval = read_wg_frequency(data);
        break;
    case FLASH_ADDR:
        retval = read_flash_addr(data);
        break;
    case FLASH_ERASE_SECTOR:
        retval = read_flash_erase_sector(data);
        break;
    case FLASH_PAGES:
        retval = read_flash_pages(data);
        break;
    case FLASH_PROTECT:
        retval = read_flash_protect(data);
        break;
    case SCRAP:
        retval = read_fpga_scrap(data);
        break;
    default:
        throw runtime_error("cmd_id " + to_string(cmd_id) + " not found!");
        break;
    }

    if (retval == false)
    {
        LOG_F(ERROR, "read cmd_id '%d' failed", cmd_id);
    }
    return retval;
}

/*
 * read_saved(), this function is called from Node::worker() thread.
 *
 * Register read_saved is done if R from a _RW point is called.
 * it returns the last written values (0 if never written)
 *   data: pointer to char array to give back the requested data
 *   cmd_id: id_nr of the requested CP/MP
 */
bool Periph_fpga::read_saved(char *data, const uint32_t cmd_id)
{
    bool retval = false;

    // check first if cmd_id excists
    auto i = rw_values.find(cmd_id);
    if (i != rw_values.end())
    {
        char *_data = rw_values[cmd_id];
        string name = pointmap->getName(cmd_id);
        uint32_t size = pointmap->getFpgaPointSizeBytes(name);
        memcpy(data, _data, size);
        retval = true;
    }
    return retval;
}

/*
 * write(), this function is called from Node::worker() thread.
 *
 * Register write is done if fpga is Online and Masked.
 *   data: pointer to char array witch holds the data to write to the requested CP
 *   cmd_id: id_nr of the requested CP
 */
bool Periph_fpga::write(char *data, const uint32_t cmd_id)
{
    bool retval = false;
    uint32_t max_n_fields;
    char beamlet_output_nof_destinations_1[2] = {1};

    if (!Masked)
    {
        return false;
    } // Not selected

    if (ucp_block_comm)
    {
        return false;
    }

    // check first if cmd_id does exist
    auto i = rw_values.find(cmd_id);
    if (i != rw_values.end())
    {
        char *_data = rw_values[cmd_id];
        string name = pointmap->getName(cmd_id);
        uint32_t size = pointmap->getFpgaPointSizeBytes(name);
        memcpy(_data, data, size);
    }

    if (!Online)
    {
        return false;
    } // Selected but not possible

    if (mmap->empty())
    {
        LOG_F(ERROR, "write(): empty mmap");
        return false;
    }

    switch (cmd_id)
    {
    case BF_WEIGHTS_XX_YY:
        retval = write_bf_weights_xx_yy(data);
        break;
    case BF_WEIGHTS_XY_YX:
        retval = write_bf_weights_xy_yx(data);
        break;
    case SCRAP:
        retval = write_fpga_scrap(data);
        break;
    case SST_OFFLOAD_WEIGHTED_SUBBANDS:
        retval = write_sst_offload_weighted_subbands(data);
        break;
    case SST_OFFLOAD_ENABLE:
        retval = write_sst_offload_enable(data);
        break;
    case SST_OFFLOAD_HDR_ETH_DESTINATION_MAC:
        retval = write_eth_mac(data, "REG_STAT_HDR_DAT_SST", "eth_destination_mac");
        break;
    case SST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS:
        retval = write_ip_address(data, "REG_STAT_HDR_DAT_SST", "ip_destination_address");
        break;
    case SST_OFFLOAD_HDR_UDP_DESTINATION_PORT:
        retval = write_udp_port(data, "REG_STAT_HDR_DAT_SST", "udp_destination_port");
        break;
    case BST_OFFLOAD_ENABLE:
        retval = write_bst_offload_enable(data);
        break;
    case BST_OFFLOAD_HDR_ETH_DESTINATION_MAC:
        retval = write_eth_mac(data, "REG_STAT_HDR_DAT_BST", "eth_destination_mac");
        break;
    case BST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS:
        retval = write_ip_address(data, "REG_STAT_HDR_DAT_BST", "ip_destination_address");
        break;
    case BST_OFFLOAD_HDR_UDP_DESTINATION_PORT:
        retval = write_udp_port(data, "REG_STAT_HDR_DAT_BST", "udp_destination_port");
        break;
    case XST_OFFLOAD_ENABLE:
        retval = write_xst_offload_enable(data);
        break;
    case XST_OFFLOAD_NOF_CROSSLETS:
        retval = write_xst_offload_nof_crosslets(data);
        break;
    case XST_OFFLOAD_HDR_ETH_DESTINATION_MAC:
        retval = write_eth_mac(data, "REG_STAT_HDR_DAT_XST", "eth_destination_mac");
        break;
    case XST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS:
        retval = write_ip_address(data, "REG_STAT_HDR_DAT_XST", "ip_destination_address");
        break;
    case XST_OFFLOAD_HDR_UDP_DESTINATION_PORT:
        retval = write_udp_port(data, "REG_STAT_HDR_DAT_XST", "udp_destination_port");
        break;
    case XST_START_TIME:
        retval = write_xst_start_time(data);
        break;
    case XST_PROCESSING_ENABLE:
        retval = write_xst_processing_enable(data);
        break;
    case XST_INTEGRATION_INTERVAL:
        retval = write_xst_integration_interval(data);
        break;
    case XST_SUBBAND_SELECT:
        retval = write_xst_subband_select(data);
        break;
    case BEAMLET_OUTPUT_ENABLE:
        retval = write_beamlet_output_enable(data);
        break;
    case BEAMLET_OUTPUT_SCALE:
        retval = write_beamlet_output_scale(data);
        break;
    case BEAMLET_OUTPUT_HDR_ETH_DESTINATION_MAC:
        retval = write_eth_mac(data, "REG_HDR_DAT", "eth_destination_mac");
        // if beamlet_output multiple destinations is available in the regmap
        // set first value and nof_destinations
        if (mmap->is_existing_register("mm/0/REG_BDO_DESTINATIONS/0/eth_destination_mac")) {
            max_n_fields = mmap->getNFields("mm/0/REG_HDR_DAT/0/eth_destination_mac");
            retval &= write_eth_mac(data, "REG_BDO_DESTINATIONS", "eth_destination_mac", max_n_fields);
            retval &= write_beamlet_output_nof_destinations(beamlet_output_nof_destinations_1);
        }
        break;
    case BEAMLET_OUTPUT_HDR_IP_DESTINATION_ADDRESS:
        retval = write_ip_address(data, "REG_HDR_DAT", "ip_destination_address");
        // if beamlet_output multiple destinations is available in the regmap
        // set first value and nof_destinations
        if (mmap->is_existing_register("mm/0/REG_BDO_DESTINATIONS/0/ip_destination_address")) {
            max_n_fields = mmap->getNFields("mm/0/REG_HDR_DAT/0/ip_destination_address");
            retval &= write_ip_address(data, "REG_BDO_DESTINATIONS", "ip_destination_address", max_n_fields);
            retval &= write_beamlet_output_nof_destinations(beamlet_output_nof_destinations_1);
        }
        break;
    case BEAMLET_OUTPUT_HDR_UDP_DESTINATION_PORT:
        retval = write_udp_port(data, "REG_HDR_DAT", "udp_destination_port");
        // if beamlet_output multiple destinations is available in the regmap
        // set first value and nof_destinations
        if (mmap->is_existing_register("mm/0/REG_BDO_DESTINATIONS/0/udp_destination_port")) {
            max_n_fields = mmap->getNFields("mm/0/REG_HDR_DAT/0/udp_destination_port");
            retval &= write_udp_port(data, "REG_BDO_DESTINATIONS", "udp_destination_port", max_n_fields);
            retval &= write_beamlet_output_nof_destinations(beamlet_output_nof_destinations_1);
        }
        break;
    case BEAMLET_OUTPUT_NOF_DESTINATIONS:
        retval = write_beamlet_output_nof_destinations(data);
        break;
    case BEAMLET_OUTPUT_MULTIPLE_HDR_ETH_DESTINATION_MAC:
        max_n_fields = mmap->getNFields("mm/0/REG_BDO_DESTINATIONS/0/eth_destination_mac");
        retval = write_eth_mac(data, "REG_BDO_DESTINATIONS", "eth_destination_mac", max_n_fields);
        // set first mac also to REG_HDR_DAT
        retval &= write_eth_mac(data, "REG_HDR_DAT", "eth_destination_mac");
        break;
    case BEAMLET_OUTPUT_MULTIPLE_HDR_IP_DESTINATION_ADDRESS:
        max_n_fields = mmap->getNFields("mm/0/REG_BDO_DESTINATIONS/0/ip_destination_address");
        retval = write_ip_address(data, "REG_BDO_DESTINATIONS", "ip_destination_address", max_n_fields);
        // set first address also to REG_HDR_DAT
        retval &= write_ip_address(data, "REG_HDR_DAT", "ip_destination_address");
        break;
    case BEAMLET_OUTPUT_MULTIPLE_HDR_UDP_DESTINATION_PORT:
        max_n_fields = mmap->getNFields("mm/0/REG_BDO_DESTINATIONS/0/udp_destination_port");
        retval = write_udp_port(data, "REG_BDO_DESTINATIONS", "udp_destination_port", max_n_fields);
        // set first port also to REG_HDR_DAT
        retval &= write_udp_port(data, "REG_HDR_DAT", "udp_destination_port");
        break;
    case BEAMLET_OUTPUT_HDR_ETH_SOURCE_MAC:
        retval = write_eth_mac(data, "REG_HDR_DAT", "eth_source_mac");
        break;
    case BEAMLET_OUTPUT_HDR_IP_SOURCE_ADDRESS:
        retval = write_ip_address(data, "REG_HDR_DAT", "ip_source_address");
        break;
    case BEAMLET_OUTPUT_HDR_UDP_SOURCE_PORT:
        retval = write_udp_port(data, "REG_HDR_DAT", "udp_source_port");
        break;
    case PROCESSING_ENABLE:
        retval = write_processing_enable(data);
        break;
    case SDP_INFO_STATION_ID:
        retval = write_sdp_info_station_id(data);
        break;
    case SDP_INFO_ANTENNA_FIELD_INDEX:
        retval = write_sdp_info_antenna_field_index(data);
        break;
    case SDP_INFO_OBSERVATION_ID:
        retval = write_sdp_info_observation_id(data);
        break;
    case SDP_INFO_NYQUIST_SAMPLING_ZONE_INDEX:
        retval = write_sdp_info_nyquist_sampling_zone_index(data);
        break;
    case SDP_INFO_ANTENNA_BAND_INDEX:
        retval = write_sdp_info_antenna_band_index(data);
        break;
    case WG_ENABLE:
        retval = write_wg_enable(data);
        break;
    case WG_AMPLITUDE:
        retval = write_wg_amplitude(data);
        break;
    case WG_PHASE:
        retval = write_wg_phase(data);
        break;
    case WG_FREQUENCY:
        retval = write_wg_frequency(data);
        break;
    case SIGNAL_INPUT_SAMPLES_DELAY:
        retval = write_signal_input_samples_delay(data);
        break;
    case JESD204B_CLEAR_RX_ERR0:
        retval = write_jesd204b_clear_rx_err0(data);
        break;
    case JESD204B_CLEAR_RX_ERR1:
        retval = write_jesd204b_clear_rx_err1(data);
        break;
    case JESD_CTRL:
        retval = write_jesd_ctrl(data);
        break;
    case SUBBAND_WEIGHTS:
        retval = write_subband_weights(data, "RAM_EQUALIZER_GAINS");
        break;
    case SUBBAND_WEIGHTS_CROSS:
        retval = write_subband_weights(data, "RAM_EQUALIZER_GAINS_CROSS");
        break;
    case SUBBAND_FIR_FILTER_COEFFICIENTS:
        retval = write_subband_fir_filter_coefficients(data);
        break;
    case BEAMLET_SUBBAND_SELECT:
        retval = write_beamlet_subband_select(data);
        break;
    case BF_WEIGHTS:
        retval = write_bf_weights(data);
        break;
    case BF_WEIGHTS_XX_XY_YX_YY:
        retval = write_bf_weights_xx_xy_yx_yy(data);
        break;
    case BF_WEIGHTS_XX:
        retval = write_bf_weights_xx(data);
        break;
    case BF_WEIGHTS_XY:
        retval = write_bf_weights_xy(data);
        break;
    case BF_WEIGHTS_YX:
        retval = write_bf_weights_yx(data);
        break;
    case BF_WEIGHTS_YY:
        retval = write_bf_weights_yy(data);
        break;
    case BF_WEIGHTS_PP:
        retval = write_bf_weights_xx(data);
        retval &= write_bf_weights_yy(data);
        break;
    case BF_RING_RX_CLEAR_TOTAL_COUNTS:
        retval = write_bf_ring_rx_clear_total_counts(data);
        break;
    case BF_RX_ALIGN_STREAM_ENABLE:
        retval = write_bf_rx_align_stream_enable(data);
        break;
    case BF_RING_NOF_TRANSPORT_HOPS:
        retval = write_bf_ring_nof_transport_hops(data);
        break;
    case SPECTRAL_INVERSION:
        retval = write_spectral_inversion(data);
        break;
    case PPS_EXPECTED_CNT:
        retval = write_pps_expected_cnt(data);
        break;
    case FLASH_ADDR:
        retval = write_flash_addr(data);
        break;
    case FLASH_ERASE_SECTOR:
        retval = write_flash_erase_sector(data);
        break;
    case FLASH_PAGES:
        retval = write_flash_pages(data);
        break;
    case FLASH_PROTECT:
        retval = write_flash_protect(data);
        break;
    case BOOT_IMAGE:
        retval = write_boot_image(data);
        break;
    case RING_NODE_OFFSET:
        retval = write_ring_node_offset(data);
        break;
    case RING_NOF_NODES:
        retval = write_ring_nof_nodes(data);
        break;
    case RING_USE_CABLE_TO_NEXT_RN:
        retval = write_ring_use_cable_to_next_rn(data);
        break;
    case RING_USE_CABLE_TO_PREVIOUS_RN:
        retval = write_ring_use_cable_to_previous_rn(data);
        break;
    case XST_RING_NOF_TRANSPORT_HOPS:
        retval = write_xst_ring_nof_transport_hops(data);
        break;
    case XST_RING_RX_CLEAR_TOTAL_COUNTS:
        retval = write_xst_ring_rx_clear_total_counts(data);
        break;
    case XST_RX_ALIGN_STREAM_ENABLE:
        retval = write_xst_rx_align_stream_enable(data);
        break;
    default:
        throw runtime_error("cmd_id: " + to_string(cmd_id) + " not found!");
        break;
    }

    if (retval == false)
    {
        LOG_F(ERROR, "write cmd_id '%d' failed", cmd_id);
    }
    return retval;
}

/*
 * monitor(), this function is called from Node::worker() thread.
 */
bool Periph_fpga::monitor()
{
    if (ucp_block_comm)
    {
        return true;
    }

    LOG_F(DEBUG, "Run monitor");
    char data[10000]; // use data buffer for functions below, data is not used

    // read_system_info() will set Online state and current_image.
    Online = read_system_info(data);
    if (flash_active)
    {
        return true;
    }

    // read 1 sec operation points

    // if factory or user image
    if (current_image >= 0)
    {
        // first get start time and pps_offset of this monitor run.
        read_monitor_pps_offset_time(data, R_UCP);

        if (pps_expected_cnt == 0)
        {
            read_pps_expected_cnt(data);
        }
        read_pps_capture_cnt(data, R_UCP);
    }
    // if user image
    if (current_image == 1)
    {
        read_signal_input_sync_timeout(data, R_UCP);
        read_signal_input_bsn(data, R_UCP);
        read_signal_input_nof_blocks(data, R_UCP);
        read_signal_input_nof_samples(data, R_UCP);
        read_signal_input_mean(data, R_UCP);
        read_signal_input_rms(data, R_UCP);
        read_signal_input_std(data, R_UCP);
        read_xst_input_sync_at_bsn(data, R_UCP);
        read_xst_output_sync_bsn(data, R_UCP);
        read_jesd204b_csr_rbd_count(data, R_UCP);
        read_jesd204b_csr_dev_syncn(data, R_UCP);
        read_jesd204b_rx_err0(data, R_UCP);
        read_jesd204b_rx_err1(data, R_UCP);
        read_sst_offload_nof_packets(data, R_UCP);
        read_sst_offload_bsn(data, R_UCP);
        read_bst_offload_nof_packets(data, R_UCP);
        read_bst_offload_bsn(data, R_UCP);
        read_xst_offload_nof_packets(data, R_UCP);
        read_xst_offload_bsn(data, R_UCP);
        read_beamlet_output_nof_packets(data, R_UCP);
        read_beamlet_output_bsn(data, R_UCP);
        read_beamlet_output_ready(data, R_UCP);
        read_beamlet_output_xon(data, R_UCP);
        read_xst_ring_rx_bsn(data, R_UCP);
        read_xst_ring_rx_nof_packets(data, R_UCP);
        read_xst_ring_rx_latency(data, R_UCP);
        read_xst_rx_align_bsn(data, R_UCP);
        read_xst_rx_align_nof_packets(data, R_UCP);
        read_xst_rx_align_latency(data, R_UCP);
        read_xst_aligned_bsn(data, R_UCP);
        read_xst_aligned_nof_packets(data, R_UCP);
        read_xst_aligned_latency(data, R_UCP);
        read_xst_ring_tx_bsn(data, R_UCP);
        read_xst_ring_tx_nof_packets(data, R_UCP);
        read_xst_ring_tx_latency(data, R_UCP);
        read_xst_rx_align_nof_replaced_packets(data, R_UCP);
        read_bf_ring_rx_bsn(data, R_UCP);
        read_bf_ring_rx_nof_packets(data, R_UCP);
        read_bf_ring_rx_latency(data, R_UCP);
        read_bf_rx_align_bsn(data, R_UCP);
        read_bf_rx_align_nof_packets(data, R_UCP);
        read_bf_rx_align_latency(data, R_UCP);
        read_bf_aligned_bsn(data, R_UCP);
        read_bf_aligned_nof_packets(data, R_UCP);
        read_bf_aligned_latency(data, R_UCP);
        read_bf_ring_tx_bsn(data, R_UCP);
        read_bf_ring_tx_nof_packets(data, R_UCP);
        read_bf_ring_tx_latency(data, R_UCP);
        read_bf_rx_align_nof_replaced_packets(data, R_UCP);
        // 1 sec debug points
        read_sst_offload_nof_valid(data, R_UCP);
        read_bst_offload_nof_valid(data, R_UCP);
        read_xst_offload_nof_valid(data, R_UCP);
        read_beamlet_output_nof_valid(data, R_UCP);
        read_xst_ring_rx_nof_valid(data, R_UCP);
        read_xst_rx_align_nof_valid(data, R_UCP);
        read_xst_aligned_nof_valid(data, R_UCP);
        read_xst_ring_tx_nof_valid(data, R_UCP);
        read_bf_ring_rx_nof_valid(data, R_UCP);
        read_bf_rx_align_nof_valid(data, R_UCP);
        read_bf_aligned_nof_valid(data, R_UCP);
        read_bf_ring_tx_nof_valid(data, R_UCP);

        // write action that need to be done in the same second.
        if (run_processing_enable)
        {
            processing_enable();
        }
    }
    LOG_F(DEBUG, "Done monitor");
    return true;
}

/************************************************
 from here only intern (private) used functions
************************************************/

/*
 * Read(), do a read to a UCP register
 * - check if addr_str is available.
 * - get information for reading.
 * - put read data in 'data_ptr' (masked and shifted on request).
 * - return true on succes else false.
 */
bool Periph_fpga::Read(const string &addr_str, uint32_t *data_ptr, bool use_mask_shift, uint32_t addr_offs, uint32_t nwords)
{
    bool ret;
    uint32_t base_addr;
    if (!mmap->getAddr(addr_str, &base_addr))
    {
        return false;
    }
    uint32_t word_span = mmap->getSpan((addr_str));

    uint32_t addr = base_addr;
    uint32_t nvalues = word_span;

    // Apply address offset, nwords if set
    if (nwords > 0)
    {
        // Check mmap/parameters
        if (!(addr_offs + nwords <= word_span))
        {
            LOG_F(ERROR, "Read: addr_offs+nwords (%d+%d) exceeds word_span (%d)", addr_offs, nwords, word_span);
            return false;
        }
        addr = base_addr + addr_offs * sizeof(uint32_t);
        nvalues = nwords;
    }

    // Read register
    ret = ucp->readRegister(addr, nvalues, data_ptr);
    ucp_reads++;
    if (ret == false)
    {
        ucp_read_fails++;
    }
    ucp_read_retries += ucp->getRetries();

    // Apply mask, if any
    if (use_mask_shift)
    {
        uint32_t shift = mmap->getShift((addr_str));
        uint32_t mask = mmap->getMask((addr_str));
        for (uint32_t i = 0; i < nvalues; i++)
        {
            data_ptr[i] = mask_shift(shift, mask, data_ptr[i]);
        }
    }

    return ret;
}

bool Periph_fpga::ReadFifo(const string &addr_str, uint32_t *data_ptr, uint32_t nvalues)
{
    uint32_t addr;

    // if a valid addr_str addr is set and function returns true
    if (mmap->getAddr(addr_str, &addr))
    {
        bool ret = ucp->readFifoRegister(addr, nvalues, data_ptr);
        ucp_reads++;
        if (ret == false)
        {
            ucp_read_fails++;
        }
        ucp_read_retries += ucp->getRetries();
        return ret;
    }
    return false;
}

/*
 * Write(), do a write to a UCP register
 * - check if addr_str is available.
 * - get information for writing.
 * - write data from 'data_ptr' (shifted and masked on request)
 * - return true on succes else false.
 * - addr_offset = address offset in words
 */
bool Periph_fpga::Write(const string &addr_str, uint32_t *data_ptr, bool wait_for_ack, bool use_shift_mask, uint32_t addr_offs, uint32_t nwords)
{
    uint32_t base_addr;
    if (!mmap->getAddr(addr_str, &base_addr))
    {
        return false;
    }

    uint32_t word_span = mmap->getSpan((addr_str));

    uint32_t addr = base_addr;
    uint32_t nvalues = word_span;

    // Apply address offset, nwords if set
    if (nwords > 0)
    {
        // Check mmap/parameters
        if (!(addr_offs + nwords <= word_span))
        {
            LOG_F(ERROR, "Write: addr_offs+nwords (%d+%d) exceeds word_span (%d)", addr_offs, nwords, word_span);
            return false;
        }
        addr = base_addr + addr_offs * sizeof(uint32_t);
        nvalues = nwords;
    }

    // Apply mask, if any
    if (use_shift_mask)
    {
        uint32_t shift = mmap->getShift((addr_str));
        uint32_t mask = mmap->getMask((addr_str));
        for (uint32_t i = 0; i < nvalues; i++)
        {
            data_ptr[i] = shift_mask(shift, mask, data_ptr[i]);
        }
    }

    // Write register
    if (mmap->canWrite(addr_str))
    {
        bool ret = ucp->writeRegister(addr, nvalues, data_ptr, wait_for_ack);
        ucp_writes++;
        if (ret == false)
        {
            ucp_write_fails++;
        }
        ucp_write_retries += ucp->getRetries();
        return ret;
    }
    return false;
}

bool Periph_fpga::WriteFifo(const string &addr_str, uint32_t *data_ptr, uint32_t nvalues)
{
    uint32_t base_addr;
    if (!mmap->getAddr(addr_str, &base_addr))
    {
        return false;
    }

    if (mmap->canWrite(addr_str))
    {
        bool ret = ucp->writeFifoRegister(base_addr, nvalues, data_ptr);
        ucp_writes++;
        if (ret == false)
        {
            ucp_write_fails++;
        }
        ucp_write_retries += ucp->getRetries();
        return ret;
    }
    return false;
}

/*
 * mask_shift (for reading)
 * mask data and shift bits using information from the mmap.
 */
uint32_t Periph_fpga::mask_shift(const uint32_t shift, const uint32_t mask, uint32_t data)
{
    return (uint32_t)((data & mask) >> shift);
}

/*
 * mask_shift (for reading)
 * mask data and shift bits using information from the mmap.
 */
uint32_t Periph_fpga::mask_shift(const string &addr_str, uint32_t data)
{
    uint32_t shift = mmap->getShift((addr_str));
    uint32_t mask = mmap->getMask((addr_str));
    uint32_t sdp_data = data;

    if (shift != 0 || mask != 0xffffffff)
    {
        sdp_data &= mask;
        sdp_data = sdp_data >> shift;
    }
    return sdp_data;
}

/*
 * shift_mask (for writing)
 * shift data and mask bits using information from the mmap.
 */
uint32_t Periph_fpga::shift_mask(const uint32_t shift, const uint32_t mask, uint32_t data)
{
    return ((uint32_t)(data << shift) & mask);
}

/*
 * shift_mask (for writing)
 * shift data and mask bits using information from the mmap.
 */
uint32_t Periph_fpga::shift_mask(const string &addr_str, uint32_t data)
{
    uint32_t shift = mmap->getShift((addr_str));
    uint32_t mask = mmap->getMask((addr_str));
    uint32_t sdp_data = data;

    if (shift != 0 || mask != 0xffffffff)
    {
        sdp_data = sdp_data << shift;
        sdp_data &= mask;
    }
    return sdp_data;
}

/*
"""Peripheral system_info
   Register map:
    31             24 23             16 15              8 7               0  wi
   |-----------------|-----------------|-----------------|-----------------|
   |                                                          use_phy[7:0] |  1
   |-----------------------------------------------------------------------|
   |                           system_info[31:0]                           |  0
   |-----------------------------------------------------------------------|

    system_info[26:24] = rom version [2:0]
    system_info[23:20] = firmware version high[3:0]
    system_info[19:16] = firmware version low[3:0]
    system_info[10]    = cs_sim (= g_sim, 0 on HW, 1 in VHDL simulation)
    system_info[9:8]   = hardware version [1:0] (= 0 for UniBoard 1A and 1B)
    system_info[7:0]   = node id[7;0]

    read_system_info() return true if Online else false
*/
bool Periph_fpga::read_system_info(char *data)
{
// mask and shift values for getting version info from SYSTEM_INFO
#define REG_ADDR_SYSTEM_INFO 0x0
#define REG_ADDR_SYSTEM_INFO_SPAN 1
#define ROM_VERSION_MASK 0x07000000
#define ROM_VERSION_SHIFT 24
#define FW_VERSION_MASK 0x00F00000
#define FW_VERSION_SHIFT 20
#define FW_SUBVERSION_MASK 0x000F0000
#define FW_SUBVERSION_SHIFT 16
#define HW_VERSION_MASK 0x0000300
#define HW_VERSION_SHIFT 8

    uint32_t nvalues = REG_ADDR_SYSTEM_INFO_SPAN; // = 1
    uint32_t addr = REG_ADDR_SYSTEM_INFO;         // = 0
    uint32_t sdp_data;
    bool retval;
    bool clear = false;

    // check if register can be read
    if (Online == true)
    {
        retval = ucp->readRegister(addr, nvalues, &sdp_data, 5);
    }
    else
    {
        retval = ucp->readRegister(addr, nvalues, &sdp_data, 1);
    }
    ucp_reads++;
    if (retval == false)
    {
        ucp_read_fails++;
    }
    ucp_read_retries += ucp->getRetries();

    // if retval==false, no response from node
    if (retval == false)
    {
        if (current_design_name != "-")
        {
            LOG_F(INFO, "design_name '%s' not active anymore", current_design_name.c_str());
        }
        clear = true;
    }
    else
    {
        // node is active, check if regmap need to be read.
        string design_name;
        // Register can be read, now get mmap if empty.
        if (!mmap->empty())
        {
            // If the design name is changed clear the mmap.
            design_name = read_design_name();
            if (design_name != current_design_name)
            {
                LOG_F(INFO, "design_name changed '%s' not active anymore", current_design_name.c_str());
                clear = true;
            }
        }
        else
        {
            // node is active and mmap is empty, try to read new mmap from node
            uint32_t rom_version = (sdp_data & ROM_VERSION_MASK) >> ROM_VERSION_SHIFT;
            retval = read_reg_map(rom_version);

            if (!mmap->empty())
            {
                LOG_F(INFO, "new mmap");
                mmap->print_screen();
                design_name = read_design_name();
                LOG_F(INFO, "now active design_name = %s", design_name.c_str());
            }
        }

        current_design_name = design_name;
        // set current_image, 0 = factory-image, 1 = user-image, is set to -1 = no image available in clear_fw_values()
        // current_image = (current_design_name.find("minimal") != string::npos) ? 0 : 1;
        current_image = (current_design_name.find("station") != string::npos) ? 1 : 0;
    }
    if (clear)
    {
        clear_fw_values();
        mmap->clear();
    }
    return retval;
}

bool Periph_fpga::read_flash_protect(char *data)
{
    bool retval = true;
    uint32_t active = flash_active ? 1 : 0;
    memcpy(data, &active, sizeof(active));
    return retval;
}

bool Periph_fpga::write_flash_protect(const char *data)
{
    bool retval = false;
    uint32_t passphrase_protect = 0;
    uint32_t passphrase_unprotect = 0xBEDA221E;
    uint32_t protect;
    memcpy(&protect, data, sizeof(protect));

    if (protect == 1) { // protect
        flash_active = false;
        retval = Write("mm/0/REG_EPCS/0/unprotect", &passphrase_protect);
        LOG_F(INFO, "flash: enable write protect");
    }
    else { // unprotect
        flash_active = true;
        retval = Write("mm/0/REG_EPCS/0/unprotect", &passphrase_unprotect);
        LOG_F(INFO, "flash: disable write protect");
    }
    return retval;
}

bool Periph_fpga::read_flash_addr(char *data)
{
    memcpy(data, &flash_addr, sizeof(flash_addr));
    return true;
}

bool Periph_fpga::write_flash_addr(const char *data)
{
    memcpy(&flash_addr, data, sizeof(flash_addr));
    LOG_F(DEBUG, "flash: addr set to 0x%08x", flash_addr);
    return true;
}

bool Periph_fpga::read_flash_pages(char *data)
{
    bool retval = true;
    uint32_t *_ptr = (uint32_t *)data;

    uint32_t b0_hi = 1;
    uint32_t b0_lo = 0;
    uint32_t usedw;
    uint32_t pages_to_read = 256;

    LOG_F(DEBUG, "flash: read 256 pages, start addr= 0x%08x", flash_addr);
    for (uint i = 0; i < pages_to_read; i++)
    {
        retval &= Write("mm/0/REG_EPCS/0/addr", &flash_addr, true, false); // set read address
        retval &= Write("mm/0/REG_EPCS/0/rden", &b0_hi, true, false);      // set read_enable=1
        retval &= Write("mm/0/REG_EPCS/0/read_bit", &b0_hi, true, false);  // set read_bit=1 (triggers read)
        for (int j = 0; j < 100; j++)
        { // try max 100 times if fifo is filled, is needed to prefent a lock
            usedw = 0;
            retval &= Read("mm/0/REG_DPMM_CTRL/0/rd_usedw", &usedw, false); // read number of words in fifo
            if (usedw == Flash_page_size_words)
            {
                break;
            } // if 1 page in the FIFO continue
            LOG_F(DEBUG, "wait, start addr 0x%08x, rd_usedw=%d", flash_addr, usedw);
        }
        if (usedw < Flash_page_size_words)
        { // something is going wrong, after 100 polls still no data in the fifo
            LOG_F(ERROR, "read_flash_page, No data in fifo");
            return false;
        }
        retval &= Write("mm/0/REG_EPCS/0/rden", &b0_lo, true, false);                 // now one page in the fifo, write read_enable=0
        retval &= ReadFifo("mm/0/REG_DPMM_DATA/0/data", _ptr, Flash_page_size_words); // read one page from FIFO

        // increment source address and destination pointer for next read
        flash_addr += Flash_page_size_bytes;
        _ptr += Flash_page_size_words;
    }
    return retval;
}

bool Periph_fpga::write_flash_pages(const char *data)
{
    bool retval = true;
    uint32_t *_ptr = (uint32_t *)data;
    uint32_t b0_hi = 1;
    uint32_t pages_to_write = 256;

    LOG_F(DEBUG, "flash: write 256 pages, start addr= 0x%08x", flash_addr);
    for (uint i = 0; i < pages_to_write; i++)
    {
        if (wait_while_epcs_busy(chrono::milliseconds(100)) == true) {                     // check if writing done, wait max 100 msec
            retval &= Write("mm/0/REG_EPCS/0/addr", &flash_addr, true, false);             // set write address
            retval &= WriteFifo("mm/0/REG_MMDP_DATA/0/data", _ptr, Flash_page_size_words); // write one page to the FIFO
            retval &= Write("mm/0/REG_EPCS/0/write_bit", &b0_hi, true, false);             // set write_bit=1 (triggers write)
            this_thread::sleep_for(chrono::microseconds(420));                             // wait while writing page

            // increment destination address and source pointer for next write
            flash_addr += Flash_page_size_bytes;
            _ptr += Flash_page_size_words;
        }
        else {
            return false;
        }
    }
    return retval;
}

bool Periph_fpga::read_flash_erase_sector(char *data)
{
    bool retval = true;

    uint32_t *_ptr = (uint32_t *)data;
    uint32_t sector = flash_addr / Flash_sector_size_bytes;
    *_ptr = sector;

    return retval;
}

bool Periph_fpga::write_flash_erase_sector(const char *data)
{
    // We need to write any address in the target sector's address range to select that sector for erase.
    // We'll use the base (lowest) address of the sectors for this: sector 0 starts at 0x0, sector 1 starts
    // at 0x40000 etc.
    //
    // first 4 bytes in data holds sector to erase.

    bool retval = true;
    uint32_t sector;
    memcpy(&sector, data, sizeof(sector));         // copy sector from data
    flash_addr = sector * Flash_sector_size_bytes; // calculate start address of sector
    LOG_F(DEBUG, "flash: erase 1 sector=%d, addr= 0x%08x", sector, flash_addr);

    uint32_t b0_hi = 1;
    // erase sector in 4 steps
    // see: https://git.astron.nl/desp/upe_gear/-/blob/master/peripherals/pi_epcs.py  in function "write_erase_sector()"
    uint erase_steps = 4;
    uint32_t erase_step_offset = 0x10000;
    for (uint i = 0; i < erase_steps; i++)
    {
        uint32_t addr = flash_addr + i * erase_step_offset;
        if (wait_while_epcs_busy(chrono::milliseconds(5000)) == true) {  // check if erasing done, wait max 5000 msec.
            retval &= Write("mm/0/REG_EPCS/0/addr", &addr, true, false); // write start address of sector
            retval &= Write("mm/0/REG_EPCS/0/sector_erase", &b0_hi);     // write sector_erase=1 (triggers erase)
            this_thread::sleep_for(chrono::milliseconds(300));           // wait while erasing sector
        }
        else {
            return false;
        }
    }
    return retval;
}

/*
 * writing a epcs register takes some time (in the ms range),
 * while it is executing the request the busy flag is 1 an set to 0 if done.
 *   msec_timeout: maximum time in msec to wait for busy flag.
 *   return: true if epcs done, false on failure.
 */
bool Periph_fpga::wait_while_epcs_busy(const chrono::milliseconds msec_timeout)
{
    uint32_t data = 0;

    auto timeout = chrono::steady_clock::now() + msec_timeout;

    while (true)
    {
        if (Read("mm/0/REG_EPCS/0/busy", &data) == false) { return false; }
        if (data == 0) { return true; }
        if (chrono::steady_clock::now() > timeout) { break; }
    }
    LOG_F(WARNING, "wait_while_epcs_busy reached max wait time");
    return false; // still busy
}

bool Periph_fpga::read_boot_image(char *data)
{
    bool retval = true;
    int32_t *_ptr = (int32_t *)data;
    *_ptr = current_image;
    return retval;
}

// working code from upe_gear is used as example.
// https://git.astron.nl/desp/upe_gear/-/blob/master/peripherals/pi_remu.py
//
bool Periph_fpga::write_boot_image(const char *data)
{
    uint32_t FACT_MODE = 0;
    uint32_t USER_MODE = 1;
    uint32_t FACT_START_ADDR = 0;
    uint32_t USER_START_ADDR = 160 * 0x40000;

    // used for param
    uint32_t PARAM_START_ADDR = 4;
    uint32_t PARAM_MODE = 5;

    bool retval = true;
    int32_t *_ptr = (int32_t *)data;
    uint32_t mode;
    uint32_t start_addr;
    uint32_t activate = 1;

    // set fpga offline (not Online) for other commands,
    Online = false;

    if (*_ptr == 0)
    {
        mode = FACT_MODE;
        start_addr = FACT_START_ADDR;
        LOG_F(INFO, "reboot: restart firmware in factory image");
    }
    else if (*_ptr == 1)
    {
        mode = USER_MODE;
        start_addr = USER_START_ADDR;
        LOG_F(INFO, "reboot: restart firmware in user image");
    }
    else
    {
        LOG_F(ERROR, "reboot: not a valid image type");
        retval = false;
    }

    // write mode
    if (retval)
    {
        retval &= Write("mm/0/REG_REMU/0/param", &PARAM_MODE);
        retval &= Write("mm/0/REG_REMU/0/data_in", &mode);
        retval &= Write("mm/0/REG_REMU/0/write_param", &activate);
        if (retval)
        {
            retval = wait_while_remu_busy(1000); // 1000 is sleeptime in usec if busy
        }
    }
    // write start_adres
    if (retval)
    {
        retval &= Write("mm/0/REG_REMU/0/param", &PARAM_START_ADDR);
        retval &= Write("mm/0/REG_REMU/0/data_in", &start_addr, true, false);
        retval &= Write("mm/0/REG_REMU/0/write_param", &activate);
        if (retval)
        {
            retval = wait_while_remu_busy(1000); // 1000 is sleeptime in usec if busy
        }
    }

    // write reconfigure
    if (retval)
    {
        uint32_t sdp_data = 0xB007FAC7;
        retval = Write("mm/0/REG_REMU/0/reconfigure", &sdp_data, false);
        LOG_F(INFO, "reboot: rebooting now");
    }

    // clear all, regmap will be read again
    clear_fw_values();
    mmap->clear();
    return retval;
}

/*
 * writing a remu register takes some time (in the ms range),
 * while it is executing the request the busy flag is 1 an set to 0 if done.
 *   sleeptime: time in usec to sleep if busy
 */
bool Periph_fpga::wait_while_remu_busy(int64_t sleeptime)
{
    uint32_t data;
    bool retval;
    for (int i = 0; i < 100; i++)
    {
        data = 1;
        retval = Read("mm/0/REG_REMU/0/busy", &data);
        if (!retval)
            break;
        if (data == 0)
            break;
        this_thread::sleep_for(chrono::microseconds(sleeptime));
    }
    if (data != 0)
    {
        retval = false; // still busy
    }
    return retval;
}

string Periph_fpga::read_design_name()
{
    uint32_t data[14];
    memset(data, 0, sizeof(data));
    if (Read("mm/0/PIO_SYSTEM_INFO/0/design_name", data))
    {
        char *str_ptr = (char *)data;
        string name = string(str_ptr);
        return name;
    }
    return "? (error)";
}

string Periph_fpga::read_design_note()
{
    uint32_t data[14];
    memset(data, 0, sizeof(data));
    if (Read("mm/0/PIO_SYSTEM_INFO/0/design_note", data))
    {
        char *str_ptr = (char *)data;
        string note = string(str_ptr);
        return note;
    }
    return "? (error)";
}

bool Periph_fpga::read_hardware_version(char *data)
{
    bool retval = true;
    string version;
    uint32_t sdp_data;
    retval = Read("mm/0/PIO_SYSTEM_INFO/0/info_hw_version", &sdp_data);
    if (retval)
    {
        uint32_t hw_version_nr = sdp_data;
        if (hw_version_nr == 1)
        {
            version = "UniBoard2b";
        }
        else if (hw_version_nr == 2)
        {
            version = "UniBoard2c";
        }
        else
        {
            version = "Unknown";
        }
        strcpy(data, version.c_str());
    }
    return retval;
}

bool Periph_fpga::read_firmware_version(char *data)
{
    bool retval = true;
    string design_name;
    string date;
    string time;
    string revision;
    string version;
    uint32_t sdp_data[14];

    memset(sdp_data, 0, sizeof(data));
    retval &= Read("mm/0/PIO_SYSTEM_INFO/0/design_name", sdp_data);
    if (retval)
    {
        char *str_ptr = (char *)sdp_data;
        design_name = string(str_ptr);
    }

    memset(sdp_data, 0, sizeof(sdp_data));
    retval &= Read("mm/0/PIO_SYSTEM_INFO/0/stamp_date", sdp_data);
    if (retval)
    {
        date = to_string(sdp_data[0]);
    }
    memset(sdp_data, 0, sizeof(sdp_data));
    retval &= Read("mm/0/PIO_SYSTEM_INFO/0/stamp_time", sdp_data);
    if (retval)
    {
        time = to_string(sdp_data[0]);
        if (time.size() == 5)
        {
            time = '0' + time;
        }
    }
    memset(sdp_data, 0, sizeof(sdp_data));
    retval &= Read("mm/0/PIO_SYSTEM_INFO/0/stamp_commit", sdp_data);
    if (retval)
    {
        char rev[12];
        memcpy(rev, sdp_data, 12);
        revision = rev;
        version = date.substr(0, 4) + "-" + date.substr(4, 2) + "-" + date.substr(6, 2) + "T" + time.substr(0, 2) + "." + time.substr(2, 2) + "." + time.substr(4, 2) + "_" + revision + "_" + design_name;
    }
    else
    {
        LOG_F(ERROR, "read_firmware_version failed");
        version = "?";
    }
    strcpy(data, version.c_str());
    return retval;
}

bool Periph_fpga::read_stamps(char *data)
{
    uint32_t sdp_data;
    bool retval = Read("mm/0/PIO_SYSTEM_INFO/0/stamp_date", &sdp_data);

    return retval;
}

bool Periph_fpga::read_global_node_index(char *data)
{
    bool retval = true;

    uint32_t *_ptr = (uint32_t *)data;
    *_ptr = GlobalNr;
    return retval;
}

bool Periph_fpga::read_fpga_temperature(char *data)
{
    bool retval = true;
    uint32_t sdp_data;
    retval = Read("mm/0/REG_FPGA_TEMP_SENS/0/temp", &sdp_data);
    if (retval == true)
    {
        // ADC to engineering format.
        // see the constants: https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug_alttemp_sense.pdf
        // page 10
        double temp = ((693. * (double)sdp_data) / 1024.) - 265;
        memcpy(data, &temp, sizeof(temp));
    }
    return retval;
}

bool Periph_fpga::read_fpga_scrap(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;
    retval = Read("mm/0/RAM_SCRAP/0/data", ptr);
    return retval;
}

bool Periph_fpga::write_fpga_scrap(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    uint32_t nvalues = mmap->getSpan("mm/0/RAM_SCRAP/0/data");
    LOG_F(DEBUG, "Periph_fpga::write_fpga_scrap %d values", nvalues);
    string val_str = "";
    for (uint i = 0; i < nvalues; i++)
    {
        val_str += to_string(_ptr[i]) + ", ";
    }
    LOG_F(DEBUG, "%s", val_str.c_str());
    bool retval = Write("mm/0/RAM_SCRAP/0/data", _ptr);
    return retval;
}

bool Periph_fpga::read_sst_offload_weighted_subbands(char *data)
{
    uint32_t sdp_data;
    bool retval = Read("mm/0/REG_DP_SELECTOR/0/input_select", &sdp_data);

    uint8_t sel = (sdp_data == 1) ? 0 : 1;
    memcpy(data, &sel, sizeof(sel));

    return retval;
}

bool Periph_fpga::write_sst_offload_weighted_subbands(const char *data)
{
    uint32_t sdp_data;
    sdp_data = (data[0] == 0) ? 1 : 0;

    bool retval = Write("mm/0/REG_DP_SELECTOR/0/input_select", &sdp_data);
    return retval;
}

bool Periph_fpga::write_sst_offload_enable(const char *data)
{
    uint32_t sdp_data;
    sdp_data = (uint32_t)data[0];
    return Write("mm/0/REG_STAT_ENABLE_SST/0/enable", &sdp_data);
}

bool Periph_fpga::write_bst_offload_enable(const char *data)
{
    bool *_ptr = (bool *)data;
    bool retval = true;
    string regname;
    uint32_t sdp_data;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        regname = "mm/" + to_string(i) + "/REG_STAT_ENABLE_BST/0/enable";
        sdp_data = (uint32_t)_ptr[i];
        retval &= Write(regname, &sdp_data);
    }
    return retval;
}

bool Periph_fpga::read_bst_offload_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        for (uint i = 0; i < nBeamsets; i++)
        {
            sdp_data = 0;
            string regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_BST_OFFLOAD/0/nof_sop";
            retval &= Read(regname, &sdp_data);
            bst_offload_nof_packets[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bst_offload_nof_packets_R");
    memcpy(data, bst_offload_nof_packets, n_bytes);
    return retval;
}

bool Periph_fpga::read_bst_offload_bsn(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        for (uint i = 0; i < nBeamsets; i++)
        {
            string regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_BST_OFFLOAD/0/bsn_at_sync";
            retval = Read(regname, sdp_data);
            bst_offload_bsn[i] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bst_offload_bsn_R");
    memcpy(data, bst_offload_bsn, n_bytes);
    return retval;
}

bool Periph_fpga::read_bst_offload_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        for (uint i = 0; i < nBeamsets; i++)
        {
            sdp_data = 0;
            string regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_BST_OFFLOAD/0/nof_valid";
            retval = Read(regname, &sdp_data);
            bst_offload_nof_valid[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bst_offload_nof_valid_R");
    memcpy(data, bst_offload_nof_valid, n_bytes);
    return retval;
}

bool Periph_fpga::read_sst_offload_nof_packets(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data = 0;
        retval = Read("mm/0/REG_BSN_MONITOR_V2_SST_OFFLOAD/0/nof_sop", &sdp_data);
        sst_offload_nof_packets = (int32_t)sdp_data;
        return retval;
    }
    memcpy(data, &sst_offload_nof_packets, sizeof(sst_offload_nof_packets));
    return retval;
}

bool Periph_fpga::read_sst_offload_nof_valid(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data = 0;
        retval = Read("mm/0/REG_BSN_MONITOR_V2_SST_OFFLOAD/0/nof_valid", &sdp_data);
        sst_offload_nof_valid = (int32_t)sdp_data;
        return retval;
    }
    memcpy(data, &sst_offload_nof_valid, sizeof(sst_offload_nof_valid));
    return retval;
}

bool Periph_fpga::read_sst_offload_bsn(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        retval = Read("mm/0/REG_BSN_MONITOR_V2_SST_OFFLOAD/0/bsn_at_sync", sdp_data);
        sst_offload_bsn = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        return retval;
    }
    memcpy(data, &sst_offload_bsn, sizeof(sst_offload_bsn));
    return retval;
}

bool Periph_fpga::write_beamlet_output_enable(const char *data)
{
    bool *_ptr = (bool *)data;
    bool retval = true;
    string regname;
    uint32_t sdp_data;

    sdp_data = 9000;
    regname = "mm/0/REG_NW_10GBE_MAC/0/tx_frame_maxlength";
    Write(regname, &sdp_data);
    regname = "mm/0/REG_NW_10GBE_MAC/0/rx_frame_maxlength";
    Write(regname, &sdp_data);

    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        regname = "mm/" + to_string(i) + "/REG_DP_XONOFF/0/enable_stream";
        sdp_data = (uint32_t)_ptr[i];
        retval &= Write(regname, &sdp_data);
    }
    return retval;
}

bool Periph_fpga::read_beamlet_output_scale(char *data)
{
    bool retval = true;
    string regname;
    uint32_t W_beamlet_scale = 16;
    double scale_factor = 1.0 / pow(2.0, double(W_beamlet_scale - 1));
    uint32_t sdp_data;
    char *_ptr = data;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        regname = "mm/" + to_string(i) + "/REG_BF_SCALE/0/scale";
        retval &= Read(regname, &sdp_data);
        double scale = (double(sdp_data) * scale_factor);
        memcpy(_ptr, &scale, sizeof(scale));
        _ptr += sizeof(scale);
    }
    return retval;
}

bool Periph_fpga::write_beamlet_output_scale(const char *data)
{
    double *wdata = new double[nBeamsets];
    memcpy(wdata, data, nBeamsets * sizeof(double));
    bool retval = true;
    uint32_t W_beamlet_scale = 16;
    double scale_factor = pow(2.0, (double)(W_beamlet_scale - 1));
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        string regname = "mm/" + to_string(i) + "/REG_BF_SCALE/0/scale";
        uint32_t sdp_data = (uint32_t)(wdata[i] * scale_factor);
        retval &= Write(regname, &sdp_data);
    }
    my_delete_arr(wdata);
    return retval;
}

bool Periph_fpga::read_beamlet_output_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        for (uint i = 0; i < nBeamsets; i++)
        {
            sdp_data = 0;
            string regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_BEAMLET_OUTPUT/0/nof_sop";
            retval &= Read(regname, &sdp_data);
            beamlet_output_nof_packets[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_nof_packets_R");
    memcpy(data, beamlet_output_nof_packets, n_bytes);
    return retval;
}

bool Periph_fpga::read_beamlet_output_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        for (uint i = 0; i < nBeamsets; i++)
        {
            sdp_data = 0;
            string regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_BEAMLET_OUTPUT/0/nof_valid";
            retval = Read(regname, &sdp_data);
            beamlet_output_nof_valid[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_nof_valid_R");
    memcpy(data, beamlet_output_nof_valid, n_bytes);
    return retval;
}

bool Periph_fpga::read_beamlet_output_bsn(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        for (uint i = 0; i < nBeamsets; i++)
        {
            string regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_BEAMLET_OUTPUT/0/bsn_at_sync";
            retval = Read(regname, sdp_data);
            beamlet_output_bsn[i] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_bsn_R");
    memcpy(data, beamlet_output_bsn, n_bytes);
    return retval;
}

bool Periph_fpga::read_beamlet_output_ready(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        for (uint i = 0; i < nBeamsets; i++)
        {
            string regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_BEAMLET_OUTPUT/0/ready_stable";
            retval = Read(regname, &sdp_data);
            beamlet_output_ready[i] = (bool)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_ready_R");
    memcpy(data, beamlet_output_ready, n_bytes);
    return retval;
}

bool Periph_fpga::read_beamlet_output_xon(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        for (uint i = 0; i < nBeamsets; i++)
        {
            string regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_BEAMLET_OUTPUT/0/xon_stable";
            retval = Read(regname, &sdp_data);
            beamlet_output_xon[i] = (bool)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_xon_R");
    memcpy(data, beamlet_output_xon, n_bytes);
    return retval;
}

bool Periph_fpga::read_beamlet_output_10gbe_tx_nof_frames(char *data)
{
    bool retval = true;
    uint32_t sdp_data[2] = {0};

    // Read number of send packets greater than 1519 octets
    retval = Read("mm/0/REG_NW_10GBE_MAC/0/tx_stats_etherstats_pkts1519toxoctets", sdp_data, false);
    uint64_t nof_frames = ((uint64_t)sdp_data[0] << 32) + sdp_data[1];
    beamlet_output_10gbe_tx_nof_frames += nof_frames;
    memcpy(data, &beamlet_output_10gbe_tx_nof_frames, sizeof(uint64_t));
    return retval;
}

bool Periph_fpga::read_beamlet_output_10gbe_rx_nof_frames(char *data)
{
    bool retval = true;
    uint32_t sdp_data[2] = {0};

    // Read number of send packets greater than 1519 octets
    retval = Read("mm/0/REG_NW_10GBE_MAC/0/rx_stats_etherstats_pkts1519toxoctets", sdp_data, false);
    uint64_t nof_frames = ((uint64_t)sdp_data[0] << 32) + sdp_data[1];
    beamlet_output_10gbe_rx_nof_frames += nof_frames;
    memcpy(data, &beamlet_output_10gbe_rx_nof_frames, sizeof(uint64_t));
    return retval;
}

bool Periph_fpga::write_xst_offload_enable(const char *data)
{
    uint32_t sdp_data;
    sdp_data = (uint32_t)data[0];
    return Write("mm/0/REG_STAT_ENABLE_XST/0/enable", &sdp_data);
}

bool Periph_fpga::write_xst_offload_nof_crosslets(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    return Write("mm/0/REG_NOF_CROSSLETS/0/nof_crosslets", _ptr);
}

bool Periph_fpga::read_xst_offload_nof_packets(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data = 0;
        retval = Read("mm/0/REG_BSN_MONITOR_V2_XST_OFFLOAD/0/nof_sop", &sdp_data);
        xst_offload_nof_packets = (int32_t)sdp_data;
        return retval;
    }

    memcpy(data, &xst_offload_nof_packets, sizeof(xst_offload_nof_packets));
    return retval;
}

bool Periph_fpga::read_xst_offload_nof_valid(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data = 0;
        retval = Read("mm/0/REG_BSN_MONITOR_V2_XST_OFFLOAD/0/nof_valid", &sdp_data);
        xst_offload_nof_valid = (int32_t)sdp_data;
        return retval;
    }
    memcpy(data, &xst_offload_nof_valid, sizeof(xst_offload_nof_valid));
    return retval;
}

bool Periph_fpga::read_xst_offload_bsn(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        retval = Read("mm/0/REG_BSN_MONITOR_V2_XST_OFFLOAD/0/bsn_at_sync", sdp_data);
        xst_offload_bsn = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        return retval;
    }
    memcpy(data, &xst_offload_bsn, sizeof(xst_offload_bsn));
    return retval;
}

bool Periph_fpga::write_beamlet_output_nof_destinations(const char *data)
{
    bool retval = true;
    uint32_t sdp_data;
    for (uint set = 0; set < nBeamsets; set++) {
        sdp_data = (uint32_t)data[set];
        retval &=  Write("mm/" + to_string(set) + "/REG_BDO_DESTINATIONS/0/nof_destinations", &sdp_data);
    }
    return retval;
}

bool Periph_fpga::read_beamlet_output_nof_destinations(char *data)
{
    bool retval = true;
    uint32_t sdp_data = 0;
    uint8_t nof_destinations[2] = {0};
    for (uint set = 0; set < nBeamsets; set++) {
        retval &= Read("mm/" + to_string(set) + "/REG_BDO_DESTINATIONS/0/nof_destinations", &sdp_data);
        nof_destinations[set] = (uint8_t)sdp_data;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_nof_destinations_R");
    memcpy(data, nof_destinations, n_bytes);
    return retval;
}

bool Periph_fpga::read_beamlet_output_nof_destinations_act(char *data)
{
    bool retval = true;
    uint32_t sdp_data = 0;
    uint8_t nof_destinations_act[2] = {0};
    for (uint set = 0; set < nBeamsets; set++) {
        retval &= Read("mm/" + to_string(set) + "/REG_BDO_DESTINATIONS/0/nof_destinations_act", &sdp_data);
        nof_destinations_act[set] = (uint8_t)sdp_data;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_nof_destinations_act_R");
    memcpy(data, nof_destinations_act, n_bytes);
    return retval;
}

bool Periph_fpga::read_beamlet_output_nof_destinations_max(char *data)
{
    bool retval = true;
    uint32_t sdp_data = 0;
    uint8_t nof_destinations_max[2] = {0};
    for (uint set = 0; set < nBeamsets; set++) {
        retval &= Read("mm/" + to_string(set) + "/REG_BDO_DESTINATIONS/0/nof_destinations_max", &sdp_data);
        nof_destinations_max[set] = (uint8_t)sdp_data;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_nof_destinations_max_R");
    memcpy(data, nof_destinations_max, n_bytes);
    return retval;
}

bool Periph_fpga::read_beamlet_output_nof_blocks_per_packet(char *data)
{
    bool retval = true;
    uint32_t sdp_data = 0;
    uint8_t nof_blocks_per_packet[2] = {0};
    for (uint set = 0; set < nBeamsets; set++) {
        retval &= Read("mm/" + to_string(set) + "/REG_BDO_DESTINATIONS/0/nof_blocks_per_packet", &sdp_data);
        nof_blocks_per_packet[set] = (uint8_t)sdp_data;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_beamlet_output_nof_blocks_per_packet_R");
    memcpy(data, nof_blocks_per_packet, n_bytes);
    return retval;
}


bool Periph_fpga::read_eth_mac(char *data, const string &port_name, const string &field_name)
{
    bool retval = true;

    uint32_t field_size = 2;  // number of words per field
    string regname = "mm/0/" + port_name + "/0/" + field_name;
    uint32_t n_periph = mmap->getNPeripherals(regname);
    uint32_t n_fields = mmap->getNFields(regname);
    n_periph = min(n_periph, nBeamsets);

    uint32_t sdp_data[64];
    stringstream mac_ss;
    string mac_str;
    for (uint i = 0; i < n_periph; i++)
    {
        // read nof_destinations_act
        memset(sdp_data, 0, sizeof(sdp_data));
        regname = "mm/" + to_string(i) + "/" + port_name + "/0/" + field_name;
        retval &= Read(regname, sdp_data);

        for (uint field_nr = 0; field_nr < n_fields; field_nr++) {
            uint64_t mac = (uint64_t)sdp_data[field_nr*field_size+1] << 32 | sdp_data[field_nr*field_size];

            mac_ss.clear();
            mac_ss.str("");
            for (int j = 5; j >= 0; j--)
            {
                mac_ss << setfill('0') << setw(2) << right << hex << ((mac >> (j * 8)) & 0xff);
                if (j > 0)
                {
                    mac_ss << ":";
                }
            }
            mac_str = mac_ss.str();
            strcpy(&(data[(i * n_fields + field_nr) * SIZE1STRING]), mac_str.c_str());
        }
    }
    return retval;
}

bool Periph_fpga::write_eth_mac(const char *data, const string &port_name, const string &field_name, uint32_t max_fields)
{
    bool retval = true;

    string regname = "mm/0/" + port_name + "/0/" + field_name;
    uint32_t n_periph = mmap->getNPeripherals(regname);
    uint32_t n_fields = mmap->getNFields(regname);
    n_periph = min(n_periph, nBeamsets);
    n_fields = min(n_fields, max_fields);
    uint32_t field_size = 2;  // number of words per field

    char sep;
    uint32_t m0, m1, m2, m3, m4, m5;
    uint32_t mac[64] = {0};
    string ds;
    stringstream ss;
    for (uint i = 0; i < n_periph; i++)
    {
        regname = "mm/" + to_string(i) + "/" + port_name + "/0/" + field_name;
        // if we get 1 value in data, and need to write more, use the first one again.
        for (uint field_nr = 0; field_nr < n_fields; field_nr++) {
            ds = &(data[(i * n_fields + field_nr) * SIZE1STRING]);
            LOG_F(DEBUG, "%s", ds.c_str());
            ss.clear();
            ss.str("");
            ss << ds;
            ss >> setbase(16) >> m0 >> sep >> m1 >> sep >> m2 >> sep >> m3 >> sep >> m4 >> sep >> m5;
            if (ss.fail() || ss.bad())
            {
                LOG_F(ERROR, "parse error in write_eth_mac (%s)", ds.c_str());
                return false;
            }
            mac[field_nr*field_size+1] = (m0 << 8) + (m1 << 0);
            mac[field_nr*field_size] = (m2 << 24) + (m3 << 16) + (m4 << 8) + m5;
        }
        retval &= Write(regname, mac);
    }
    return retval;
}

bool Periph_fpga::read_ip_address(char *data, const string &port_name, const string &field_name)
{
    bool retval = true;
    string regname = "mm/0/" + port_name + "/0/" + field_name;
    uint32_t n_periph = mmap->getNPeripherals(regname);
    uint32_t n_fields = mmap->getNFields(regname);
    n_periph = min(n_periph, nBeamsets);

    uint32_t sdp_data[32];
    stringstream ip_ss;
    string ip_str;
    for (uint i = 0; i < n_periph; i++)
    {
        regname = "mm/" + to_string(i) + "/" + port_name + "/0/" + field_name;
        retval &= Read(regname, sdp_data);

        for (uint field_nr = 0; field_nr < n_fields; field_nr++) {
            ip_ss.clear();
            ip_ss.str("");
            for (int j = 3; j >= 0; j--)
            {
                ip_ss << dec << ((sdp_data[field_nr] >> (j * 8)) & 0xff);
                if (j > 0)
                {
                    ip_ss << ".";
                }
            }
            ip_str = ip_ss.str();
            strcpy(&(data[(i * n_fields + field_nr) * SIZE1STRING]), ip_str.c_str());
        }
    }
    return retval;
}

bool Periph_fpga::write_ip_address(const char *data, const string &port_name, const string &field_name, uint32_t max_fields)
{
    bool retval = true;
    string regname = "mm/0/" + port_name + "/0/" + field_name;
    uint32_t n_periph = mmap->getNPeripherals(regname);
    uint32_t n_fields = mmap->getNFields(regname);
    n_periph = min(n_periph, nBeamsets);
    n_fields = min(n_fields, max_fields);

    char sep;
    uint32_t ip_nr;
    uint32_t ip[32] = {0};
    uint ip0, ip1, ip2, ip3;
    string ds;
    stringstream ss;
    for (uint i = 0; i < n_periph; i++)
    {
        regname = "mm/" + to_string(i) + "/" + port_name + "/0/" + field_name;
        // if we get 1 value in data, and need to write more, use the first one again.
        for (uint field_nr = 0; field_nr < n_fields; field_nr++) {
            ds = &(data[(i * n_fields + field_nr) * SIZE1STRING]);
            LOG_F(DEBUG, "%s", ds.c_str());
            ss.clear();
            ss.str("");
            ss << ds;
            ss >> setbase(10) >> ip0;
            ss >> sep;
            ss >> setbase(10) >> ip1;
            ss >> sep;
            ss >> setbase(10) >> ip2;
            ss >> sep;
            ss >> setbase(10) >> ip3;
            if (ss.fail() || ss.bad())
            {
                LOG_F(ERROR, "parse error in write_ip_address (%s)", ds.c_str());
                return false;
            }
            ip_nr = (ip0 & 0xff) << 24;
            ip_nr += (ip1 & 0xff) << 16;
            ip_nr += (ip2 & 0xff) << 8;
            ip_nr += ip3 & 0xff;
            ip[field_nr] = ip_nr;
        }
        retval = Write(regname, ip);
    }
    return retval;
}

bool Periph_fpga::read_udp_port(char *data, const string &port_name, const string &field_name)
{
    uint16_t *_ptr = (uint16_t *)data;
    bool retval = true;

    string regname = "mm/0/" + port_name + "/0/" + field_name;
    uint32_t n_periph = mmap->getNPeripherals(regname);
    uint32_t n_fields = mmap->getNFields(regname);
    n_periph = min(n_periph, nBeamsets);

    uint32_t sdp_data[32] = {0};
    for (uint i = 0; i < n_periph; i++)
    {
        regname = "mm/" + to_string(i) + "/" + port_name + "/0/" + field_name;
        retval &= Read(regname, sdp_data);
        for (uint field_nr = 0; field_nr < n_fields; field_nr++) {
            *_ptr = (uint16_t)sdp_data[field_nr];
            _ptr++;
        }
    }
    return retval;
}

bool Periph_fpga::write_udp_port(const char *data, const string &port_name, const string &field_name, uint32_t max_fields)
{
    uint16_t *_ptr = (uint16_t *)data;
    bool retval = true;

    string regname = "mm/0/" + port_name + "/0/" + field_name;
    uint32_t n_periph = mmap->getNPeripherals(regname);
    uint32_t n_fields = mmap->getNFields(regname);
    n_periph = min(n_periph, nBeamsets);
    n_fields = min(n_fields, max_fields);

    uint32_t sdp_data[32];
    for (uint i = 0; i < n_periph; i++)
    {
        // if we get 1 value in data, and need to write more, use the first one again.
        memset(sdp_data, 0, sizeof(sdp_data));
        for (uint field_nr = 0; field_nr < n_fields; field_nr++) {
            sdp_data[field_nr] = (uint32_t)(_ptr[i*n_fields+field_nr]);
        }
        regname = "mm/" + to_string(i) + "/" + port_name + "/0/" + field_name;
        retval &= Write(regname, sdp_data);
    }
    return retval;
}

bool Periph_fpga::read_xst_start_time(char *data)
{
    bool retval = true;

    memcpy(data, &xst_start_time, sizeof(xst_start_time));
    return retval;
}

bool Periph_fpga::write_xst_start_time(const char *data)
{
    bool retval = true;
    memcpy(&xst_start_time, data, sizeof(xst_start_time));
    return retval;
}

bool Periph_fpga::read_xst_processing_enable(char *data)
{
    bool retval = true;

    uint32_t sdp_data = 0;
    retval = Read("mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/mon_output_enable", &sdp_data);

    bool *_ptr = (bool *)data;
    *_ptr = (bool)sdp_data;
    return retval;
}

bool Periph_fpga::write_xst_processing_enable(const char *data)
{
    bool retval = true;
    uint32_t sdp_data[2];
    bool processing_enable = (bool)data[0];

    // disable xst_processing, if processing_enable is true, it also need too be false first
    memset(sdp_data, 0, sizeof(sdp_data));
    retval &= Write("mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/ctrl_enable", sdp_data);

    // enable xst_processing again if processing_enable is true
    if (processing_enable == true)
    {
        // minimal time_delta for caluclating the bsn latency is 2 seconds.
        double min_time_delta = 2.0;

        // calculate xst_start_bsn based on requested xst_start_time.
        uint64_t xst_start_bsn = (uint64_t)(xst_start_time * C_F_adc / C_N_fft);

        // get bsn and add latency
        memset(sdp_data, 0, sizeof(sdp_data));
        retval &= Read("mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/mon_input_bsn_at_sync", sdp_data);
        if (retval == true)
        {
            uint64_t start_bsn = (((uint64_t)sdp_data[1] << 32) + sdp_data[0]);
            LOG_F(INFO, "xsub bsn=%lu", (long)start_bsn);
            start_bsn = start_bsn + (uint64_t)(min_time_delta * C_F_adc / C_N_fft);
            // if requested xst_start_bsn > start_bsn use it.
            if (xst_start_bsn > start_bsn) start_bsn = xst_start_bsn;
            xst_start_time = (double)start_bsn / (C_F_adc / C_N_fft);
            LOG_F(INFO, "new xsub bsn=%lu", (long)start_bsn);
            sdp_data[0] = (uint32_t)(start_bsn & 0xffffffff);
            sdp_data[1] = (uint32_t)((start_bsn >> 32) & 0xffffffff);
            // write sheduled bsn
            retval &= Write("mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/ctrl_start_bsn", sdp_data);
            sdp_data[0] = 1;
            // write ctrl_enable = 1
            retval &= Write("mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/ctrl_enable", sdp_data);
        }
    }
    return retval;
}

bool Periph_fpga::read_xst_integration_interval(char *data)
{
    bool retval = true;

    uint32_t sdp_data;
    retval = Read("mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/ctrl_interval_size", &sdp_data);
    double interval = (double)sdp_data * C_T_adc;
    memcpy(data, &interval, sizeof(interval));
    return retval;
}

bool Periph_fpga::write_xst_integration_interval(const char *data)
{
    double interval;
    memcpy(&interval, data, sizeof(interval));
    uint32_t sdp_data = (uint32_t)round(interval * C_F_adc);
    return Write("mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/ctrl_interval_size", &sdp_data);
}

bool Periph_fpga::read_xst_subband_select(char *data)
{
    bool retval = true;

    uint32_t sdp_data[16];
    memset(sdp_data, 0, sizeof(sdp_data));
    retval &= Read("mm/0/REG_CROSSLETS_INFO/0/step", sdp_data);
    retval &= Read("mm/0/REG_CROSSLETS_INFO/0/offset", &(sdp_data[1]));

    uint32_t *_ptr = (uint32_t *)data;
    for (uint i = 0; i < (C_N_step + C_N_crosslets_max); i++)
    {
        *_ptr = sdp_data[i];
        _ptr++;
    }
    return retval;
}

bool Periph_fpga::read_xst_input_sync_at_bsn(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        memset(sdp_data, 0, sizeof(sdp_data));
        string regname;
        regname = "mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/mon_input_bsn_at_sync";
        retval = Read(regname, sdp_data);
        xst_input_bsn_at_sync = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        return retval;
    }

    memcpy(data, &xst_input_bsn_at_sync, sizeof(xst_input_bsn_at_sync));
    return retval;
}

bool Periph_fpga::read_xst_output_sync_bsn(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        memset(sdp_data, 0, sizeof(sdp_data));
        string regname;
        regname = "mm/0/REG_BSN_SYNC_SCHEDULER_XSUB/0/mon_output_sync_bsn";
        retval = Read(regname, sdp_data);
        xst_output_sync_bsn = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        return retval;
    }

    memcpy(data, &xst_output_sync_bsn, sizeof(xst_output_sync_bsn));
    return retval;
}

bool Periph_fpga::write_xst_subband_select(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = true;

    retval &= Write("mm/0/REG_CROSSLETS_INFO/0/offset", &(_ptr[1]));
    retval &= Write("mm/0/REG_CROSSLETS_INFO/0/step", &(_ptr[0]));
    return retval;
}

bool Periph_fpga::write_processing_enable(const char *data)
{
    uint32_t sdp_data;
    bool processing_enable = (bool)data[0];
    if (processing_enable)
    {
        LOG_F(INFO, "set run_processing_enable flag");
        // schedule a procecess enable sequence, it will be done in the first monitor run.
        run_processing_enable = true;
    }
    LOG_F(INFO, "turn off processing");
    sdp_data = 0;
    if (Write("mm/0/REG_BSN_SOURCE_V2/0/dp_on", &sdp_data) == false)
    {
        LOG_F(ERROR, "Write error (mm/0/REG_BSN_SOURCE_V2/0/dp_on) 0");
        return false;
    }
    return true;
}

// This function is called from the monitor only, to be sure it is executed in the same second.
bool Periph_fpga::processing_enable(void)
{
    // write REG_BSN_SOURCE_V2.dp_on = 0
    // read  PIO_JESD_CTRL.disable --> jesd_ctrl_disable
    // write PIO_JESD_CTRL.reset = 1 and PIO_JESD_CTRL.disable = jesd_ctrl_disable
    // write PIO_JESD_CTRL.reset = 0 and PIO_JESD_CTRL.disable = jesd_ctrl_disable
    // write REG_BSN_SOURCE_V2.bsn_init [calc val for next sec]
    // write REG_BSN_SOURCE_V2.bsn_time_offset [calc val if odd]
    // write REG_BSN_SOURCE_V2.dp_on_pps = 1
    // write REG_BSN_SOURCE_V2.dp_on = 1

    uint32_t sdp_data[2];
    uint32_t jesd_ctrl_disable = 0;

    // for start second, bsn and offset the start time from the monitor function is used
    uint32_t start_second = monitor_start_time.tv_sec + 1;
    uint64_t bsn_init = (uint64_t)ceil((double)start_second * C_F_adc / C_N_fft);
    uint32_t offset = (start_second % 2 == 0) ? 0 : 512;

    // turn off processing is done in write_processing_enable()

    LOG_F(INFO, "reset jesd");
    // reset jesd
    if (Read("mm/0/PIO_JESD_CTRL/0/disable", &jesd_ctrl_disable) == false)
    {
        LOG_F(ERROR, "Write error (mm/0/PIO_JESD_CTRL/0/disable)");
        return false;
    }
    sdp_data[0] = shift_mask("mm/0/PIO_JESD_CTRL/0/reset", 1) | shift_mask("mm/0/PIO_JESD_CTRL/0/disable", jesd_ctrl_disable);
    if (Write("mm/0/PIO_JESD_CTRL/0/disable", sdp_data, true, false) == false)
    {
        LOG_F(ERROR, "Write error (mm/0/PIO_JESD_CTRL/0/reset)");
        return false;
    }
    this_thread::sleep_for(chrono::microseconds(1));
    sdp_data[0] = shift_mask("mm/0/PIO_JESD_CTRL/0/reset", 0) | shift_mask("mm/0/PIO_JESD_CTRL/0/disable", jesd_ctrl_disable);
    if (Write("mm/0/PIO_JESD_CTRL/0/disable", sdp_data, true, false) == false)
    {
        LOG_F(ERROR, "Write error (mm/0/PIO_JESD_CTRL/0/reset)");
        return false;
    }

    LOG_F(INFO, "turn on processing");
    // this function is called from the monitor, so monitor_start_time can be used for tv_sec
    sdp_data[0] = (uint32_t)(bsn_init & 0xffffffff);
    sdp_data[1] = (uint32_t)((bsn_init >> 32) & 0xffffffff);
    if (Write("mm/0/REG_BSN_SOURCE_V2/0/bsn_init", sdp_data) == false)
    {
        LOG_F(ERROR, "Write error (mm/0/REG_BSN_SOURCE_V2/0/bsn_init)");
        return false;
    }
    // set bsn time offset, 0 for even seconds, 512 for odd seconds
    sdp_data[0] = offset;
    if (Write("mm/0/REG_BSN_SOURCE_V2/0/bsn_time_offset", sdp_data) == false)
    {
        LOG_F(ERROR, "Write error (mm/0/REG_BSN_SOURCE_V2/0/bsn_time_offset)");
        return false;
    }
    sdp_data[0] = shift_mask("mm/0/REG_BSN_SOURCE_V2/0/dp_on", 1) | shift_mask("mm/0/REG_BSN_SOURCE_V2/0/dp_on_pps", 1);
    if (Write("mm/0/REG_BSN_SOURCE_V2/0/dp_on", sdp_data, true, false) == false)
    {
        LOG_F(ERROR, "Write error (mm/0/REG_BSN_SOURCE_V2/0/dp_on) 1");
        return false;
    }
    run_processing_enable = false;
    return true;
}

bool Periph_fpga::write_sdp_info_station_id(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = Write("mm/0/REG_SDP_INFO/0/station_id", _ptr);
    return retval;
}

bool Periph_fpga::write_sdp_info_antenna_field_index(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = Write("mm/0/REG_SDP_INFO/0/antenna_field_index", _ptr);
    return retval;
}

bool Periph_fpga::write_sdp_info_observation_id(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = Write("mm/0/REG_SDP_INFO/0/observation_id", _ptr);
    return retval;
}

bool Periph_fpga::write_sdp_info_nyquist_sampling_zone_index(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = Write("mm/0/REG_SDP_INFO/0/nyquist_zone_index", _ptr);
    return retval;
}

bool Periph_fpga::write_sdp_info_antenna_band_index(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = Write("mm/0/REG_SDP_INFO/0/antenna_band_index", _ptr);
    return retval;
}

// Waveform generator functions "_wg_"
/*
When FPGA_wg_enable_RW is set False, then disable the WG via mode = c_mode_off = 0.
When FPGA_wg_enable_RW is set True, then enable the WG via mode = c_mode_calc = 1. Once enabled, the
WG starts or restarts when it gets a trigger from the BSN scheduler. The trigger has to occur at the
same BSN for all WG that are enabled, to ensure that they start synchronously. Any WG that are not
enabled will ignore the trigger. The exact BSN at which the WG start is don't care. The trigger is
scheduled via start_bsn in REG_BSN_SCHEDULER. The current BSN can be read from the REG_BSN_SCHEDULER
on one of the FPGAs. Assume the communication to write the start_bsn in all FPGAs will take
less than 1 ms, then a margin of 10 - 100 ms is sufficient. The BSN period corresponds to 5.12 μs, so
a c_bsn_latency = 20000 (≈ 100 ms) is sufficient for start_bsn = current_bsn + c_bsn_latency.
The MP reports False when mode = c_mode_off = 0, else True.
Note:
  The nof_samples field and mode field share an address in REG_DIAG_WG. The nof_samples = 2**W_wg_buf = 1024.
*/

bool Periph_fpga::write_wg_enable(const char *data)
{
    bool *_ptr = (bool *)data;
    bool retval = true;
    uint32_t sdp_data[4];
    bool wg_enable;
    bool set_bsn = false;

    string regname1;
    string regname2;

    for (uint i = 0; i < C_S_pn; i++)
    {
        regname1 = "mm/0/REG_WG/" + to_string(i) + "/mode";
        regname2 = "mm/0/REG_WG/" + to_string(i) + "/nof_samples";
        wg_enable = _ptr[i];
        if (wg_enable == true) {
            // turn on waveform
            set_bsn = true;
            sdp_data[0] = shift_mask(regname1, C_WG_MODE_CALC) | shift_mask(regname2, 1024); // TODO: make constant
        }
        else {
            // turn off waveform
            sdp_data[0] = shift_mask(regname1, C_WG_MODE_OFF) | shift_mask(regname2, 1024); // TODO: make constant
        }
        retval &= Write(regname1, sdp_data, true, false);
    }

    // get bsn and add latency
    if (set_bsn == true) {
        regname1 = "mm/0/REG_BSN_SCHEDULER/0/scheduled_bsn";
        retval &= Read(regname1, sdp_data);
        uint64_t scheduled_bsn = (((uint64_t)sdp_data[1] << 32) + sdp_data[0]);
        LOG_F(INFO, "bsn=%lu", (long)scheduled_bsn);
        scheduled_bsn += C_BSN_LATENCY;
        LOG_F(INFO, "new bsn=%lu", (long)scheduled_bsn);
        sdp_data[0] = (uint32_t)(scheduled_bsn & 0xffffffff);
        sdp_data[1] = (uint32_t)((scheduled_bsn >> 32) & 0xffffffff);
        retval &= Write(regname1, sdp_data);
    }
    return retval;
}

bool Periph_fpga::read_ddr_gigabytes(char *data)
{
    bool retval = true;
    uint32_t sdp_data = 0;
    string regname = "mm/0/REG_IO_DDR_MB_I/0/reg_io_ddr";
    retval = Read(regname, &sdp_data);
    uint32_t ddr_gigabytes = (sdp_data >> 16) & 0x0000ffff;
    memcpy(data, &ddr_gigabytes, sizeof(uint32_t));
    return retval;
}

bool Periph_fpga::read_ddr_calibrated(char *data)
{
    bool retval = true;
    uint32_t sdp_data = 0;
    string regname = "mm/0/REG_IO_DDR_MB_I/0/reg_io_ddr";
    retval = Read(regname, &sdp_data);
    bool ddr_calibrated = bool(sdp_data & 0x00000001);
    memcpy(data, &ddr_calibrated, sizeof(bool));
    return retval;
}

/*
ampl = FPGA_wg_amplitude_RW * c_ampl_unit
  where c_ampl_unit = 2^16.
  MP converts ampl into FPGA_wg_amplitude_R.
*/
bool Periph_fpga::read_wg_amplitude(char *data)
{
    bool retval = true;
    uint32_t sdp_data[12];
    memset(sdp_data, 0, sizeof(sdp_data));
    string regname;
    for (uint i = 0; i < C_S_pn; i++)
    {
        regname = "mm/0/REG_WG/" + to_string(i) + "/ampl";
        retval = Read(regname, &sdp_data[i]);
    }

    double rdata[C_S_pn] = {0.0};
    for (uint i = 0; i < C_S_pn; i++)
    {
        double ampl = ((double)sdp_data[i]) / C_WG_AMPL_UNIT;
        rdata[i] = ampl;
    }
    memcpy(data, rdata, sizeof(rdata));
    return retval;
}

bool Periph_fpga::write_wg_amplitude(const char *data)
{
    double wdata[C_S_pn];
    memcpy(wdata, data, sizeof(wdata));

    bool retval = true;
    for (uint i = 0; i < C_S_pn; i++)
    {
        string regname = "mm/0/REG_WG/" + to_string(i) + "/ampl";
        double ampl = wdata[i] * C_WG_AMPL_UNIT;
        uint32_t sdp_data = (uint32_t)ampl;
        retval = Write(regname, &sdp_data);
    }
    return retval;
}

/*
phase = FPGA_wg_phase_RW * c_phase_unit
  where c_phase_unit = 2^16 / 360.
  MP converts phase into FPGA_wg_phase_R.
*/
bool Periph_fpga::read_wg_phase(char *data)
{
    bool retval = true;
    string regname;
    double rdata[C_S_pn] = {0.0};
    for (uint i = 0; i < C_S_pn; i++)
    {
        regname = "mm/0/REG_WG/" + to_string(i) + "/phase";
        uint32_t sdp_data = 0;
        retval = Read(regname, &sdp_data);
        double phase = ((double)sdp_data) / C_WG_PHASE_UNIT;
        rdata[i] = phase;
    }
    memcpy(data, rdata, sizeof(rdata));
    return retval;
}

bool Periph_fpga::write_wg_phase(const char *data)
{
    double wdata[C_S_pn] = {0.0};
    memcpy(wdata, data, sizeof(wdata));

    bool retval = true;
    for (uint i = 0; i < C_S_pn; i++)
    {
        string regname = "mm/0/REG_WG/" + to_string(i) + "/phase";
        double phase = wdata[i] * C_WG_PHASE_UNIT;
        uint32_t sdp_data = (uint32_t)phase;
        retval = Write(regname, &sdp_data);
    }
    return retval;
}

/*
freq = FPGA_wg_frequency_RW / f_adc
freq = freq % 1  # to wrap freq in [0, 1> interval
freq = freq * c_freq_unit
  where f_adc = 200e6 and c_freq_unit = 2^31.
  MP converts freq into FPGA_wg_frequency_R.
*/
bool Periph_fpga::read_wg_frequency(char *data)
{
    bool retval = true;
    double rdata[C_S_pn] = {0.0};
    for (uint i = 0; i < C_S_pn; i++)
    {
        string regname = "mm/0/REG_WG/" + to_string(i) + "/freq";
        uint32_t sdp_data = 0;
        retval &= Read(regname, &sdp_data);
        double freq = (((double)sdp_data) / C_WG_FREQ_UNIT) * C_F_adc;
        rdata[i] = freq;
    }
    memcpy(data, rdata, sizeof(rdata));
    return retval;
}

bool Periph_fpga::write_wg_frequency(const char *data)
{
    double wdata[C_S_pn] = {0.0};
    memcpy(wdata, data, sizeof(wdata));
    bool retval = true;

    for (uint i = 0; i < C_S_pn; i++)
    {
        string regname = "mm/0/REG_WG/" + to_string(i) + "/freq";
        double freq = wdata[i] / C_F_adc;
        double intpart;
        freq = modf(freq, &intpart);
        freq = freq * C_WG_FREQ_UNIT;
        uint32_t sdp_data = (uint32_t)freq;
        retval = Write(regname, &sdp_data);
    }
    return retval;
}

bool Periph_fpga::write_signal_input_samples_delay(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = true;
    uint32_t sdp_data;
    uint32_t min_sample_delay = 0;
    uint32_t max_sample_delay = 4095;
    string regname;
    for (uint i = 0; i < C_S_pn; i++)
    {
        uint32_t sample_delay = *_ptr;
        if ((sample_delay < min_sample_delay) || (sample_delay > max_sample_delay))
        {
            LOG_F(ERROR, "signal_input_sample_delay not in range<%d:%d>", min_sample_delay, max_sample_delay);
            retval = false;
        }
        else
        {
            regname = "mm/0/REG_DP_SHIFTRAM/" + to_string(i) + "/shift";
            sdp_data = sample_delay;
            retval = Write(regname, &sdp_data);
            _ptr++;
        }
    }
    return retval;
}

bool Periph_fpga::read_signal_input_sync_timeout(char *data, int mode)
{
    bool retval = true;
    uint32_t sdp_data;
    string regname;
    regname = "mm/0/REG_BSN_MONITOR_INPUT/0/sync_timeout";
    retval = Read(regname, &sdp_data);
    signal_input_sync_timeout = (bool)sdp_data;

    return retval;
}

bool Periph_fpga::read_current_bsn(char *data)
{
    bool retval = true;
    uint32_t sdp_data[2];

    string regname = "mm/0/REG_BSN_SCHEDULER/0/scheduled_bsn";
    retval &= Read(regname, sdp_data);
    uint64_t scheduled_bsn = (((uint64_t)sdp_data[1] << 32) + sdp_data[0]);

    memcpy(data, &scheduled_bsn, sizeof(scheduled_bsn));
    return retval;
}

bool Periph_fpga::read_signal_input_bsn(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        if (signal_input_sync_timeout == true)
        {
            signal_input_bsn = -1;
        }
        else
        {
            uint32_t sdp_data[2];
            memset(sdp_data, 0, sizeof(sdp_data));
            string regname;
            regname = "mm/0/REG_BSN_MONITOR_INPUT/0/bsn_at_sync";
            retval = Read(regname, sdp_data);
            signal_input_bsn = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }

    memcpy(data, &signal_input_bsn, sizeof(signal_input_bsn));
    return retval;
}

bool Periph_fpga::read_signal_input_nof_blocks(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        if (signal_input_sync_timeout == true)
        {
            signal_input_nof_blocks = -1;
        }
        else
        {
            uint32_t sdp_data;
            string regname;
            regname = "mm/0/REG_BSN_MONITOR_INPUT/0/nof_sop";
            retval = Read(regname, &sdp_data);
            signal_input_nof_blocks = (int32_t)sdp_data;
        }
        return retval;
    }

    memcpy(data, &signal_input_nof_blocks, sizeof(signal_input_nof_blocks));
    return retval;
}

bool Periph_fpga::read_signal_input_nof_samples(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        if (signal_input_sync_timeout == true)
        {
            signal_input_nof_samples = -1;
        }
        else
        {
            uint32_t sdp_data;
            string regname;
            regname = "mm/0/REG_BSN_MONITOR_INPUT/0/nof_valid";
            retval = Read(regname, &sdp_data);
            signal_input_nof_samples = (int32_t)sdp_data;
        }
        return retval;
    }

    memcpy(data, &signal_input_nof_samples, sizeof(signal_input_nof_samples));
    return retval;
}

bool Periph_fpga::read_signal_input_mean(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        memset(sdp_data, 0, sizeof(sdp_data));
        string regname;
        for (uint i = 0; i < C_S_pn; i++)
        {
            regname = "mm/0/REG_ADUH_MONITOR/" + to_string(i) + "/mean_sum";
            retval = Read(regname, sdp_data);
            int64_t mean_sum = (int64_t)(((int64_t)sdp_data[1] << 32) + sdp_data[0]);
            signal_input_mean[i] = (double)mean_sum / C_N_CLK_PER_PPS;
        }
        return retval;
    }
    memcpy(data, signal_input_mean, sizeof(signal_input_mean));
    return retval;
}

bool Periph_fpga::read_signal_input_rms(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        memset(sdp_data, 0, sizeof(sdp_data));
        string regname;
        for (uint i = 0; i < C_S_pn; i++)
        {
            regname = "mm/0/REG_ADUH_MONITOR/" + to_string(i) + "/power_sum";
            retval = Read(regname, sdp_data);
            int64_t power_sum = (int64_t)(((int64_t)sdp_data[1] << 32) + sdp_data[0]);
            signal_input_rms[i] = sqrt((double)power_sum / C_N_CLK_PER_PPS);
        }
    }
    memcpy(data, signal_input_rms, sizeof(signal_input_rms));
    return retval;
}

bool Periph_fpga::read_signal_input_std(char *data, int mode)
{
    /*
     *  before calling this function call first
     *  read_signal_input_mean and read_signal_input_rms
     */
    bool retval = true;
    if (mode == R_UCP)
    {
        for (uint i = 0; i < C_S_pn; i++)
        {
            signal_input_std[i] = sqrt(pow(signal_input_rms[i], 2.0) - pow(signal_input_mean[i], 2.0));
        }
    }

    memcpy(data, signal_input_std, sizeof(signal_input_std));
    return retval;
}

bool Periph_fpga::read_signal_input_data_buffer(char *data)
{
    bool retval = true;
    uint32_t sdp_data[C_V_si_db];
    string regname;
    int16_t *_ptr = (int16_t *)data;
    for (uint i = 0; i < C_S_pn; i++)
    {
        memset(sdp_data, 0, sizeof(sdp_data));
        regname = "mm/0/RAM_DIAG_DATA_BUFFER_BSN/" + to_string(i) + "/data";
        retval &= Read(regname, sdp_data);
        for (uint j = 0; j < C_V_si_db; j++)
        {
            *_ptr = (int16_t)((sdp_data[j] & 0x3FFF) << 2) / 4;
            _ptr++;
        }
    }
    return retval;
}

bool Periph_fpga::read_jesd204b_csr_rbd_count(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        string regname;
        for (uint i = 0; i < C_S_pn; i++)
        {
            regname = "mm/0/JESD204B/" + to_string(i) + "/csr_rbd_count";
            retval = Read(regname, &sdp_data);
            jesd_csr_rbd_count[i] = (uint32_t)sdp_data;
        }
        return retval;
    }

    memcpy(data, jesd_csr_rbd_count, sizeof(jesd_csr_rbd_count));
    return retval;
}

bool Periph_fpga::read_jesd204b_csr_dev_syncn(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        string regname;
        for (uint i = 0; i < C_S_pn; i++)
        {
            regname = "mm/0/JESD204B/" + to_string(i) + "/csr_dev_syncn";
            retval = Read(regname, &sdp_data);
            jesd_csr_dev_syncn[i] = (uint32_t)sdp_data;
        }
        return retval;
    }

    memcpy(data, jesd_csr_dev_syncn, sizeof(jesd_csr_dev_syncn));
    return retval;
}

bool Periph_fpga::read_jesd204b_rx_err0(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        string regname;
        for (uint i = 0; i < C_S_pn; i++)
        {
            regname = "mm/0/JESD204B/" + to_string(i) + "/rx_err0";
            retval = Read(regname, &sdp_data);
            jesd_rx_err0[i] = (uint32_t)sdp_data;
        }
        return retval;
    }

    memcpy(data, jesd_rx_err0, sizeof(jesd_rx_err0));
    return retval;
}

bool Periph_fpga::read_jesd204b_rx_err1(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        string regname;
        for (uint i = 0; i < C_S_pn; i++)
        {
            regname = "mm/0/JESD204B/" + to_string(i) + "/rx_err1";
            retval = Read(regname, &sdp_data);
            jesd_rx_err1[i] = (uint32_t)sdp_data;
        }
        return retval;
    }

    memcpy(data, jesd_rx_err1, sizeof(jesd_rx_err1));
    return retval;
}

bool Periph_fpga::write_jesd204b_clear_rx_err0(const char *data)
{
    bool retval = true;
    
    if (data[0] == 1) {
        uint32_t sdp_data = 0xFFFFFFFF;
        retval &= Write("mm/0/JESD204B/0/rx_err0", &sdp_data, true, false);
    }

    return retval;
}

bool Periph_fpga::write_jesd204b_clear_rx_err1(const char *data)
{
     bool retval = true;
    
    if (data[0] == 1) {
        uint32_t sdp_data = 0xFFFFFFFF;
        retval &= Write("mm/0/JESD204B/0/rx_err1", &sdp_data, true, false);
    }

    return retval;
}


bool Periph_fpga::write_jesd_ctrl(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = true;

    uint32_t sdp_data;

    memcpy(&sdp_data, _ptr, sizeof(uint32_t));

    // reset and disable have same register, use_mask_flag is set to false
    retval &= Write("mm/0/PIO_JESD_CTRL/0/reset", &sdp_data, true, false);

    return retval;
}

bool Periph_fpga::read_jesd_ctrl(char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = true;

    uint32_t sdp_data;

    // reset and disable have same register, use_mask_flag is set to false
    retval &= Read("mm/0/PIO_JESD_CTRL/0/reset", &sdp_data, false);

    memcpy(_ptr, &sdp_data, sizeof(uint32_t));

    return retval;
}

bool Periph_fpga::write_subband_weights(const char *data, const string &port_name)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = true;

    string regname = "mm/0/" + port_name + "/0/data";
    uint32_t n_ports = mmap->getNPorts(regname);
    uint32_t n_fields = mmap->getNFields(regname);

    uint32_t *sdp_data = new uint32_t[n_fields];

    if (n_ports > (nFilterbanks * (C_S_pn / C_Q_fft)))
    {
        n_ports = nFilterbanks * (C_S_pn / C_Q_fft);
    }

    for (uint p = 0; p < n_ports; p++)
    {
        regname = "mm/0/" + port_name + "/" + to_string(p) + "/data";
        for (uint f = 0; f < n_fields; f++)
        {
            memcpy(&(sdp_data[f]), _ptr, sizeof(uint32_t));
            _ptr++;
        }
        retval &= Write(regname, sdp_data);
    }
    my_delete_arr(sdp_data);
    return retval;
}

bool Periph_fpga::read_subband_weights(char *data, const string &port_name)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = true;

    string regname = "mm/0/" + port_name + "/0/data";
    uint32_t n_ports = mmap->getNPorts(regname);
    uint32_t n_fields = mmap->getNFields(regname);

    uint32_t *sdp_data = new uint32_t[n_fields];

    if (n_ports > (nFilterbanks * (C_S_pn / C_Q_fft)))
    {
        n_ports = nFilterbanks * (C_S_pn / C_Q_fft);
    }

    for (uint p = 0; p < n_ports; p++)
    {
        regname = "mm/0/" + port_name + "/" + to_string(p) + "/data";
        retval &= Read(regname, sdp_data);
        for (uint f = 0; f < n_fields; f++)
        {
            memcpy(_ptr, &(sdp_data[f]), sizeof(uint32_t));
            _ptr++;
        }
    }
    my_delete_arr(sdp_data);
    return retval;
}

bool Periph_fpga::write_subband_fir_filter_coefficients(const char *data)
{
    int16_t *_ptr = (int16_t *)data;
    bool retval = true;

    uint32_t n_inst = mmap->getNInstances("mm/?/RAM_FIL_COEFS/0/data");
    uint32_t n_ports = mmap->getNPorts("mm/0/RAM_FIL_COEFS/0/data");
    uint32_t n_fields = mmap->getNFields("mm/0/RAM_FIL_COEFS/0/data");

    string regname;
    uint32_t *sdp_data = new uint32_t[n_fields];

    n_inst = min(n_inst, nFilterbanks);

    for (uint i = 0; i < n_inst; i++)
    {
        for (uint p = 0; p < n_ports; p++)
        {
            regname = "mm/" + to_string(i) + "/RAM_FIL_COEFS/" + to_string(p) + "/data";
            for (uint f = 0; f < n_fields; f++)
            {
                memcpy(&(sdp_data[f]), _ptr, sizeof(int16_t));
                _ptr++;
            }
            retval &= Write(regname, sdp_data);
        }
    }
    my_delete_arr(sdp_data);
    return retval;
}

bool Periph_fpga::read_subband_fir_filter_coefficients(char *data)
{
    int16_t *_ptr = (int16_t *)data;
    bool retval = true;

    uint32_t n_inst = mmap->getNInstances("mm/?/RAM_FIL_COEFS/0/data");
    uint32_t n_ports = mmap->getNPorts("mm/0/RAM_FIL_COEFS/0/data");
    uint32_t n_fields = mmap->getNFields("mm/0/RAM_FIL_COEFS/0/data");

    string regname;
    uint32_t *sdp_data = new uint32_t[n_fields];

    n_inst = min(n_inst, nFilterbanks);

    for (uint i = 0; i < n_inst; i++)
    {
        for (uint p = 0; p < n_ports; p++)
        {
            regname = "mm/" + to_string(i) + "/RAM_FIL_COEFS/" + to_string(p) + "/data";
            retval &= Read(regname, sdp_data);
            for (uint f = 0; f < n_fields; f++)
            {
                memcpy(_ptr, &(sdp_data[f]), sizeof(int16_t));
                _ptr++;
            }
        }
    }
    my_delete_arr(sdp_data);
    return retval;
}
/* universal read_all_from_port
 * reads all data (all instances, ports and fields) from a (port_name, field_name) combination.
 * this function is used if no other conversions need to be done.
 */
bool Periph_fpga::read_all_from_port(char *data, const uint32_t cmd_id, const string &port_name, const string &field_name)
{
    bool retval = true;

    string regname = "mm/0/" + port_name + "/0/" + field_name;
    uint32_t n_periph = mmap->getNPeripherals(regname);
    uint32_t n_ports = mmap->getNPorts(regname);
    uint32_t n_fields = mmap->getNFields(regname);
    uint32_t span = mmap->getSpan(regname);

    string pointname = pointmap->getName(cmd_id);
    uint32_t format = pointmap->getFormat(pointname);

    size_t format_size = reg_format_size_in_bytes(format);

    uint32_t *sdp_data = new uint32_t[span];
    char *_ptr = data;

    if (n_periph > nBeamsets)
    {
        n_periph = nBeamsets;
    }

    for (uint i = 0; i < n_periph; i++)
    {
        for (uint j = 0; j < n_ports; j++)
        {
            regname = "mm/" + to_string(i) + "/" + port_name + "/" + to_string(j) + "/" + field_name;

            memset(sdp_data, 0, (span * sizeof(uint32_t)));
            retval &= Read(regname, sdp_data);
            if (retval)
            {
                // if a string
                if (format == REG_FORMAT_STRING)
                {
                    memcpy(_ptr, sdp_data, n_fields);
                    _ptr += SIZE1STRING;
                }
                // if >= 4 bytes (uint32_t)
                else if (format_size >= sizeof(uint32_t))
                {
                    memcpy(_ptr, sdp_data, (n_fields * format_size));
                    _ptr += (n_fields * format_size);
                }
                // if < 4 (uint32_t)
                else
                {
                    for (uint field_nr = 0; field_nr < n_fields; field_nr++)
                    {
                        memcpy(_ptr, &sdp_data[field_nr], format_size);
                        _ptr += format_size;
                    }
                }
            }
            // not valid sdp_data, set pointer to memory for next port sdp_data
            else
            {
                _ptr += (format_size * n_fields);
            }
        }
    }
    my_delete_arr(sdp_data);
    return retval;
}

bool Periph_fpga::read_pps_expected_cnt(char *data)
{
    bool retval = true;
    uint32_t sdp_data = 0;
    retval = Read("mm/0/PIO_PPS/0/expected_cnt", &sdp_data);
    pps_expected_cnt = sdp_data;

    uint32_t *_ptr = (uint32_t *)data;
    *_ptr = pps_expected_cnt;
    return retval;
}

bool Periph_fpga::write_pps_expected_cnt(const char *data)
{
    uint32_t *_ptr = (uint32_t *)data;
    bool retval = true;

    retval = Write("mm/0/PIO_PPS/0/expected_cnt", _ptr);
    return retval;
}

bool Periph_fpga::read_pps_present(char *data, int mode)
{
    // pps_present is set in read_pps_capture_cnt()
    bool retval = true;

    bool *_ptr = (bool *)data;
    *_ptr = pps_present;
    return retval;
}

bool Periph_fpga::read_pps_capture_cnt(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data = 0;
        retval = Read("mm/0/PIO_PPS/0/capture_cnt", &sdp_data);
        pps_capture_cnt = sdp_data;
        // check if capture count is as expected, and set pps_present
        if (pps_capture_cnt == pps_expected_cnt) {
            pps_present = true;
        }
        else {
            pps_present = false;
            pps_error_cnt++;
        }
    }

    uint32_t *_ptr = (uint32_t *)data;
    *_ptr = pps_capture_cnt;
    return retval;
}

bool Periph_fpga::read_pps_error_cnt(char *data)
{
    bool retval = true;
    uint32_t *_ptr = (uint32_t *)data;
    *_ptr = pps_error_cnt;
    return retval;
}

bool Periph_fpga::read_time_since_last_pps(char *data)
{
    bool retval = true;

    uint32_t sdp_data = 0;
    retval = Read("mm/0/PIO_PPS/0/offset_cnt", &sdp_data);
    double read_time_since_last_pps = (double)(sdp_data * C_200MHZ_1_CNT_NS) / 1e9;
    memcpy(data, &read_time_since_last_pps, sizeof(read_time_since_last_pps));
    return retval;
}

bool Periph_fpga::read_monitor_pps_offset_time(char *data, int mode)
{
    bool retval = true;
    if (mode == R_UCP)
    {
        uint32_t sdp_data = 0;
        clock_gettime(CLOCK_REALTIME, &monitor_start_time);
        retval = Read("mm/0/PIO_PPS/0/offset_cnt", &sdp_data);
        monitor_pps_offset_time = (double)(sdp_data * C_200MHZ_1_CNT_NS) / 1e9;
    }
    memcpy(data, &monitor_pps_offset_time, sizeof(monitor_pps_offset_time));
    return retval;
}

/* beamlet_subband_select
 * OPC-ua -> [N_pn][A_pn][N_pol][N_beamlets_ctrl]
 * SDP -> RAM_SS_SS_WIDE[S_sub_bf][N_pol]  2 instances. 6 ports.
 *
 *   N_pn = number of fpga's (this function handles 1 fpga)
 *   A_pn = number of dual pol antenneas per fpga = ports
 *   N_pol = number of polarisations = 2
 *   S_sub_bf = beamset = 488 beamlets
 *   N_beamsets = number of beamset 1 or 2
 *   N_beamlets_ctrl = N_beamsets * S_sub_bf =  488 or 976
 *
 *
 * p.. = ((((port * C_N_pol) + pol) * nBeamsets) + inst) * C_S_sub_bf;
 *
 * nBeamsets=1
 * 0    = ((((0 * C_N_pol) + 0  ) * nBeamsets) + 0) * 488
 * 488  = ((((0 * C_N_pol) + 1  ) * nBeamsets) + 0) * 488
 * 976  = ((((1 * C_N_pol) + 0  ) * nBeamsets) + 0) * 488
 * 1464 = ((((1 * C_N_pol) + 1  ) * nBeamsets) + 0) * 488
 * Beamsets=2
 * 0    = ((((0 * C_N_pol) + 0  ) * nBeamsets) + 0) * 488
 * 976  = ((((0 * C_N_pol) + 1  ) * nBeamsets) + 0) * 488
 * 488  = ((((0 * C_N_pol) + 0  ) * nBeamsets) + 1) * 488
 * 1464 = ((((0 * C_N_pol) + 1  ) * nBeamsets) + 1) * 488
 * 1952 = ((((1 * C_N_pol) + 0  ) * nBeamsets) + 0) * 488
 * 2928 = ((((1 * C_N_pol) + 1  ) * nBeamsets) + 0) * 488
 * 2440 = ((((1 * C_N_pol) + 0  ) * nBeamsets) + 1) * 488
 * 3416 = ((((1 * C_N_pol) + 1  ) * nBeamsets) + 1) * 488
 */

bool Periph_fpga::read_beamlet_subband_select(char *data)
{
    bool retval = true;

    uint32_t n_inst = mmap->getNInstances("mm/?/RAM_SS_SS_WIDE/0/data");
    uint32_t n_ports = mmap->getNPorts("mm/0/RAM_SS_SS_WIDE/0/data");
    uint32_t span = mmap->getSpan("mm/0/RAM_SS_SS_WIDE/0/data");

    n_inst = min(n_inst, nBeamsets);

    uint32_t *in_data = new uint32_t[span];
    uint32_t *out_data = (uint32_t *)data;

    uint32_t pin;
    uint32_t pout;

    string regname;
    for (uint port = 0; port < n_ports; port++)
    { // loop over A_pn (dual polarization antenna index)
        for (uint inst = 0; inst < n_inst; inst++)
        { // loop over beamsets
            regname = "mm/" + to_string(inst) + "/RAM_SS_SS_WIDE/" + to_string(port) + "/data";
            LOG_F(INFO, "read_beamlet_subband_select regname=%s", regname.c_str());

            memset(in_data, 0, span * sizeof(uint32_t));
            retval &= Read(regname, in_data, false);

            for (uint pol = 0; pol < C_N_pol; pol++)
            {
                for (uint blet = 0; blet < C_S_sub_bf; blet++)
                {
                    pin = blet * C_N_pol + pol;
                    pout = ((((port * C_N_pol) + pol) * nBeamsets) + inst) * C_S_sub_bf + blet;
                    out_data[pout] = (uint32_t)((in_data[pin] - pol) / C_N_pol);
                }
            }
        }
    }
    my_delete_arr(in_data);
    return retval;
}

bool Periph_fpga::write_beamlet_subband_select(const char *data)
{

    bool retval = true;

    uint32_t n_inst = mmap->getNInstances("mm/?/RAM_SS_SS_WIDE/0/data");
    uint32_t n_ports = mmap->getNPorts("mm/0/RAM_SS_SS_WIDE/0/data");
    uint32_t span = mmap->getSpan("mm/0/RAM_SS_SS_WIDE/0/data");

    n_inst = min(n_inst, nBeamsets);

    uint32_t *out_data = new uint32_t[span];
    uint32_t *in_data = (uint32_t *)data;

    uint32_t pin;
    uint32_t pout;

    string regname;
    for (uint port = 0; port < n_ports; port++)
    {
        for (uint inst = 0; inst < n_inst; inst++)
        {
            memset(out_data, 0, span * sizeof(uint32_t));
            for (uint pol = 0; pol < C_N_pol; pol++)
            {
                for (uint blet = 0; blet < C_S_sub_bf; blet++)
                {
                    pin = ((((port * C_N_pol) + pol) * nBeamsets) + inst) * C_S_sub_bf + blet;
                    pout = blet * C_N_pol + pol;

                    out_data[pout] = in_data[pin] * C_N_pol + pol;
                }
            }
            regname = "mm/" + to_string(inst) + "/RAM_SS_SS_WIDE/" + to_string(port) + "/data";
            LOG_F(INFO, "write_beamlet_subband_select regname=%s", regname.c_str());
            retval &= Write(regname, out_data, true, false);
        }
    }
    my_delete_arr(out_data);
    return retval;
}

/*
 * Read or write Beam Former weights as blocks of S_SUB_BF (=488) weights.
 *
 *                                  <-- inst --><---- port ----><---- span ---->
 * The weights are stored in RAM as: [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * set,pol_bf,a,pol = The weight range to read/write.
 *
 * ptr = pointer to get write data from (rw="w) or to save read data to (rw="r").
 */
uint Periph_fpga::bf_weights_rw(const string &rw, uint set, uint pol_bf, uint a, uint pol, uint32_t *ptr)
{
    bool retval = true;
    // Convert set,pol_bf,a,pol to inst,port,offset required for Read/Write
    uint inst = set;
    uint port = pol_bf * C_A_pn + a;
    uint offs = pol * C_S_sub_bf;

    string regname = "mm/" + to_string(inst) + "/RAM_BF_WEIGHTS/" + to_string(port) + "/data";

    LOG_F(DEBUG, "bf_weights_rw - inst=%d port=%d offset=%d", inst, port, offs);

    // Read or write a block of C_S_sub_bf (488) weights
    if (rw == "r")
    {
        retval &= Read(regname, ptr, false, offs, C_S_sub_bf);
    }
    else if (rw == "w")
    {
        retval &= Write(regname, ptr, true, false, offs, C_S_sub_bf);
    }
    return retval;
}

/*
 * BF weights RAM (raw access): Read
 * . Don't reorder like XX,YX,.. functions.
 */
bool Periph_fpga::read_bf_weights(char *data)
{
    bool retval = true;

    uint32_t n_inst = mmap->getNInstances("mm/?/RAM_BF_WEIGHTS/0/data");
    uint32_t n_ports = mmap->getNPorts("mm/0/RAM_BF_WEIGHTS/0/data");
    uint32_t span = mmap->getSpan("mm/0/RAM_BF_WEIGHTS/0/data");

    n_inst = min(n_inst, nBeamsets);

    uint32_t *ptr = (uint32_t *)data;
    string regname;
    for (uint i = 0; i < n_inst; i++)
    {
        for (uint p = 0; p < n_ports; p++)
        {

            //            LOG_F(INFO, "read_bf_weights i,p: %d,%d span=%d", i, p, span);
            regname = "mm/" + to_string(i) + "/RAM_BF_WEIGHTS/" + to_string(p) + "/data";
            retval &= Read(regname, ptr);
            ptr += span;
        }
    }
    return retval;
}

/*
 * BF weights RAM (raw access): Write
 * . Don't reorder like XX,YX,.. functions.
 */
bool Periph_fpga::write_bf_weights(const char *data)
{
    bool retval = true;

    uint32_t n_inst = mmap->getNInstances("mm/?/RAM_BF_WEIGHTS/0/data");
    uint32_t n_ports = mmap->getNPorts("mm/0/RAM_BF_WEIGHTS/0/data");
    uint32_t span = mmap->getSpan("mm/0/RAM_BF_WEIGHTS/0/data");

    n_inst = min(n_inst, nBeamsets);

    uint32_t *ptr = (uint32_t *)data;
    string regname;
    for (uint i = 0; i < n_inst; i++)
    {
        for (uint p = 0; p < n_ports; p++)
        {
            regname = "mm/" + to_string(i) + "/RAM_BF_WEIGHTS/" + to_string(p) + "/data";
            retval &= Write(regname, ptr);
            ptr += span;
        }
    }
    return retval;
}

//======================================================================================================================================
// XX, YY
//======================================================================================================================================
/*
 * Read Beam Former Weights: weight sets XX, YY
 *
 * This function transposes [A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to read the RAM.
 *
 * The full set of weights sits in RAM as                             [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 * We target the right  weights by selecting  pol_bf,pol=[(0,0),(1,1)][       0,1][     0,1][0..5][  0,1][  0..487] #11712 weights
 *
 * We need to save the the read weights as (note: no N_POL_BF array dimension): [A_PN][N_POL][N_BEAMSETS][S_SUB_BF] #11712 weights
 * (pol_bf,pol) maps to (pol) as follows:
 * . (0,0) -> (0) XX
 * . (1,1) -> (1) YY
 */
bool Periph_fpga::read_bf_weights_xx_yy(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [A_PN][N_POL][N_BEAMSETS]
    for (uint a = 0; a < C_A_pn; a++)
    {
        for (uint pol = 0; pol < C_N_pol; pol++)
        {
            uint pol_bf = pol; // pol_bf=pol
            for (uint set = 0; set < nBeamsets; set++)
            {
                // Transpose the RAM address, read S_SUB_BF weights
                retval &= bf_weights_rw("r", set, pol_bf, a, pol, ptr);
                ptr += C_S_sub_bf;
            }
        }
    }

    return retval;
}

/*
 * Write Beam Former Weights: weight sets XX, YY
 *
 * This function transposes [A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to write the RAM.
 *
 * The full set of weights sits in RAM as                             [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 * We target the right  weights by selecting  pol_bf,pol=[(0,0),(1,1)][       0,1][     0,1][0..5][  0,1][  0..487] #11712 weights
 *
 * We weights to write are given as (note: no N_POL_BF array dimension):        [A_PN][N_POL][N_BEAMSETS][S_SUB_BF] #11712 weights
 * (pol_bf,pol) maps to (pol) as follows:
 * . (0,0) -> (0) XX
 * . (1,1) -> (1) YY
 */
bool Periph_fpga::write_bf_weights_xx_yy(const char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [A_PN][N_POL][N_BEAMSETS]
    for (uint a = 0; a < C_A_pn; a++)
    {
        for (uint pol = 0; pol < C_N_pol; pol++)
        {
            uint pol_bf = pol; // pol_bf=pol
            for (uint set = 0; set < nBeamsets; set++)
            {
                // Transpose the RAM address, read S_SUB_BF weights
                retval &= bf_weights_rw("w", set, pol_bf, a, pol, ptr);
                ptr += C_S_sub_bf;
            }
        }
    }

    return retval;
}

//======================================================================================================================================
// XY, YX
//======================================================================================================================================
/*
 * Read Beam Former Weights: weight sets XY, YX
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to read the RAM.
 *
 * The full set of weights sits in RAM as                              [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                             [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] #11712 weights
 * We target the right  weights by selecting (pol_bf,pol)=(0,1),(1,0): [     0,1][0..5][  0,1][       0,1][  0..487] #11712 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX
 * (     0,   1) -> XY
 * (     1,   0) -> YX
 * (     1,   1) -> YY
 * We can select XY and YX by assigning pol=not(pol_bf).
 */
bool Periph_fpga::read_bf_weights_xy_yx(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [A_PN][N_POL][N_BEAMSETS]
    for (uint a = 0; a < C_A_pn; a++)
    {
        for (uint pol = 0; pol < C_N_pol; pol++)
        {
            uint pol_bf = 1 - pol; // pol_bf=not(pol)
            for (uint set = 0; set < nBeamsets; set++)
            {
                // Transpose the RAM address, read S_SUB_BF weights
                retval &= bf_weights_rw("r", set, pol, a, pol_bf, ptr); // positions of pol_bf,pol swapped as we want pol_bf,pol=(0,1) first
                ptr += C_S_sub_bf;
            }
        }
    }

    return retval;
}

/*
 * Write Beam Former Weights: weight sets XY, YX
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to write the RAM.
 *
 * The full set of weights sits in RAM as                              [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                             [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] #11712 weights
 * We target the right  weights by selecting (pol_bf,pol)=(0,1),(1,0): [     0,1][0..5][  0,1][       0,1][  0..487] #11712 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX
 * (     0,   1) -> XY
 * (     1,   0) -> YX
 * (     1,   1) -> YY
 * We can select XY and YX by assigning pol=not(pol_bf).
 */
bool Periph_fpga::write_bf_weights_xy_yx(const char *data)
{

    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [A_PN][N_POL][N_BEAMSETS]
    for (uint a = 0; a < C_A_pn; a++)
    {
        for (uint pol = 0; pol < C_N_pol; pol++)
        {
            uint pol_bf = 1 - pol; // pol_bf=not(pol)
            for (uint set = 0; set < nBeamsets; set++)
            {
                // Transpose the RAM address, read S_SUB_BF weights
                retval &= bf_weights_rw("w", set, pol, a, pol_bf, ptr); // positions of pol_bf,pol swapped as we want pol_bf,pol=(0,1) first
                ptr += C_S_sub_bf;
            }
        }
    }

    return retval;
}

//======================================================================================================================================
// XX, XY, YX, YY
//======================================================================================================================================
/*
 * Read Beam Former Weights: weight sets XX, XY, YX, YY
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to read the RAM.
 *
 * The full set of weights sits in RAM as              [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as             [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] #23424 weights
 * We target all weights                               [     0,1][0..5][  0,1][       0,1][  0..487] #23424 weights
 */
bool Periph_fpga::read_bf_weights_xx_xy_yx_yy(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    for (uint pol_bf = 0; pol_bf < C_N_pol_bf; pol_bf++)
    {
        for (uint a = 0; a < C_A_pn; a++)
        {
            for (uint pol = 0; pol < C_N_pol; pol++)
            {
                for (uint set = 0; set < nBeamsets; set++)
                {
                    // Transpose the RAM address, read S_SUB_BF weights
                    retval &= bf_weights_rw("r", set, pol_bf, a, pol, ptr);
                    ptr += C_S_sub_bf;
                }
            }
        }
    }

    return retval;
}

/*
 * Write Beam Former Weights: weight sets XX, XY, YX, YY
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to write the RAM.
 *
 * The full set of weights sits in RAM as              [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to write the the read weights as            [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] #23424 weights
 * We target all weights                               [     0,1][0..5][  0,1][       0,1][  0..487] #23424 weights
 */
bool Periph_fpga::write_bf_weights_xx_xy_yx_yy(const char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    for (uint pol_bf = 0; pol_bf < C_N_pol_bf; pol_bf++)
    {
        for (uint a = 0; a < C_A_pn; a++)
        {
            for (uint pol = 0; pol < C_N_pol; pol++)
            {
                for (uint set = 0; set < nBeamsets; set++)
                {
                    // Transpose the RAM address, read S_SUB_BF weights
                    retval &= bf_weights_rw("w", set, pol_bf, a, pol, ptr);
                    ptr += C_S_sub_bf;
                }
            }
        }
    }

    return retval;
}

//======================================================================================================================================
// XX
//======================================================================================================================================
/*
 * Read Beam Former Weights: weight sets XX
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to read the RAM.
 *
 * The full set of weights sits in RAM as                        [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                       [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] # 5856 weights
 * We target the right  weights by selecting (pol_bf,pol)=(0,0): [       0][0..5][  0,1][         0][  0..487] # 5856 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX (these are selected)
 * (     0,   1) -> XY
 * (     1,   0) -> YX
 * (     1,   1) -> YY
 */
bool Periph_fpga::read_bf_weights_xx(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    uint pol_bf = 0;
    for (uint a = 0; a < C_A_pn; a++)
    {
        uint pol = 0;
        for (uint set = 0; set < nBeamsets; set++)
        {
            // Transpose the RAM address, read S_SUB_BF weights
            retval &= bf_weights_rw("r", set, pol_bf, a, pol, ptr);
            ptr += C_S_sub_bf;
        }
    }

    return retval;
}

/*
 * Write Beam Former Weights: weight sets XX
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to write the RAM.
 *
 * The full set of weights sits in RAM as                        [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                       [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] # 5856 weights
 * We target the right  weights by selecting (pol_bf,pol)=(0,0): [     0,1][0..5][  0,1][       0,1][  0..487] # 5856 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX (these are selected)
 * (     0,   1) -> XY
 * (     1,   0) -> YX
 * (     1,   1) -> YY
 */
bool Periph_fpga::write_bf_weights_xx(const char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    uint pol_bf = 0;
    for (uint a = 0; a < C_A_pn; a++)
    {
        uint pol = 0;
        for (uint set = 0; set < nBeamsets; set++)
        {
            // Transpose the RAM address, read S_SUB_BF weights
            retval &= bf_weights_rw("w", set, pol_bf, a, pol, ptr);
            ptr += C_S_sub_bf;
        }
    }

    return retval;
}

//======================================================================================================================================
// XY
//======================================================================================================================================
/*
 * Read Beam Former Weights: weight sets XY
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to read the RAM.
 *
 * The full set of weights sits in RAM as                       [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                      [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] # 5856 weights
 * We target the right  weights by selecting (pol_bf,pol)=(0,1) [       0][0..5][  0,1][         0][  0..487] # 5856 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX
 * (     0,   1) -> XY (these are selected)
 * (     1,   0) -> YX
 * (     1,   1) -> YY
 */
bool Periph_fpga::read_bf_weights_xy(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    uint pol_bf = 0;
    for (uint a = 0; a < C_A_pn; a++)
    {
        uint pol = 1;
        for (uint set = 0; set < nBeamsets; set++)
        {
            // Transpose the RAM address, read S_SUB_BF weights
            retval &= bf_weights_rw("r", set, pol_bf, a, pol, ptr);
            ptr += C_S_sub_bf;
        }
    }

    return retval;
}

/*
 * Write Beam Former Weights: weight sets XY
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to write the RAM.
 *
 * The full set of weights sits in RAM as                        [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                       [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] # 5856 weights
 * We target the right  weights by selecting (pol_bf,pol)=(0,1): [     0,1][0..5][  0,1][       0,1][  0..487] # 5856 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX
 * (     0,   1) -> XY (these are selected)
 * (     1,   0) -> YX
 * (     1,   1) -> YY
 */
bool Periph_fpga::write_bf_weights_xy(const char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    uint pol_bf = 0;
    for (uint a = 0; a < C_A_pn; a++)
    {
        uint pol = 1;
        for (uint set = 0; set < nBeamsets; set++)
        {
            // Transpose the RAM address, read S_SUB_BF weights
            retval &= bf_weights_rw("w", set, pol_bf, a, pol, ptr);
            ptr += C_S_sub_bf;
        }
    }

    return retval;
}

//======================================================================================================================================
// YX
//======================================================================================================================================
/*
 * Read Beam Former Weights: weight sets YX
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to read the RAM.
 *
 * The full set of weights sits in RAM as                       [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                      [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] # 5856 weights
 * We target the right  weights by selecting (pol_bf,pol)=(1,0) [       0][0..5][  0,1][         0][  0..487] # 5856 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX
 * (     0,   1) -> XY
 * (     1,   0) -> YX(these are selected)
 * (     1,   1) -> YY
 */
bool Periph_fpga::read_bf_weights_yx(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    uint pol_bf = 1;
    for (uint a = 0; a < C_A_pn; a++)
    {
        uint pol = 0;
        for (uint set = 0; set < nBeamsets; set++)
        {
            // Transpose the RAM address, read S_SUB_BF weights
            retval &= bf_weights_rw("r", set, pol_bf, a, pol, ptr);
            ptr += C_S_sub_bf;
        }
    }

    return retval;
}

bool Periph_fpga::write_bf_weights_yx(const char *data)
{
    /*
     * Write Beam Former Weights: weight sets YX
     *
     * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to write the RAM.
     *
     * The full set of weights sits in RAM as                        [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
     *
     * We need to save the the read weights as                       [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] # 5856 weights
     * We target the right  weights by selecting (pol_bf,pol)=(1,0): [     0,1][0..5][  0,1][       0,1][  0..487] # 5856 weights
     *
     * (pol_bf, pol) -> weights
     * (     0,   0) -> XX
     * (     0,   1) -> XY
     * (     1,   0) -> YX (these are selected)
     * (     1,   1) -> YY
     */
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    uint pol_bf = 1;
    for (uint a = 0; a < C_A_pn; a++)
    {
        uint pol = 0;
        for (uint set = 0; set < nBeamsets; set++)
        {
            // Transpose the RAM address, read S_SUB_BF weights
            retval &= bf_weights_rw("w", set, pol_bf, a, pol, ptr);
            ptr += C_S_sub_bf;
        }
    }

    return retval;
}

//======================================================================================================================================
// YY
//======================================================================================================================================
/*
 * Read Beam Former Weights: weight sets YY
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to read the RAM.
 *
 * The full set of weights sits in RAM as                       [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                      [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] # 5856 weights
 * We target the right  weights by selecting (pol_bf,pol)=(1,1) [       0][0..5][  0,1][         0][  0..487] # 5856 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX
 * (     0,   1) -> XY
 * (     1,   0) -> YX
 * (     1,   1) -> YY(these are selected)
 */
bool Periph_fpga::read_bf_weights_yy(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    uint pol_bf = 1;
    for (uint a = 0; a < C_A_pn; a++)
    {
        uint pol = 1;
        for (uint set = 0; set < nBeamsets; set++)
        {
            // Transpose the RAM address, read S_SUB_BF weights
            retval &= bf_weights_rw("r", set, pol_bf, a, pol, ptr);
            ptr += C_S_sub_bf;
        }
    }

    return retval;
}

/*
 * Write Beam Former Weights: weight sets YY
 *
 * This function transposes [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] to [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] to write the RAM.
 *
 * The full set of weights sits in RAM as                        [N_BEAMSETS][N_POL_BF][A_PN][N_POL][S_SUB_BF] #23424 weights
 *
 * We need to save the the read weights as                       [N_POL_BF][A_PN][N_POL][N_BEAMSETS][S_SUB_BF] # 5856 weights
 * We target the right  weights by selecting (pol_bf,pol)=(1,1): [     0,1][0..5][  0,1][       0,1][  0..487] # 5856 weights
 *
 * (pol_bf, pol) -> weights
 * (     0,   0) -> XX
 * (     0,   1) -> XY
 * (     1,   0) -> YX
 * (     1,   1) -> YY (these are selected)
 */
bool Periph_fpga::write_bf_weights_yy(const char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;

    // Loop through [N_POL_BF][A_PN][N_POL][N_BEAMSETS]
    uint pol_bf = 1;
    for (uint a = 0; a < C_A_pn; a++)
    {
        uint pol = 1;
        for (uint set = 0; set < nBeamsets; set++)
        {
            // Transpose the RAM address, read S_SUB_BF weights
            retval &= bf_weights_rw("w", set, pol_bf, a, pol, ptr);
            ptr += C_S_sub_bf;
        }
    }

    return retval;
}

bool Periph_fpga::write_ring_node_offset(const char *data)
{
    uint32_t *ptr = (uint32_t *)data;
    bool retval = Write("mm/0/REG_RING_INFO/0/o_rn", ptr);
    return retval;
}

bool Periph_fpga::write_ring_nof_nodes(const char *data)
{
    uint32_t *ptr = (uint32_t *)data;
    bool retval = Write("mm/0/REG_RING_INFO/0/n_rn", ptr);
    return retval;
}

bool Periph_fpga::write_ring_use_cable_to_next_rn(const char *data)
{
    uint32_t sdp_data = (uint32_t)data[0];
    bool retval = Write("mm/0/REG_RING_INFO/0/use_cable_to_next_rn", &sdp_data);
    return retval;
}

bool Periph_fpga::write_ring_use_cable_to_previous_rn(const char *data)
{
    uint32_t sdp_data = (uint32_t)data[0];
    bool retval = Write("mm/0/REG_RING_INFO/0/use_cable_to_previous_rn", &sdp_data);
    return retval;
}

bool Periph_fpga::write_xst_ring_nof_transport_hops(const char *data)
{
    uint32_t *ptr = (uint32_t *)data;
    bool retval = Write("mm/0/REG_RING_LANE_INFO_XST/0/transport_nof_hops", ptr);
    return retval;
}

bool Periph_fpga::write_xst_ring_rx_clear_total_counts(const char *data)
{
    bool retval = true;
    if (data[0] == 1)
    {
        uint32_t clear = 1;
        retval &= Write("mm/0/REG_DP_BLOCK_VALIDATE_ERR_XST/0/clear", &clear);
        retval &= Write("mm/0/REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC_XST/0/clear", &clear);
    }
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_clear_total_counts(char *data)
{
    data[0] = 0;
    return true;
}

bool Periph_fpga::write_xst_rx_align_stream_enable(const char *data)
{
    bool retval = true;
    bool *ptr = (bool *)data;

    uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_ALIGN_V2_XSUB/0/enable");
    string regname;
    for (uint p = 0; p < n_ports; p++)
    {
        uint32_t sdp_data = (*ptr == true) ? 1 : 0;
        regname = "mm/0/REG_BSN_ALIGN_V2_XSUB/" + to_string(p) + "/enable";
        retval &= Write(regname, &sdp_data);
        ptr++;
    }
    return retval;
}

bool Periph_fpga::read_xst_rx_align_stream_enable(char *data)
{
    bool retval = true;
    bool *ptr = (bool *)data;

    uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_ALIGN_V2_XSUB/0/enable");
    string regname;
    for (uint p = 0; p < n_ports; p++)
    {
        uint32_t sdp_data = 0;
        regname = "mm/0/REG_BSN_ALIGN_V2_XSUB/" + to_string(p) + "/enable";
        retval &= Read(regname, &sdp_data);
        *ptr = (sdp_data == 0) ? false : true;
        ptr++;
    }
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_total_nof_packets_received(char *data)
{
    uint32_t sdata[2] = {0};
    bool retval = Read("mm/0/REG_DP_BLOCK_VALIDATE_ERR_XST/0/total_block_count", sdata);
    uint64_t rdata = (uint64_t)sdata[1] << 32 | sdata[0];
    memcpy(data, &rdata, sizeof(rdata));
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_total_nof_packets_discarded(char *data)
{
    uint32_t *ptr = (uint32_t *)data;
    bool retval = Read("mm/0/REG_DP_BLOCK_VALIDATE_ERR_XST/0/total_discarded_blocks", ptr);
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_total_nof_sync_received(char *data)
{
    uint32_t *ptr = (uint32_t *)data;
    bool retval = Read("mm/0/REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC_XST/0/nof_sync", ptr);
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_total_nof_sync_discarded(char *data)
{
    uint32_t *ptr = (uint32_t *)data;
    bool retval = Read("mm/0/REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC_XST/0/nof_sync_discarded", ptr);
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_bsn(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RING_RX_XST/0/bsn_at_sync");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data[2];
            regname = "mm/0/REG_BSN_MONITOR_V2_RING_RX_XST/" + to_string(p) + "/bsn_at_sync";
            retval &= Read(regname, sdp_data);
            xst_ring_rx_bsn[p] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_xst_ring_rx_bsn_R");
    memcpy(data, xst_ring_rx_bsn, n_bytes);
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RING_RX_XST/0/nof_sop");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RING_RX_XST/" + to_string(p) + "/nof_sop";
            retval &= Read(regname, &sdp_data);
            xst_ring_rx_nof_packets[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_xst_ring_rx_nof_packets_R");
    memcpy(data, xst_ring_rx_nof_packets, n_bytes);
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RING_RX_XST/0/nof_valid");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RING_RX_XST/" + to_string(p) + "/nof_valid";
            retval &= Read(regname, &sdp_data);
            xst_ring_rx_nof_valid[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_xst_ring_rx_nof_valid_R");
    memcpy(data, xst_ring_rx_nof_valid, n_bytes);
    return retval;
}

bool Periph_fpga::read_xst_ring_rx_latency(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RING_RX_XST/0/latency");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RING_RX_XST/" + to_string(p) + "/latency";
            retval &= Read(regname, &sdp_data);
            xst_ring_rx_latency[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_xst_ring_rx_latency_R");
    memcpy(data, xst_ring_rx_latency, n_bytes);
    return retval;
}

bool Periph_fpga::read_xst_rx_align_bsn(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_XSUB/0/bsn_at_sync");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data[2];
            regname = "mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_XSUB/" + to_string(p) + "/bsn_at_sync";
            retval &= Read(regname, sdp_data);
            xst_rx_align_bsn[p] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }
    memcpy(data, xst_rx_align_bsn, sizeof(xst_rx_align_bsn));
    return retval;
}

bool Periph_fpga::read_xst_rx_align_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_XSUB/0/nof_sop");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_XSUB/" + to_string(p) + "/nof_sop";
            retval &= Read(regname, &sdp_data);
            xst_rx_align_nof_packets[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    memcpy(data, xst_rx_align_nof_packets, sizeof(xst_rx_align_nof_packets));
    return retval;
}

bool Periph_fpga::read_xst_rx_align_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_XSUB/0/nof_valid");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_XSUB/" + to_string(p) + "/nof_valid";
            retval &= Read(regname, &sdp_data);
            xst_rx_align_nof_valid[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    memcpy(data, xst_rx_align_nof_valid, sizeof(xst_rx_align_nof_valid));
    return retval;
}

bool Periph_fpga::read_xst_rx_align_latency(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_XSUB/0/latency");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_XSUB/" + to_string(p) + "/latency";
            retval &= Read(regname, &sdp_data);
            xst_rx_align_latency[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    memcpy(data, xst_rx_align_latency, sizeof(xst_rx_align_latency));
    return retval;
}

bool Periph_fpga::read_xst_aligned_bsn(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        retval &= Read("mm/0/REG_BSN_MONITOR_V2_ALIGNED_XSUB/0/bsn_at_sync", sdp_data);
        xst_aligned_bsn = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        return retval;
    }
    memcpy(data, &xst_aligned_bsn, sizeof(xst_aligned_bsn));
    return retval;
}

bool Periph_fpga::read_xst_aligned_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        retval &= Read("mm/0/REG_BSN_MONITOR_V2_ALIGNED_XSUB/0/nof_sop", &sdp_data);
        xst_aligned_nof_packets = (int32_t)sdp_data;
        return retval;
    }
    memcpy(data, &xst_aligned_nof_packets, sizeof(xst_aligned_nof_packets));
    return retval;
}

bool Periph_fpga::read_xst_aligned_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        retval &= Read("mm/0/REG_BSN_MONITOR_V2_ALIGNED_XSUB/0/nof_valid", &sdp_data);
        xst_aligned_nof_valid = (int32_t)sdp_data;
        return retval;
    }
    memcpy(data, &xst_aligned_nof_valid, sizeof(xst_aligned_nof_valid));
    return retval;
}

bool Periph_fpga::read_xst_aligned_latency(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data;
        retval &= Read("mm/0/REG_BSN_MONITOR_V2_ALIGNED_XSUB/0/latency", &sdp_data);
        xst_aligned_latency = (int32_t)sdp_data;
        return retval;
    }
    memcpy(data, &xst_aligned_latency, sizeof(xst_aligned_latency));
    return retval;
}

bool Periph_fpga::read_xst_ring_tx_bsn(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RING_TX_XST/0/bsn_at_sync");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data[2];
            regname = "mm/0/REG_BSN_MONITOR_V2_RING_TX_XST/" + to_string(p) + "/bsn_at_sync";
            retval &= Read(regname, sdp_data);
            xst_ring_tx_bsn[p] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }

    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_xst_ring_tx_bsn_R");
    memcpy(data, xst_ring_tx_bsn, n_bytes);
    return retval;
}

bool Periph_fpga::read_xst_ring_tx_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RING_TX_XST/0/nof_sop");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RING_TX_XST/" + to_string(p) + "/nof_sop";
            retval &= Read(regname, &sdp_data);
            xst_ring_tx_nof_packets[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_xst_ring_tx_nof_packets_R");
    memcpy(data, xst_ring_tx_nof_packets, n_bytes);
    return retval;
}

bool Periph_fpga::read_xst_ring_tx_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RING_TX_XST/0/nof_valid");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RING_TX_XST/" + to_string(p) + "/nof_valid";
            retval &= Read(regname, &sdp_data);
            xst_ring_tx_nof_valid[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_xst_ring_tx_nof_valid_R");
    memcpy(data, xst_ring_tx_nof_valid, n_bytes);
    return retval;
}

bool Periph_fpga::read_xst_ring_tx_latency(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RING_TX_XST/0/latency");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_MONITOR_V2_RING_TX_XST/" + to_string(p) + "/latency";
            retval &= Read(regname, &sdp_data);
            xst_ring_tx_latency[p] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_xst_ring_tx_latency_R");
    memcpy(data, xst_ring_tx_latency, n_bytes);
    return retval;
}

bool Periph_fpga::read_xst_rx_align_nof_replaced_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_ALIGN_V2_XSUB/0/replaced_pkt_cnt");
        string regname;
        for (uint p = 0; p < n_ports; p++)
        {
            uint32_t sdp_data;
            regname = "mm/0/REG_BSN_ALIGN_V2_XSUB/" + to_string(p) + "/replaced_pkt_cnt";
            retval &= Read(regname, &sdp_data);
            memcpy(&(xst_rx_align_nof_replaced_packets[p]), &sdp_data, sizeof(sdp_data));
        }
        return retval;
    }
    memcpy(data, xst_rx_align_nof_replaced_packets, sizeof(xst_rx_align_nof_replaced_packets));
    return retval;
}

bool Periph_fpga::write_bf_ring_nof_transport_hops(const char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;
    string regname;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        regname = "mm/" + to_string(i) + "/REG_RING_LANE_INFO_BF/0/transport_nof_hops";
        retval &= Write(regname, ptr);
        ptr++;
    }
    return retval;
}

bool Periph_fpga::write_bf_ring_rx_clear_total_counts(const char *data)
{
    bool retval = true;
    if (data[0] == 1)
    {
        uint32_t clear = 1;
        string regname1;
        string regname2;
        for (uint32_t i = 0; i < nBeamsets; i++)
        {
            regname1 = "mm/" + to_string(i) + "/REG_DP_BLOCK_VALIDATE_ERR_BF/0/clear";
            regname2 = "mm/" + to_string(i) + "/REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC_BF/0/clear";
            retval &= Write(regname1, &clear);
            retval &= Write(regname2, &clear);
        }
    }
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_clear_total_counts(char *data)
{
    bool *ptr = (bool *)data;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        *ptr = 0;
        ptr++;
    }
    return true;
}

bool Periph_fpga::write_bf_rx_align_stream_enable(const char *data)
{
    bool retval = true;
    bool *ptr = (bool *)data;
    uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_ALIGN_V2_BF/0/enable");
    string regname;
    uint32_t sdp_data;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        for (uint p = 0; p < n_ports; p++)
        {
            sdp_data = (*ptr == true) ? 1 : 0;
            regname = "mm/" + to_string(i) + "/REG_BSN_ALIGN_V2_BF/" + to_string(p) + "/enable";
            retval &= Write(regname, &sdp_data);
            ptr++;
        }
    }
    return retval;
}

bool Periph_fpga::read_bf_rx_align_stream_enable(char *data)
{
    bool retval = true;
    bool *ptr = (bool *)data;
    uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_ALIGN_V2_BF/0/enable");
    string regname;
    uint32_t sdp_data;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        for (uint p = 0; p < n_ports; p++)
        {
            regname = "mm/" + to_string(i) + "/REG_BSN_ALIGN_V2_BF/" + to_string(p) + "/enable";
            retval &= Read(regname, &sdp_data);
            *ptr = (sdp_data == 0) ? false : true;
            ptr++;
        }
    }
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_total_nof_packets_received(char *data)
{
    uint64_t *rdata = new uint64_t[nBeamsets];

    bool retval = true;

    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        string regname = "mm/" + to_string(i) + "/REG_DP_BLOCK_VALIDATE_ERR_BF/0/total_block_count";
        uint32_t sdata[2] = {0};
        retval = Read(regname, sdata);
        rdata[i] = (uint64_t)sdata[1] << 32 | sdata[0];
    }
    memcpy(data, rdata, sizeof(uint64_t) * nBeamsets);
    my_delete_arr(rdata);
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_total_nof_packets_discarded(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;
    string regname;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        regname = "mm/" + to_string(i) + "/REG_DP_BLOCK_VALIDATE_ERR_BF/0/total_discarded_blocks";
        retval &= Read(regname, ptr);
        ptr++;
    }
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_total_nof_sync_received(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;
    string regname;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        regname = "mm/" + to_string(i) + "/REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC_BF/0/nof_sync";
        retval &= Read(regname, ptr);
        ptr++;
    }
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_total_nof_sync_discarded(char *data)
{
    bool retval = true;
    uint32_t *ptr = (uint32_t *)data;
    string regname;
    for (uint32_t i = 0; i < nBeamsets; i++)
    {
        regname = "mm/" + to_string(i) + "/REG_DP_BLOCK_VALIDATE_BSN_AT_SYNC_BF/0/nof_sync_discarded";
        retval &= Read(regname, ptr);
        ptr++;
    }
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_bsn(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data[2];
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RING_RX_BF/0/bsn_at_sync";
            retval &= Read(regname, sdp_data);
            bf_ring_rx_bsn[i] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }
    memcpy(data, bf_ring_rx_bsn, sizeof(bf_ring_rx_bsn[0]) * nBeamsets);
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RING_RX_BF/0/nof_sop";
            retval &= Read(regname, &sdp_data);
            bf_ring_rx_nof_packets[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_ring_rx_nof_packets_R");
    memcpy(data, bf_ring_rx_nof_packets, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RING_RX_BF/0/nof_valid";
            retval &= Read(regname, &sdp_data);
            bf_ring_rx_nof_valid[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_ring_rx_nof_valid_R");
    memcpy(data, bf_ring_rx_nof_valid, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_ring_rx_latency(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RING_RX_BF/0/latency";
            retval &= Read(regname, &sdp_data);
            bf_ring_rx_latency[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_ring_rx_latency_R");
    memcpy(data, bf_ring_rx_latency, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_rx_align_bsn(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_BF/0/bsn_at_sync");
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            for (uint p = 0; p < n_ports; p++)
            {
                uint32_t sdp_data[2];
                regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RX_ALIGN_BF/" + to_string(p) + "/bsn_at_sync";
                retval &= Read(regname, sdp_data);
                bf_rx_align_bsn[i * n_ports + p] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
            }
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_rx_align_bsn_R");
    memcpy(data, bf_rx_align_bsn, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_rx_align_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_BF/0/nof_sop");
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            for (uint p = 0; p < n_ports; p++)
            {
                uint32_t sdp_data;
                regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RX_ALIGN_BF/" + to_string(p) + "/nof_sop";
                retval &= Read(regname, &sdp_data);
                bf_rx_align_nof_packets[i * n_ports + p] = (int32_t)sdp_data;
            }
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_rx_align_nof_packets_R");
    memcpy(data, bf_rx_align_nof_packets, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_rx_align_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_BF/0/nof_valid");
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            for (uint p = 0; p < n_ports; p++)
            {
                uint32_t sdp_data;
                regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RX_ALIGN_BF/" + to_string(p) + "/nof_valid";
                retval &= Read(regname, &sdp_data);
                bf_rx_align_nof_valid[i * n_ports + p] = (int32_t)sdp_data;
            }
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_rx_align_nof_valid_R");
    memcpy(data, bf_rx_align_nof_valid, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_rx_align_latency(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t n_ports = mmap->getNPorts("mm/0/REG_BSN_MONITOR_V2_RX_ALIGN_BF/0/latency");
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            for (uint p = 0; p < n_ports; p++)
            {
                uint32_t sdp_data;
                regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RX_ALIGN_BF/" + to_string(p) + "/latency";
                retval &= Read(regname, &sdp_data);
                bf_rx_align_latency[i * n_ports + p] = (int32_t)sdp_data;
            }
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_rx_align_latency_R");
    memcpy(data, bf_rx_align_latency, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_aligned_bsn(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        uint32_t sdp_data[2];
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_ALIGNED_BF/0/bsn_at_sync";
            retval &= Read(regname, sdp_data);
            bf_aligned_bsn[i] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_aligned_bsn_R");
    memcpy(data, bf_aligned_bsn, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_aligned_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_ALIGNED_BF/0/nof_sop";
            retval &= Read(regname, &sdp_data);
            bf_aligned_nof_packets[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_aligned_nof_packets_R");
    memcpy(data, bf_aligned_nof_packets, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_aligned_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_ALIGNED_BF/0/nof_valid";
            retval &= Read(regname, &sdp_data);
            bf_aligned_nof_valid[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_aligned_nof_valid_R");
    memcpy(data, bf_aligned_nof_valid, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_aligned_latency(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_ALIGNED_BF/0/latency";
            retval &= Read(regname, &sdp_data);
            bf_aligned_latency[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_aligned_latency_R");
    memcpy(data, bf_aligned_latency, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_ring_tx_bsn(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data[2];
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RING_TX_BF/0/bsn_at_sync";
            retval &= Read(regname, sdp_data);
            bf_ring_tx_bsn[i] = (((int64_t)sdp_data[1] << 32) + sdp_data[0]);
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_ring_tx_bsn_R");
    memcpy(data, bf_ring_tx_bsn, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_ring_tx_nof_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RING_TX_BF/0/nof_sop";
            retval &= Read(regname, &sdp_data);
            bf_ring_tx_nof_packets[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_ring_tx_nof_packets_R");
    memcpy(data, bf_ring_tx_nof_packets, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_ring_tx_nof_valid(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RING_TX_BF/0/nof_valid";
            retval &= Read(regname, &sdp_data);
            bf_ring_tx_nof_valid[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_ring_tx_nof_valid_R");
    memcpy(data, bf_ring_tx_nof_valid, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_ring_tx_latency(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_MONITOR_V2_RING_TX_BF/0/latency";
            retval &= Read(regname, &sdp_data);
            bf_ring_tx_latency[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_ring_tx_latency_R");
    memcpy(data, bf_ring_tx_latency, n_bytes);
    return retval;
}

bool Periph_fpga::read_bf_rx_align_nof_replaced_packets(char *data, int mode)
{
    bool retval = true;

    if (mode == R_UCP)
    {
        string regname;
        for (uint i = 0; i < nBeamsets; i++)
        {
            uint32_t sdp_data;
            regname = "mm/" + to_string(i) + "/REG_BSN_ALIGN_V2_BF/0/replaced_pkt_cnt";
            retval &= Read(regname, &sdp_data);
            bf_rx_align_nof_replaced_packets[i] = (int32_t)sdp_data;
        }
        return retval;
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_bf_rx_align_nof_replaced_packets_R");
    memcpy(data, bf_rx_align_nof_replaced_packets, n_bytes);
    return retval;
}

bool Periph_fpga::write_wdi_override(char *data)
{
    uint32_t sdp_data = 0xB007FAC7;
    return Write("mm/0/PIO_WDI/0/wdi_override", &sdp_data);
}

bool Periph_fpga::read_reg_map(uint32_t rom_version)
{
    uint32_t nvalues = REG_ADDR_ROM_SYSTEM_SPAN;
    uint32_t addr = REG_ADDR_ROM_SYSTEM;
    uint32_t *data = new uint32_t[nvalues];
    memset(data, 0, nvalues * sizeof(uint32_t));

    LOG_F(INFO, "read regmap for rom version %d", rom_version);

    bool ret = ucp->readRegister(addr, nvalues, data);
    ucp_reads++;
    if (ret == false)
    {
        ucp_read_fails++;
    }
    ucp_read_retries += ucp->getRetries();

    if (ret == false)
    {
        my_delete_arr(data);
        LOG_F(ERROR, "ucp->readRegister failed");
        return false;
    }

    for (uint i = 0; i < nvalues; i++)
    {
        data[i] = ntohl(data[i]);
    }

    string reg_map_str;

    // rom version 3 and higher returns a compressed regmap in zlib format.
    if (rom_version >= 3)
    {
        // get size of compressed data, end if data == 0
        uint32_t data_size = 0;
        for (uint i = 0; i < nvalues; i++)
        {
            if (data[i] != 0)
            {
                data_size++;
            }
        }
        unsigned long compressed_data_size = data_size * sizeof(uint32_t);
        unsigned char *ch_ptr = (unsigned char *)data;

        LOG_F(DEBUG, "compressed_data_size %lu", compressed_data_size);
        unsigned long uncompressed_data_size = nvalues * sizeof(uint32_t) * 4;
        unsigned char *uncompressed_data_ptr = new unsigned char[uncompressed_data_size];

        // uncompress data
        int result = uncompress(uncompressed_data_ptr, &uncompressed_data_size, ch_ptr, compressed_data_size);
        LOG_F(DEBUG, "uncompressed_data_size %lu", uncompressed_data_size);

        char *uncompressed_data = new char[uncompressed_data_size];

        if (result == Z_OK)
        {
            memcpy(uncompressed_data, uncompressed_data_ptr, uncompressed_data_size);
            reg_map_str = uncompressed_data;
            LOG_F(DEBUG, "reg_map_str=%s", reg_map_str.c_str());
        }
        else
        {
            LOG_F(ERROR, "uncompress error %d", result);
        }

        my_delete_arr(uncompressed_data_ptr);
        my_delete_arr(uncompressed_data);
    }
    else
    {
        char *str_ptr = (char *)data;
        reg_map_str = str_ptr;
    }

    istringstream iss_regmap(reg_map_str);
    mmap_to_regmap(mmap, iss_regmap);
    my_delete_arr(data);
    return true;
}

bool Periph_fpga::read_spectral_inversion(char *data)
{
    uint32_t sdp_data;
    bool retval = Read("mm/0/REG_SI/0/enable", &sdp_data);
    bool specinv[C_S_pn] = {false};
    for (uint i=0; i< C_S_pn; i++) {
        if (sdp_data & (1 << i)) {
            specinv[i] = true;
        }
    }
    uint32_t n_bytes = pointmap->getFpgaPointSizeBytes("FPGA_spectral_inversion_R");
    memcpy(data, specinv, n_bytes);
    return retval;
}

bool Periph_fpga::write_spectral_inversion(const char *data)
{
    bool *ptr = (bool *)data;
    uint32_t sdp_data = 0;
    for (uint i=0; i< C_S_pn; i++) {
        if (ptr[i] == true) {
            sdp_data += (1 << i);
        }
    }
    return Write("mm/0/REG_SI/0/enable", &sdp_data);
}
