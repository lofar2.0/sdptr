/* *************************************************************************
 * Copyright 2023
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *********************************************************************** */

/* *************************************************************************
 * Author:
 * . Leon Hiemstra
 * . Pieter Donker
 * Purpose:
 * . opc-ua to ucp translator
 * Description:
 * . class for fpga registers (peripherals) that need some more actions
 * *********************************************************************** */

#ifndef __PERIPH_FPGA_H__
#define __PERIPH_FPGA_H__

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <chrono>

#include "../io/ucp.h"
#include "../tools/util.h"
#include "../registers.h"
#include "../constants.h"

// cmd_id, id to glue an opc-ua cp/mp point to a fpga read/write function.
// cmd_id's < MAX_ID_INTERN_POINT can always be read
#define GLOBAL_NODE_INDEX 1
#define BOOT_IMAGE 2
#define UCP_STATUS 3
#define UCP_BLOCK 4
#define MAX_ID_INTERN_POINT 10

#define SYSTEM_INFO 51
#define STAMPS 52
#define NOTE 53
#define FIRMWARE_VERSION 54
#define HARDWARE_VERSION 55
#define TEMP 60
#define PPS_PRESENT 100
#define PPS_EXPECTED_CNT 101
#define PPS_CAPTURE_CNT 102
#define PPS_ERROR_CNT 103
#define TIME_SINCE_LAST_PPS 104
#define MONITOR_PPS_OFFSET_TIME 105
#define PROCESSING_ENABLE 110
#define CURRENT_BSN 119
#define SIGNAL_INPUT_BSN 120
#define SIGNAL_INPUT_NOF_BLOCKS 121
#define SIGNAL_INPUT_NOF_SAMPLES 122
#define SIGNAL_INPUT_MEAN 123
#define SIGNAL_INPUT_RMS 124
#define SIGNAL_INPUT_STD 125
#define SIGNAL_INPUT_SAMPLES_DELAY 126
#define SIGNAL_INPUT_DATA_BUFFER 127
#define SIGNAL_INPUT_HISTOGRAM 128
#define JESD204B_CSR_RBD_COUNT 140
#define JESD204B_CSR_DEV_SYNCN 141
#define JESD204B_RX_ERR0 142
#define JESD204B_RX_ERR1 143
#define JESD204B_CLEAR_RX_ERR0 144
#define JESD204B_CLEAR_RX_ERR1 145
#define JESD_CTRL 146
#define SUBBAND_WEIGHTS 150
#define SUBBAND_WEIGHTS_CROSS 151
#define SPECTRAL_INVERSION 152
#define SUBBAND_FIR_FILTER_COEFFICIENTS 153
#define SST_OFFLOAD_ENABLE 200
#define SST_OFFLOAD_WEIGHTED_SUBBANDS 202
#define SST_OFFLOAD_HDR_ETH_DESTINATION_MAC 210
#define SST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS 211
#define SST_OFFLOAD_HDR_UDP_DESTINATION_PORT 212
#define SST_OFFLOAD_NOF_PACKETS 220
#define SST_OFFLOAD_NOF_VALID 221
#define SST_OFFLOAD_BSN 222
#define BST_OFFLOAD_ENABLE 300
#define BST_OFFLOAD_HDR_ETH_DESTINATION_MAC 310
#define BST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS 311
#define BST_OFFLOAD_HDR_UDP_DESTINATION_PORT 312
#define BST_OFFLOAD_NOF_PACKETS 320
#define BST_OFFLOAD_NOF_VALID 321
#define BST_OFFLOAD_BSN 322
#define XST_OFFLOAD_ENABLE 400
#define XST_SUBBAND_SELECT 401
#define XST_INTEGRATION_INTERVAL 402
#define XST_START_TIME 403
#define XST_PROCESSING_ENABLE 404
#define XST_OFFLOAD_NOF_CROSSLETS 406
#define XST_OFFLOAD_HDR_ETH_DESTINATION_MAC 410
#define XST_OFFLOAD_HDR_IP_DESTINATION_ADDRESS 411
#define XST_OFFLOAD_HDR_UDP_DESTINATION_PORT 412
#define XST_INPUT_SYNC_AT_BSN 420
#define XST_OUTPUT_SYNC_BSN 421
#define XST_RX_ALIGN_NOF_REPLACED_PACKETS 425
#define XST_RX_ALIGN_STREAM_ENABLE 426
#define XST_OFFLOAD_BSN 430
#define XST_OFFLOAD_NOF_PACKETS 431
#define XST_OFFLOAD_NOF_VALID 432
#define XST_RX_ALIGN_BSN 440
#define XST_RX_ALIGN_NOF_PACKETS 441
#define XST_RX_ALIGN_NOF_VALID 442
#define XST_RX_ALIGN_LATENCY 443
#define XST_ALIGNED_BSN 450
#define XST_ALIGNED_NOF_PACKETS 451
#define XST_ALIGNED_NOF_VALID 452
#define XST_ALIGNED_LATENCY 453
#define XST_RING_RX_BSN 460
#define XST_RING_RX_NOF_PACKETS 461
#define XST_RING_RX_NOF_VALID 462
#define XST_RING_RX_LATENCY 463
#define XST_RING_TX_BSN 470
#define XST_RING_TX_NOF_PACKETS 471
#define XST_RING_TX_NOF_VALID 472
#define XST_RING_TX_LATENCY 473
#define XST_RING_NOF_TRANSPORT_HOPS 480
#define XST_RING_RX_CLEAR_TOTAL_COUNTS 490
#define XST_RING_RX_TOTAL_NOF_PACKETS_RECEIVED 491
#define XST_RING_RX_TOTAL_NOF_PACKETS_DISCARDED 492
#define XST_RING_RX_TOTAL_NOF_SYNC_RECEIVED 493
#define XST_RING_RX_TOTAL_NOF_SYNC_DISCARDED 494
#define BEAMLET_OUTPUT_ENABLE 500
#define BEAMLET_OUTPUT_SCALE 501
#define BEAMLET_OUTPUT_HDR_ETH_DESTINATION_MAC 510
#define BEAMLET_OUTPUT_HDR_IP_DESTINATION_ADDRESS 511
#define BEAMLET_OUTPUT_HDR_UDP_DESTINATION_PORT 512
#define BEAMLET_OUTPUT_HDR_ETH_SOURCE_MAC 513
#define BEAMLET_OUTPUT_HDR_IP_SOURCE_ADDRESS 514
#define BEAMLET_OUTPUT_HDR_UDP_SOURCE_PORT 515
#define BEAMLET_OUTPUT_NOF_DESTINATIONS 516
#define BEAMLET_OUTPUT_MULTIPLE_HDR_ETH_DESTINATION_MAC 517
#define BEAMLET_OUTPUT_MULTIPLE_HDR_IP_DESTINATION_ADDRESS 518
#define BEAMLET_OUTPUT_MULTIPLE_HDR_UDP_DESTINATION_PORT 519
#define BEAMLET_OUTPUT_NOF_DESTINATIONS_ACT 520
#define BEAMLET_OUTPUT_NOF_DESTINATIONS_MAX 521
#define BEAMLET_OUTPUT_NOF_BLOCKS_PER_PACKET 522
#define BEAMLET_SUBBAND_SELECT 525
#define BEAMLET_OUTPUT_NOF_PACKETS 531
#define BEAMLET_OUTPUT_NOF_VALID 532
#define BEAMLET_OUTPUT_BSN 533
#define BEAMLET_OUTPUT_READY 534
#define BEAMLET_OUTPUT_XON 535
#define BEAMLET_OUTPUT_10GBE_TX_NOF_FRAMES 536
#define BEAMLET_OUTPUT_10GBE_RX_NOF_FRAMES 537
#define BF_WEIGHTS 600
#define BF_WEIGHTS_XX_YY 610
#define BF_WEIGHTS_XY_YX 611
#define BF_WEIGHTS_XX_XY_YX_YY 612
#define BF_WEIGHTS_XX 613
#define BF_WEIGHTS_XY 614
#define BF_WEIGHTS_YX 615
#define BF_WEIGHTS_YY 616
#define BF_WEIGHTS_PP 617
#define BF_RX_ALIGN_STREAM_ENABLE 620
#define BF_RX_ALIGN_NOF_REPLACED_PACKETS 621
#define BF_RING_TX_BSN 630
#define BF_RING_TX_NOF_PACKETS 631
#define BF_RING_TX_NOF_VALID 632
#define BF_RING_TX_LATENCY 633
#define BF_RING_RX_BSN 640
#define BF_RING_RX_NOF_PACKETS 641
#define BF_RING_RX_NOF_VALID 642
#define BF_RING_RX_LATENCY 643
#define BF_RX_ALIGN_BSN 650
#define BF_RX_ALIGN_NOF_PACKETS 651
#define BF_RX_ALIGN_NOF_VALID 652
#define BF_RX_ALIGN_LATENCY 653
#define BF_ALIGNED_BSN 660
#define BF_ALIGNED_NOF_PACKETS 661
#define BF_ALIGNED_NOF_VALID 662
#define BF_ALIGNED_LATENCY 663
#define BF_RING_NOF_TRANSPORT_HOPS 680
#define BF_RING_RX_CLEAR_TOTAL_COUNTS 690
#define BF_RING_RX_TOTAL_NOF_PACKETS_RECEIVED 691
#define BF_RING_RX_TOTAL_NOF_PACKETS_DISCARDED 692
#define BF_RING_RX_TOTAL_NOF_SYNC_RECEIVED 693
#define BF_RING_RX_TOTAL_NOF_SYNC_DISCARDED 694
#define RING_NODE_OFFSET 700
#define RING_NOF_NODES 701
#define RING_USE_CABLE_TO_NEXT_RN 705
#define RING_USE_CABLE_TO_PREVIOUS_RN 706
#define SDP_INFO_STATION_ID 800
#define SDP_INFO_ANTENNA_FIELD_INDEX 801
#define SDP_INFO_OBSERVATION_ID 802
#define SDP_INFO_NYQUIST_SAMPLING_ZONE_INDEX 803
#define SDP_INFO_ANTENNA_BAND_INDEX 804
#define SDP_INFO_F_ADC 805
#define SDP_INFO_FSUB_TYPE 806
#define SDP_INFO_BLOCK_PERIOD 807
#define WG_ENABLE 900
#define WG_AMPLITUDE 901
#define WG_PHASE 902
#define WG_FREQUENCY 903
#define FLASH_ADDR 951
#define FLASH_PROTECT 952
#define FLASH_ERASE_SECTOR 953
#define FLASH_PAGES 954
#define SCRAP 999
#define DDR_GIGABYTES 1000
#define DDR_CALIBRATED 1001

class Periph_fpga
{
private:
    UCP *ucp;
    CMMap *mmap;
    uint32_t GlobalNr;
    uint32_t nFilterbanks;
    uint32_t nBeamsets;

    CPointMap *pointmap;

    // Masked is set by the client and is used to set the fpga's to communinicate with, true is communicate, false do not.
    bool Masked;
    // Online is the communication state with the fpga true if no errors, false is no communication.
    // state is set in read_system_info() function that is called every second from the monitor() function.
    bool Online;

    bool ucp_block_comm;
    uint64_t ucp_reads;
    uint64_t ucp_read_retries;
    uint64_t ucp_read_fails;
    uint64_t ucp_writes;
    uint64_t ucp_write_retries;
    uint64_t ucp_write_fails;

    std::string current_design_name;
    uint32_t current_hw_version;
    std::string current_fw_version;
    int32_t current_image; // current_image, -1 = no image available, 0 = factory-image, 1 = user-image

    bool signal_input_sync_timeout;
    int64_t signal_input_bsn;
    int32_t signal_input_nof_blocks;
    int32_t signal_input_nof_samples;
    double signal_input_mean[C_S_pn];
    double signal_input_rms[C_S_pn];
    double signal_input_std[C_S_pn];
    double xst_start_time;
    int64_t xst_input_bsn_at_sync;
    int64_t xst_output_sync_bsn;
    int32_t sst_offload_nof_packets;
    int32_t sst_offload_nof_valid;
    int64_t sst_offload_bsn;
    int32_t bst_offload_nof_packets[C_N_beamsets_sdp];
    int32_t bst_offload_nof_valid[C_N_beamsets_sdp];
    int64_t bst_offload_bsn[C_N_beamsets_sdp];
    int32_t xst_offload_nof_packets;
    int32_t xst_offload_nof_valid;
    int64_t xst_offload_bsn;
    int32_t beamlet_output_nof_packets[C_N_beamsets_sdp];
    int32_t beamlet_output_nof_valid[C_N_beamsets_sdp];
    int64_t beamlet_output_bsn[C_N_beamsets_sdp];
    bool beamlet_output_ready[C_N_beamsets_sdp];
    bool beamlet_output_xon[C_N_beamsets_sdp];
    uint64_t beamlet_output_10gbe_tx_nof_frames;
    uint64_t beamlet_output_10gbe_rx_nof_frames;
    uint32_t jesd_csr_rbd_count[C_S_pn];
    uint32_t jesd_csr_dev_syncn[C_S_pn];
    uint32_t jesd_rx_err0[C_S_pn];
    uint32_t jesd_rx_err1[C_S_pn];
    bool xst_processing_enable;
    double monitor_pps_offset_time;
    uint32_t pps_expected_cnt;
    bool pps_present;
    uint32_t pps_capture_cnt;
    uint32_t pps_error_cnt;
    int64_t xst_ring_rx_bsn[C_N_pn_max];
    int32_t xst_ring_rx_nof_packets[C_N_pn_max];
    int32_t xst_ring_rx_nof_valid[C_N_pn_max];
    int32_t xst_ring_rx_latency[C_N_pn_max];
    int64_t xst_rx_align_bsn[C_P_sq];
    int32_t xst_rx_align_nof_packets[C_P_sq];
    int32_t xst_rx_align_nof_valid[C_P_sq];
    int32_t xst_rx_align_latency[C_P_sq];
    int32_t xst_rx_align_nof_replaced_packets[C_P_sq];
    int64_t xst_aligned_bsn;
    int32_t xst_aligned_nof_packets;
    int32_t xst_aligned_nof_valid;
    int32_t xst_aligned_latency;
    int64_t xst_ring_tx_bsn[C_N_pn_max];
    int32_t xst_ring_tx_nof_packets[C_N_pn_max];
    int32_t xst_ring_tx_nof_valid[C_N_pn_max];
    int32_t xst_ring_tx_latency[C_N_pn_max];

    int64_t bf_ring_rx_bsn[C_N_beamsets_sdp];
    int32_t bf_ring_rx_nof_packets[C_N_beamsets_sdp];
    int32_t bf_ring_rx_nof_valid[C_N_beamsets_sdp];
    int32_t bf_ring_rx_latency[C_N_beamsets_sdp];
    int64_t bf_rx_align_bsn[C_N_beamsets_sdp * C_P_sum];
    int32_t bf_rx_align_nof_packets[C_N_beamsets_sdp * C_P_sum];
    int32_t bf_rx_align_nof_valid[C_N_beamsets_sdp * C_P_sum];
    int32_t bf_rx_align_latency[C_N_beamsets_sdp * C_P_sum];
    int32_t bf_rx_align_nof_replaced_packets[C_N_beamsets_sdp * C_P_sum];
    int64_t bf_aligned_bsn[C_N_beamsets_sdp];
    int32_t bf_aligned_nof_packets[C_N_beamsets_sdp];
    int32_t bf_aligned_nof_valid[C_N_beamsets_sdp];
    int32_t bf_aligned_latency[C_N_beamsets_sdp];
    int64_t bf_ring_tx_bsn[C_N_beamsets_sdp];
    int32_t bf_ring_tx_nof_packets[C_N_beamsets_sdp];
    int32_t bf_ring_tx_nof_valid[C_N_beamsets_sdp];
    int32_t bf_ring_tx_latency[C_N_beamsets_sdp];

    std::map<uint32_t, char *> rw_values;

    bool flash_active;
    uint32_t flash_addr;
    // std::ofstream rbf_wf;
    uint32_t Flash_fact_sector_start, Flash_fact_sector_end, Flash_user_sector_start, Flash_user_sector_end, Flash_select,
        Flash_pages_per_sector, Flash_page_size_words, Flash_page_size_bytes, Flash_sector_size_words, Flash_sector_size_bytes;

    struct timespec monitor_start_time;
    bool run_processing_enable;

    bool Read(const std::string &addr_str, uint32_t *data_ptr, bool use_mask_shift = true, uint32_t offset = 0, uint32_t nval = 0);
    bool ReadFifo(const std::string &addr_str, uint32_t *data_ptr, uint32_t nvalues);
    bool Write(const std::string &addr_str, uint32_t *data_ptr, bool wait_for_ack = true, bool use_shift_mask = true, uint32_t offset = 0, uint32_t nval = 0);
    bool WriteFifo(const std::string &addr_str, uint32_t *data_ptr, uint32_t nvalues);

    uint32_t mask_shift(const uint32_t shift, const uint32_t mask, uint32_t data);
    uint32_t mask_shift(const std::string &addr_str, uint32_t data);
    uint32_t shift_mask(const uint32_t shift, const uint32_t mask, uint32_t data);
    uint32_t shift_mask(const std::string &addr_str, uint32_t data);

    bool clear_fw_values();

    bool read_flash_addr(char *data);
    bool write_flash_addr(const char *data);
    bool read_flash_protect(char *data);
    bool write_flash_protect(const char *data);
    bool read_flash_erase_sector(char *data);
    bool write_flash_erase_sector(const char *data);
    bool read_flash_pages(char *data);
    bool write_flash_pages(const char *data);
    bool wait_while_epcs_busy(const std::chrono::milliseconds msec_timeout);

    bool read_boot_image(char *data);
    bool write_boot_image(const char *data);
    bool wait_while_remu_busy(int64_t sleeptime);

    std::string read_design_name();
    std::string read_design_note();

    bool read_system_info(char *data);
    bool read_hardware_version(char *data);
    bool read_firmware_version(char *data);
    bool read_stamps(char *data);

    bool read_global_node_index(char *data);

    bool read_fpga_temperature(char *data);

    bool read_fpga_scrap(char *data);
    bool write_fpga_scrap(const char *data);

    bool read_sst_offload_weighted_subbands(char *data);
    bool write_sst_offload_weighted_subbands(const char *data);
    bool write_sst_offload_enable(const char *data);
    bool read_sst_offload_nof_packets(char *data, int mode);
    bool read_sst_offload_nof_valid(char *data, int mode);
    bool read_sst_offload_bsn(char *data, int mode);

    bool write_bst_offload_enable(const char *data);
    bool read_bst_offload_nof_packets(char *data, int mode);
    bool read_bst_offload_nof_valid(char *data, int mode);
    bool read_bst_offload_bsn(char *data, int mode);

    bool write_beamlet_output_enable(const char *data);
    bool read_beamlet_output_scale(char *data);
    bool write_beamlet_output_scale(const char *data);
    bool read_beamlet_output_nof_packets(char *data, int mode);
    bool read_beamlet_output_nof_valid(char *data, int mode);
    bool read_beamlet_output_bsn(char *data, int mode);
    bool read_beamlet_output_ready(char *data, int mode);
    bool read_beamlet_output_xon(char *data, int mode);
    bool read_beamlet_output_10gbe_tx_nof_frames(char *data);
    bool read_beamlet_output_10gbe_rx_nof_frames(char *data);

    bool read_beamlet_output_nof_destinations(char *data);
    bool write_beamlet_output_nof_destinations(const char *data);
    bool read_beamlet_output_nof_destinations_act(char *data);
    bool read_beamlet_output_nof_destinations_max(char *data);
    bool read_beamlet_output_nof_blocks_per_packet(char *data);

    bool read_xst_subband_select(char *data);
    bool write_xst_subband_select(const char *data);
    bool read_xst_input_sync_at_bsn(char *data, int mode);
    bool read_xst_output_sync_bsn(char *data, int mode);
    bool read_xst_integration_interval(char *data);
    bool write_xst_integration_interval(const char *data);
    bool read_xst_start_time(char *data);
    bool write_xst_start_time(const char *data);
    bool read_xst_processing_enable(char *data);
    bool write_xst_processing_enable(const char *data);
    bool write_xst_offload_enable(const char *data);
    bool write_xst_offload_nof_crosslets(const char *data);
    bool read_xst_offload_nof_packets(char *data, int mode);
    bool read_xst_offload_nof_valid(char *data, int mode);
    bool read_xst_offload_bsn(char *data, int mode);

    bool read_eth_mac(char *data, const std::string &port_name, const std::string &field_name);
    bool write_eth_mac(const char *data, const std::string &port_name, const std::string &field_name, uint32_t max_fields = UINT32_MAX);
    bool read_ip_address(char *data, const std::string &port_name, const std::string &field_name);
    bool write_ip_address(const char *data, const std::string &port_name, const std::string &field_name, uint32_t max_fields = UINT32_MAX);
    bool read_udp_port(char *data, const std::string &port_name, const std::string &field_name);
    bool write_udp_port(const char *data, const std::string &port_name, const std::string &field_name, uint32_t max_fields = UINT32_MAX);

    bool write_processing_enable(const char *data);
    bool processing_enable(void);

    bool write_sdp_info_station_id(const char *data);
    bool write_sdp_info_antenna_field_index(const char *data);
    bool write_sdp_info_observation_id(const char *data);
    bool write_sdp_info_nyquist_sampling_zone_index(const char *data);
    bool write_sdp_info_antenna_band_index(const char *data);

    bool read_ddr_gigabytes(char *data);
    bool read_ddr_calibrated(char *data);

    bool write_wg_enable(const char *data);
    bool read_wg_amplitude(char *data);
    bool write_wg_amplitude(const char *data);
    bool read_wg_phase(char *data);
    bool write_wg_phase(const char *data);
    bool write_wg_frequency(const char *data);
    bool read_wg_frequency(char *data);

    bool write_signal_input_samples_delay(const char *data);
    bool read_signal_input_sync_timeout(char *data, int mode);
    bool read_current_bsn(char *data);
    bool read_signal_input_bsn(char *data, int mode);
    bool read_signal_input_nof_blocks(char *data, int mode);
    bool read_signal_input_nof_samples(char *data, int mode);
    bool read_signal_input_mean(char *data, int mode);
    bool read_signal_input_rms(char *data, int mode);
    bool read_signal_input_std(char *data, int mode);
    bool read_signal_input_data_buffer(char *data);

    bool read_jesd204b_csr_rbd_count(char *data, int mode);
    bool read_jesd204b_csr_dev_syncn(char *data, int mode);
    bool read_jesd204b_rx_err0(char *data, int mode);
    bool read_jesd204b_rx_err1(char *data, int mode);
    bool write_jesd204b_clear_rx_err0(const char *data);
    bool write_jesd204b_clear_rx_err1(const char *data);
    bool write_jesd_ctrl(const char *data);
    bool read_jesd_ctrl(char *data);

    bool write_subband_weights(const char *data, const std::string &port_name);
    bool read_subband_weights(char *data, const std::string &port_name);

    bool read_subband_fir_filter_coefficients(char *data);
    bool write_subband_fir_filter_coefficients(const char *data);

    bool read_beamlet_subband_select(char *data);
    bool write_beamlet_subband_select(const char *data);

    uint bf_weights_rw(const std::string &rw, uint set, uint pol_bf, uint a, uint pol, uint32_t *ptr);

    bool read_bf_weights(char *data);
    bool write_bf_weights(const char *data);

    bool read_bf_weights_xx_yy(char *data);
    bool write_bf_weights_xx_yy(const char *data);

    bool read_bf_weights_xy_yx(char *data);
    bool write_bf_weights_xy_yx(const char *data);

    bool read_bf_weights_xx_xy_yx_yy(char *data);
    bool write_bf_weights_xx_xy_yx_yy(const char *data);

    bool read_bf_weights_xx(char *data);
    bool write_bf_weights_xx(const char *data);

    bool read_bf_weights_xy(char *data);
    bool write_bf_weights_xy(const char *data);

    bool read_bf_weights_yx(char *data);
    bool write_bf_weights_yx(const char *data);

    bool read_bf_weights_yy(char *data);
    bool write_bf_weights_yy(const char *data);

    bool read_pps_expected_cnt(char *data);
    bool write_pps_expected_cnt(const char *data);
    bool read_pps_present(char *data, int mode);
    bool read_pps_capture_cnt(char *data, int mode);
    bool read_pps_error_cnt(char *data);
    bool read_time_since_last_pps(char *data);
    bool read_monitor_pps_offset_time(char *data, int mode);
    bool write_wdi_override(char *data);
    bool read_spectral_inversion(char *data);
    bool write_spectral_inversion(const char *data);

    bool write_ring_node_offset(const char *data);
    bool write_ring_nof_nodes(const char *data);
    bool write_ring_use_cable_to_next_rn(const char *data);
    bool write_ring_use_cable_to_previous_rn(const char *data);

    bool write_xst_ring_nof_transport_hops(const char *data);
    bool read_xst_ring_rx_clear_total_counts(char *data);
    bool write_xst_ring_rx_clear_total_counts(const char *data);
    bool read_xst_rx_align_stream_enable(char *data);
    bool write_xst_rx_align_stream_enable(const char *data);
    bool read_xst_rx_align_nof_replaced_packets(char *data, int mode);
    bool read_xst_ring_rx_total_nof_packets_received(char *data);
    bool read_xst_ring_rx_total_nof_packets_discarded(char *data);
    bool read_xst_ring_rx_total_nof_sync_received(char *data);
    bool read_xst_ring_rx_total_nof_sync_discarded(char *data);
    bool read_xst_ring_rx_bsn(char *data, int mode);
    bool read_xst_ring_rx_nof_packets(char *data, int mode);
    bool read_xst_ring_rx_nof_valid(char *data, int mode);
    bool read_xst_ring_rx_latency(char *data, int mode);
    bool read_xst_rx_align_bsn(char *data, int mode);
    bool read_xst_rx_align_nof_packets(char *data, int mode);
    bool read_xst_rx_align_nof_valid(char *data, int mode);
    bool read_xst_rx_align_latency(char *data, int mode);
    bool read_xst_aligned_bsn(char *data, int mode);
    bool read_xst_aligned_nof_packets(char *data, int mode);
    bool read_xst_aligned_nof_valid(char *data, int mode);
    bool read_xst_aligned_latency(char *data, int mode);
    bool read_xst_ring_tx_bsn(char *data, int mode);
    bool read_xst_ring_tx_nof_packets(char *data, int mode);
    bool read_xst_ring_tx_nof_valid(char *data, int mode);
    bool read_xst_ring_tx_latency(char *data, int mode);

    bool write_bf_ring_nof_transport_hops(const char *data);
    bool read_bf_ring_rx_clear_total_counts(char *data);
    bool write_bf_ring_rx_clear_total_counts(const char *data);
    bool read_bf_rx_align_stream_enable(char *data);
    bool write_bf_rx_align_stream_enable(const char *data);
    bool read_bf_rx_align_nof_replaced_packets(char *data, int mode);
    bool read_bf_ring_rx_total_nof_packets_received(char *data);
    bool read_bf_ring_rx_total_nof_packets_discarded(char *data);
    bool read_bf_ring_rx_total_nof_sync_received(char *data);
    bool read_bf_ring_rx_total_nof_sync_discarded(char *data);
    bool read_bf_ring_rx_bsn(char *data, int mode);
    bool read_bf_ring_rx_nof_packets(char *data, int mode);
    bool read_bf_ring_rx_nof_valid(char *data, int mode);
    bool read_bf_ring_rx_latency(char *data, int mode);
    bool read_bf_rx_align_bsn(char *data, int mode);
    bool read_bf_rx_align_nof_packets(char *data, int mode);
    bool read_bf_rx_align_nof_valid(char *data, int mode);
    bool read_bf_rx_align_latency(char *data, int mode);
    bool read_bf_aligned_bsn(char *data, int mode);
    bool read_bf_aligned_nof_packets(char *data, int mode);
    bool read_bf_aligned_nof_valid(char *data, int mode);
    bool read_bf_aligned_latency(char *data, int mode);
    bool read_bf_ring_tx_bsn(char *data, int mode);
    bool read_bf_ring_tx_nof_packets(char *data, int mode);
    bool read_bf_ring_tx_nof_valid(char *data, int mode);
    bool read_bf_ring_tx_latency(char *data, int mode);

    bool read_all_from_port(char *data, const uint32_t cmd_id, const std::string &port_name, const std::string &field_name);
    bool read_reg_map(uint32_t rom_version);

public:
    Periph_fpga(uint global_nr, std::string ipaddr, uint udpport, uint n_filterbanks, uint n_beamsets);
    ~Periph_fpga();

    bool read(char *data, const uint32_t cmd_id);
    bool read_saved(char *data, const uint32_t cmd_id);
    bool write(char *data, const uint32_t cmd_id);
    bool monitor();

    bool isOnline(void);
    bool isMasked(void);
    void setMasked(bool mask);
    double monitorPpsOffsetTime(void);
    struct timespec monitorStartTime(void);

    void ucpSetCommunicationState(bool state);
    bool UcpCommunicationStatus(void);

    void ucpResetCounters(bool reset);

    uint64_t ucpNofReads(void);
    uint64_t ucpNofReadRetries(void);
    uint64_t ucpNofReadFailures(void);
    uint64_t ucpNofWrites(void);
    uint64_t ucpNofWriteRetries(void);
    uint64_t ucpNofWriteFailures(void);
};

inline bool Periph_fpga::isOnline(void) { return Online; }
inline bool Periph_fpga::isMasked(void) { return Masked; }
inline void Periph_fpga::setMasked(bool mask) { Masked = mask; }
inline double Periph_fpga::monitorPpsOffsetTime(void) { return monitor_pps_offset_time; }
inline struct timespec Periph_fpga::monitorStartTime(void) { return monitor_start_time; }

inline void Periph_fpga::ucpSetCommunicationState(bool state) { ucp_block_comm = state; }
inline bool Periph_fpga::UcpCommunicationStatus(void) { return ucp_block_comm; }

inline void Periph_fpga::ucpResetCounters(bool reset) {
    if (reset) {
        ucp_reads = 0;
        ucp_read_retries = 0;
        ucp_read_fails = 0;
        ucp_writes = 0;
        ucp_write_retries = 0;
        ucp_write_fails = 0;
    }
}

inline uint64_t Periph_fpga::ucpNofReads(void) { return ucp_reads; }
inline uint64_t Periph_fpga::ucpNofReadRetries(void) { return ucp_read_retries; }
inline uint64_t Periph_fpga::ucpNofReadFailures(void) { return ucp_read_fails; }
inline uint64_t Periph_fpga::ucpNofWrites(void) { return ucp_writes; }
inline uint64_t Periph_fpga::ucpNofWriteRetries(void) { return ucp_write_retries; }
inline uint64_t Periph_fpga::ucpNofWriteFailures(void) { return ucp_write_fails; }

#endif // __PERIPH_FPGA_H__
