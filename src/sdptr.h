/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . main program
* *********************************************************************** */

#ifndef SDPTR_H
#define SDPTR_H 1

#include <string>
#include <atomic>
#include <ctime>
// #include <sys/types.h>

#include "fpga_map.h"
#include "tr.h"
// #include "io/tcpsocket.h"

#include "../config.h"

// get VERSION from ../config.h
#define SDPTR_VERSION VERSION

class NODE_config {
public:
    int gn;
    int board_nr;
    int fpga_nr;
    std::string ipaddr;
    int udp_port;
};

class Serverdat {
public:
    FpgaMap *fpga_map;
    TranslatorMap *tr;

    int32_t ua_server_port;
    int32_t n_fpgas;
    int32_t first_fpga_nr;
    int32_t n_filterbanks;
    int32_t n_beamsets;
    std::string ip_prefix;

    std::list<class NODE_config> node_config;

    double tod_pps_delta;
    double pps_monitor_delta;

    std::atomic<uint32_t> uptime;
    std::atomic<uint32_t> start_time;
    std::atomic<uint32_t> tod;
};

#endif
