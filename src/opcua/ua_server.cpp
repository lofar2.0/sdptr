/* *************************************************************************
 * Copyright 2023
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *********************************************************************** */

/* *************************************************************************
 * Author:
 * . Leon Hiemstra
 * . Pieter Donker
 * Purpose:
 * . opc-ua to ucp translator
 * Description:
 * . this is the opc_ua server
 * *********************************************************************** */

// You need to compile with _REENTRANT defined since this uses threads
#ifndef _REENTRANT
#define _REENTRANT 1
#endif

#include <open62541/plugin/log_stdout.h>
#include <open62541/server.h>
#include <open62541/server_config_default.h>

#include <iostream>
#include <cstring>
#include <cstdio>
#include <csignal>
#include <cstdlib>
#include <stdexcept>

#include "ua_server.h"
#include "../constants.h"
#include "../tools/util.h"
#include "../tools/loguru.h"

using namespace std;

#define UA_LOFAR_NAMESPACE "http://lofar.eu"

UA_Boolean running = true;
extern Serverdat SD;

UA_Server *mUaServer;
UA_UInt16 mUaLofarNameSpace;
CPointMap *pointmap = NULL;
CPointMap *trmap = NULL;

/*
 * Links of interest:
 * Datatypes: https://open62541.org/doc/current/types.html
 *            https://open62541.org/doc/current/tutorial_datatypes.html
 *            https://open62541.org/doc/0.1/group__types.html
 *            https://open62541.org/doc/current/types_generated.html?highlight=ua_datatype
 * Special Server functions: https://open62541.org/doc/0.3/server.html
 *                           https://open62541.org/doc/0.2/server.html
 * About nodes: https://open62541.org/doc/current/nodestore.html
 */

// for debug only:
void ua_print_nodeid(UA_NodeId *nd)
{
    // note: this function complains:
    // *** Error in `./sdptr': free(): invalid pointer: 0x00007ffc2e7da720 ***
    // --> must be a bug in UA_NodeId_print()
    UA_String ua_str;
    LOG_F(DEBUG, "ua_print_nodeid:");
    UA_NodeId_print(nd, &ua_str);

    char *str = new char[ua_str.length + 1];
    memcpy(str, ua_str.data, ua_str.length);
    str[ua_str.length] = 0; // add end of string char
    LOG_F(DEBUG, "%s", str);
    my_delete_arr(str);
}

/*
 * ua_read_DataSource(), this function is called by the opc-ua server to read a point.
 */
static UA_StatusCode ua_read_DataSource(UA_Server *server,
                                        const UA_NodeId *sessionId, void *sessionContext,
                                        const UA_NodeId *nodeId, void *nodeContext,
                                        UA_Boolean sourceTimeStamp, const UA_NumericRange *range,
                                        UA_DataValue *dataValue)
{
    char *regname = new char[nodeId->identifier.string.length + 1];
    memcpy(regname, nodeId->identifier.string.data, nodeId->identifier.string.length);
    regname[nodeId->identifier.string.length] = 0; // add end of string char
    LOG_F(DEBUG, "ua_read_DataSource: reading from %s", regname);
    UA_NodeId currentNodeId = UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname);
    UA_NodeId ntype;
    // UA_StatusCode st =
    UA_Server_readDataType(server, currentNodeId, &ntype);
    // assert(st == UA_STATUSCODE_GOOD);
    // printf("update_FPGA_Variable nodeid %s ns=%d DataType=%d numeric=%d\n",(char *)regname.c_str(),
    //       ntype.namespaceIndex,ntype.identifierType,ntype.identifier.numeric);
    // output: update_FPGA_Variable nodeid FPGA_version_R ns=0 DataType=0 numeric=12
    // ua_print_nodeid(&ntype);
    // output: i=12

    // ntype.identifier.numeric is the datatype e.g. for UA_Float=10, UA_String=12
    // printf("update_FPGA_Variable UA_TYPES_FLOAT=%d numeric=%d\n",UA_TYPES_FLOAT,ntype.identifier.numeric);
    // output: update_FPGA_Variable UA_TYPES_FLOAT=9 numeric=10
    // weird...  must do -1
    // see: https://open62541.org/doc/current/types.html

    size_t n_values = 0;
    size_t data_size;
    char *data = nullptr;

    // read values for point
    try
    {
        // Translator regnames start with "TR_" others "FPGA_"
        if (strncmp(regname, "TR_", 3) == 0)
        {
            n_values = trmap->getDataSize(regname);
            data_size = trmap->getFpgaPointSizeBytes(regname);
            // If it is a array, use fixed node size
            if (trmap->getNodesSize(regname) > 1) {
                n_values *= OPCUA_FIXED_N_NODES;
                data_size *= OPCUA_FIXED_N_NODES;
            }
            data = new char[data_size];
            memset(data, 0, data_size);
            SD.tr->read(data, regname);
        }
        else
        {
            // FPGA_ always an array, use fixed node size
            n_values = OPCUA_FIXED_N_NODES * pointmap->getDataSize(regname);
            data_size = OPCUA_FIXED_N_NODES * pointmap->getFpgaPointSizeBytes(regname);
            data = new char[data_size];
            memset(data, 0, data_size);
            SD.fpga_map->read(data, regname);
        }
    }
    catch (runtime_error &e)
    {
        cerr << "ua_read_DataSource error: " << e.what() << endl;
    }
    uint32_t type = ntype.identifier.numeric - 1;
    // LOG_F(INFO, "ua_read_DataSource: regname=%s, type=%d, n_values=%zu, data_size=%zu", regname, type, n_values, data_size);
    // if scalar (only TR_* points)
    if (n_values == 1)
    {
        if (type == UA_TYPES_STRING)
        {
            UA_String value = UA_STRING_ALLOC((char *)data);
            UA_Variant_setScalarCopy(&dataValue->value, &value, &UA_TYPES[UA_TYPES_STRING]);
            dataValue->hasValue = true;
        }
        else
        {
            UA_Variant_setScalarCopy(&dataValue->value, data, &UA_TYPES[type]);
            dataValue->hasValue = true;
        }
    }
    // else array
    else
    {
        if (type == UA_TYPES_STRING)
        {
            UA_String *values = (UA_String *)UA_Array_new(n_values, &UA_TYPES[UA_TYPES_STRING]);
            char *ptr = (char *)data;
            for (unsigned int i = 0; i < n_values; i++)
            {
                values[i] = UA_STRING_ALLOC((char *)ptr);
                ptr += SIZE1STRING;
            }
            UA_Variant_setArray(&dataValue->value, values, n_values, &UA_TYPES[UA_TYPES_STRING]);
            dataValue->hasValue = true;
        }
        else
        {
            void *values = UA_Array_new(n_values, &UA_TYPES[type]);

            size_t sz = n_values * (size_t)UA_TYPES[type].memSize;
            // LOG_F(INFO, "ua_read_DataSource: regname=%s, sz=%zu", regname, sz);
            memcpy(values, data, sz);
            UA_Variant_setArray(&dataValue->value, values, n_values, &UA_TYPES[type]);
            dataValue->hasValue = true;
        }
    }

    my_delete_arr(data);
    my_delete_arr(regname);
    return UA_STATUSCODE_GOOD;
}

/*
 * ua_write_DataSource(), this function is called by the opc-ua server to write a point.
 */
static UA_StatusCode ua_write_DataSource(UA_Server *server,
                                         const UA_NodeId *sessionId, void *sessionContext,
                                         const UA_NodeId *nodeId, void *nodeContext,
                                         const UA_NumericRange *range, const UA_DataValue *data)
{
    bool retval = false;
    char *regname = new char[nodeId->identifier.string.length + 1];
    memcpy(regname, nodeId->identifier.string.data, nodeId->identifier.string.length);
    regname[nodeId->identifier.string.length] = 0; // add end of string char
    UA_NodeId currentNodeId = UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname);
    UA_NodeId ntype;
    UA_Server_readDataType(server, currentNodeId, &ntype);
    LOG_F(DEBUG, "ua_write_DataSource: writing to %s, type=%u", regname, ntype.identifier.numeric - 1);

    // TODO: can this be removed.
    // UA_Variant nv_dims;
    // https://open62541.org/doc/0.2/server.html
    // UA_Server_readArrayDimensions(server, currentNodeId, &nv_dims);

    // check if there is data to write, if not retval remains false.
    if (data->hasValue && data->value.arrayLength > 0)
    {
        // data.value is of type UA_Variant.
        uint type = ntype.identifier.numeric - 1;
        char *data_sdp = NULL;
        if (type == UA_TYPES_STRING)
        {
            size_t sz = data->value.arrayLength * SIZE1STRING;
            data_sdp = new char[sz];
            memset(data_sdp, 0, sz);
            UA_String *dptr = (UA_String *)data->value.data;
            UA_String str_data;
            for (uint i=0; i<data->value.arrayLength; i++)
            {
                str_data = dptr[i];
                memcpy(&(data_sdp[i * SIZE1STRING]), str_data.data, str_data.length);
            }
            retval = true;
        }
        else
        {
            try
            {
                size_t sz = data->value.arrayLength * (size_t)UA_TYPES[type].memSize;
                data_sdp = new char[sz];
                memcpy(data_sdp, data->value.data, sz);
                retval = true;
            }
            catch (runtime_error &e)
            {
                cerr << "ua_write_DataSource error: " << e.what() << endl;
                retval = false;
            }
        }

        if (retval)
        {
            try
            {
                if (strncmp(regname, "TR_", 3) == 0)
                {
                    SD.tr->write(data_sdp, regname);
                }
                else
                {
                    SD.fpga_map->write(data_sdp, regname);
                }
            }
            catch (runtime_error &e)
            {
                cerr << "ua_write_DataSource error: " << e.what() << endl;
                retval = false;
            }
        }
        else
        {
            cerr << "ua_write no retval" << endl;
        }

        if (data_sdp != NULL) { my_delete_arr(data_sdp); }
    }
    my_delete_arr(regname);
    return UA_STATUSCODE_GOOD;
}

/*
 * ua_add_Variable_[data_type]() functions to add a UA_Variant variable (point) to the server
 */

template<typename T, typename D>
static void ua_add_Variable_internal(
    UA_Server *server, const string& regname, const size_t size, const string& perm,
    const UA_DataType* type, D default_value
) {
    UA_VariableAttributes vattr = UA_VariableAttributes_default;
    vattr.displayName = UA_LOCALIZEDTEXT((char *)"locale", (char *)regname.c_str());
    vattr.dataType = type->typeId;
    T value = T(default_value);
    T *values = nullptr;

    if      (perm == "RW") { vattr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE; }
    else if (perm == "WO") { vattr.accessLevel = UA_ACCESSLEVELMASK_WRITE; }
    else if (perm == "RO") { vattr.accessLevel = UA_ACCESSLEVELMASK_READ; }

    // Scalar
    if (size == 1) {
        vattr.valueRank = UA_VALUERANK_SCALAR;

        // T value = T(default_value);

        // setScalarCopy otherwise value must remain in scope until call to:
        // UA_Server_addDataSourceVariableNode
        // UA_Variant_setScalarCopy(&vattr.value, &value, type);
        UA_Variant_setScalar(&vattr.value, &value, type);
    }
    else {
        vattr.valueRank = UA_VALUERANK_ONE_DIMENSION;

        vattr.arrayDimensions = (UA_UInt32 *)UA_Array_new(1, &UA_TYPES[UA_TYPES_UINT32]);
        vattr.arrayDimensions[0] = size;
        vattr.arrayDimensionsSize = 1;

        values = (T *) UA_Array_new(size, type);
        for (uint32_t i=0; i < size; i++) {
            values[i] = default_value;
        }

        UA_Variant_setArray(&vattr.value, values, size, type);
        vattr.value.arrayDimensions = vattr.arrayDimensions;
        vattr.value.arrayDimensionsSize = 1;

//         LOG_F(INFO, "vattr.value: arrayDimensions=%u arrayDimensionsSize=%u arrayLength=%u",
//                      (uint32_t)vattr.value.arrayDimensions[0], (uint32_t)vattr.value.arrayDimensionsSize, (uint32_t)vattr.value.arrayLength);
    }

    UA_DataSource DataSource;
    DataSource.read = ua_read_DataSource;
    DataSource.write = ua_write_DataSource;

    UA_StatusCode retval = UA_Server_addDataSourceVariableNode(
        server,
        UA_NODEID_STRING(mUaLofarNameSpace, (char *)regname.c_str()), // currentNodeId
        UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),                 // parentNodeId
        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),                     // parentReferenceNodeId
        UA_QUALIFIEDNAME(mUaLofarNameSpace, (char *)regname.c_str()), // node browse name
        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
        vattr, DataSource, nullptr, nullptr
    );
    // handle error
    if (retval != UA_STATUSCODE_GOOD) {
        LOG_F(ERROR, "Error adding node");
    }
}
/* end of ua_add_Variable_[data_type]() functions */

/*
 *  ua_add_Variable(), main function to add variabel(point) for given format(data_type) to
 *  the opc-ua server by calling an ua_add_Variable_[data_type]() functions.
 *  if data size is 1, its a scalar.
 */
static void ua_add_Variable(UA_Server *server, const string& regname, const uint32_t format, const size_t size, const string& perm)
{
    LOG_F(INFO, "ua_add_Variable() regname=%s format=%u size=%zu perm=%s", regname.c_str(), format, size, perm.c_str());

    switch (format)
    {
    case REG_FORMAT_STRING:
        ua_add_Variable_internal<UA_String>(server, regname, size, perm, &UA_TYPES[UA_TYPES_STRING], UA_STRING_ALLOC((char *)"unknown"));
        break;
    case REG_FORMAT_INT64:
        ua_add_Variable_internal<UA_Int64, int64_t>(server, regname, size, perm, &UA_TYPES[UA_TYPES_INT64], 0);
        break;
    case REG_FORMAT_UINT64:
        ua_add_Variable_internal<UA_UInt64, uint64_t>(server, regname, size, perm, &UA_TYPES[UA_TYPES_UINT64], 0);
        break;
    case REG_FORMAT_INT32:
        ua_add_Variable_internal<UA_Int32, int32_t>(server, regname, size, perm, &UA_TYPES[UA_TYPES_INT32], 0);
        break;
    case REG_FORMAT_UINT32:
        ua_add_Variable_internal<UA_UInt32, uint32_t>(server, regname, size, perm, &UA_TYPES[UA_TYPES_UINT32], 0);
        break;
    case REG_FORMAT_INT16:
        ua_add_Variable_internal<UA_Int16, int16_t>(server, regname, size, perm, &UA_TYPES[UA_TYPES_INT16], 0);
        break;
    case REG_FORMAT_UINT16:
        ua_add_Variable_internal<UA_UInt16, uint16_t>(server, regname, size, perm, &UA_TYPES[UA_TYPES_UINT16], 0);
        break;
    case REG_FORMAT_UINT8:
        ua_add_Variable_internal<UA_Byte, uint8_t>(server, regname, size, perm, &UA_TYPES[UA_TYPES_BYTE], 0);
        break;
    case REG_FORMAT_FLOAT:
        ua_add_Variable_internal<UA_Float, float>(server, regname, size, perm, &UA_TYPES[UA_TYPES_FLOAT], 0.0);
        break;
    case REG_FORMAT_DOUBLE:
        ua_add_Variable_internal<UA_Double, double>(server, regname, size, perm, &UA_TYPES[UA_TYPES_DOUBLE], 0.0);
        break;
    case REG_FORMAT_BOOLEAN:
        ua_add_Variable_internal<UA_Boolean, bool>(server, regname, size, perm, &UA_TYPES[UA_TYPES_BOOLEAN], false);
        break;
    default:
        LOG_F(ERROR, "Unknown format [%d] not yet supported", format);
        break;
    }
}

/*
 * ua_server_init(), get all FPGA_ and TR_ poins and add them to the opc-ua server.
 *
 * SD.fpga_map->get_pointMap()   -->  FPGA_*  (points defined in src/fpga.cpp)
 * SD.tr->getTranslatorMap()  -->  TR_*    (points defined in src/tr.cpp)
 */
int ua_server_init(const uint port_number)
{
    mUaServer = UA_Server_new();
    // UA_ServerConfig_setDefault(UA_Server_getConfig(mUaServer));
    UA_ServerConfig_setMinimal(UA_Server_getConfig(mUaServer), port_number, nullptr);
    pointmap = SD.fpga_map->get_pointMap();
    trmap = SD.tr->getTranslatorMap();

    mUaLofarNameSpace = UA_Server_addNamespace(mUaServer, UA_LOFAR_NAMESPACE);
    LOG_F(INFO, "UA Server ns=%d for %s", mUaLofarNameSpace, UA_LOFAR_NAMESPACE);
    LOG_F(INFO, "UA Server add pointMap");
    vector<string> regnames = pointmap->getRegnames("");
    for (auto m : regnames)
    {
        // FPGA_ point always array, use fixed node size
        uint32_t format = pointmap->getFormat(m);
        size_t size = OPCUA_FIXED_N_NODES * pointmap->getDataSize(m);
        string perm = pointmap->getPerm(m);
        ua_add_Variable(mUaServer, m, format, size, perm);
    }

    LOG_F(INFO, "UA Server add translatorMap");
    vector<string> trregnames = trmap->getRegnames("");
    for (auto m : trregnames)
    {
        uint32_t format = trmap->getFormat(m);
        size_t size = trmap->getDataSize(m);
        // TR_ points, if it is a array, use fixed node size
        if (trmap->getNodesSize(m) > 1) {
            size *= OPCUA_FIXED_N_NODES;
        }
        string perm = trmap->getPerm(m);
        ua_add_Variable(mUaServer, m, format, size, perm);
    }
    return 0;
}

/*
 * ua_server_run(), start opc-ua server, called from sdptr.cpp
 */
int ua_server_run(void)
{
    UA_StatusCode retval;
    retval = UA_Server_run(mUaServer, &running);
    LOG_F(INFO, "UA Server Stopped");

    // Server stopped, cleunup nodes
    pointmap = SD.fpga_map->get_pointMap();
    trmap = SD.tr->getTranslatorMap();

    vector<string> regnames = pointmap->getRegnames("");
    for (auto m : regnames)
    {
        UA_NodeId currentNodeId = UA_NODEID_STRING(mUaLofarNameSpace, (char *)m.c_str());
        UA_Server_deleteNode(mUaServer, currentNodeId, true);
        LOG_F(INFO, "UA Server Delete Node %s, %d", m.c_str(), currentNodeId.identifier.numeric);
    }

    vector<string> trregnames = trmap->getRegnames("");
    for (auto m : trregnames)
    {
        UA_NodeId currentNodeId = UA_NODEID_STRING(mUaLofarNameSpace, (char *)m.c_str());
        UA_Server_deleteNode(mUaServer, currentNodeId, true);
        LOG_F(INFO, "UA Server Delete Node %s, %d", m.c_str(), currentNodeId.identifier.numeric);
    }

    UA_Server_delete(mUaServer);
    LOG_F(INFO, "UA Server Deleted");

    return retval == UA_STATUSCODE_GOOD ? EXIT_SUCCESS : EXIT_FAILURE;
}

int ua_server_stop(void)
{
    LOG_F(INFO, "UA Stop Server");
    running = false;
    return 0;
}
