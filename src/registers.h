/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* . register class used to hold register information
* *********************************************************************** */

#ifndef __REGISTERMAP_H__
#define __REGISTERMAP_H__

#include <string>
#include <map>
#include <vector>

#define SIZE1STRING 100

#define REG_ADDR_ROM_SYSTEM      (0x10000)
#define REG_ADDR_ROM_SYSTEM_SPAN    (8192)


#define REG_FORMAT_UNKNOWN  0
#define REG_FORMAT_BOOLEAN  1
#define REG_FORMAT_INT8     2
#define REG_FORMAT_UINT8    3
#define REG_FORMAT_INT16    4
#define REG_FORMAT_UINT16   5
#define REG_FORMAT_INT32    6
#define REG_FORMAT_UINT32   7
#define REG_FORMAT_INT64    8
#define REG_FORMAT_UINT64   9
#define REG_FORMAT_FLOAT    10
#define REG_FORMAT_DOUBLE   11
#define REG_FORMAT_STRING   12

size_t reg_format_size_in_bytes(int32_t reg_type);
size_t reg_format_size_in_words(int32_t reg_type);


class CMMap {
private:

    typedef struct {
        uint32_t base;
        uint32_t n_peripherals;
        uint32_t n_mm_ports;
        uint32_t span;
        uint32_t mask;
        uint32_t shift;
        uint32_t n_fields;
        std::string access;
        std::string port_type;
        std::string radix;
        uint32_t mm_port_span;
        uint32_t peripheral_span;
    } register_info;

    std::map <std::string, register_info> reg;

public:
    CMMap();
    ~CMMap();
    bool empty() { return reg.empty(); }
    void clear() { reg.clear(); }
    bool add_register(const std::string& name, const uint32_t base, const uint32_t n_peripherals, const uint32_t n_mm_ports, const uint32_t span,
                      const uint32_t mask, const uint32_t shift, const uint32_t n_fields, const std::string& access, const std::string& radix,
                      const std::string& port_type, const uint32_t peripheral_span, const uint32_t mm_port_span);

    bool update_register(const std::string& name, const uint32_t base, const uint32_t n_peripherals, const uint32_t n_mm_ports, const uint32_t span,
                      const uint32_t mask, const uint32_t shift, const uint32_t n_fields, const std::string& access, const std::string& radix,
                      const std::string& port_type, const uint32_t peripheral_span, const uint32_t mm_port_span);

    bool is_existing_register(const std::string& name);
    bool find_register(const std::string& name);
    void print(std::ostringstream& strs, std::string prefix);
    void print_screen(void);

    uint32_t getNInstances(const std::string& name);
    uint32_t getBaseAddr(const std::string& name);
    uint32_t getSpan(const std::string& name);
    std::string getPerm(const std::string& name);
    uint32_t getMask(const std::string& name);
    uint32_t getShift(const std::string& name);
    uint32_t getNFields(const std::string& name);
    std::string getRadix(const std::string& name);
    std::string getPortType(const std::string& name);
    uint32_t getNPeripherals(const std::string& name);
    uint32_t getNPorts(const std::string& name);
    uint32_t getPortSpan(const std::string& name);
    uint32_t getPeripheralSpan(const std::string& name);
    bool type_isfifo(const std::string& name);
    
    bool getAddr(const std::string& name, uint32_t *addr);
    uint32_t getValidAddr(const std::string& name,
                          const uint32_t size);

    bool canRead(const std::string& name);
    bool canWrite(const std::string& name);

   std::vector<std::string> getRegnames(const std::string& prefix);
};

inline uint32_t CMMap::getBaseAddr(const std::string& name) { return reg[name].base; }
inline uint32_t CMMap::getSpan(const std::string& name) { return reg[name].span; }
inline std::string CMMap::getPerm(const std::string& name) { return reg[name].access; }
inline uint32_t CMMap::getMask(const std::string& name) { return reg[name].mask; }
inline uint32_t CMMap::getShift(const std::string& name) { return reg[name].shift; }
inline uint32_t CMMap::getNFields(const std::string& name) { return reg[name].n_fields; }
inline std::string CMMap::getRadix(const std::string& name) { return reg[name].radix; }
inline std::string CMMap::getPortType(const std::string& name) { return reg[name].port_type; }
inline uint32_t CMMap::getNPeripherals(const std::string& name) { return reg[name].n_peripherals; }
inline uint32_t CMMap::getNPorts(const std::string& name) { return reg[name].n_mm_ports; }
inline uint32_t CMMap::getPortSpan(const std::string& name) { return reg[name].mm_port_span; }
inline uint32_t CMMap::getPeripheralSpan(const std::string& name) { return reg[name].peripheral_span; }
inline bool CMMap::type_isfifo(const std::string& name) { return (reg[name].port_type == "FIFO"); }

class CPointMap {
private:

    typedef struct {
        uint32_t cmd_id;
        uint32_t n_nodes;
        uint32_t n_data;
        std::string access;
        uint32_t format;
    } register_info;

    std::map <std::string, register_info> reg;

 public:
    CPointMap();
    ~CPointMap();

    bool add_register   (const std::string& name, const uint32_t cmd_id, const uint32_t n_nodes, const uint32_t n_data,
                         const std::string& access, const uint32_t format);
    bool update_register(const std::string& name, const uint32_t cmd_id, const uint32_t n_nodes, const uint32_t n_data,
                         const std::string& access, const uint32_t format);
    bool is_existing_register(std::string name);
    bool find_register(std::string name);
    void print(std::ostringstream& strs, const std::string& prefix);
    void print_screen(void);

    std::string getName(const uint32_t cmd_id);
    uint32_t getCommandId(const std::string& name);
    size_t getNodesSize(const std::string& name);
    size_t getDataSize(const std::string& name);
    size_t getFpgaPointSizeBytes(const std::string& name);
    size_t getPointSize(const std::string& name);
    std::string getPerm(const std::string& name);
    uint32_t getFormat(const std::string& name);
    bool canRead(const std::string& name, bool log=true);
    bool canWrite(const std::string& name, bool log=true);

    std::vector<std::string> getRegnames(const std::string& prefix);
};

inline uint32_t CPointMap::getCommandId(const std::string& name) { return reg[name].cmd_id; }
inline size_t CPointMap::getNodesSize(const std::string& name) { return (size_t)reg[name].n_nodes; }
inline size_t CPointMap::getDataSize(const std::string& name) { return (size_t)reg[name].n_data; }
inline size_t CPointMap::getFpgaPointSizeBytes(const std::string& name) { return (size_t)(reg[name].n_data * reg_format_size_in_bytes(reg[name].format)); }
inline size_t CPointMap::getPointSize(const std::string& name) { return (size_t)(reg[name].n_nodes * reg[name].n_data * reg_format_size_in_bytes(reg[name].format)); }
inline std::string CPointMap::getPerm(const std::string& name) { return reg[name].access; }
inline uint32_t CPointMap::getFormat(const std::string& name) { return reg[name].format; }

#endif // __REGISTERMAP_H__
