/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* .
* *********************************************************************** */

#ifndef __UTIL_H__
#define __UTIL_H__

#include <string>
#include <vector>
#include <ctime>
#include <sstream>
#include <chrono>
#include <cstring>

#include "../registers.h"

#define my_delete(x) {delete x; x = NULL;}
#define my_delete_arr(x) {delete[] x; x = NULL;}

/* TicToc can be used in developtime to time duration of parts of code
start with: "tic( id_str );"
end with: "toc();" the time spend between tic and toc is now logged with the id string.

usage:
timeit = Tictoc();

timeit.tic("func1");
... something usefull
timeit.toc();

result in logging:
  date_time [module] INFO| tictoc (func1) CPU time: 3.1 ms. Real time: 3 ms.
*/
class Tictoc {
private:
  std::string name;
  std::clock_t c_start;
  std::chrono::high_resolution_clock::time_point t_start;
public:
  Tictoc() {};
  ~Tictoc() {};
  void tic(const std::string& idstr="");
  void toc(void);
};

std::string exec(const char* cmd);

#endif // __UTIL_H__
