/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* .
* *********************************************************************** */

#include <string>
#include <iostream>

#include "loguru.h"
#include "mmap.h"

using namespace std;


typedef struct {
    string   port_name;
    uint32_t n_peripherals;
    uint32_t n_ports;
    string   port_type;
    string   field_name;
    uint32_t base_addr;  // in MM words
    uint32_t n_fields;
    string   access_mode;
    string   radix;
    uint32_t mm_mask;
    uint32_t user_mask;
    uint32_t span;
    uint32_t shift;
    uint32_t peripheral_span;
    uint32_t mm_port_span;
} mm_info_t;

void mmap_add_register(CMMap *mmap, mm_info_t mm_info)
{
    bool update;
    uint32_t base_addr = mm_info.base_addr;
    uint32_t addr;
    string full_name;
    for (uint i=0; i<mm_info.n_peripherals; i++) {
        for (uint j=0; j<mm_info.n_ports; j++) {
            update = false;
            full_name = "mm/" + to_string(i) + "/" + mm_info.port_name + "/" + to_string(j) + "/" + mm_info.field_name;

            if (mmap->is_existing_register(full_name)) {
                // mm_info.base_addr = mmap->getBaseAddr(full_name);
                update = true;
            }
            addr = base_addr + (i * mm_info.peripheral_span) + (j * mm_info.mm_port_span);

            if (update) {
                mmap->update_register(full_name,
                                       addr,
                                       mm_info.n_peripherals,
                                       mm_info.n_ports,
                                       mm_info.span,
                                       mm_info.mm_mask,
                                       mm_info.shift,
                                       mm_info.n_fields,
                                       mm_info.access_mode,
                                       mm_info.radix,
                                       mm_info.port_type,
                                       mm_info.peripheral_span,
                                       mm_info.mm_port_span);
            }
            else {
                mmap->add_register(full_name,
                                    addr,
                                    mm_info.n_peripherals,
                                    mm_info.n_ports,
                                    mm_info.span,
                                    mm_info.mm_mask,
                                    mm_info.shift,
                                    mm_info.n_fields,
                                    mm_info.access_mode,
                                    mm_info.radix,
                                    mm_info.port_type,
                                    mm_info.peripheral_span,
                                    mm_info.mm_port_span);
            }
        }
    }
}

bool mmap_to_regmap(CMMap *mmap, istringstream& iss)
{
    ostringstream err_str;

    mm_info_t mm_info;
    mm_info_t last_mm_info;

    uint64_t start_addr44;  // 64bit value, but first 44 bits are used for address.

    int mm_mask_hi, mm_mask_lo;
    int user_mask_hi, user_mask_lo;

    char line[250];
    string val_str;
    char sep;

    iss.getline(line, sizeof(line));
    iss.getline(line, sizeof(line));
    while (iss.getline(line, sizeof(line))) {
        bool same_mask = false;
        bool same_field_name = false;
        mm_info = last_mm_info;
        stringstream strs(line);

        strs >> val_str;  // get port name
        if (val_str != "-") {
            mm_info.port_name = val_str;
            mm_info.peripheral_span = 1;
            mm_info.mm_port_span = 1;
            mm_info.n_peripherals = 1;
            mm_info.n_ports = 1;
        }

        strs >> val_str;  // get n_peripherals
        // mm_info.n_peripherals = 1;
        if (val_str != "-") {
            mm_info.n_peripherals = stoi(val_str);
        }

        strs >> val_str;  // get n_ports
        // mm_info.n_ports = 1;
        if (val_str != "-") {
            mm_info.n_ports = stoi(val_str);
        }

        strs >> val_str;  // get port_type
        if (val_str != "-") {
            mm_info.port_type = val_str;
        }

        strs >> val_str;  // get field_name
        if (val_str != "-") {
            mm_info.field_name = val_str;
            mm_info.span = 0;
        }
        else {
            same_mask = true;
            same_field_name = true;
        }

        strs >> hex >> start_addr44; // get start_address in dword addresses
        if (!same_field_name) {
            mm_info.base_addr = (uint32_t)start_addr44;
        }

        strs >> val_str;  // get n_fields
        if (val_str != "-") {
            mm_info.n_fields = stoi(val_str);
        }

        strs >> val_str;  // get access_mode
        if (val_str != "-") {
            mm_info.access_mode = val_str;
        }

        strs >> val_str;  // get radix
        if (val_str != "-") {
            mm_info.radix = val_str;
        }

        /* Code to remove sscanf */
        strs >> dec >> mm_mask_hi;
        strs >> sep;
        strs >> dec >> mm_mask_lo;
        uint32_t mask = 0;
        for (int i=mm_mask_lo; i<=mm_mask_hi; i++) {
            mask |= (1 << i);
        }
        if (!same_mask) {
            mm_info.mm_mask = mask;
        }
        mm_info.shift = mm_mask_lo;

        strs >> val_str;  // get user_mask
        if (val_str != "-") {
            stringstream ss(val_str);
            ss >> dec >> user_mask_hi;
            ss >> sep;
            ss >> dec >> user_mask_lo;
            mask = 0;
            for (int i=user_mask_lo; i<=user_mask_hi; i++) {
                mask |= (1<<i);
            }
            mm_info.user_mask = mask;
        }
        else {
            mm_info.user_mask = mm_info.mm_mask;
        }

        uint32_t span;
        if (mm_info.radix == "char8") {
            span = mm_info.n_fields / sizeof(uint32_t);
        }
        else {
            span = mm_info.n_fields;
        }
        mm_info.span += span;

        strs >> val_str;  // get peripheral_span
        if (val_str != "-") {
            if (mm_info.n_peripherals > 1 || mm_info.n_ports > 1) {
                mm_info.peripheral_span = stoi(val_str);
            }
        }

        strs >> val_str;  // get mm_port_span
        if (val_str != "-") {
            if (mm_info.n_peripherals > 1 || mm_info.n_ports > 1) {
                mm_info.mm_port_span = stoi(val_str);
            }
        }

        if (strs.fail() || strs.bad()) {
            LOG_F(ERROR, "import_mmap_file: invalid");
            strs.clear();
        }
        else {
            last_mm_info = mm_info;
            mmap_add_register(mmap, mm_info);
        }
    }

    // LOG_F(DEBUG, "regmap:";
    // regmap.print_screen();

    return true;
}
