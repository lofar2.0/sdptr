/* *************************************************************************
* Copyright 2023
* ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
* P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
* *********************************************************************** */

/* *************************************************************************
* Author:
* . Leon Hiemstra
* . Pieter Donker
* Purpose:
* . opc-ua to ucp translator
* Description:
* .
* *********************************************************************** */
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <string>
#include <cmath>
#include <algorithm>
#include <ctime>
#include <sys/time.h>

#include "loguru.h"
#include "util.h"


using namespace std;

#define TIME_STR_LEN 20


void Tictoc::tic(const string& idstr)
{
    name = idstr;
    c_start = clock();
    t_start = chrono::high_resolution_clock::now();
}

void Tictoc::toc(void)
{
    clock_t c_end = clock();
    auto t_end = chrono::high_resolution_clock::now();

    struct timeval tv;
    gettimeofday(&tv, NULL);
    time_t secs = tv.tv_sec;
    struct tm * now = gmtime(&secs);
    char time_str[TIME_STR_LEN];
    sprintf(time_str, "%02d:%02d:%02d.%06ld", now->tm_hour, now->tm_min, now->tm_sec, tv.tv_usec);

    LOG_F(INFO, "tictoc (%s) CPU time: %f ms. Real time: %ld ms.",
                 name.c_str(),
                 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC,
                 (signed long)chrono::duration_cast<chrono::milliseconds>(t_end - t_start).count());
}


string exec(const char* cmd) {
    /*
      execute cmd and return result.
    */

    char buffer[128];
    string result = "";
    FILE* pipe = popen(cmd, "r");
    if (!pipe) throw runtime_error("popen() failed!");
    try {
        while (fgets(buffer, sizeof buffer, pipe) != NULL) {
            result += buffer;
        }
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);
    return result;
}