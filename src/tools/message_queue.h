
#pragma once

#ifndef MESSAGE_QUEUE_H
#define MESSAGE_QUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

template<class Type>
class CMessageQueue
{
public:
    CMessageQueue& operator = (const CMessageQueue&) = delete;
    CMessageQueue(const CMessageQueue& mq) = delete;


    CMessageQueue() :_queue(), _mutex(), _condition(){}
    virtual ~CMessageQueue(){}

    void Push(Type msg) {
        std::lock_guard <std::mutex> lock(_mutex);
        _queue.push(msg);
        // When using blocking mode to get a message from a message queue, by condition Alert the waiting thread when a new message arrives
        _condition.notify_one();
    }

    //blocked Defines the access mode to be either synchronously blocked or non-blocking
    bool Pop(Type& msg, bool isBlocked = true) {
        if (isBlocked) {
            std::unique_lock <std::mutex> lock(_mutex);
            while (_queue.empty()) {
                _condition.wait(lock);
            }
            // Note that this 1 Segment must be placed in if In the statement because lock The domain of life is only in if Inside the curly braces
            msg = std::move(_queue.front());
            _queue.pop();
            return true;
        }
        else {
            std::lock_guard<std::mutex> lock(_mutex);
            if (_queue.empty())
                return false;
            msg = std::move(_queue.front());
            _queue.pop();
            return true;
        }
    }

    int32_t Size() {
        std::lock_guard<std::mutex> lock(_mutex);
        return _queue.size();
    }

    bool Empty() {
        std::lock_guard<std::mutex> lock(_mutex);
        return _queue.empty();
    }
private:
    std::queue<Type> _queue;             // A queue where messages are stored
    mutable std::mutex _mutex;           // Synchronization lock
    std::condition_variable _condition;  // Achieve synchronous retrieval of messages
};

#endif//MESSAGE_QUEUE_H