/* *************************************************************************
 * Copyright 2023
 * ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
 * P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * *********************************************************************** */

/* *************************************************************************
 * Author:
 * . Leon Hiemstra
 * . Pieter Donker
 * Purpose:
 * . opc-ua to ucp translator
 * Description:
 * . register class used to hold register information
 * *********************************************************************** */

// You need to compile with _REENTRANT defined since this uses threads
#ifndef _REENTRANT
#define _REENTRANT 1
#endif

#include <iostream>
#include <iomanip>
#include <sstream>

#include "registers.h"
#include "tools/loguru.h"

using namespace std;

size_t reg_format_size_in_bytes(int32_t reg_type)
{
    size_t sz = 0;
    switch (reg_type)
    {
    case REG_FORMAT_FLOAT:
        sz = sizeof(float);
        break;
    case REG_FORMAT_DOUBLE:
        sz = sizeof(double);
        break;
    case REG_FORMAT_INT64:
        sz = sizeof(int64_t);
        break;
    case REG_FORMAT_UINT64:
        sz = sizeof(uint64_t);
        break;
    case REG_FORMAT_INT32:
        sz = sizeof(int32_t);
        break;
    case REG_FORMAT_UINT32:
        sz = sizeof(uint32_t);
        break;
    case REG_FORMAT_INT16:
        sz = sizeof(int16_t);
        break;
    case REG_FORMAT_UINT16:
        sz = sizeof(uint16_t);
        break;
    case REG_FORMAT_INT8:
        sz = sizeof(int8_t);
        break;
    case REG_FORMAT_UINT8:
        sz = sizeof(uint8_t);
        break;
    case REG_FORMAT_STRING:
        sz = sizeof(char) * SIZE1STRING;
        break;
    case REG_FORMAT_BOOLEAN:
        sz = sizeof(bool);
        break;
    default:
        LOG_F(INFO, "size_in_bytes, Not supported datatype (%d)", reg_type);
        break;
    }
    return sz;
}

size_t reg_format_size_in_words(int32_t reg_type)
{
    size_t sz = reg_format_size_in_bytes(reg_type);
    if (sz == 0)
        return 0;
    if (sz <= 4)
        return 1;
    return (size_t)(sz / 4);
}

//
// CMMap used for holding fpga mm register information
//
CMMap::CMMap() {}
CMMap::~CMMap() { reg.clear(); }

bool CMMap::add_register(const string &name, const uint32_t base, const uint32_t n_peripherals, const uint32_t n_mm_ports,
                         const uint32_t span, const uint32_t mask,
                         const uint32_t shift, const uint32_t n_fields, const string &access, const string &radix,
                         const string &port_type, const uint32_t peripheral_span, const uint32_t mm_port_span)
{
    LOG_F(DEBUG, "CMMap::add_register: %s", name.c_str());
    if (is_existing_register(name))
    {
        LOG_F(ERROR, "CMMap::add_register: %s  already exist!", name.c_str());
        return false;
    }
    register_info r = {base, n_peripherals, n_mm_ports, span, mask, shift, n_fields, access, port_type, radix, mm_port_span, peripheral_span};
    reg.insert(reg.end(), pair<string, register_info>(name, r));
    return true;
}

bool CMMap::update_register(const string &name, const uint32_t base, const uint32_t n_peripherals, const uint32_t n_mm_ports,
                            const uint32_t span, const uint32_t mask,
                            const uint32_t shift, const uint32_t n_fields, const string &access, const string &radix,
                            const string &port_type, const uint32_t peripheral_span, const uint32_t mm_port_span)
{
    LOG_F(DEBUG, "CMMap::update_register: %s", name.c_str());
    if (!is_existing_register(name))
    {
        LOG_F(ERROR, "CMMap::update_register: %s  not exist!", name.c_str());
        return false;
    }
    reg[name].port_type = port_type;
    reg[name].radix = radix;
    reg[name].access = access;
    reg[name].shift = shift;
    reg[name].n_fields = n_fields;
    reg[name].mask = mask;
    reg[name].span = span;
    reg[name].base = base;
    reg[name].n_peripherals = n_peripherals;
    reg[name].n_mm_ports = n_mm_ports;
    reg[name].mm_port_span = mm_port_span;
    reg[name].peripheral_span = peripheral_span;
    return true;
}

bool CMMap::is_existing_register(const string &name)
{
    auto i = reg.find(name);
    if (i != reg.end())
    {
        return true;
    }
    return false;
}

bool CMMap::find_register(const string &name)
{
    if (is_existing_register(name))
    {
        return true;
    }
    LOG_F(ERROR, "Register[%s] not existing.", name.c_str());
    return false;
}

// #define MAX_NOF_INSTANCES 1024

// void CMMap::print(ostringstream& strs, string prefix)
// {
//     for (auto m : reg) {
//         strs << prefix << " register[" << m.first << "]"
//              << " base=0x"   << hex << m.second.base
//              << " size="     << dec << m.second.span
//              << " mask=0x"   << hex << m.second.mask
//              << " shift="    << dec << m.second.shift
//              << " access="   << m.second.access
//              << " type="     << m.second.port_type
//              << endl;
//     }
// }

void CMMap::print_screen(void)
{
    for (auto m : reg)
    {
        LOG_F(DEBUG, "%-68s, base=0x%08x, n_ports=%02d, size=%04d, mask=0x%08x, shift=%02d, access=%2s, port_type=%4s, port_span=%04d, periph_span=%05d",
              m.first.c_str(), m.second.base, m.second.n_mm_ports, m.second.span, m.second.mask, m.second.shift,
              m.second.access.c_str(), m.second.port_type.c_str(), m.second.mm_port_span, m.second.peripheral_span);
    }
}

vector<string> CMMap::getRegnames(const string &prefix)
{
    vector<string> regnames;
    for (auto m : reg)
    {
        regnames.push_back(prefix + m.first);
    }
    return regnames;
}

uint32_t CMMap::getNInstances(const string &name)
{
    string _name = name;
    // replace "mm/?/..." with "mm/0/..."
    _name.replace(3, 1, "0");
    return reg[_name].n_peripherals;
}

bool CMMap::getAddr(const string &name, uint32_t *addr)
{
    if (!find_register(name))
    {
        return false;
    }

    *addr = getBaseAddr(name) * sizeof(uint32_t);
    return true;
}

uint32_t CMMap::getValidAddr(const string &name,
                             const uint32_t size)
{
    if (!find_register(name))
    {
        throw runtime_error("Register[" + name + "] not existing!");
    }
    uint32_t base = getBaseAddr(name) * sizeof(uint32_t);
    uint32_t span = getSpan(name);

    if (size > span && !type_isfifo(name))
    {
        throw runtime_error("Register[" + name + "] getValidAddr() out of range!");
    }
    uint32_t addr = base;
    return addr;
}

bool CMMap::canRead(const string &name)
{
    if (!find_register(name))
    {
        return false;
    }

    string perm = getPerm(name);
    if (perm == "RO" || perm == "RW")
    {
        return true;
    }
    LOG_F(ERROR, "No read permission for register[%s]", name.c_str());
    return false;
}

bool CMMap::canWrite(const string &name)
{
    if (!find_register(name))
    {
        return false;
    }

    string perm = getPerm(name);
    if (perm == "WO" || perm == "RW")
    {
        return true;
    }
    LOG_F(ERROR, "No write permission for register[%s]", name.c_str());
    return false;
}

//
// CPointMap used for assigning opc-ua points
//
CPointMap::CPointMap() {}

CPointMap::~CPointMap() { reg.clear(); }

bool CPointMap::add_register(const string &name, const uint32_t cmd_id, const uint32_t n_nodes, const uint32_t n_data,
                             const string &access, const uint32_t format)
{
    LOG_F(INFO, "CPointMap::add_register: %s", name.c_str());
    if (is_existing_register(name))
    {
        LOG_F(WARNING, "CPointMap::add_register: %s already exist!", name.c_str());
        return false;
    }
    register_info r = {cmd_id, n_nodes, n_data, access, format};
    reg.insert(reg.end(), pair<string, register_info>(name, r));
    return true;
}

bool CPointMap::update_register(const string &name, const uint32_t cmd_id, const uint32_t n_nodes, const uint32_t n_data,
                                const string &access, const uint32_t format)
{
    LOG_F(INFO, "CPointMap::update_register: %s", name.c_str());
    if (!is_existing_register(name))
    {
        LOG_F(ERROR, "CPointMap::update_register: %s not exist!", name.c_str());
        return false;
    }
    reg[name].cmd_id = cmd_id;
    reg[name].n_nodes = n_nodes;
    reg[name].n_data = n_data;
    reg[name].access = access;
    reg[name].format = format;
    return true;
}

bool CPointMap::is_existing_register(string name)
{
    auto i = reg.find(name);
    if (i != reg.end())
    {
        return true;
    }
    return false;
}

bool CPointMap::find_register(string name)
{
    if (is_existing_register(name))
    {
        return true;
    }
    LOG_F(ERROR, "Register[%s] not existing.", name.c_str());
    return false;
}

void CPointMap::print(ostringstream &strs, const string &prefix)
{
    for (auto m : reg)
    {
        strs << prefix << " register[" << m.first << "]"
             << " cmd_id=" << m.second.cmd_id
             << " n_nodes=" << dec << m.second.n_nodes
             << " n_data=" << dec << m.second.n_data
             << " access=" << m.second.access
             << " format=" << m.second.format << endl;
    }
}

void CPointMap::print_screen(void)
{
    for (auto m : reg)
    {
        cout << " register[" << m.first << "]"
             << " cmd_id=" << m.second.cmd_id
             << " n_nodes=" << dec << m.second.n_nodes
             << " n_data=" << dec << m.second.n_data
             << " access=" << m.second.access
             << " format=" << m.second.format << endl;
    }
}

vector<string> CPointMap::getRegnames(const string &prefix)
{
    vector<string> regnames;
    for (auto m : reg)
    {
        regnames.push_back(prefix + m.first);
    }
    return regnames;
}

string CPointMap::getName(const uint32_t cmd_id)
{
    for (auto m : reg)
    {
        if (m.second.cmd_id == cmd_id)
        {
            return m.first;
        }
    }
    return "";
}

bool CPointMap::canRead(const string &name, bool log)
{
    if (!find_register(name))
    {
        return false;
    }

    string perm = getPerm(name);
    if (perm == "RO" || perm == "RW")
    {
        return true;
    }
    if (log)
    {
        LOG_F(WARNING, "No read permission for mmap register[%s]", name.c_str());
    }
    return false;
}

bool CPointMap::canWrite(const string &name, bool log)
{
    if (!find_register(name))
    {
        return false;
    }

    string perm = getPerm(name);
    if (perm == "WO" || perm == "RW")
    {
        return true;
    }
    if (log)
    {
        LOG_F(WARNING, "No write permission for mmap register[%s]", name.c_str());
    }
    return false;
}
