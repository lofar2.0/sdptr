# Running SDPTR regressiontests.

## Usage in terminal

Start a terminal on dop386 (with connection to the arts uniboards):

cd git/sdptr
source ./init_sdptr_regressiontest.sh

*Now the commands from the regressiontest dir can be used.*

### running all tests from 'fw_tests.txt'

fw_regressiontest.sh --sdptr-ip=localhost --sdptr-port=4842 --test --test-runs=3

### writing flash, build sdptr & restart and running all tests from 'fw_tests.txt'

fw_regressiontest.sh --sdptr-ip=localhost --sdptr-port=4842  --rbf=[path to rbf-file] --flash --sdptr --test --test-runs=3

## use a cron job to start the fw_regressiontest (on regtest@dop349)

### run on dop386, writing flash, build sdptr & restart and running all tests from 'fw_tests.txt' 5x and send a mail

`fw_regressiontest.sh --remote-ip=10.87.0.186 --sdptr-ip=localhost --sdptr-port=4842 --sdptr --flash --rbf=latest --test --test-runs=5 --send-mail`

### example line to use in a cronjob (use 'crontab -e' to edit)

`0 23 * * 0 /home/regtest/git/sdptr/regressiontest/fw_regressiontest.sh --remote-ip=10.87.0.186 --sdptr-ip=localhost --sdptr-port=4842 --sdptr --flash --rbf=latest --test --test-runs=5 --send-mail > fw_sdptr_cron.log 2>&1`

* `0 23 * * 0` start every sunday at 23:00
* `--remote-ip=10.87.0.186` ip-adres of the test maschine, connected to the uniboards.(regtest@dop386)
* `--sdptr-ip=localhost` ip-adres(localhost) of the maschine where sdptr is running.(running on dop386)
* `--sdptr-port=4842` port-number of used by sdptr for accepting opc-commands.
* `--sdptr` build and restart sdptr.
* `--flash` flash given rbf file to the uniboards in the user image.
* `--rbf=latest` use lates quartus build. (done with quartus_build_images_cron.sh on dop349)
* `--test` run all tests from 'fw_tests.txt'.
* `--test-runs=5` run all test 5x.
* `--send-mail` send a mail if done.
* `> fw_sdptr_cron.log` put logging of this cronjob in "fw_sdptr_cron.log"
* `2>&1` stdout and stderr in same file
