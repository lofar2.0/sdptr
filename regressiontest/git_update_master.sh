#!/usr/bin/env bash
set -e

# reset local repository, checkout master and pull from remote repository
cd ${HOME}/git/sdptr
git reset --hard
git checkout master
git pull
echo "Now on updated master branch"
