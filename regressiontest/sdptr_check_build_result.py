#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
#   Check logfile for errors and write errors to mailfile.
#   expected logfile is from sdptr(c++) build process.
# Usage:
#   sdptr_check_build_result.py <logfilename> <mailfilename>
############################################################################

import os
import sys


def read_log_file(filename):
    """ readin filename and return text """
    cwd = __file__[:__file__.rfind('/')]
    full_filename = os.path.join(cwd, filename)
    with open(full_filename, 'r') as fd:
        log = fd.read()
    return log


def check_for_error(log):
    """ get all errors from the log text """
    info = []
    add_until_err_end = False
    for line in log.splitlines():
        if "error:" in line.lower():
            info.append(line)
            add_until_err_end = True
            continue
        if "make[" in line.lower():
            add_until_err_end = False
        if add_until_err_end:
            info.append(line)
    return '\n'.join(info)


def write_to_mailfile(info):
    """ write build info to the mail file """
    with open(mailfilename, 'a') as fd:
        if info:
            fd.write("Build failed:\n")
            fd.write(info)
            fd.write("\nFAILED\n")
        else:
            fd.write("Build passed.\n")
            fd.write("\nPASSED\n")


def main():
    """ read the logfile, check for errors and write result to the mailfile """
    log = read_log_file(logfilename)
    info = check_for_error(log)
    write_to_mailfile(info)
    if info:
        print("Found error(s) in code:")
        print(info)
        print("FAILED")
        return 1

    print("PASSED")
    return 0


if __name__ == "__main__":
    logfilename = sys.argv[1]
    mailfilename = sys.argv[2]
    result = main()
    sys.exit(result)
