#!/usr/bin/env bash
# set -e
###############################################################################
#
# Copyright (C) 2018
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Build sdptr.
# . Run tests with uniboard stub. (see: sdptr/test/py/stub/unb2_stub.py)
# Description:
#  - Build new sdptr
#  - Startup sdptr and the uniboard stub.
#  - Use sdp_rw.py to check software version and a rw_point (wg_enable).
#
#  ./sdptr_regression_test.sh pc --test-run       # build sdptr and run checks
#  ./sdptr_regression_test.sh none --test-run     # run checks only
###############################################################################

###############################################################################
# Source environment
# . BEWARE: do not put this (or similar) in your .bashrc:
#   . if [ -z "$PS1" ]; then return; fi.
#   This will stop sourcing of .bashrc immediately as a Cron job is not run interactively.
###############################################################################
. $HOME/.bashrc

# set sdptr
INIT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"/../
source ${INIT_DIR}/init_sdptr_python.sh

if [ ! -d "${REGRESSIONTEST_LOG_DIR}" ]; then
    mkdir $REGRESSIONTEST_LOG_DIR
fi

LOGFILE="${REGRESSIONTEST_LOG_DIR}"/regressiontest.log
MAILFILE="${REGRESSIONTEST_LOG_DIR}"/mail.txt

cd "${REGRESSIONTEST_DIR}"

# Delete any previous log files
if [ -f "${LOGFILE}" ]; then
    rm "${LOGFILE}"
fi

# Delete any previous txt files
if [ -f "${MAILFILE}" ]; then
    rm "${MAILFILE}"
fi

# copy all stdout output also to the logfile
exec > >(tee -ia $LOGFILE)
# copy all stderr output also to the same logfile
exec 2> >(tee -ia $LOGFILE)

sdptr_info "$0" "Result will be in ${LOGFILE}"

# helper function for command parsing
exit_with_error() {
    sdptr_error_noexit "${0}" "$@"
    cat <<@EndOfHelp@
Usage: $(basename "${0}") buildsystem [options]
Arguments: build_system     Name of build system = pc or none, if none sdptr is not build

Options: --test-run         Start sdptr and the uniboard stub and do some communication tests.
--> Note: It does not matter where the options are placed: before, in between or after the arguments.
@EndOfHelp@
    exit 1
}

# parse cmdline
POSITIONAL=()
run_test=
while [[ $# -gt 0 ]]
do
    case $1 in
        --test-run)
            run_test=1
            ;;
        -*|--*)
            exit_with_error "Unknown option: ${1}"
            ;;
        *)  POSITIONAL+=("$1")
            ;;
    esac
    shift
done
if [ ${#POSITIONAL[@]} -gt 0 ]; then
    set -- "${POSITIONAL[@]}"
fi

# check the positional parameters
if [ $# -ne 1 ]; then
    exit_with_error "Wrong number of arguments specified."
fi
buildsystem=$1

build_on_pc() {
    echo "Build SDP Translator from clean git/master checkout on a PC" >> "${MAILFILE}"
    sdptr_info "$0" "Build on PC"
    sdptr_exec "$0" sdptr_auto_build.sh

    # check log file for errors
    sdptr_exec "$0" sdptr_check_build_result.py "${LOGFILE}" "${MAILFILE}"
    if grep -Fxq "PASSED" "${LOGFILE}"; then
        # if found
        sdptr_info "$0" "build passed"
        BUILD_PASSED="true"
    else
        # if not found
        sdptr_warning "$0" "build failed"
    fi
}

test_run() {
    # process [options] '> /dev/null 2>&1 &'  Start process in background an do not send program output to stdout
    # Start stub and sdptr for 16 fpga's
    sdptr_info "$0" "Start stub"
    "${STUB_DIR}"/unb2_stub.py -f 16 -g 0 > /dev/null 2>&1 &
    # wait some time.
    sleep 1
    sdptr_info "$0" "Start sdptr using stub"
    "${RUN_DIR}"/sdptr -f 16 -g 0 -d -s > /dev/null 2>&1 &
    # wait some time.
    sleep 8
    #  All proceeses should be started now.
    sdptr_info "$0" "Test opcua"

    # read firmware_version, and check if "uniboard_stub" is part of the name.
    firmware_versions=$(sdp_rw.py --host localhost --port 4840 -r firmware_version)
    if [[ $firmware_versions == *"uniboard_stub"* ]]; then
        sdptr_info "$0" "right firmware_versions received"
        RUN_PASSED="true"
    else
        sdptr_info "$0" "wrong firmware_versions received"
        RUN_PASSED=
    fi

    # read wg_enable, and check if only "False" is returned.
    wg_enable=$(sdp_rw.py --host localhost --port 4840 -r wg_enable)
    if [[ ${wg_enable} != *"True"* ]]; then
        sdptr_info "$0" "wg_enable is False"
        RUN_PASSED="true"
    else
        sdptr_info "$0" "wg_enable is True"
        RUN_PASSED=
    fi

    # write wg_enable = True.
    sdptr_info "$0" "Turn on wg_enable"
    sdp_rw.py --host localhost --port 4840 -w wg_enable [True]*12*16 > /dev/null 2>&1
    sleep 1
    # read wg_enable, and check if only "True" is returned.
    wg_enable=$(sdp_rw.py --host localhost --port 4840 -r wg_enable)
    if [[ ${wg_enable} != *"False"* ]]; then
        sdptr_info "$0" "wg_enable is True"
        RUN_PASSED="true"
    else
        sdptr_info "$0" "wg_enable is False"
        RUN_PASSED=
    fi

    #  'pkill -f "proc_name"'  kill all processes with proc_name
    sdptr_info "$0" "Stop all started background processes"
    pkill -f "sdptr -f"
    pkill -f "unb2_stub.py"
}

BUILD_PASSED=
RUN_PASSED=
if [ "${buildsystem}" == "pc" ]; then
    build_on_pc
fi

if [ "${buildsystem}" == "none" ]; then
    echo "Do not build sdptr"
    BUILD_PASSED="none"
fi

if [ -n "${run_test}" ] && [ -n "${BUILD_PASSED}" ]; then
    echo "Test if sdptr is working"
    test_run
fi

# print build and/or run state on last rows
if [ "${BUILD_PASSED}" != "none" ]; then
    if [ -n "${BUILD_PASSED}" ]; then
        echo "BUILD PASSED"
    else
        echo "BUILD FAILED"
    fi
fi

if [ -n "${run_test}" ]; then
    if [ -n "${RUN_PASSED}" ]; then
        echo "RUN PASSED"
    else
        echo "RUN FAILED"
    fi
fi

if [ -n "${send_mail}" ]; then
    sdptr_exec "$0" sdptr_regressiontest_mail.py --logfile "${LOGFILE}"
fi

unset INIT_DIR