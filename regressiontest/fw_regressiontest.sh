#!/usr/bin/env bash
# set -e

###############################################################################
#
# Copyright (C) 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

###############################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Load new uniboard firmware and sdptr software and test loaded version.
# . Run tests from file.
# Description:
#  Copy new software to pi, restart and test version (--sdptr).
#  Flash new user firmware, reload image and test version (--flash).
#  Run test from "fw_tests.txt" (--test)
#  Send test response mail to adresses in "mailing_list.txt" (--send-mail).
#
#  ./fw_regressiontest.sh --remote-ip=10.87.0.186
#                         --sdptr-ip=localhost --sdptr-port=4842 --sdptr
#                         --rbf=<path to rbf file>
#
#  or (latest rbf build will be used from latest_rbf_file dir)
#  ./fw_regressiontest.sh --remote-ip=10.87.0.186
#                         --sdptr-ip=localhost --sdptr-port=4842 --sdptr
###############################################################################

export TERM=xterm-256color

###############################################################################
# Source environment
# . BEWARE: do not put this (or similar) in your .bashrc:
#   . if [ -z "$PS1" ]; then return; fi. /home/regtest/git/radiohdl/regressiontest/modelsim_regression_test_vhdl_cron.sh
#   This will stop sourcing of .bashrc immediately as a Cron job is not run interactively.
###############################################################################
source "${HOME}"/.bashrc

ssh-add "${HOME}"/.ssh/id_rsa

# set sdptr
INIT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"/../
source "${INIT_DIR}"/init_sdptr_regressiontest.sh

LOGFILE=${REGRESSIONTEST_LOG_DIR}/fw_regressiontest.log
MAILFILE=${REGRESSIONTEST_LOG_DIR}/fw_mail.txt
TESTFILE=${REGRESSIONTEST_DIR}/fw_tests.txt
EXPTESTFILE=${REGRESSIONTEST_DIR}/exp_fw_tests.txt
EXPTESTFILE_CLEAN=${REGRESSIONTEST_DIR}/exp_fw_tests_clean.txt

RADIOHDL_BUILD_RESULT=${HOME}/hdl_build_result

# Delete any previous log file if it exist
[[ -f "${LOGFILE}" ]] && rm "${LOGFILE}"

# Delete any previous mail file if it exist
[[ -f "${MAILFILE}" ]] && rm "${MAILFILE}"

# copy all stdout output also to the logfile
exec > >(tee -ia "${LOGFILE}")
# copy all stderr output also to the same logfile
exec 2> >(tee -ia "${LOGFILE}")

sdptr_info "$0" "Result will be in ${LOGFILE}"

# helper function for command parsing
exit_with_error() {
    sdptr_error_noexit "$0" "$@"
    cat <<@EndOfHelp@
Usage: $(basename "$0") [options]

Options: --remote-ip=*      Ip address of remote maschine to run this test.
         --sdptr-ip=*       Ip address of SDP translator host (RaspberryPi) normaly 10.99.0.250.
         --sdptr-port=*     Udp port of SDP translator to connect to, normaly 4840.
         --rbf=*            Rbf file to flash (full path).
         --flash            Flash new firmware to the fpgas and reload.
         --sdptr            Copy and restart new version of the SDP Translator.
         --test             Do tests from fw_tests.txt.
         --test-runs=*      Number of test runus, normaly 3x.
         --send-mail        Send mail with test status.
--> Note: It does not matter where the options are placed: before, in between or after the arguments.
@EndOfHelp@
    exit 1
}

# parse cmdline
remote_ip=
sdptr_ip="localhost"
sdptr_port=
rbffile="latest"
flash=
sdptr=
test=
test_runs=1
send_mail=
while [[ $# -gt 0 ]]
do
    case $1 in
        --remote-ip=*)
            remote_ip=${1#*=}
            ;;
        --sdptr-ip=*)
            sdptr_ip=${1#*=}
            ;;
        --sdptr-port=*)
            sdptr_port=${1#*=}
            ;;
        --rbf=*)
            rbffile=${1#*=}
            ;;
        --flash)
            flash=1
            ;;
        --sdptr)
            sdptr=1
            ;;
        --test)
            test=1
            ;;
        --test-runs=*)
            test_runs=${1#*=}
            ;;
        --send-mail)
            send_mail=1
            ;;
        -*|--*)
            exit_with_error "Unknown option: ${1}"
            ;;
        *)  POSITIONAL+=("$1")
            ;;
    esac
    shift
done

export SDPTR_IP="${sdptr_ip}"
export SDPTR_PORT="${sdptr_port}"

if [[ -n "${flash}" ]] && [[ "${rbffile}" == "latest" ]]; then
    latest_dir=$(ls -1t "${RADIOHDL_BUILD_RESULT}" | grep lofar2_unb2b_sdp_station | head -1)
    rbffiles=${RADIOHDL_BUILD_RESULT}/${latest_dir}/*.rbf
    full_rbf_filename=$(ls ${rbffiles} | grep -E "*[0-9a-f]{9}.rbf")
    rbf_filename=$(basename ${full_rbf_filename})
    rm -f "${HOME}"/latest_rbf_file/*
    cp "${full_rbf_filename}" "${HOME}/latest_rbf_file/${rbf_filename}"
fi

# check parameter combinations
if [[ -z "${sdptr_ip}" ]]; then
    exit_with_error "ip address of sdptr host needed --sdptr-ip. (normaly localhost)"
fi

if [[ -z "${sdptr_port}" ]]; then
    exit_with_error "opcua portnumber of sdptr needed --sdptr-port. (normaly 4840 or 4842)"
fi

if [[ -n "${flash}" ]] && [[ -z "${rbffile}" ]]; then
    exit_with_error "need rbf file for flashing --rbf=[full-path/*.rbf]"
fi

if [[ -n "${test}" ]] && [[ -z "${rbffile}" ]]; then
    exit_with_error "need rbf file for version test --rbf=[full-path/*.rbf]"
fi

kill_old_test_processes() {
    kill $(ps -aux | grep 'git/sdptr/test' | awk '{print $2}')
}

flash_fw() {
    echo "Flash ${rbffile} to USER image" | tee -a ${MAILFILE}
    sdptr_exec "$0" sdp_firmware.py --host "${sdptr_ip}" --port "${sdptr_port}" --write --image USER --file "${rbffile} -vv"
    exitcode=$?
    if [[ $exitcode -eq 0 ]]; then
        echo -e "- PASSED\n" | tee -a ${MAILFILE}
    else
        echo -e "- FAILED\n" | tee -a ${MAILFILE}
    fi
    echo $exitcode
}

reload_fw() {
    echo "Reload USER image" | tee -a ${MAILFILE}
    sdptr_exec "$0" sdp_rw.py --host "${sdptr_ip}" --port "${sdptr_port}" -w boot_image "[1]*16"
    echo "wait until rebooted"
    sleep 20

    #  All firmware should be started now.
    reload_passed="true"
    # get firmware version name from rbf filename
    rbf_firmware_version=$(basename "${rbffile}" | cut -d "-" -f1)
    rbf_git_hash=$(basename "${rbffile}" | cut -d "-" -f2 | cut -d "." -f1)

    echo "Expected firmware version name: '${rbf_firmware_version}',  git hash: '${rbf_git_hash}'"
    # Read firmware_version from fpga's and write to file.
    sdp_rw.py --host "${sdptr_ip}" --port "${sdptr_port}" -r firmware_version > firmware_versions.txt
    # And check if same as from loaded rbf file.
    cat < firmware_versions.txt | {
        while read -r line; do
            if [[ -n "${line}" ]]; then
                # check if line starts with node
                if [[ ${line} == "node"* ]]; then
                    node_info=$(echo "${line}" | cut -d ":" -f1)
                    firmware_version=$(echo "${line}" | cut -d "_" -f3-)
                    firmware_git_hash=$(echo "${line}" | cut -d "_" -f2)
                    echo "Running firmware version name: '${firmware_version}',  git hash: '${firmware_git_hash}'"
                    if [[ "${firmware_version}" != "${rbf_firmware_version}" ]]; then
                        echo "- Wrong firmware versions received from: ${node_info}" | tee -a ${MAILFILE}
                        reload_passed=
                    fi
                    if [[ ${firmware_git_hash} != "${rbf_git_hash}" ]]; then
                        echo "- Wrong firmware git hash received from: ${node_info}" | tee -a ${MAILFILE}
                        reload_passed=
                    fi
                fi
            fi
        done
    }
    if [[ -n "${reload_passed}" ]]; then
        echo -e "- PASSED\n" | tee -a ${MAILFILE}
    else
        echo -e "- FAILED\n" | tee -a ${MAILFILE}
    fi
    echo 0
}

build_sdptr() {
    echo "Build new sdptr" | tee -a ${MAILFILE}
    # build new sdptr
    sdptr_exec "$0" cd "${SDPTR_DIR}"
    sdptr_exec "$0" autoreconf -f -i
    sdptr_exec "$0" ./configure
    sdptr_exec "$0" make clean
    sdptr_exec "$0" ./tag_software_version.py
    sdptr_exec "$0" make
    exitcode=$?

    if [[ $exitcode -eq 0 ]]; then
        echo -e "- PASSED\n" | tee -a ${MAILFILE}
    else
        echo -e "- FAILED\n" | tee -a ${MAILFILE}
    fi
    echo 0
}

restart_sdp_translator() {
    echo "Restart SDP translator" | tee -a ${MAILFILE}
    sdptr_restart_passed=

    # stop sdptr and reload
    sdptr_exec "$0" restart_sdptr
    sleep 20
    #  All processes should be started now and boards detected.

    # read sdptr version.
    sdp_version_str=$(sdp_rw.py --host "${sdptr_ip}" --port "${sdptr_port}" -r software_version)
    sdp_version=$(echo "${sdp_version_str}" | head -n1 | cut -d ":" -f2 | tr -d '[:space:]')
    echo "Running SDP translator version name: '${sdp_version}'"

    if [[ "${sdp_version}" != *"None"* ]]; then
        sdptr_restart_passed="true"
        echo -e "- PASSED\n" | tee -a ${MAILFILE}
    else
        echo -e "- FAILED\n" | tee -a ${MAILFILE}
    fi

    [[ -z "${sdptr_restart_passed}" ]] && sdptr_passed=
    [[ -n "${sdptr_passed}" ]] && echo 0 || echo 1
}

check_active_nodes() {
    echo "Check if all FPGAs are connected" | tee -a ${MAILFILE}
    # check if all nodes have factory or user image loaded.
    sdp_boot=$(sdp_rw.py --host "${sdptr_ip}" --port "${sdptr_port}" -r boot_image)
    [[ "${sdp_boot}" = *"-1"* ]] && nodes_check_passed= || nodes_check_passed="true"
    if [[ -n "${nodes_check_passed}" ]]; then
        echo -e "- PASSED\n" | tee -a ${MAILFILE}
    else
        echo "- Not all FPGAs are connected" | tee -a ${MAILFILE}
        echo -e "- FAILED\n" | tee -a ${MAILFILE}
    fi
}

restart_and_check_sdptr() {
    echo "Copy new SDP translator to /usr/local/bin and restart" | tee -a ${MAILFILE}
    n_restarts=0
    max_restarts=3
    boot_passed=
    while [[ -z "${boot_passed}" ]] && [[ n_restarts -lt max_restarts ]]; do
        # copy new sdptr to run location
        sdptr_exec "$0" rm -f /usr/local/bin/sdptr
        sdptr_exec "$0" cp -v "${SDPTR_DIR}"/src/sdptr /usr/local/bin/
        # stop sdptr and reload
        sdptr_exec "$0" restart_sdptr
        let "n_restarts = n_restarts + 1"
        sleep 20

        #  All processes should be started now.
        echo "Check if same SDP translator versions"
        sdptr_passed="true"
        sdptr_version=$(head -n 1 "${SDPTR_DIR}"/version.txt)
        echo "Expected SDP translator version name: '${sdptr_version}'"
        # read sdptr version.
        sdp_version_str=$(sdp_rw.py --host "${sdptr_ip}" --port "${sdptr_port}" -r software_version)
        sdp_version=$(echo "${sdp_version_str}" | head -n1 | cut -d ":" -f2 | tr -d '[:space:]')
        echo "Running SDP translator version name: '${sdp_version}'"
        if [[ "${sdp_version}" != "${sdptr_version}" ]]; then
            echo "- Expected version: '${sdptr_version}'" | tee -a ${MAILFILE}
            echo "- Running version : '${sdp_version}'" | tee -a ${MAILFILE}
            sdptr_passed=
        fi

        # check if all nodes are in user image
        sdp_boot=$(sdp_rw.py --host "${sdptr_ip}" --port "${sdptr_port}" -r boot_image)
        [[ "${sdp_boot}" = *"-1"* ]] && boot_passed= || boot_passed="true"
    done

    if [[ n_restarts -gt 1 ]]; then
        echo "- Restarted SDP translator ${n_restarts} time(s)" | tee -a ${MAILFILE}
    fi
    if [[ -n "${boot_passed}" ]]; then
        echo -e "- PASSED\n" | tee -a ${MAILFILE}
    else
        echo -e "- FAILED\n" | tee -a ${MAILFILE}
    fi
    [[ -z "${boot_passed}" ]] && sdptr_passed=
    [[ -n "${sdptr_passed}" ]] && echo 0 || echo 1
}

look_for_passed() {
    # ignore case
    local orig_nocasematch=$(shopt -p nocasematch; true)
    shopt -s nocasematch

    passed=
    while read -r line; do
        if [[ -n "${line}" ]]; then
           [[ "${line}" = *"PASSED"* ]] && passed="true"
           [[ "${line}" = *"SUCCESS"* ]] && passed="true"
        fi
    done < ${logfile_nr}

    # set back original nocasematch
    $orig_nocasematch
    echo "${passed}"
}

look_for_failed() {
    # ignore case
    local orig_nocasematch=$(shopt -p nocasematch; true)
    shopt -s nocasematch

    failed=
    while read -r line; do
        if [[ -n "${line}" ]]; then
           [[ "${line}" = *"FAILED"* ]] && failed="true"
           [[ "${line}" = *"FAILURE"* ]] && failed="true"
           [[ "${line}" = *"ERROR"* ]] && [[ "${line}" != *"_error"* ]] && failed="true"
        fi
    done < ${logfile_nr}

    # set back original nocasematch
    $orig_nocasematch
    echo "${failed}"
}

run_test() {
    echo -e "\nRun all tests from fw_tests.txt ${test_runs}x" | tee -a ${MAILFILE}
    test_passed="true"

    # substitude env variables
    envsubst < ${TESTFILE} > ${EXPTESTFILE}
    # skip lines starting with #
    grep -v '^#' < ${EXPTESTFILE} > ${EXPTESTFILE_CLEAN}

    for run in $(seq 1 $test_runs); do
        echo "- $(date +%F)  $(date +%T): Start testrun ${run} from ${test_runs}" | tee -a ${MAILFILE}
        test_nr=0
        while read -r line; do
            if [[ -n "${line}" ]]; then
                let "test_nr = test_nr + 1"
                logfile_nr=${REGRESSIONTEST_LOG_DIR}/"run-${run}-test-${test_nr}.log"
                echo "${line}" > ${logfile_nr}
                ${line} &>> ${logfile_nr}
                if [[ -n $(look_for_passed) ]] && [[ -z $(look_for_failed) ]]; then
                    echo "  - Run test ${test_nr}: ${line}"
                    echo "    - PASSED"
                else
                    test_passed=
                    echo "  - Test ${test_nr}: ${line}" | tee -a ${MAILFILE}
                    echo "    FAILED (see ${logfile_nr})" | tee -a ${MAILFILE}
                fi
            fi
        done < ${EXPTESTFILE_CLEAN}
    done

    if [[ "${test_passed}" = "true" ]]; then
        echo -e "\n- All tests PASSED" | tee -a ${MAILFILE}
    else
        echo -e "\n- Some tests FAILED" | tee -a ${MAILFILE}
    fi
}

if [[ -n "${remote_ip}" ]]; then
    # copy rbf file to flash to the boards
    sdptr_exec "$0" scp "${HOME}/latest_rbf_file/${rbf_filename}" "${remote_ip}":"${HOME}/latest_rbf_file/${rbf_filename}"
    # first update remote sdptr dir (git pull)
    sdptr_exec "$0" ssh -t "${remote_ip}" "${HOME}"/git/sdptr/regressiontest/git_update_master.sh
    # kill old scrips on remote pc
    remote_cmd="/home/regtest/git/sdptr/regressiontest/kill_old_processes.sh"
    # run this script on remote pc
    remote_cmd="/home/regtest/git/sdptr/regressiontest/fw_regressiontest.sh"
    remote_args="--sdptr-ip=${sdptr_ip} --sdptr-port=${sdptr_port}"
    # add options to remote args
    [[ -n "${sdptr}" ]] && remote_args="${remote_args} --sdptr"
    [[ -n "${flash}" ]] && remote_args="${remote_args} --rbf="${HOME}/latest_rbf_file/${rbf_filename}" --flash"
    [[ -n "${test}" ]] && remote_args="${remote_args} --test --test-runs=${test_runs}"

    sdptr_exec "$0" ssh -A -t "${remote_ip}" "${remote_cmd}" "${remote_args}"
    # get remote logfiles
    sdptr_exec "$0" scp "${remote_ip}":"${LOGFILE}" "${REGRESSIONTEST_LOG_DIR}/remote_fw_regressiontest.log"
    sdptr_exec "$0" scp "${remote_ip}":"${REGRESSIONTEST_LOG_DIR}/run-*-test-*.log" "${REGRESSIONTEST_LOG_DIR}/"
    sdptr_exec "$0" scp "${remote_ip}":"${MAILFILE}" "${MAILFILE}"

else
    sdptr_restart_passed=
    nodes_check_passed=
    reload_passed=
    sdptr_passed=
    test_passed=

    # kill hanging processes of last test
    kill_old_test_processes
    # restart/start sdp translator if not running
    restart_sdp_translator
    # check if all nodes are active, if sdptr is available
    [[ -n "${sdptr_restart_passed}" ]] && check_active_nodes

    # run options only if sdptr is running and all nodes are available
    if [[ -n "${sdptr_restart_passed}" ]] && [[ -n "${nodes_check_passed}" ]]; then
        [[ -z "${flash}" ]] && reload_passed="false"
        [[ -z "${sdptr}" ]] && sdptr_passed="false"
        [[ -z "${test}" ]] && test_passed="false"

        # run selected options, on error *_passed is empty
        [[ -n "${flash}" ]] && flash_fw && reload_fw
        [[ -n "${reload_passed}" ]] && [[ -n "${sdptr}" ]] && build_sdptr && restart_and_check_sdptr
        [[ -n "${sdptr_passed}" ]] && [[ -n "${test}" ]] && run_test
    else
        echo "FAILED to run --flash, --sdptr or --test, sdptr not running or boards not available" | tee -a ${MAILFILE}
    fi

    # print build and/or run state on last rows

    [[ "${sdptr_restart_passed}" = "true" ]] && echo "SDPTR-RESTART-PASSED" || echo "SDPTR-RESTART-FAILED"
    [[ "${nodes_check_passed}" = "true" ]] && echo "NODES-CHECK-PASSED" || echo "NODES-CHECK-FAILED"

    if [[ -n "${sdptr}" ]]; then
        [[ "${sdptr_passed}" = "true" ]] && echo "SDPTR-PASSED" || echo "SDPTR-FAILED"
    fi

    if [[ -n "${flash}" ]]; then
        [[ "${reload_passed}" = "true" ]] && echo "FLASH-PASSED" || echo "FLASH-FAILED"
    fi

    if [[ -n "${test}" ]]; then
        [[ "${test_passed}" = "true" ]] && echo "TEST-PASSED" || echo "TEST-FAILED"
    fi
fi

[[ "${test_passed}" = "true" ]] && echo "TEST-PASSED" > "${REGRESSIONTEST_LOG_DIR}/result.txt" || echo "TEST-FAILED" > "${REGRESSIONTEST_LOG_DIR}/result.txt"

if [[ -n "${send_mail}" ]]; then
    sdptr_exec "$0" fw_regressiontest_mail.py --logfile "${LOGFILE}"
fi
