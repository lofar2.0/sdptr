#!/usr/bin/env bash

###############################################################################
#
# Copyright (C) 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# This script can be used to kill all hanging fw_regressiontest scripts after a failure.
# It is used in fw_regressiontest.py to kill processes om the remote test pc and
# can be used standalone.

kill -9 $(ps -aux | grep 'fw_regressiontest.sh' | awk '{print $2}')
kill -9 $(ps -aux | grep 'git/sdptr/test' | awk '{print $2}')

echo 0