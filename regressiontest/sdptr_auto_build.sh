#!/usr/bin/env bash
# set -e
###############################################################################
#
# Copyright (C) 2018 
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
###############################################################################

###############################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Build sdptr (SDP Translator) written in c++.
# Description:
# . Configure make system, clean old build, tag software version and build again sdptr.
###############################################################################

export "TERM=xterm-256color"

echo "start init_sdptr"
INIT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"/../
cd "${INIT_DIR}"
. ./init_sdptr.sh
echo "init_sdptr done"

# read generic functions/definitions
. "${SDPTR_DIR}"/generic.sh

# configure all make files
sdptr_exec "$0" autoreconf -f -i
sdptr_exec "$0" ./configure

# clean all generated files
sdptr_exec "$0" make clean

# build again and store output in a log file
sdptr_exec "$0" ./tag_software_version.py
sdptr_exec "$0" make