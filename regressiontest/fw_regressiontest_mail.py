#!/usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . e-mail result of sdptr regression test, this script is
#   called by fw_regressiontest.sh
# Description:
# . read recipients from ~/sdptr_mail_list.txt.
# . send email with sdptr test result to recipients.
#
# ##########################################################################

import sys
import os
from email.mime.text import MIMEText
from subprocess import Popen, PIPE
from argparse import ArgumentParser


def main():
    maillist = get_mail_list()

    this_pc_ip = get_host_ip()
    git_hash = get_git_hash()

    message =   "------------------------------------------------------------------\n"
    message +=  "  This is an automatically generated email, don't reply it \n"
    message += f"  Logging can be found on IP {this_pc_ip} in \n"
    message += f"  - {os.path.dirname(result_file_name)}/ \n"
    message += f"    - program: {os.path.basename(result_file_name)} \n"
    message +=  "    - tests  : run-*-test-*.log \n"
    message +=  "------------------------------------------------------------------\n"
    message += "\n"
    message += f"GIT revision: {git_hash}\n"
    message += "\n"

    reg_mail_info = get_regression_mail_info()
    result = "FAILED" if "FAILED" in reg_mail_info else "PASSED"
    message += reg_mail_info

    msg = MIMEText(message)
    msg["From"] = "regtest"
    msg["To"] = ", ".join(maillist)
    if args.nochange is False:
        msg["Subject"] = f"[REGTEST] - SDP FW - {result}"
    else:
        msg["Subject"] = f"[REGTEST] - SDP FW - nothing changed - {result}"

    print(msg)

    p = Popen(["/usr/sbin/sendmail", "-t", "-oi"], stdin=PIPE)
    p.communicate(msg.as_bytes())


def get_host_ip():
    p = Popen("hostname -I", shell=True, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    return stdout.strip().decode("utf-8").split()[0]


def get_git_hash():
    # Get the GIT revision to put in the email subject
    os.chdir(os.getenv("SDPTR_DIR"))
    p = Popen("git rev-parse HEAD", shell=True, stdout=PIPE, stderr=PIPE)
    stdout, stderr = p.communicate()
    git_revision = stdout.decode("utf-8")
    os.chdir(os.getenv("HOME"))
    return git_revision


def get_mail_list():
    filename = "sdptr_mail_list.txt"
    recipients = []
    with open(os.path.join(os.getenv("HOME"), filename), "r") as fd:
        data = fd.readlines()
    for line in data:
        if line.startswith("#"):
            continue
        address = line.split("#")[0].strip()
        if "@" in address:
            recipients.append(address)
    print(recipients)
    return recipients


def get_regression_mail_info():
    try:
        with open(os.path.join(out_dir, mail_file_name), "r") as fd:
            info = fd.read()
        return info
    except FileNotFoundError:
        return f'FAILURE, "{mail_file_name}" not found'


if __name__ == "__main__":
    # Parse command line arguments
    argparser = ArgumentParser(description="SDPTR regressiontest mail script.")
    argparser.add_argument(
        "--logfile", type=str, help="report no test done"
    )
    argparser.add_argument(
        "-n", "--nochange", action="store_true", default=False, help="report no test done"
    )
    argparser.add_argument(
        "-v",
        "--verbosity",
        default="INFO",
        help="stdout log level can be [ERROR | WARNING | INFO | DEBUG]",
    )
    args = argparser.parse_args()

    result_file_name = args.logfile
    out_dir = os.path.dirname(args.logfile)
    mail_file_name = "fw_mail.txt"

    sys.exit(main())
