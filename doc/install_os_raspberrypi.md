##### Installation of raspbian os on a SDcard.

Download Raspberry Pi Imager for your operating system

- Linux:   https://downloads.raspberrypi.org/imager/imager_latest_amd64.deb
- Windows: https://downloads.raspberrypi.org/imager/imager_latest.exe
- macOS:   https://downloads.raspberrypi.org/imager/imager_latest.dmg 

Install and run the Imager (you need a sd-card reader).

- CHOOSE OS,  "Raspberry Pi OS (other)"  >  "Raspberry Pi OS Lite (32-bit)"
- CHOOSE STORA..., "Select SDcard"
- SETTINGS, check 'Enable SSH', 'Set locale settings' and select 'Time zone' SAVE 
- WRITE.

##### Startup and configuration.

Put the SD-card in the RaspberryPi and startup, login=pi, password=raspberry.

```
sudo raspi-config
```

##### Install missing tools/programs.

```
sudo apt-get install git vim screen
```

>For only running "sdptr" more is not needed.