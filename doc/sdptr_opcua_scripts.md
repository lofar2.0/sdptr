### Python SDP control using OPC-ua.

Python scripts to test and control the "SDP Translator" (OPC-ua to SDP).

Source init_sdptr.sh in git/sdptr to get access to all script from everywhere, only in the active terminal.
```
cd git/sdptr
. ./init_sdptr.sh
```
The scrips listed below can be run without the ./
```
sdp_rw -h
```

#### available scripts.
```
sdp_rw.py             # universal script for reading/writing all available points. 
beamlet_stream.py     # control beamlet stream.
stat_stream_sst.py    # control/monitor/plot SST statistics streams.
stat_stream_bst.py    # control/monitor/plot BST statistics streams.
stat_stream_xst.py    # control/monitor/plot XST statistics streams.
subband_weights.py    # set/get beamlet weights.
bf_weights.py         # set/get/test bf weights.
ring_common.py        # set/get ring settings.
wg.py                 # control waveform generator.
plot_input_data.py    # receive input data and make a plot from it.
plot_histogram.py     # receive histogram data and make a plot from it.
sdp_firmware.py       # reboot, read, write, verify, test an image.
sdp_speedtest.py      # test read performance of the sdp translator.
```

>more info about the scripts can be found in: ./test/py/readme.md
