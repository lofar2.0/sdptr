#### Running the SDP Translator (sdptr).

sdptr can run without hardware connected.

first source init_sdptr.sh
```
cd ~/git/sdptr
. ./init_sdptr.sh
```
Now the translator can be run from everywhere
```
sdptr [options]
```

options:

```
-h, --help      : This screen
-v, --version   : SDPTR version
-d, --nodeamon  : With this flag, sdptr runs NOT as daemon
-p, --port      : OPC-ua port to use, defaults to 4840
-f, --fpgas     : Specify number of FPGAs, defaults to 16
-g, --first_gn  : Specify first global number, defaults to 0
-b, --beamsets  : Specify number of beamsets, defaults to 1
-i, --ip_prefix : Specify ip prefix, defaults to '10.99.'
-V, --verbosity : debug level -9=OFF, -3=FATAL, -2=ERROR, -1=WARNING, 0=INFO, 1=DEBUG, defaults to 0
```

#### Setup for automatic running
The following files need to be copied to '/usr/local/bin' as root:
- sdptr
- run_sdptr (script used by user or cron)
- restart_sdptr (script used by user)

To be able to run the LBA and HBA translator at the same time make in the same directory 2 symbolic links:
```
ln -s sdptr sdptr-lba 
ln -s sdptr sdptr-hba
```

To autostart sdptr for LBA and HBA on powerup of the raspberrypi or if it was stopped for an other reason, add the run_sdptr to the cronjob.

Start the cron editor, **sudo crontab -e** and add the following line, if done save and close the file. 
```
* * * * * /usr/local/bin/run_sdptr
```


#### Restart the sdp translator(s) manualy
The following command can be used to restart the both translators:
```
sudo restart_sdptr
```



