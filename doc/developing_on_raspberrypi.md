#### Setting up Raspberrypi-4 for running and developing the sdp translator (sdptr)

##### Install raspbian os on a SDcard.

see `install_os_raspberrypi.md`

##### Setup for developing sdptr.

Install missing tools/programs.

```
sudo apt-get install build-essential gcc pkg-config cmake autoconf automake
sudo apt-get install vim screen
```

Add a git directory.

```
mkdir ~/git
cd ~/git
```

##### Installation of open62541 opc-ua library.

The opc-ua library is needed by sdptr.

 Get source code:

```
git clone https://github.com/open62541/open62541.git
cd ~/git/open62541
git submodule update --init --recursive
```

Compile and install.

```
mkdir ~/git/open62541/build
cd ~/git/open62541/build
cmake ..
make
sudo make install
```

##### Installation and compiling sdptr.

Install from git repository.

```
cd ~/git
git clone https://git.astron.nl/lofar2.0/sdptr.git
```

First time or after changing the Makefile

```
cd ~/git/sdptr
autoreconf -f -i
./configure
```

Compile sdptr.

```
cd ~/git/sdptr
make
```

##### Running sdptr.

see `running_sdptr.md`
