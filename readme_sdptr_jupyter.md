# Running SDPTR scrips in jupyter.

## Running sdptr scripts from a jupyter terminal

### Start a terminal.

menu[ + ] -> Other -> Terminal.

In the terminal, goto the `sdptr` dir and source init.

`source ./init_sdptr_jupyter.sh`

Now the commands from the control dir can be used.

Aliases for use with --host

* LBA  use --host $HL
* HBA0 use --host $HH0
* HBA1 use --host $HH1

*'menu[ + ]' is the blue button with + sign*

### Using sdp_rw.py

sdp_rw.py --host $HL -l  # list all available opcua points

sdp_rw.py --host $HL -r  firmware_version

sdp_rw.py --host $HL -r  boot_image\
sdp_rw.py --host $HL -w  boot_image "[1] * 16"

### writing new images to the uniboards

* put new image in ${HOME}/notebooks/unb2_images
* change flash.config if needed
* write image

flash user image into boards

LBA `./flash_image unb2c --host $HL --user`\
HBA0 `./flash_image unb2c --host $HH0 --user`\
HBA1 `./flash_image unb2c --host $HH1 --user`

see result with

`cat "${HOME}/notebooks/sdptr/flash_status.txt"`

reboot with

LBA `sdp_rw.py --host $HL -w  boot_image "[1] * 16"`\
HBA0 `sdp_rw.py --host $HH0 -w  boot_image "[1] * 4"`\
HBA1 `sdp_rw.py --host $HH1 -w  boot_image "[1] * 4"`

## Running SDPTR scripts from jupyter notebook

From a jupyter cell run a terminal command.

flash factory image, it will do a write, read and verify.\
`!cd ${HOME}/notebooks/sdptr && source ./init_sdptr_jupyter.sh > nohup && ./flash_image unb2c --host="${HL}" --fact`

flash user image, it will do a write only.\
`!cd ${HOME}/notebooks/sdptr && source ./init_sdptr_jupyter.sh > nohup && ./flash_image unb2c --host="${HL}" --user`

flash factory-image (write, read and verify) if succes flash also user-image (write only).\
`!cd ${HOME}/notebooks/sdptr && source ./init_sdptr_jupyter.sh > nohup && ./flash_image unb2c --host="${HL}" --fact --user`

see flash result.\
`!cat "${HOME}/notebooks/sdptr/flash_status.txt"`

## Using OPCua read/write commands direct from a jupyter notebook

An example how to use the opc-ua client direct in a cell see:
'test/notebooks/read_firmware_version_example.ipynb'
