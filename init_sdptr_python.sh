#!/usr/bin/env bash
# set -e
###############################################################################
#
# Copyright (C) 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

#
# Initialisation script to setup the environment variables in this terminal
#

# only set variables if we didn't set them before
if [ "${python_init_read:-not_set}" = "not_set" ]; then

# Make sure the script is sourced and not accidentally given execution rights and just executes it.
if [[ "$_" == "${0}" ]]; then
    echo "ERROR: Use this command with '. ' or 'source '"
    sleep 1
    exit
fi

# Figure out where this script is located and set environment variables accordingly
SDPTR_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
export UNB2_IMAGES="${HOME}/unb2_images"

echo "This terminal will be setup for running python test scripts"

# used to check if a program exists
command_exists () {
    type "$1" &> /dev/null ;
}

# source or make/source a virtual python3 environment
if [[ -d "${SDPTR_DIR}/venv-sdptr" ]]; then
    # source venv only
    source ${SDPTR_DIR}/venv-sdptr/bin/activate
else
    # install a python virtual environment if it not excists
    echo "First check needed c++ build tools, exit if missing"
    if ! command_exists make ; then
        echo "first install missing build-essential use:"
        echo "  sudo apt install build-essential"
        exit 1
    fi
    if ! command_exists gcc ; then
        echo "first instal missing gcc use:"
        echo "  sudo apt install gcc"
        exit 1
    fi
    if ! command_exists g++ ; then
        echo "first install missing g++ use:"
        echo "  sudo apt install g++"
        exit 1
    fi
    if ! command_exists python3-config ; then
        echo "first install missing python3-dev use:"
        echo "  sudo apt install python3-dev"
        exit 1
    fi
    python3 -m pip install virtualenv
    python3 -m virtualenv ${SDPTR_DIR}/venv-sdptr
    source ${SDPTR_DIR}/venv-sdptr/bin/activate
    # install dependicies
    python -m pip install numpy matplotlib asyncua ipykernel
    python -m pip install Cython faster-fifo
fi

source ${SDPTR_DIR}/generic.sh


# Extend the PATH and PYTHONPATH variables
sdptr_pathadd "PATH" ${SDPTR_DIR}/test/py/control ${SDPTR_DIR}/test/py/stub
sdptr_pathadd "PYTHONPATH" ${SDPTR_DIR}/test/py/

# Mark the fact that we read this file and end the guarded part
python_init_read="yes"
fi