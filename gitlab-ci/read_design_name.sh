#!/bin/bash

IMAGE=$1
UNB=2
PN=0

cd /home/schuur/git/upe_gear
source init_upe.sh

# Below command will return 0 when design name matches, otherwise 1.
util_system_info.py --unb2 2 --pn2 0 -n 2 | grep Design | grep $IMAGE

# exit with return code of last command
exit $?
