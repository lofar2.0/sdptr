import unittest
import os

RBF = '/home/schuur/git/hdl/applications/lofar2/images/lofar2_unb2b_sdp_station_full-r8026db491.rbf'
# RBF = '/home/schuur/git/hdl/applications/lofar2/images/lofar2_unb2b_sdp_station_bf-rc125dfd6d.rbf'

# The design name that should be read out
IMAGE = 'lofar2_unb2b_sdp_station_full-r1684353841'
IMAGE = 'lofar2_unb2b_sdp_station_full-r895903289'


class HardwareTest(unittest.TestCase):

    def setUp(self):
        """
        Program the FPGA. Below code will be replaced by calls to the OPC-UA
        python client. For now we are using UPE GEAR in a shell script to
        progam.
        """
        os.system(f'./program_fpga.sh {RBF}')

    def test_design_name(self):
        """
        Read out the firmware design name and check if the name matches the
        programming file. This will be replaced by code that uses the OPC-UA
        python client.
        """
        self.assertEqual(os.system(f'./read_design_name.sh {IMAGE}'), 0)


if __name__ == '__main__':
    unittest.main()
