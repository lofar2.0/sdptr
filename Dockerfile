ARG SOURCE_IMAGE
FROM ${SOURCE_IMAGE} AS build

# Install build tools for sdptr and the C language OPC-UA lib
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y software-properties-common && \
    DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:open62541-team/ppa && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y autoconf automake git make g++ build-essential pkg-config libboost-dev libboost-regex-dev libboost-system-dev libboost-program-options-dev libopen62541-1-dev libopen62541-1-tools zlib1g-dev && \
    apt-get clean

WORKDIR /sdptr

# Copy sdptr
COPY . /sdptr

# Build and install
RUN cd /sdptr && \
    autoreconf -v -f -i && \
    ./configure && \
    ./tag_software_version.py && \
    bash -c "make -j `nproc` install"


FROM ubuntu:20.04

# Install build tools for sdptr and the C language OPC-UA lib
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y software-properties-common && \
    DEBIAN_FRONTEND=noninteractive add-apt-repository ppa:open62541-team/ppa && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y libopen62541-1 zlib1g && \
    apt-get clean

COPY --from=build /usr/local/ /usr/local/
RUN ln -s /usr/local/bin/sdptr /usr/local/bin/sdptr-lba
RUN ln -s /usr/local/bin/sdptr /usr/local/bin/sdptr-hba
RUN ln -s /usr/local/bin/sdptr /usr/local/bin/sdptr-hba0
RUN ln -s /usr/local/bin/sdptr /usr/local/bin/sdptr-hba1