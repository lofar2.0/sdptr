#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Plot histogram data from signal_input_histogram (RAM_ST_HISTOGRAM)
# Description:
# . run ./plot_histogram.py -h for help
# ##########################################################################


import sys
import logging

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list
from base.constants import C_SDP


class Histogram:
    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger("Histogram")
        self.n_nodes = self.client.read("sdp_config_nof_fpgas")  # get number of nodes from server
        self.first_node_nr = self.client.read(
            "sdp_config_first_fpga_nr"
        )  # get number of nodes from server
        self.active_nodes = list(range(self.first_node_nr, self.first_node_nr + self.n_nodes))
        self.data = None
        self.signed_data = True

    def read(self):
        """Read histogram from all nodes and all signal inputs in active_nodes

        Yields data[n_nodes][S_PN][N_HISTO]
        Bin full scale +-256 corresponds to ADC full scale +-8192 (= +-2**13)
        """
        """Read histogram from all nodes and all signal inputs in active_nodes

        Yields data[n_nodes][S_PN][N_HISTO]
        Bin full scale +-256 corresponds to ADC full scale +-8192 (= +-2**13)
        """
        self.data = self.client.read("signal_input_histogram")

    def plot(self, nodes, inputs):
        import numpy as np

        # import matplotlib
        # matplotlib.use('Agg')
        import matplotlib.pyplot as plt

        nodes = self.active_nodes if nodes is None else nodes

        if self.data is None:
            self.read()

        np_data = np.array(self.data)
        
        S_PN = C_SDP.S_pn
        N_HISTO = 512
        BINS_FS = N_HISTO // 2  # bins full scale
        ADC_FS = 2**(C_SDP.W_adc - 1)  # ADC full scale
        N_int_adc = C_SDP.f_adc_MHz * 1e6

        for nr in list(nodes):
            for si in list(inputs):
                ni = nr % self.n_nodes
                s1 = (ni * S_PN * N_HISTO) + (si * N_HISTO)
                s2 = s1 + N_HISTO

                # log (use -vv to show info)
                self.logger.info("node={}, input={}".format(ni, si))

                # log (use -vv to show info)
                self.logger.info("node={}, input={}".format(ni, si))
                _data = np_data[s1:s2]
                if self.signed_data:
                    _lo, _hi = np.split(_data, 2)
                    _data = np.concatenate((_hi, _lo))
                self.logger.info("used data : {}".format(_data))
                self.logger.info("min value : {}".format(_data.min()))
                self.logger.info("max value : {}".format(_data.max()))
                self.logger.info("sum values: {}".format(_data.sum()))

                # plot
                bin_range = range(-BINS_FS, BINS_FS)
                plt.bar(bin_range, height=_data, width=0.90)
                # plt.tight_layout()
                plt.title("Histogram for signal input %i on node %d" % (si, nr))
                plt.xlabel("Input signal level, %d corresponds to ADC full scale = %d" % (BINS_FS, ADC_FS))
                plt.ylabel("Number of occurances, total bins sum = %.e" % N_int_adc)

                plt.grid()
                plt.show()
        return True


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    histogram = Histogram(client)

    try:
        histogram.read()
        histogram.plot(node_list, input_list)
    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(textwrap.dedent("""\
            opcua client command line argument parser:

            Enable, disable WG on all signal inputs (or use ADC data)
            > wg.py --host 10.99.0.250 --port 4842 --setphase 0 --setfreq 2000000 --setampl 0.7 --enable
            > wg.py --host 10.99.0.250 --port 4842 --disable

            Plot histogram of input signal level
            > plot_histogram.py --host 10.99.0.250 --port 4842 -n 65 -i 0 -vv
            > plot_histogram.py --host 10.99.0.250 --port 4842 -n 65 -i 0
            > plot_histogram.py --host 10.99.0.250 --port 4842 -n 65 -i 3,4

            close figure to show next, cntr-c to break.
            \n""")),
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument("-i", "--inputs", type=str, help="inputs to use")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None
    input_list = arg_str_to_list(args.inputs) if args.inputs else range(12)

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
