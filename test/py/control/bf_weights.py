#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Daniel van der Schuur
# Purpose:
# . Test BF weights read/write functionality.
# Description:
# . run ./bf_weights.py -h for help
# ##########################################################################


import sys
import logging
import argparse

import numpy as np

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list
from base.constants import C_SDP

N_PN = C_SDP.N_pn_max  # = 16
N_POL_BF = C_SDP.N_pol_bf  # = 2, number of beam polarizations, X and Y.
A_PN = C_SDP.A_pn  # = 6, number of dual polarization antennas per Processing Node (PN) FPGA.
N_POL = C_SDP.N_pol  # = 2, number of antenna polarizations, X and Y.
S_SUB_BF = C_SDP.S_sub_bf  # = 488, number of selected subbands for beamlets per beamset


class BFWeights:
    """ BFWeights class """
    def __init__(self, client):
        self.client = client
        # get sdp_config from client
        self.n_nodes = self.client.read("sdp_config_nof_fpgas")
        self.n_beamsets = self.client.read("sdp_config_nof_beamsets")
        np.set_printoptions(threshold=np.inf)
        np.set_printoptions(linewidth=315)

        # The physical layout of the FPGA BF RAM.
        self.BF_RAM_SHAPE = (self.n_nodes, self.n_beamsets, N_POL_BF, A_PN, N_POL, S_SUB_BF)

        # https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface
        # . Table: List of OPC-UA CP for BF beamlets
        #   Note: [n_beamsets][S_SUB_BF]=N_BEAMLETS_CTRL.
        #
        # Control Point        | (         .. ,        2,     6,     2,             .. ,      488)
        self.XX_YY_SHAPE = (
            self.n_nodes,
            1,
            A_PN,
            N_POL,
            self.n_beamsets,
            S_SUB_BF,
        )  # 187392 co-polarization weights.    Set of XX and YY weights.
        self.XY_YX_SHAPE = (
            self.n_nodes,
            1,
            A_PN,
            N_POL,
            self.n_beamsets,
            S_SUB_BF,
        )  # 187392 cross-polarization weights. Set of XY and YX weights.
        self.XX_XY_YX_YY_SHAPE = (
            self.n_nodes,
            N_POL_BF,
            A_PN,
            N_POL,
            self.n_beamsets,
            S_SUB_BF,
        )  # 374784 full Jones weights.         Set of XX,XY,YX and YY weights.
        self.XX_SHAPE = (
            self.n_nodes,
            1,
            A_PN,
            1,
            self.n_beamsets,
            S_SUB_BF,
        )  # 93696 weights. Create X-pol beamlets by weighting X-pol antenna inputs.
        self.XY_SHAPE = (
            self.n_nodes,
            1,
            A_PN,
            1,
            self.n_beamsets,
            S_SUB_BF,
        )  # 93696 weights. Create X-pol beamlets by weighting Y-pol antenna inputs.
        self.YX_SHAPE = (
            self.n_nodes,
            1,
            A_PN,
            1,
            self.n_beamsets,
            S_SUB_BF,
        )  # 93696 weights. Create Y-pol beamlets by weighting X-pol antenna inputs.
        self.YY_SHAPE = (
            self.n_nodes,
            1,
            A_PN,
            1,
            self.n_beamsets,
            S_SUB_BF,
        )  # 93696 weights. Create Y-pol beamlets by weighting Y-pol antenna inputs.
        self.PP_SHAPE = (
            self.n_nodes,
            1,
            A_PN,
            1,
            self.n_beamsets,
            S_SUB_BF,
        )  # 93696 weights. Create YX-pol beamlets by weighting YX-pol antenna inputs.

        # Number of weights per node
        # self.N_W_XX_YY       = np.prod(self.XX_YY_SHAPE[1:])
        # self.N_W_XY_YX       = np.prod(self.XY_YX_SHAPE[1:])
        self.N_W_XX_XY_YX_YY = np.prod(self.XX_XY_YX_YY_SHAPE[1:])
        self.N_W_XX = np.prod(self.XX_SHAPE[1:])
        self.N_W_XY = np.prod(self.XY_SHAPE[1:])
        self.N_W_YX = np.prod(self.YX_SHAPE[1:])
        self.N_W_YY = np.prod(self.YY_SHAPE[1:])
        self.N_W_PP = np.prod(self.PP_SHAPE[1:])
        self.nodes = []

    def set_nodes(self, nodes):
        """ Set nodes """
        self.nodes = list(range(self.n_nodes)) if nodes is None else nodes

    def wr_ram(self, weight):
        """ Write RAM """
        wr_weights_list = self.n_nodes * self.N_W_XX_XY_YX_YY * [weight]
        self.client.write("bf_weights", wr_weights_list)

    def rd_ram(self):
        """
        Read RAM and return Numpy array in BF RAM shape.
        """
        rd_data = self.client.read("bf_weights")
        rd_data_arr = np.array(rd_data).reshape(self.BF_RAM_SHAPE)
        return rd_data_arr

    ###########################################################################
    # XX
    ###########################################################################
    def wr_xx(self, weight):
        """
        Write XX weights only
        """
        wr_weights_list = self.n_nodes * self.N_W_XX * [weight]
        self.client.write("bf_weights_xx", wr_weights_list)

    def test_wr_xx(self, weight):
        """
        Write XX in OPC-UA order, read back as untransposed BF RAM data, transpose here, check.
        """
        self.wr_xx(weight)
        rd_data_arr = self.rd_ram()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            #               (n_nodes, n_beamsets, N_POL_BF, A_PN, N_POL, S_SUB_BF)
            xx = rd_data_arr[pi, :, 0, :, 0, :]

            if np.all(xx == weight):
                print("XX:", "PN", pn, "WR PASSED")
            else:
                print("XX:", "PN", pn, "WR FAILED")

    def test_rd_xx(self, weight):
        """
        Read XX in OPC-UA order, check.
        """
        # Read weights
        rd_weights = self.client.read("bf_weights_xx")
        rd_weights_arr = np.array(rd_weights).reshape(self.XX_SHAPE)

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract XX weights from all weights
            #                             [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            rd_weights_xx = rd_weights_arr[pi, :, :, :, :, :]

            # Compare written and read weights
            if np.all(rd_weights_xx == weight):
                print("XX:", "PN", pn, "RD PASSED")
            else:
                print("XX:", "PN", pn, "RD FAILED")

    ###########################################################################
    # XY
    ###########################################################################
    def wr_xy(self, weight):
        """
        Write XY weights only
        """
        wr_weights_list = self.n_nodes * self.N_W_XY * [weight]
        self.client.write("bf_weights_xy", wr_weights_list)

    def test_wr_xy(self, weight):
        """
        Write XY in OPC-UA order, read back as untransposed BF RAM data, transpose here, check.
        """
        self.wr_xy(weight)
        rd_data_arr = self.rd_ram()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            #               (n_nodes, n_beamsets, N_POL_BF, A_PN, N_POL, S_SUB_BF)
            xy = rd_data_arr[pi, :, 0, :, 1, :]

            if np.all(xy == weight):
                print("XY:", "PN", pn, "WR PASSED")
            else:
                print("XY:", "PN", pn, "WR FAILED")

    def test_rd_xy(self, weight):
        """
        Read XY in OPC-UA order, check.
        """
        # Read weights
        rd_weights = self.client.read("bf_weights_xy")
        rd_weights_arr = np.array(rd_weights).reshape(self.XY_SHAPE)

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract XY weights from all weights
            #                             [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            rd_weights_xy = rd_weights_arr[pi, :, :, :, :, :]

            # Compare written and read weights
            if np.all(rd_weights_xy == weight):
                print("XY:", "PN", pn, "RD PASSED")
            else:
                print("XY:", "PN", pn, "RD FAILED")

    ###########################################################################
    # YX
    ###########################################################################
    def wr_yx(self, weight):
        """
        Write YX weights only
        """
        wr_weights_list = self.n_nodes * self.N_W_YX * [weight]
        self.client.write("bf_weights_yx", wr_weights_list)

    def test_wr_yx(self, weight):
        """
        Write YX in OPC-UA order, read back as untransposed BF RAM data, transpose here, check.
        """
        self.wr_yx(weight)
        rd_data_arr = self.rd_ram()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            #               (n_nodes, n_beamsets, N_POL_BF, A_PN, N_POL, S_SUB_BF)
            yx = rd_data_arr[pi, :, 1, :, 0, :]

            if np.all(yx == weight):
                print("YX:", "PN", pn, "WR PASSED")
            else:
                print("YX:", "PN", pn, "WR FAILED")

    def test_rd_yx(self, weight):
        """
        Read YX in OPC-UA order, check.
        """
        # Read weights
        rd_weights = self.client.read("bf_weights_yx")
        rd_weights_arr = np.array(rd_weights).reshape(self.YX_SHAPE)

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract XY weights from all weights
            #                             [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            rd_weights_yx = rd_weights_arr[pi, :, :, :, :, :]

            # Compare written and read weights
            if np.all(rd_weights_yx == weight):
                print("YX:", "PN", pn, "RD PASSED")
            else:
                print("YX:", "PN", pn, "RD FAILED")

    ###########################################################################
    # YY
    ###########################################################################
    def wr_yy(self, weight):
        """
        Write YY weights only
        """
        wr_weights_list = self.n_nodes * self.N_W_YY * [weight]
        self.client.write("bf_weights_yy", wr_weights_list)

    def test_wr_yy(self, weight):
        """
        Write YY in OPC-UA order, read back as untransposed BF RAM data, transpose here, check.
        """
        self.wr_yy(weight)
        rd_data_arr = self.rd_ram()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            #               (n_nodes, n_beamsets, N_POL_BF, A_PN, N_POL, S_SUB_BF)
            yy = rd_data_arr[pi, :, 1, :, 1, :]

            if np.all(yy == weight):
                print("YY:", "PN", pn, "WR PASSED")
            else:
                print("YY:", "PN", pn, "WR FAILED")

    def test_rd_yy(self, weight):
        """
        Read YY in OPC-UA order, check.
        """
        # Read weights
        rd_weights = self.client.read("bf_weights_yy")
        rd_weights_arr = np.array(rd_weights).reshape(self.YY_SHAPE)

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract YY weights from all weights
            #                             [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            rd_weights_yy = rd_weights_arr[pi, :, :, :, :, :]

            # Compare written and read weights
            if np.all(rd_weights_yy == weight):
                print("YY:", "PN", pn, "RD PASSED")
            else:
                print("YY:", "PN", pn, "RD FAILED")

    ###########################################################################
    # PP
    ###########################################################################
    def wr_pp(self, weight):
        """
        Write PP (XX and YY same weights)
        """
        wr_weights_list = self.n_nodes * self.N_W_PP * [weight]
        self.client.write("bf_weights_pp", wr_weights_list)

    def test_wr_pp(self, weight):
        """
        Write PP in OPC-UA order, read back as XX and YY weights, check if same.
        """
        # Write weights using FPGA_bf_weights_pp_RW
        self.wr_pp(weight)

        # Read weights using FPGA_bf_weights_xx_R and FPGA_bf_weights_yy_R
        rd_weights = self.client.read("bf_weights_xx")
        rd_weights_xx_arr = np.array(rd_weights).reshape(self.XX_SHAPE)
        rd_weights = self.client.read("bf_weights_yy")
        rd_weights_yy_arr = np.array(rd_weights).reshape(self.YY_SHAPE)

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract XX, YY weights from all weights
            #                             [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            rd_weights_xx = rd_weights_xx_arr[pi, :, :, :, :, :]
            rd_weights_yy = rd_weights_yy_arr[pi, :, :, :, :, :]

            # Compare written and read xx and yy weights
            if np.all(rd_weights_xx == weight) and np.all(rd_weights_yy == weight):
                print("PP:", "PN", pn, "RD PASSED")
            else:
                print("PP:", "PN", pn, "RD FAILED")

    def test_rd_pp(self, weight):
        """
        Write XX only and Read PP in OPC-UA order, check.
        """
        # Write XX weights using FPGA_bf_weights_xx_RW
        self.wr_xx(weight)

        # write wrong weight in YY using FPGA_bf_weights_yy_RW
        yy_weight = weight * 2
        self.wr_yy(yy_weight)

        # Read weights using FPGA_bf_weights_pp_RW
        rd_weights = self.client.read("bf_weights_pp")
        rd_weights_arr = np.array(rd_weights).reshape(self.PP_SHAPE)

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract XX, YY weights from all weights
            #                             [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            rd_weights_pp = rd_weights_arr[pi, :, :, :, :, :]

            # Compare written xx and read pp weights
            if np.all(rd_weights_pp == weight):
                print("PP:", "PN", pn, "RD PASSED")
            else:
                print("PP:", "PN", pn, "RD FAILED")

    ###########################################################################
    # XX,XY,YX,YY
    ###########################################################################
    def rd_xx_xy_yx_yy(self):
        rd_data = self.client.read("bf_weights_xx_xy_yx_yy")
        weights = np.array(rd_data).reshape(self.XX_XY_YX_YY_SHAPE)
        return weights

    def wr_xx_xy_yx_yy(self, weight_xx, weight_xy, weight_yx, weight_yy):
        """
        Write the entire weights array with recognizable weights.
        """
        # Create weights array
        wr_weights = np.zeros(self.XX_XY_YX_YY_SHAPE, dtype=int)

        # Fill weights array [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
        wr_weights[:, 0, :, 0, :, :] = weight_xx
        wr_weights[:, 0, :, 1, :, :] = weight_xy
        wr_weights[:, 1, :, 0, :, :] = weight_yx
        wr_weights[:, 1, :, 1, :, :] = weight_yy

        # Flatten & write weights
        self.client.write("bf_weights_xx_xy_yx_yy", wr_weights.flatten())

    def test_rd_xx_xy_yx_yy(self, weight_xx, weight_xy, weight_yx, weight_yy):
        """
        Read back all (XX_XY_YX_YY) weights, check
        """
        # Read back all weights
        rd_weights = self.rd_xx_xy_yx_yy()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract XX,XY,YX,YY weights from all weights
            #              [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            xx = rd_weights[pi, 0, :, 0, :, :]
            xy = rd_weights[pi, 0, :, 1, :, :]
            yx = rd_weights[pi, 1, :, 0, :, :]
            yy = rd_weights[pi, 1, :, 1, :, :]

            # Compare written and read weights
            if np.all(xx == weight_xx):
                print("XX_XY_YX_YY/XX:", "PN", pn, "PASSED")
            else:
                print("XX_XY_YX_YY/XX:", "PN", pn, "FAILED")
            if np.all(xy == weight_xy):
                print("XX_XY_YX_YY/XY:", "PN", pn, "PASSED")
            else:
                print("XX_XY_YX_YY/XY:", "PN", pn, "FAILED")
            if np.all(yx == weight_yx):
                print("XX_XY_YX_YY/YX:", "PN", pn, "PASSED")
            else:
                print("XX_XY_YX_YY/YX:", "PN", pn, "FAILED")
            if np.all(yy == weight_yy):
                print("XX_XY_YX_YY/YY:", "PN", pn, "PASSED")
            else:
                print("XX_XY_YX_YY/YY:", "PN", pn, "FAILED")

    ###########################################################################
    # XX,YY
    ###########################################################################
    def wr_xx_yy(self, weight_xx, weight_yy):
        """
        Write XX_YY weight
        """
        # Create weights array
        wr_weights = np.zeros(self.XX_YY_SHAPE, dtype=int)

        # Fill weights array [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
        wr_weights[:, :, :, 0, :, :] = weight_xx
        wr_weights[:, :, :, 1, :, :] = weight_yy

        # Flatten & write weights
        self.client.write("bf_weights_xx_yy", wr_weights.flatten())

    def test_wr_xx_yy(self, weight_xx, weight_yy):
        """
        Write XX,YY in OPC-UA order, read back as untransposed BF RAM data, transpose here, check.
        """
        self.wr_xx_yy(weight_xx, weight_yy)
        rd_data_arr = self.rd_ram()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            #               (n_nodes, n_beamsets, N_POL_BF, A_PN, N_POL, S_SUB_BF)
            xx = rd_data_arr[pi, :, 0, :, 0, :]
            yy = rd_data_arr[pi, :, 1, :, 1, :]

            if np.all(xx == weight_xx):
                print("XX_YY/XX:", "PN", pn, "WR PASSED")
            else:
                print("XX_YY/XX:", "PN", pn, "WR FAILED")
            if np.all(yy == weight_yy):
                print("XX_YY/YY:", "PN", pn, "WR PASSED")
            else:
                print("XX_YY/YY:", "PN", pn, "WR FAILED")

    def test_rd_xx_yy(self, weight_xx, weight_yy):
        """
        Read back all (XX_XY_YX_YY) weights, check if XX, YY weights match
        """
        # Read back all weights
        rd_weights = self.rd_xx_xy_yx_yy()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract XX and YY weights from all weights
            #              [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            xx = rd_weights[pi, 0, :, 0, :, :]
            yy = rd_weights[pi, 1, :, 1, :, :]

            # Compare written and read weights
            if np.all(xx == weight_xx):
                print("XX_YY/XX:", "PN", pn, "RD PASSED")
            else:
                print("XX_YY/XX:", "PN", pn, "RD FAILED")
            if np.all(yy == weight_yy):
                print("XX_YY/YY:", "PN", pn, "RD PASSED")
            else:
                print("XX_YY/YY:", "PN", pn, "RD FAILED")

    ###########################################################################
    # XY,YX
    ###########################################################################
    def wr_xy_yx(self, weight_xy, weight_yx):
        """
        Write XY_YX weights
        """
        # Create weights array
        wr_weights = np.zeros(self.XY_YX_SHAPE, dtype=int)

        # Fill weights array [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
        wr_weights[:, :, :, 0, :, :] = weight_xy
        wr_weights[:, :, :, 1, :, :] = weight_yx

        # Flatten & write weights
        self.client.write("bf_weights_xy_yx", wr_weights.flatten())

    def test_wr_xy_yx(self, weight_xy, weight_yx):
        """
        Write XY,YX in OPC-UA order, read back as untransposed BF RAM data, transpose here, check.
        """
        self.wr_xy_yx(weight_xy, weight_yx)
        rd_data_arr = self.rd_ram()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            #               (n_nodes, n_beamsets, N_POL_BF, A_PN, N_POL, S_SUB_BF)
            xy = rd_data_arr[pi, :, 0, :, 1, :]
            yx = rd_data_arr[pi, :, 1, :, 0, :]

            if np.all(xy == weight_xy):
                print("XY_YX/XY:", "PN", pn, "WR PASSED")
            else:
                print("XY_YX/XY:", "PN", pn, "WR FAILED")
            if np.all(yx == weight_yx):
                print("XY_YX/YX:", "PN", pn, "WR PASSED")
            else:
                print("XY_YX/YX:", "PN", pn, "WR FAILED")

    def test_rd_xy_yx(self, weight_xy, weight_yx):
        """
        Read back all (XX_XY_YX_YY) weights, check if XY, YX weights match
        """
        # Read back all weights
        rd_weights = self.rd_xx_xy_yx_yy()

        # Check result of all targeted PN
        for pn in self.nodes:
            pi = pn % N_PN
            # Extract XX and YY weights from all weights
            #              [n_nodes, N_POL_BF, A_PN, N_POL, n_beamsets, S_SUB_BF]
            xy = rd_weights[pi, 0, :, 1, :, :]
            yx = rd_weights[pi, 1, :, 0, :, :]

            # Compare written and read weights
            if np.all(xy == weight_xy):
                print("XY_YX/XY:", "PN", pn, "RD PASSED")
            else:
                print("XY_YX/XY:", "PN", pn, "RD FAILED")
            if np.all(yx == weight_yx):
                print("XY_YX/YX:", "PN", pn, "RD PASSED")
            else:
                print("XY_YX/YX:", "PN", pn, "RD FAILED")

    def test(self):
        """
        Perform write & readback tests for all BF weights CP.
        """
        # Write, read back raw RAM, reorder, verify
        self.test_wr_xx(1111)
        self.test_wr_xy(2222)
        self.test_wr_yx(3333)
        self.test_wr_yy(4444)

        # Read RAM in correct order, verify
        self.test_rd_xx(1111)
        self.test_rd_xy(2222)
        self.test_rd_yx(3333)
        self.test_rd_yy(4444)

        self.test_wr_pp(1414)
        self.test_rd_pp(4141)

        # Write all weights
        self.wr_xx_xy_yx_yy(111, 222, 333, 444)
        self.test_rd_xx_xy_yx_yy(111, 222, 333, 444)

        # Write, read back raw RAM, reorder, verify
        self.test_wr_xx_yy(5555, 6666)
        self.test_rd_xx_yy(5555, 6666)
        self.test_wr_xy_yx(7777, 8888)
        self.test_rd_xy_yx(7777, 8888)


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask=%s", str(client.get_mask()))  # read back nodes set.

    bf_weights = BFWeights(client)

    try:
        bf_weights.set_nodes(node_list)

        if args.test:
            bf_weights.test()
    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument(
        "--host", dest="host", type=str, default="dop369", help="host to connect to"
    )
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument(
        "--test",
        action="store_true",
        help="Test the BF weights RAM by writing and reading back weights.",
    )
    #

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: %s", str(args))

    sys.exit(main())
