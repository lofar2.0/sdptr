#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . readout running sdp_firmware and boot sdp_firmware (facory or user image)
# . also remote flash of sdp_firmware.
# Description:
#   run ./sdp_sdp_firmware.py -h for help
#
# flash USER image, read back and verify. --image [FACT | USER]  --file is full path to rbf file.
#   ./sdp_sdp_firmware.py --host 10.99.0.253 --port 4840 --write --read --verify --image USER --file [image].rbf
# use --reboot and --image to load the image
# ##########################################################################


import sys

sys.path.insert(0, "..")
import os
import logging
import argparse

import math
import struct
import numpy as np

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class SDPfirmware:
    def __init__(self, client):
        self.client = client

        self.logger = logging.getLogger("SDPfirmware")
        self.bank_types = {"fact": 0, "user": 1}
        self.n_nodes = self.client.n_nodes  # get number of nodes from server
        self.active_nodes = self.client.node_list
        self.bank = None
        self.bank_nr = 0
        self.word_size_bytes = 4
        self.fact_sector_start = 0
        self.fact_sector_end = 159
        self.user_sector_start = 160
        self.user_sector_end = 319
        self.pages_per_sector = 1024
        self.page_size_words = 64
        self.page_size_bytes = self.page_size_words * 4
        self.sector_size_words = self.pages_per_sector * self.page_size_words
        self.sector_size_bytes = self.pages_per_sector * self.page_size_bytes
        self.n_pages_per_rw = 256
        self.expected_pps_cnt = 200 * 10**6
        self.rbf_data = None
        self.rbf_size_bytes = 0
        self.rbf_n_pages = 0
        self.rbf_n_sectors = 0

    def add_rbf_file(self, filename):
        try:
            self.logger.debug(f"opening file {filename}")
            rbf = open(filename, "rb").read()
        except BaseException:
            self.logger.error(f"exception while opening file {filename}")
            return
        # need to be word aligned
        n_pad_bytes = (
            (self.word_size_bytes - (len(rbf) % self.word_size_bytes))
            if (len(rbf) % self.word_size_bytes) > 0
            else 0
        )
        if n_pad_bytes > 0:
            rbf += n_pad_bytes * b"\0"

        self.rbf_data = rbf
        self.rbf_size_bytes = len(rbf)
        self.rbf_n_pages = int(math.ceil(self.rbf_size_bytes / self.page_size_bytes))
        self.rbf_n_sectors = int(math.ceil(self.rbf_n_pages / self.pages_per_sector))
        self.logger.info(f"add rbf data from {filename}")
        self.logger.info(f"- rbf size     : {self.rbf_size_bytes} bytes")
        self.logger.info(f"- rbf n_pages  : {self.rbf_n_pages} pages")
        self.logger.info(f"- rbf n_sectors: {self.rbf_n_sectors} sectors")

    def set_img(self, img):
        self.bank = img.lower()
        self.bank_nr = self.bank_types[self.bank]

    def is_bank_set(self):
        if self.bank is None:
            self.logger.error('Image not set, use "set_img(bank)"" first')
            return False
        return True

    def active(self):
        loaded = ["fact" if i == 0 else "user" for i in self.client.read("boot_image")]
        self.logger.info(f"active images={loaded}")
        return loaded

    def reboot(self):
        if self.is_bank_set():
            self.client.write("boot_image", ([self.bank_nr] * self.n_nodes))
            return True
        return False

    def flash_protect(self, protect):
        """
        protect: 0=unprotect, 1=protect
        """
        self.logger.debug(f"flash protect = {protect}")
        return self.client.write("flash_protect", ([protect] * self.n_nodes))

    def flash_erase(self):
        """
        img: image to erase, fact or user
        """
        if self.is_bank_set():
            result = True
            self.logger.info(f"flash erase {self.bank.upper()} image")

            if self.bank == "fact":
                n_sectors = self.fact_sector_end - self.fact_sector_start + 1
                sector = self.fact_sector_start
            else:
                n_sectors = self.user_sector_end - self.user_sector_start + 1
                sector = self.user_sector_start

            # if self.rbf_data:
            #     n_sectors = min(n_sectors, (self.rbf_n_sectors + 1))

            self.logger.info(f"flash erase {n_sectors} sectors")

            max_retries = 3
            sectors_done = 0
            while sectors_done < n_sectors:
                n_retries = 0
                while True:
                    result = self.client.write("flash_erase", ([sector] * self.n_nodes))
                    if result is True:
                        sectors_done += 1
                        sector += 1
                        break
                    n_retries += 1
                    if n_retries == max_retries:
                        print(f" fail erase sector {sector} after {n_retries} retries.")
                        return False
                proc_done = (sectors_done / n_sectors) * 100
                if proc_done % 10.0 < 100 / n_sectors:
                    self.logger.info(f"flash_erase {int(proc_done)}% done")
            return result
        return False

    # def reverse_byte(self, byte):
    #     """
    #     Fast way to reverse a byte on 64-bit platforms.
    #     """
    #     return (byte * 0x0202020202 & 0x010884422010) % 1023

    # def reverse_word(self, word):
    #     """
    #     Fast way to reverse a word on 64-bit platforms.
    #     """
    #     B0 = word & 0xFF
    #     B1 = (word & 0xFF00) >> 8
    #     B2 = (word & 0xFF0000) >> 16
    #     B3 = (word & 0xFF000000) >> 24
    #     reversed_word = (
    #         (self.reverse_byte(B0) << 24)
    #         | (self.reverse_byte(B1) << 16)
    #         | (self.reverse_byte(B2) << 8)
    #         | (self.reverse_byte(B3))
    #     )
    #     return reversed_word

    def reverse_bits(self, word):
        binstr = bin(word)[2:]
        zeros = 32 - len(binstr)
        bin_word = '0b' + '0' * zeros + binstr
        return int(bin_word[:1:-1], 2)

    def flash_read(self):
        """
        img: fact or user
        filename: rbf image file to write back read image from flash
        """
        bytes_per_read = self.n_pages_per_rw * self.page_size_bytes

        if self.is_bank_set():
            self.logger.info(f"flash read {self.bank.upper()} image")

            if self.bank == "fact":
                n_sectors = self.fact_sector_end - self.fact_sector_start + 1
                addr = self.fact_sector_start * self.sector_size_bytes
            else:
                n_sectors = self.user_sector_end - self.user_sector_start + 1
                addr = self.user_sector_start * self.sector_size_bytes

            # if self.rbf_data:
            #     n_sectors = min(n_sectors, (self.rbf_n_sectors + 1))

            n_pages = n_sectors * self.pages_per_sector
            if self.rbf_data:
                n_pages = min(n_pages, self.rbf_n_pages)

            self.logger.info(f"flash read {n_pages} pages")
            n_reads = int(math.ceil(n_pages / self.n_pages_per_rw))

            files = {}
            for node_nr in self.active_nodes:
                name = f"read_{self.bank}_{node_nr}.rbf"
                fullname = os.path.join(os.getcwd(), name)
                files[node_nr] = open(fullname, "wb")

            self.logger.debug(f"flash read addr 0x{addr:08x}")
            result = self.client.write("flash_addr", ([addr] * self.n_nodes))
            if result is False:
                self.logger.error(f"flash_read, setting addr {addr}")
                return False

            self.logger.info(f"number of flash_reads = {n_reads}")
            reads_done = 0
            while reads_done < n_reads:
                rev_data = self.client.read("flash_pages")
                data = [self.reverse_bits(rd) for rd in rev_data]

                fmt = "I" * len(data)
                data8 = struct.pack(fmt, *data)

                for nr, nn in enumerate(self.active_nodes):
                    s1 = nr * bytes_per_read
                    s2 = s1 + bytes_per_read
                    _data = data8[s1:s2]
                    files[nn].write(_data)

                reads_done += 1
                proc_done = (reads_done / n_reads) * 100
                if proc_done % 10.0 < 100 / n_reads:
                    self.logger.info(f"flash_read {int(proc_done)}% done")

            for node_nr, fd in files.items():
                self.logger.debug(f"close rbf file for node {node_nr}")
                fd.close()

    def flash_write(self):
        """
        img: fact or user
        filename: rbf image file with image to write to flash
        """
        if self.is_bank_set():
            self.logger.info(f"flash write {self.bank.upper()} image")

            rbf_size_word = self.rbf_size_bytes // self.word_size_bytes

            rbf_words = struct.unpack("I" * rbf_size_word, self.rbf_data)
            rbf_rev_words = [self.reverse_bits(i) for i in rbf_words]

            self.logger.debug(f"total {self.rbf_n_pages} pages to write")

            if self.bank == "fact":
                addr = self.fact_sector_start * self.sector_size_bytes
            else:
                addr = self.user_sector_start * self.sector_size_bytes

            page = 0
            result = self.client.write("flash_addr", ([addr] * self.n_nodes))
            if result is False:
                self.logger.error(f"flash_write, setting addr {addr}")
                return False

            n_writes = int(math.ceil(self.rbf_n_pages / self.n_pages_per_rw))
            self.logger.info(f"number of flash writes = {n_writes}")
            max_retries = 3
            writes_done = 0
            while writes_done < n_writes:
                start = page * self.page_size_words
                stop = (page + self.n_pages_per_rw) * self.page_size_words
                rbf_page_data = rbf_rev_words[start:stop]

                # len(rbf_page_data) < (64 * 256) = 16384
                if len(rbf_page_data) < (self.page_size_words * self.n_pages_per_rw):
                    rbf_page_data += [0] * (
                        (self.page_size_words * self.n_pages_per_rw) - len(rbf_page_data)
                    )

                n_retries = 0
                while True:
                    result = self.client.write("flash_pages", (rbf_page_data * self.n_nodes))
                    if result is True:
                        break
                    n_retries += 1
                    if n_retries == max_retries:
                        self.logger.error(f"flash_write, fail to write page {page} after {n_retries} retries.")
                        return False

                page += self.n_pages_per_rw
                writes_done += 1
                proc_done = (writes_done  / n_writes) * 100
                if proc_done % 10.0 < 100 / n_writes:
                    self.logger.info(f"flash_write {int(proc_done)}% done")
            return result
        return False

    def flash_verify(self, filename, nodelist):
        for node in nodelist:
            # check only first selected fpga
            read_file = f"read_{self.bank}_{node}.rbf"
            full_read_file = os.path.join(os.getcwd(), read_file)
            self.logger.info(f'compare image "{read_file}" with "{filename}"')
            same = True
            try:
                rbf_size_bytes = os.path.getsize(filename)
                self.logger.debug(f"rbf_size_bytes = {rbf_size_bytes}")
                fd1 = open(filename, "rb")
                fd2 = open(full_read_file, "rb")
                byte_cnt = 0
                while same:
                    if byte_cnt == rbf_size_bytes:
                        break
                    w1 = fd1.read(4)
                    w2 = fd2.read(4)
                    if w1 != w2:
                        self.logger.error(f"byte {byte_cnt} not same: w1={w1}, w2={w2}")
                        same = False
                    byte_cnt += 4

                self.logger.debug(f"checked {byte_cnt} bytes")
            except BaseException as err:
                handle_exception(err)

            fd1.close()
            fd2.close()

            if not same:
                self.logger.info("NOT SAME")
                return False
            self.logger.info("SAME")
            return True

    def read_fw_version(self, version_test=None):
        """
        Read firmware version.
        return list of requested nodes or True when version==version_test, else False.
        """
        version = np.array(self.client.read("firmware_version"))[node_list]

        if version_test is None:
            return version
        else:
            result_list = []

            # Issue: we have a mismatch between name formats:
            # . RBF name (version_test) : 'lofar2_unb2b_sdp_full-r9ff51058a'
            # . Version  (version)      : '2022-01-12T13.14.20_9ff51058a_lofar2_unb2b_sdp_station_full'
            # So we're checking whether both the name and the hash from version_test are in the version
            # - in that case the test passes.
            name, githash = version_test.split("-r")
            for n in version:
                result = True
                if name not in n or githash not in n:
                    result = False
                result_list.append(result)
            return result

    def pps_test(self):
        """
        Read pps_present and pps_capture_cnt, return True when pps_present is true
        and pps_capture_cnt = 200M for all requested nodes, else False.
        """
        pps_present = np.array(self.client.read("pps_present"))[node_list]
        pps_capture_cnt = np.array(self.client.read("pps_capture_cnt"))[node_list]
        return all(pps_present) and all(x == self.expected_pps_cnt for x in pps_capture_cnt)


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()
    if not client.connected:
        logger.error("stopping, client not connected.")
        return 1

    client.set_mask(node_list)
    logger.debug(f"fpga mask={client.get_mask()}")  # read back nodes set.

    sdp_firmware = SDPfirmware(client)
    nodelist = sdp_firmware.active_nodes if node_list is None else node_list
    try:
        if args.file:
            sdp_firmware.add_rbf_file(args.file)

        if args.write:
            sdp_firmware.set_img(args.image)
            if sdp_firmware.flash_protect(0):
                if sdp_firmware.flash_erase():
                    exit_state = int(not sdp_firmware.flash_write())
            sdp_firmware.flash_protect(1)

        if args.read:
            sdp_firmware.set_img(args.image)
            sdp_firmware.flash_read()

        if args.verify:
            sdp_firmware.set_img(args.image)
            sdp_firmware.flash_verify(args.file, nodelist)

        if args.active:
            logger.info(sdp_firmware.active())

        if args.reboot:
            sdp_firmware.set_img(args.image)
            logger.info(sdp_firmware.reboot())
            if args.image == "USER":
                logger.info("It takes 8 seconds before user image is active")

        if args.version:
            print(sdp_firmware.read_fw_version())

        if args.version_test:
            result = sdp_firmware.read_fw_version(args.version_test)
            client.disconnect()
            # Convert False->1 and True->0
            exit_state = int(not result)

        if args.pps_test:
            result = sdp_firmware.pps_test()
            client.disconnect()
            # Convert False->1 and True->0
            exit_state = int(not result)
    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG, default=WARNING")

    parser.add_argument("--active", action="store_true", help="show loaded bank, fact or user")
    parser.add_argument(
        "--image",
        type=str,
        choices=["FACT", "USER"],
        help="image to read/write/verify FACT or USER",
    )
    parser.add_argument("--file", type=str, help="image to flash, full path to rbf file")
    parser.add_argument("--reboot", action="store_true", help="reboot, also use --image")
    parser.add_argument(
        "--write", action="store_true", help="write image to flash, also use --image and --file"
    )
    parser.add_argument(
        "--read",
        action="store_true",
        help="read image from flash and save it to a file, also use --image and --file",
    )
    parser.add_argument(
        "--verify", action="store_true", help="verify read image, also use --image and --file"
    )

    parser.add_argument("--version", action="store_true", help="Read firmware version")
    parser.add_argument(
        "--version_test", type=str, help="Read firmware version and compare to VERSION_TEST"
    )

    parser.add_argument(
        "--pps-test", action="store_true", help="Test if PPS is present and has 200M PPS count"
    )

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 2 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval(f"logging.{LOGLEVEL[verbosity_nr]}")

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.debug(f"parsed arguments: {args}")

    if (args.write or args.read or args.verify or args.reboot) and not args.image:
        logger.error("image not given, use --image [FACT or USER]")
        sys.exit(1)

    if (args.write or args.verify) and not args.file:
        logger.error("rbf file not given, use --file")
        sys.exit(1)

    sys.exit(main())
