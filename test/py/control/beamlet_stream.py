#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker, Reinier vd Walle
# Purpose:
# . Control and setup the beamlet stream.
# Description:
# . run ./beamlet_stream.py -h for help
#
# Remark:
# . Use c_beamlet_scale = 2**10, for full scale WG and N_ant = 1, see [2].
#
# References:
# . [1] ICD SC-SDP
#   https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface
# . [2] L4 SDPFW Decision: LOFAR2.0 SDP Firmware Quantization Model,
#   https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L4+SDPFW+Decision%3A+LOFAR2.0+SDP+Firmware+Quantization+Model
#
# ##########################################################################

import sys
import logging
import time
import collections
import numpy as np

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list
from base.constants import C_SDP
from base.sdp_functions import parse_stream_destination, start_processing, stop_all_streams
from base.stream_reader import StreamReader, print_header
from base.stream_control import StreamControl


class BeamletStream(object):
    """
    BeamletStream class
    """
    def __init__(self, client, dest_mac, dest_ip):
        self.logger = logging.getLogger("BeamletStream")
        self.client = client
        self.n_nodes = self.client.n_nodes  # get number of nodes from server
        self.n_rn = self.client.n_nodes  # all nodes used in ring
        self.o_rn = self.client.gn_first_fpga
        self.last_rn = self.n_rn - 1  # last node in ring relative to o_rn
        self.n_beamsets = self.client.read("sdp_config_nof_beamsets")
        self.packet_type = "BEAMLET"
        self.dest_mac = dest_mac
        self.dest_ip = dest_ip
        self.dest_udp_port = 5000
        self.stream_reader = None
        self.stream_control = None
        self.next_packet_nr = 0
        self.stdout = self.logger.getEffectiveLevel() > logging.INFO
        stop_all_streams(self.client)

    def print_log(self, fstr):
        print(fstr) if self.stdout else self.logger.info(fstr)

    def default_source_addresses(self):
        """Determine source MAC, IP, UDP port off each GN."""
        sourceMACs = []
        sourceIPs = []
        sourceUDPs = []
        for gn in self.client.fpga_gn_list:
            xx = gn // C_SDP.fpgas_per_board
            yy = gn % C_SDP.fpgas_per_board
            zz = yy + 1
            sourceMACs.append(f"00:22:86:08:{xx:02x}:{yy:02x}")
            sourceIPs.append(f"192.168.{xx}.{zz}")
            sourceUDPs.append(0xD000 | (gn & 0xFF))
        return sourceMACs, sourceIPs, sourceUDPs

    def default_destination_addresses(self, port=None):
        """Use same destination address for all GN."""
        destMACs = [self.dest_mac] * self.n_nodes * self.n_beamsets
        destIPs = [self.dest_ip] * self.n_nodes * self.n_beamsets
        _port = self.dest_udp_port if port is None else int(port)
        destUDPs = [_port] * self.n_nodes * self.n_beamsets
        return destMACs, destIPs, destUDPs

    def setup_stream_network(self, port=None):
        """
        Set up network addresses for beamlet stream.

        Consisting of MAC, IP, and UDP port for source and for destination.
        """
        # Source configuration
        sourceMACs, sourceIPs, sourceUDPs = self.default_source_addresses()
        self.client.write("beamlet_output_hdr_eth_source_mac", sourceMACs)
        self.client.write("beamlet_output_hdr_ip_source_address", sourceIPs)
        self.client.write("beamlet_output_hdr_udp_source_port", sourceUDPs)

        # Destination configuration
        destMACs, destIPs, destUDPs = self.default_destination_addresses(port)
        self.client.write("beamlet_output_hdr_eth_destination_mac", destMACs)
        self.client.write("beamlet_output_hdr_ip_destination_address", destIPs)
        self.client.write("beamlet_output_hdr_udp_destination_port", destUDPs)
        time.sleep(1.0)

    def setup_stream_loopback(self, rn_pairs):
        """
        Set up network addresses for beamlet stream loopback between FPGA nodes.

        To test packet traffic on the 10GbE interface.
        Set of MAC, IP, and UDP port for source and for destination,to be able to do loopback
        via a network switch.
        The loopback is between pairs of different GN, that send to eachother.
        The rn_pairs contains one or more pairs of RN indices, that select GN from
        self.client.fpga_gn_list that will connect to eachother. The RN indices are in
        range(self.n_rn). For any GN that are not used with loopback, the default destination
        MAC, IP and UDP port is set. For example:

        - rn_pairs = (0, 1), (10, 8)  # order in RN pair tuple is dont care
        - rn index:            0,  1,  2,  3,   4,  5,  6,  7,   8,  9, 10, 11,  12, 13, 14, 15
        - self.client.fpga_gn_list =
                              64, 65, 66, 67,  68, 69, 70, 71,  72, 73, 74, 75,  76, 77, 78, 79
        - destination GN:     65, 64,  -,  -,   -,  -,  -,  -,  73,  -, 72,  -,   -,  -,  -,  -

        Default send to next neighbour in ring, if rn_pairs == None.
        """
        # Source configuration
        sourceMACs, sourceIPs, sourceUDPs = self.default_source_addresses()
        self.client.write("beamlet_output_hdr_eth_source_mac", sourceMACs)
        self.client.write("beamlet_output_hdr_ip_source_address", sourceIPs)
        self.client.write("beamlet_output_hdr_udp_source_port", sourceUDPs)

        # Default destination configuration
        destMACs, destIPs, destUDPs = self.default_destination_addresses()

        # Overwrite destinations for loopback node pairs
        for rnPair in rn_pairs:
            gnPair = (self.client.fpga_gn_list[rnPair[0]], self.client.fpga_gn_list[rnPair[1]])
            # Send between the two GN in gnPair, so 0 --> 1 and 1 --> 0
            for txrx in [(0, 1), (1, 0)]:
                # Send from GN at index gnIndex to destination with global node number gnDest
                gnIndex = self.client.fpga_gn_list.index(gnPair[txrx[0]])
                gnDest = gnPair[txrx[1]]
                xx = gnDest // C_SDP.fpgas_per_board
                yy = gnDest % C_SDP.fpgas_per_board
                zz = yy + 1
                for bset in range(self.n_beamsets):
                    # Set MAC, IP of destination GN
                    destMACs[gnIndex * self.n_beamsets + bset] = f"00:22:86:08:{xx:02x}:{yy:02x}"
                    destIPs[gnIndex * self.n_beamsets + bset] = f"192.168.{xx}.{zz}"

        self.client.write("beamlet_output_hdr_eth_destination_mac", destMACs)
        self.client.write("beamlet_output_hdr_ip_destination_address", destIPs)
        self.client.write("beamlet_output_hdr_udp_destination_port", destUDPs)
        time.sleep(1.0)

    def bsn_monitor(self, mtime):
        """mtime: monitor runtime in seconds"""
        ntimes = 0
        start = time.time()
        while ntimes < mtime:
            while start > time.time():
                time.sleep(0.001)
            start += 1.0
            ntimes += 1
            nof_packets = self.client.read("beamlet_output_nof_packets")
            nof_valid = self.client.read("beamlet_output_nof_valid")
            self.print_log(f"BEAMLET bsn nof_packets: {nof_packets}")
            self.print_log(f"        bsn nof_valid  : {nof_valid}")

    def start_stream_reader(self):
        """ Start stream reader """
        self.stream_reader = StreamReader(self.dest_udp_port, self.packet_type, n_workers=4)
        self.stream_control = StreamControl(self.client, self.stream_reader)
        self.stream_reader.start()

    def stop_stream_reader(self, timeout=60):
        """ Stop stream reader """
        if self.stream_reader:
            # print_result("stop_stream_reader", True)
            # Wait until SDPFW has stopped stream output
            timeout_time = time.time() + timeout
            while time.time() < timeout_time and not self.stream_reader.done():
                time.sleep(0.0001)
            self.stream_reader.stop()

    def print_header(self):
        """ Print header """
        bsn, index = self.stream_reader.data.get_packet_bsn_index(self.next_packet_nr)
        if bsn is not None and index is not None:
            hdr = self.stream_reader.data.get_header(bsn, index)
            recv_time = self.stream_reader.data.get_receive_time(bsn, index)

            print_header(hdr, stdout=self.stdout, recv_time=recv_time)
            self.next_packet_nr += 1
            return True
        return False

    def test_header(self, nof_packets):
        """ Test header """
        # Read beamlet_output_scale to determine exp_beamlet_scale in beamlet output header
        # . assume all PN use same, so use index [0] for all PN
        # . station_id and antenna_field_index are part of station_info
        rd_beamlet_scale = self.client.read("beamlet_output_scale")

        # Read SDP info to determine expected fields in beamlet output header,
        # . assume all PN use same, so use index [0] for all PN
        rd_observation_id = self.client.read("sdp_info_observation_id")
        rd_station_id = self.client.read("sdp_info_station_id")
        rd_antenna_field_index = self.client.read("sdp_info_antenna_field_index")
        exp_observation_id = rd_observation_id[0]
        exp_station_id = rd_station_id[0]
        exp_antenna_field_index = rd_antenna_field_index[0]

        # BSN is not checked as we can lose packets and the packets are read using threads which causes re-ordering.
        exp_marker = 98
        exp_version_id = 5
        exp_gn_index = list(range(self.o_rn, self.o_rn + self.n_nodes))
        exp_beamlet_width = 8
        exp_payload_error = 0
        exp_beamlet_scale = int(rd_beamlet_scale[0] * 2**C_SDP.W_beamlet_scale_fraction)
        exp_beamlet_index = [0, C_SDP.S_sub_bf]
        exp_nof_blocks_per_packet = 4
        exp_nof_beamlets_per_block = C_SDP.S_sub_bf
        exp_block_period = 5120  # [ns]
        correct_headers = 0
        wrong_headers = 0
        hdr = None
        # first read in the data.
        if self.stream_control.record_n_packets(nof_packets):
            # now analyse the data
            self.logger.info("Received %d BSN's", self.stream_reader.data.nof_received_bsn())
            try:
                for bsn in self.stream_reader.data.get_all_bsn()[:-1]:  # do not test last bsn
                    for index in self.stream_reader.data.get_all_index(bsn=bsn):
                        hdr = self.stream_reader.data.get_header(bsn, index)
                        correct = [hdr.marker == exp_marker,
                                   hdr.version_id == exp_version_id,
                                   hdr.observation_id == exp_observation_id,
                                   hdr.antenna_field_index == exp_antenna_field_index,
                                   hdr.station_id == exp_station_id,
                                   hdr.gn_index in exp_gn_index,
                                   hdr.beamlet_width == exp_beamlet_width,
                                   hdr.payload_error == exp_payload_error,
                                   hdr.beamlet_scale == exp_beamlet_scale,
                                   hdr.beamlet_index in exp_beamlet_index,
                                   hdr.nof_blocks_per_packet == exp_nof_blocks_per_packet,
                                   hdr.nof_beamlets_per_block == exp_nof_beamlets_per_block,
                                   hdr.block_period == exp_block_period]

                        if all(correct):
                            correct_headers = correct_headers + 1
                        else:
                            wrong_headers = wrong_headers + 1
                            self.logger.error("Encountered wrong beamlet output header (%s):", correct)
                            print_header(hdr, stdout=self.stdout)
            except KeyboardInterrupt:
                self.print_log(" user hit ctrl-c")
                return

            self.logger.info("Done validating headers. Found %d correct headers and %d wrong headers.", correct_headers, wrong_headers)

            self.print_log("")
            self.print_log(f"self.n_nodes = {self.n_nodes}")
            self.print_log(f"self.n_rn = {self.n_rn}")
            self.print_log(f"self.o_rn = {self.o_rn}")
            self.print_log(f"self.last_rn = {self.last_rn}")
            self.print_log("")

        if (wrong_headers == 0 and correct_headers > 0):
            self.print_log("PASSED")
        else:
            self.print_log("FAILED")

        return (wrong_headers == 0 and correct_headers > 0)

    def _pack_complex(self, z):
        """ Pack complex value """
        return ((int(z.imag) & 0xffff) << 16) | (int(z.real) & 0xffff)  # im & re

    def _bf_calculate_expected_beamlet(self, sp_subband_ampl, sp_subband_phase, sp_bf_gain, sp_bf_phase,
                                       rem_subband_ampl, rem_subband_phase, rem_bf_gain, rem_bf_phase, nof_rem):
        sp_ampl   = sp_subband_ampl * sp_bf_gain
        sp_phase  = sp_subband_phase + sp_bf_phase
        sp_cplx   = sp_ampl * np.exp(1j * np.deg2rad(sp_phase))
        rem_ampl  = rem_subband_ampl * rem_bf_gain
        rem_phase = rem_subband_phase + rem_bf_phase
        rem_cplx  = rem_ampl * np.exp(1j * np.deg2rad(rem_phase))
        sum_cplx = sp_cplx + nof_rem * rem_cplx
        return sum_cplx

    def test_beamlets(self, node_list, nof_packets):
        sp_rn = node_list[0] - self.o_rn
        nof_rn = len(node_list)
        c_nof_rem = nof_rn * C_SDP.S_pn - 1
        c_unit_bf_weight = 2**C_SDP.W_bf_weight_fraction
        c_full_scale_ampl = 2**(C_SDP.W_adc - 1)
        c_subband = 102.0
        c_beamlet = 10
        c_use_transpose = True
        c_nof_blocks_per_packet = 4
        c_nof_values_per_block = C_SDP.N_pol_bf * C_SDP.S_sub_bf  # number of single pol beamlet values
        c_beamlet_x = C_SDP.N_pol_bf * c_beamlet
        c_beamlet_y = c_beamlet_x + 1  # beamlet values X and Y always come per pair in order X, Y
        c_beamlet_scale = 1.0 / (2.0**10)
        c_subband_phase_offset = -90.0  # WG with zero phase sinus yields subband with -90 degrees phase (negative Im, zero Re)
        c_beamlet_output_delta = 2  # +- delta margin

        c_sp = sp_rn * C_SDP.S_pn + 3
        c_sp_ampl = 0.5
        c_sp_phase = -110.0
        c_sp_remnant_ampl = 0.1
        c_sp_remnant_phase = 15.0
        c_bf_x_gain = 0.7
        c_bf_x_phase = 30.0
        c_bf_y_gain = 0.6
        c_bf_y_phase = 40.0
        c_bf_remnant_x_gain = 0.05
        c_bf_remnant_x_phase = 170.0
        c_bf_remnant_y_gain = 0.04
        c_bf_remnant_y_phase = -135.0

        c_wg_ampl_lsb         = c_sp_ampl         * c_full_scale_ampl  # Amplitude in number of lsb
        c_wg_remnant_ampl_lsb = c_sp_remnant_ampl * c_full_scale_ampl  # Amplitude in number of lsb
        c_exp_subband_ampl    = c_wg_ampl_lsb * C_SDP.G_subband_sine
        c_exp_subband_phase   = c_sp_phase + c_subband_phase_offset
        c_exp_remnant_subband_ampl = c_wg_remnant_ampl_lsb * C_SDP.G_subband_sine
        c_exp_remnant_subband_phase = c_sp_remnant_phase + c_subband_phase_offset

        Pol = collections.namedtuple('Pol', ['x', 'y'])
        c_bf_weight = Pol(c_unit_bf_weight * c_bf_x_gain * np.exp(1j * np.deg2rad(c_bf_x_phase)),
                          c_unit_bf_weight * c_bf_y_gain * np.exp(1j * np.deg2rad(c_bf_y_phase)))
        c_bf_remnant_weight = Pol(c_unit_bf_weight * c_bf_remnant_x_gain * np.exp(1j * np.deg2rad(c_bf_remnant_x_phase)),
                                  c_unit_bf_weight * c_bf_remnant_y_gain * np.exp(1j * np.deg2rad(c_bf_remnant_y_phase)))

        c_diag_wg_latency = 10.0
        c_sdp_shiftram_latency = 3.0
        c_wg_latency = c_diag_wg_latency + c_sdp_shiftram_latency
        c_wg_phase_offset = 360.0 * c_wg_latency * (c_subband / C_SDP.N_fft)
        c_wg_phase = c_sp_phase + c_wg_phase_offset
        c_wg_remnant_phase = c_sp_remnant_phase + c_wg_phase_offset

        c_exp_beamlet_x = self._bf_calculate_expected_beamlet(c_exp_subband_ampl, c_exp_subband_phase, c_bf_x_gain, c_bf_x_phase,
                                                              c_exp_remnant_subband_ampl, c_exp_remnant_subband_phase,
                                                              c_bf_remnant_x_gain, c_bf_remnant_x_phase, c_nof_rem)
        c_exp_beamlet_y = self._bf_calculate_expected_beamlet(c_exp_subband_ampl, c_exp_subband_phase, c_bf_y_gain, c_bf_y_phase,
                                                              c_exp_remnant_subband_ampl, c_exp_remnant_subband_phase,
                                                              c_bf_remnant_y_gain, c_bf_remnant_y_phase, c_nof_rem)
        c_exp_beamlet_x_out = c_beamlet_scale * c_exp_beamlet_x
        c_exp_beamlet_y_out = c_beamlet_scale * c_exp_beamlet_y

        # Arrays
        # Define subband weights
        unit_sub_gains = [2**C_SDP.W_sub_weight_fraction] * self.n_nodes * C_SDP.S_pn * C_SDP.N_sub
        unit_bf_weights = [2**C_SDP.W_bf_weight_fraction] * C_SDP.S_sub_bf
        # Set default bf weights for c_sp to unit.
        bf_sp_weights_x = unit_bf_weights[:]
        bf_sp_weights_y = unit_bf_weights[:]
        # Overwrite bf weights of c_sp at c_beamlet.
        bf_sp_weights_x[c_beamlet] = self._pack_complex(c_bf_weight.x)
        bf_sp_weights_y[c_beamlet] = self._pack_complex(c_bf_weight.y)

        # Set default bf weights for remnant signals to unit.
        bf_remnant_weights_x = unit_bf_weights[:]
        bf_remnant_weights_y = unit_bf_weights[:]
        # Overwrite bf weights of remnant signals at c_beamlet.
        bf_remnant_weights_x[c_beamlet] = self._pack_complex(c_bf_remnant_weight.x)
        bf_remnant_weights_y[c_beamlet] = self._pack_complex(c_bf_remnant_weight.y)

        # BF subband selection
        beamlet_selects_single = list(np.arange(0, C_SDP.S_sub_bf))
        beamlet_selects_single[c_beamlet] = int(c_subband)
        beamlet_selects = beamlet_selects_single * self.n_nodes * C_SDP.P_pfb * C_SDP.N_pol_bf

        # Use default RING configuration
        self.logger.info(self.client.read("ring_node_offset"))
        self.logger.info(self.client.read("ring_nof_nodes"))
        self.logger.info(self.client.read("ring_use_cable_to_next_rn"))
        self.logger.info(self.client.read("ring_use_cable_to_previous_rn"))
        self.logger.info(self.client.read("bf_ring_nof_transport_hops"))

        # Setup
        self.client.write("spectral_inversion", ([False] * self.n_nodes * C_SDP.S_pn))
        self.client.write("subband_weights", (unit_sub_gains))
        self.client.write("beamlet_output_scale", ([c_beamlet_scale] * self.n_nodes * self.n_beamsets))

        # Write bf unit weights
        bf_weights_xx = bf_remnant_weights_x * C_SDP.P_pfb * self.n_nodes
        bf_weights_xy = bf_remnant_weights_x * C_SDP.P_pfb * self.n_nodes
        bf_weights_yx = bf_remnant_weights_y * C_SDP.P_pfb * self.n_nodes
        bf_weights_yy = bf_remnant_weights_y * C_SDP.P_pfb * self.n_nodes

        weight_index = (c_sp // C_SDP.N_pol) * C_SDP.S_sub_bf
        if (c_sp % C_SDP.N_pol) == 0:  # X_antenna_pol
            bf_weights_xx[weight_index : weight_index + C_SDP.S_sub_bf] = bf_sp_weights_x
            bf_weights_yx[weight_index : weight_index + C_SDP.S_sub_bf] = bf_sp_weights_y
        else:  # Y_antenna_pol
            bf_weights_xy[weight_index : weight_index + C_SDP.S_sub_bf] = bf_sp_weights_x
            bf_weights_yy[weight_index : weight_index + C_SDP.S_sub_bf] = bf_sp_weights_y

        self.client.write("bf_weights_xx", bf_weights_xx)
        self.client.write("bf_weights_xy", bf_weights_xy)
        self.client.write("bf_weights_yx", bf_weights_yx)
        self.client.write("bf_weights_yy", bf_weights_yy)
        self.client.write("beamlet_subband_select", beamlet_selects)

        # WG configuration
        wg_ampl = [c_sp_remnant_ampl] * C_SDP.S_pn * self.n_nodes
        wg_freq = [C_SDP.f_adc_MHz * 10**6 * (c_subband / C_SDP.N_fft)] * C_SDP.S_pn * self.n_nodes
        wg_phase = [np.deg2rad(c_wg_remnant_phase)] * C_SDP.S_pn * self.n_nodes
        wg_ampl[c_sp] = c_sp_ampl
        wg_phase[c_sp] = np.deg2rad(c_wg_phase)

        self.client.write("wg_enable", [False] * self.n_nodes * C_SDP.S_pn)
        time.sleep(0.1)
        self.client.write("wg_amplitude", wg_ampl)
        self.client.write("wg_frequency", wg_freq)
        self.client.write("wg_phase", wg_phase)
        self.client.write("wg_enable", [True] * self.n_nodes * C_SDP.S_pn)

        self.logger.info('Waiting for a few sync periods...')
        time.sleep(6.0)

        # Validation
        correct_data = 0
        wrong_data = 0
        # first read in the data.
        if self.stream_control.record_n_packets(nof_packets):
            # now analyse the data
            all_bsn = self.stream_reader.data.get_all_bsn()[:-1]  # do not test last bsn
            self.logger.info("Received %d BSN's", len(all_bsn))

            for bsn_nr, bsn in enumerate(all_bsn):
                try:
                    beamlets = self.stream_reader.data.get_data(bsn, 0)
                    # For one arbitrary beamlet packet print_log its contents
                    if bsn_nr == 10:
                        self.print_log(f"c_subband = {c_subband}")
                        self.print_log(f"c_beamlet = {c_beamlet}")
                        self.print_log(f"Beamlet X (re) expected = {c_exp_beamlet_x_out.real}")
                        self.print_log(f"Beamlet X (im) expected = {c_exp_beamlet_x_out.imag}")
                        self.print_log(f"Beamlet Y (re) expected = {c_exp_beamlet_y_out.real}")
                        self.print_log(f"Beamlet Y (im) expected = {c_exp_beamlet_y_out.imag}")
                        self.print_log(f"beamlets packet length = {len(beamlets)}")
                        self.print_log(f"beamlets packet data = {beamlets}")  # uncomment to debug packet data order
                        for blk in range(c_nof_blocks_per_packet):
                            if c_use_transpose:
                                bx = (blk + c_beamlet * c_nof_blocks_per_packet) * C_SDP.N_pol_bf
                                by = bx + 1
                                self.print_log(f"Transposed beamlet value indices: x = {bx} y = {by}")
                            else:
                                bx = blk * c_nof_values_per_block + c_beamlet_x
                                by = bx + 1
                                self.print_log(f"Beamlet value indices: x = {bx} y = {by}")

                    data_is_correct = True
                    for blk in range(c_nof_blocks_per_packet):
                        if c_use_transpose:
                            bx = (blk + c_beamlet * c_nof_blocks_per_packet) * C_SDP.N_pol_bf
                        else:
                            bx = blk * c_nof_values_per_block + c_beamlet_x
                        by = bx + 1  # beamlet X and Y always come per pair in order X, Y

                        if not (beamlets[bx].real > (c_exp_beamlet_x_out.real - c_beamlet_output_delta) and
                                beamlets[bx].real < (c_exp_beamlet_x_out.real + c_beamlet_output_delta)):
                            self.logger.error("Wrong beamlet X[%d][re] output. actual = %d, expected = %d",
                                              bx, beamlets[bx].real, c_exp_beamlet_x_out.real)
                            data_is_correct = False

                        if not (beamlets[bx].imag > (c_exp_beamlet_x_out.imag - c_beamlet_output_delta) and
                                beamlets[bx].imag < (c_exp_beamlet_x_out.imag + c_beamlet_output_delta)):
                            self.logger.error("Wrong beamlet X[%d][im] output. actual = %d, expected = %d",
                                              bx, beamlets[bx].imag, c_exp_beamlet_x_out.imag)
                            data_is_correct = False

                        if not (beamlets[by].real > (c_exp_beamlet_y_out.real - c_beamlet_output_delta) and
                                beamlets[by].real < (c_exp_beamlet_y_out.real + c_beamlet_output_delta)):
                            self.logger.error("Wrong beamlet Y[%d][re] output. actual = %d, expected = %d",
                                              by, beamlets[by].real, c_exp_beamlet_y_out.real)
                            data_is_correct = False

                        if not (beamlets[by].imag > (c_exp_beamlet_y_out.imag - c_beamlet_output_delta) and
                                beamlets[by].imag < (c_exp_beamlet_y_out.imag + c_beamlet_output_delta)):
                            self.logger.error("Wrong beamlet Y[%d][im] output. actual = %d, expected = %d",
                                              by, beamlets[by].imag, c_exp_beamlet_y_out.imag)
                            data_is_correct = False

                    if data_is_correct:
                        self.logger.debug("Beamlet X (re) output. actual = %d, expected = %d",
                                          beamlets[c_beamlet_x].real, c_exp_beamlet_x_out.real)
                        self.logger.debug("Beamlet X (im) output. actual = %d, expected = %d",
                                          beamlets[c_beamlet_x].imag, c_exp_beamlet_x_out.imag)
                        self.logger.debug("Beamlet Y (re) output. actual = %d, expected = %d",
                                          beamlets[c_beamlet_y].real, c_exp_beamlet_y_out.real)
                        self.logger.debug("Beamlet Y (im) output. actual = %d, expected = %d",
                                          beamlets[c_beamlet_y].imag, c_exp_beamlet_y_out.imag)
                        correct_data += 1
                    else:
                        wrong_data += 1
                        self.logger.error("Wrong beamlet: n_bsn=%d, bsn_nr=%d, bsn=%d",
                                          self.stream_reader.data.nof_received_bsn(), bsn_nr, bsn)
                        if wrong_data == 200 and wrong_data == bsn_nr + 1:
                            self.logger.error("Tested 200 beamlets, all wrong, stopping now.")
                            break

                except KeyboardInterrupt:
                    self.print_log(" user hit ctrl-c")
                    break

        self.client.write("wg_enable", [False] * self.n_nodes * C_SDP.S_pn)
        self.logger.info("Done validating beamlets. Found %d correct beamlet packets and %d wrong beamlet packets.",
                         correct_data, wrong_data)

        if (wrong_data == 0 and correct_data > 0):
            self.print_log("PASSED")
        else:
            self.print_log("FAILED")

        return (wrong_data == 0 and correct_data > 0)

    def get_rn_pairs(self, rn_list):
        """Derive RN pairs from ring nodes list.

        The ring nodes list must contain at least one pair of unique RN.
        Example: rn_list = [2, 3, 8, 9] --> rn_pairs = [(2, 3), (8, 9)]
        """
        if len(rn_list) == 0:
            self.logger.error("Empty ring_nodes list")
            self.print_log("FAILED")
            return []
        if len(rn_list) % 2 == 1:
            self.logger.error("Odd number of RN in ring_nodes list")
            self.print_log("FAILED")
            return []
        if len(rn_list) != len(list(set(rn_list))):
            self.logger.error("Duplicate RN in ring_nodes list")
            self.print_log("FAILED")
            return []
        rn_even = rn_list[0::2]
        rn_odd = rn_list[1::2]
        rn_pairs = [(re, ro) for re, ro in zip(rn_even, rn_odd)]
        return rn_pairs

    def test_10gbe_output(self, rn_list, rn_pairs, mtime):
        """Test 10gbe output between rn_pairs of RN for mtime seconds.

        Input:
        . rn_list  : list of ring nodes that are used in this 10gbe output test
        . rn_pairs : use self.get_rn_pairs(rn_list) to derive pairs of RN from rn_list, that send to eachother
        . mtime    : test time in seconds
        """
        self.logger.info("rn_list = %s", str(rn_list))
        self.logger.info("rn_pairs = %s", str(rn_pairs))
        self.logger.info("mtime = %f s", mtime)

        # Prepare beamlet output disable for all RN
        disable = [False] * self.client.n_nodes * self.n_beamsets
        # Prepare beamlet output enable for RN in rn_list
        enable = [False] * self.client.n_nodes * self.n_beamsets
        for rn in rn_list:
            for bset in range(self.n_beamsets):
                enable[rn * self.n_beamsets + bset] = True

        # Start test
        rd_start_tx_nof_frames = self.client.read("beamlet_output_10gbe_tx_nof_frames")
        rd_start_rx_nof_frames = self.client.read("beamlet_output_10gbe_rx_nof_frames")
        rd_start_bsn = self.client.read("current_bsn")
        self.client.write("beamlet_output_enable", enable)
        rd_beamlet_output_enable = self.client.read("beamlet_output_enable")

        # Run test for mtime seconds
        if mtime < 10:
            time.sleep(mtime)
        else:
            # log progress
            N = 5
            for t in range(N):
                time.sleep(mtime / N)
                progress = (t + 1) * int(100 / N)
                self.logger.info('    %d %%' % progress)

        # Read 10GbE beamlet output link status during active beamlet data output
        # Note: These MP are not useful, see BSN monitor VHDL.
        rd_beamlet_output_xon = self.client.read("beamlet_output_xon")
        rd_beamlet_output_ready = self.client.read("beamlet_output_ready")

        # End test
        rd_end_bsn = self.client.read("current_bsn")
        self.client.write("beamlet_output_enable", disable)
        rd_end_tx_nof_frames = self.client.read("beamlet_output_10gbe_tx_nof_frames")
        rd_end_rx_nof_frames = self.client.read("beamlet_output_10gbe_rx_nof_frames")

        # Log raw test results
        self.logger.info("rd_beamlet_output_xon = %s", str(rd_beamlet_output_xon))
        self.logger.info("rd_beamlet_output_ready = %s", str(rd_beamlet_output_ready))
        self.logger.info("rd_start_tx_nof_frames = %s", str(rd_start_tx_nof_frames))
        self.logger.info("rd_start_rx_nof_frames = %s", str(rd_start_rx_nof_frames))
        self.logger.info("rd_start_bsn = %s", str(rd_start_bsn))
        self.logger.info("rd_beamlet_output_enable = %s", str(rd_beamlet_output_enable))
        self.logger.info("rd_end_bsn = %s", str(rd_end_bsn))
        self.logger.info("rd_end_tx_nof_frames = %s", str(rd_end_tx_nof_frames))
        self.logger.info("rd_end_rx_nof_frames = %s", str(rd_end_rx_nof_frames))

        # Use current BSN measurements on first node to estimate test duration
        T_sub = C_SDP.N_fft / (C_SDP.f_adc_MHz * 1e6)
        testDuration = (rd_end_bsn[0] - rd_start_bsn[0]) * T_sub
        self.logger.info("testDuration = %f s", testDuration)

        # Determine number of tx and rx frames during test
        txNofFrames = np.array(rd_end_tx_nof_frames) -  np.array(rd_start_tx_nof_frames)
        rxNofFrames = np.array(rd_end_rx_nof_frames) -  np.array(rd_start_rx_nof_frames)

        # Determine expected number of rx frames for the rn_pairs. The expected number of
        # frames for Rx is number frames that were send by corresponding Tx.
        expNofFrames = np.zeros(self.client.n_nodes, dtype=np.int64)
        for rnPair in rn_pairs:
            expNofFrames[rnPair[0]] = txNofFrames[rnPair[1]]
            expNofFrames[rnPair[1]] = txNofFrames[rnPair[0]]

        c_sdp_cep_nof_blocks_per_packet = 4
        f_sub = 1 / T_sub
        expNofFramesPerSecond = f_sub / c_sdp_cep_nof_blocks_per_packet

        # Log number of frames
        self.logger.info("txNofFrames = %s", str(list(txNofFrames)))
        self.logger.info("expNofFrames = %s", str(list(expNofFrames)))
        self.logger.info("rxNofFrames = %s", str(list(rxNofFrames)))
        for rn in rn_list:
            self.logger.info('RN-%d: Tx %d, Rx %d frames in total', rn, txNofFrames[rn], rxNofFrames[rn])
        for rn in rn_list:
            self.logger.info('RN-%d: Tx %f, Rx %f frames per s',
                             rn, txNofFrames[rn] / testDuration, rxNofFrames[rn] / testDuration)
        self.logger.info("expNofFramesPerSecond = %f", expNofFramesPerSecond)

        # Verify number of received frames. Keep checking also when intermediate result
        # is already False, because other links may still succeed when one fails.
        result = True
        # . Verify active link, so that frames can be send at all
        for rn in rn_list:
            if txNofFrames[rn] == 0:
                result = False
                self.logger.error('RN-%d: Nothing send, no active 10GbE link', rn)
        # . Verify expected number of rx frames per second, becomes more accurate for longer mtime
        for rn in rn_list:
            rxNofFramesPerSecond = rxNofFrames[rn] / testDuration
            # Typically margin = 100 / mtime is suitable, but on 15 jul 2024 with one 100 s regression test
            # a difference of 126 occurred (so much more than 100 / mtime = 1). The four other reruns all
            # did pass ok (2024-07-13T21.08.29_bdcc35639_lofar2_unb2b_sdp_station_full_wg). Purpose of this
            # check is to confirm that the rxNofFramesPerSecond is close to expNofFramesPerSecond. The
            # exact hard verification is done by rxNofFrames = expNofFrames. Therefore it is fine to use a
            # larger margin for this frame rate verification.
            # margin = 100 / mtime
            margin = 250
            if rxNofFramesPerSecond < expNofFramesPerSecond - margin or \
               rxNofFramesPerSecond > expNofFramesPerSecond + margin:
                result = False
                self.logger.error('RN-%d: Rx %f differs too much from Expected %f frames per s',
                                  rn, rxNofFramesPerSecond, expNofFramesPerSecond)
        # . Verify total number of rx frames equals total number of send frames, otherwise frames got lost
        for rn in rn_list:
            if rxNofFrames[rn] != expNofFrames[rn]:
                result = False
                self.logger.error('RN-%d: Rx %d != Tx %d frames', rn, rxNofFrames[rn], expNofFrames[rn])
        if result:
            self.print_log("PASSED")
        else:
            self.print_log("FAILED")
        return result


def run_setup(_setup, _args, beamlet_stream):
    if _setup:
        beamlet_stream.setup_stream_network()


def run_stream(_stream, _args, beamlet_stream):
    if _stream == "OFF":
        beamlet_stream.stream_control.stream_off()
    elif _stream == "ON":
        beamlet_stream.stream_control.stream_on()


def run_bsn_monitor(_bsn_monitor, _args, beamlet_stream):
    if _bsn_monitor:
        beamlet_stream.bsn_monitor(_args.mtime)


def run_headers(_headers, _args, beamlet_stream):
    if _headers:
        beamlet_stream.start_stream_reader()
        stop_time = time.time() + _args.mtime
        while True:
            try:
                beamlet_stream.print_header()
                if time.time() > stop_time:
                    break
            except KeyboardInterrupt:
                print(" user hit ctrl-c")
                break
        beamlet_stream.stop_stream_reader()


def run_test_header(_test_header, _args, client, beamlet_stream):
    if _test_header:
        node_list = client.node_list
        n_nodes = client.read("sdp_config_nof_fpgas")
        start_rn = node_list[0] - beamlet_stream.o_rn
        end_rn = node_list[-1] - beamlet_stream.o_rn

        if not all((client.read("processing_enable")[start_rn:end_rn + 1])):
            client.write("processing_enable", ([True] * n_nodes))
        beamlet_stream.setup_stream_network()
        beamlet_stream.start_stream_reader()
        beamlet_stream.test_header(_args.nof_packets)
        beamlet_stream.stop_stream_reader()


def run_test_beamlets(_test_beamlets, _args, client, beamlet_stream):
    if _test_beamlets:
        node_list = client.node_list
        n_nodes = client.read("sdp_config_nof_fpgas")
        start_rn = node_list[0] - beamlet_stream.o_rn
        end_rn = node_list[-1] - beamlet_stream.o_rn

        if not all((client.read("processing_enable")[start_rn:end_rn + 1])):
            client.write("processing_enable", ([True] * n_nodes))
        beamlet_stream.setup_stream_network()
        beamlet_stream.start_stream_reader()
        beamlet_stream.test_beamlets(node_list, _args.nof_packets)
        beamlet_stream.stop_stream_reader()


def run_test_10gbe_output(_test_10bge_output, _args, client, beamlet_stream):
    if _test_10bge_output:
        # Choose to let GN send to eachother per pair of RN, this implies that
        # an even number of RN must be given and all RN must be different.
        rn_list = arg_str_to_list(_args.ring_nodes) if _args.ring_nodes else []
        rn_pairs = beamlet_stream.get_rn_pairs(rn_list)
        if len(rn_pairs) > 0:
            # Setup loopback between nodes
            beamlet_stream.setup_stream_loopback(rn_pairs)
            # Run loopback test
            beamlet_stream.test_10gbe_output(rn_list, rn_pairs, _args.mtime)
            # Restore network output
            beamlet_stream.setup_stream_network()


def main():
    exit_state = 0
    global stream_mac, stream_ip

    client = OpcuaClient(args.host, args.port)
    client.connect()
    node_list = nodelist
    if node_list is None:
        node_list = client.node_list  # if no --nodes in arguments, select all.

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    beamlet_stream = BeamletStream(client, stream_mac, stream_ip)

    try:
        # If necessary start SDP processing
        start_processing(client)

        run_setup(args.setup, args, beamlet_stream)

        run_stream(args.stream, args, beamlet_stream)

        run_bsn_monitor(args.bsn_monitor, args, beamlet_stream)

        run_headers(args.headers, args, beamlet_stream)

        run_test_header(args.test_header, args, client, beamlet_stream)

        run_test_beamlets(args.test_beamlets, args, client, beamlet_stream)

        run_test_10gbe_output(args.test_10gbe_output, args, client, beamlet_stream)

    except BaseException as err:
        exit_state = handle_exception(err)

    stop_all_streams(client)
    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(textwrap.dedent("""\
            opcua client command line argument parser:
            1) --bsn-monitor of beamlet output
              # use --setup to setup beamlet stream
              # use --stream to enable output or in other terminal use sdp_rw.py
              beamlet_stream.py --host 10.99.0.250 --port 4842 -n 64:79 --ip dop386 --mac dop386 --setup --stream ON --bsn-monitor --mtime 3 -vv
              beamlet_stream.py --host 10.99.0.250 --port 4842 -n 64:79 --ip dop386 --mac dop386 --setup --stream OFF --bsn-monitor --mtime 3 -vv
              sdp_rw.py --host 10.99.0.250 --port 4842 -r beamlet_output_enable
              sdp_rw.py --host 10.99.0.250 --port 4842 -w beamlet_output_enable [True]*16

            2) --headers --> print headers during mtime
              beamlet_stream.py --host 10.99.0.250 --port 4842 -n 64:79 --ip dop386 --mac dop386 --setup --stream ON --headers --mtime 3

            3) --test-header --> valid packets = mtime * N_beamsets packets
              beamlet_stream.py --host 10.99.0.250 --port 4842 -n 64:79 --ip dop386 --mac dop386 --test-header --mtime 3

            4) --test-beamlets --> verifies expected beamlets
              beamlet_stream.py --host 10.99.0.250 --port 4842 -n 64:79 --ip dop386 --mac dop386 --test-beamlets --mtime 1
            5) --test-10gbe-output --> verifies beamlet packet rate via 10gbe output
              beamlet_stream.py --host 10.99.0.250 --port 4842 -n 64:79 --ip dop386 --mac dop386 --test-10gbe-output -r 0,1 --mtime 1\n""")),
        formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="global nodes to use")
    parser.add_argument("-r", "--ring-nodes", type=str, help="ring nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument("--ip", type=str, help="ip of destination machine")
    parser.add_argument("--mac", type=str, help="mac of destination machine")
    parser.add_argument("--setup", action="store_true", help="setup beamlet stream")
    parser.add_argument(
        "--stream", type=str, choices=["OFF", "ON"], help="turn off/on beamlet stream"
    )
    parser.add_argument("--bsn-monitor", action="store_true", help="monitor bsn BEAMLET output")
    parser.add_argument(
        "--mtime",
        type=float,
        default=1.0,
        help=textwrap.dedent("""set time to monitor bsn BEAMLET output, in seconds, default = 1 when used with --bsn-monitor""")
    )
    parser.add_argument(
        "--nof_packets",
        type=int,
        default=1000,
        help=textwrap.dedent("""set number of incoming packets to record, default = 1000 used with --test-header or --test-beamlets""")
    )
    parser.add_argument("--headers", action="store_true", help="print beamlet packet headers")
    parser.add_argument("--test-header", action="store_true", help="validate contents of the beamlet packet headers")
    parser.add_argument("--test-beamlets", action="store_true", help="validate contents of the beamlet packet data")
    parser.add_argument("--test-10gbe-output", action="store_true", help="validate number of beamlet packets via 10GbE")

    args = parser.parse_args()

    nodelist = arg_str_to_list(args.nodes) if args.nodes else None
    rnlist = arg_str_to_list(args.ring_nodes) if args.ring_nodes else None
    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    stream_mac, stream_ip = parse_stream_destination(args.mac, args.ip, "10G")
    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")
    logger.info("parsed arguments: %s", str(args))

    if args.setup:
        if not stream_ip or not stream_mac:
            logger.error("need ip and mac for this request")
            sys.exit(1)

    sys.exit(main())
