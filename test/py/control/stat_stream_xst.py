#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
#
# Purpose:
# . Control, monitor and setup XST statistics stream
#
# Description:
# . run ./stat_stream_xst.py -h for help
#
# References:
# [1] https://support.astron.nl/confluence/pages/viewpage.action?spaceKey=L2M&title=L5+SDPFW+Design+Document%3A+Subband+Correlator
# [2] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Ring
# [3] https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface
#
# ##########################################################################

import sys
import logging
import time
import numpy as np

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list

from base.constants import C_SDP
from base.sdp_functions import parse_stream_destination, start_processing, stop_all_streams
from base.stream_reader import StreamReader, print_header
from base.stream_control import StreamControl
from wg import WaveformGenerator


class XstStatisticStream(object):
    def __init__(self, client, dest_mac, dest_ip, dest_udp_port):
        self.logger = logging.getLogger("XstStatisticStream")
        self.client = client
        self.n_nodes = self.client.n_nodes  # get number of nodes from server
        self.n_rn = self.client.n_nodes  # all nodes used in ring
        self.o_rn = self.client.gn_first_fpga
        self.last_rn = self.n_rn - 1  # last node in ring relative to o_rn
        self.s_ant = self.n_nodes * C_SDP.S_pn  # assume signal input 0 is on first node
        self.p_sq = self.n_nodes // 2 + 1  # see [1]
        self.nof_hops = self.p_sq - 1  # see [1, 2]
        self.dest_mac = dest_mac
        self.dest_ip = dest_ip
        self.dest_udp_port = dest_udp_port
        self.stream_reader = None
        self.stream_control = None
        self.next_packet_nr = 0
        self.stdout = self.logger.getEffectiveLevel() > logging.INFO
        stop_all_streams(self.client)

    def print_log(self, fstr):
        print(fstr) if self.stdout else self.logger.info(fstr)

    # == OPC-UA CP for XSub ==
    @property
    def xst_subband_select(self):
        return self.client.read("xst_subband_select")

    @xst_subband_select.setter
    def xst_subband_select(self, sb_select):
        return self.client.write("xst_subband_select", sb_select)

    @property
    def xst_integration_interval(self):
        return self.client.read("xst_integration_interval")

    @xst_integration_interval.setter
    def xst_integration_interval(self, interval):
        return self.client.write("xst_integration_interval", interval)

    @property
    def xst_start_time(self):
        return self.client.read("xst_start_time")

    @xst_start_time.setter
    def xst_start_time(self, start_time):
        response = self.client.write("xst_start_time", start_time)
        print_result("xst_start_time", response)

    @property
    def xst_processing_enable(self):
        return self.client.read("xst_processing_enable")

    @xst_processing_enable.setter
    def xst_processing_enable(self, enable):
        response = self.client.write("xst_processing_enable", enable)
        print_result("xst_processing_enable", response)

    @property
    def xst_offload_hdr_eth_destination_mac(self):
        return self.client.read("xst_offload_hdr_eth_destination_mac")

    @xst_offload_hdr_eth_destination_mac.setter
    def xst_offload_hdr_eth_destination_mac(self, mac):
        response =  self.client.write("xst_offload_hdr_eth_destination_mac", mac)
        print_result("xst_offload_hdr_eth_destination_mac", response)

    @property
    def xst_offload_hdr_ip_destination_address(self):
        return self.client.read("xst_offload_hdr_ip_destination_address")

    @xst_offload_hdr_ip_destination_address.setter
    def xst_offload_hdr_ip_destination_address(self, ip):
        response =  self.client.write("xst_offload_hdr_ip_destination_address", ip)
        print_result("xst_offload_hdr_ip_destination_address", response)

    @property
    def xst_offload_hdr_udp_destination_port(self):
        return self.client.read("xst_offload_hdr_udp_destination_port")

    @xst_offload_hdr_udp_destination_port.setter
    def xst_offload_hdr_udp_destination_port(self, port):
        response =  self.client.write("xst_offload_hdr_udp_destination_port", port)
        print_result("xst_offload_hdr_udp_destination_port", response)

    @property
    def xst_offload_nof_crosslets(self):
        return self.client.read("xst_offload_nof_crosslets")

    @xst_offload_nof_crosslets.setter
    def xst_offload_nof_crosslets(self, n_crosslets):
        response =  self.client.write("xst_offload_nof_crosslets", n_crosslets)
        print_result("xst_offload_nof_crosslets", response)

    @property
    def xst_offload_enable(self):
        return self.client.read("xst_offload_enable")

    @xst_offload_enable.setter
    def xst_offload_enable(self, enable):
        response =  self.client.write("xst_offload_enable", enable)
        print_result("xst_offload_enable", response)

    #  == OPC-UA MP for XSub ==
    @property
    def xst_input_bsn_at_sync(self):
        return self.client.read("xst_input_bsn_at_sync")

    @property
    def xst_output_sync_bsn(self):
        return self.client.read("xst_output_sync_bsn")

    @property
    def xst_offload_bsn(self):
        return self.client.read("xst_offload_bsn")

    @property
    def xst_offload_nof_packets(self):
        return self.client.read("xst_offload_nof_packets")

    @property
    def xst_offload_nof_valid(self):
        return self.client.read("xst_offload_nof_valid")

    @property
    def xst_ring_nof_transport_hops(self):
        return self.client.read("xst_ring_nof_transport_hops")

    # == OPC-UA CP for Ring in XSub ==
    @xst_ring_nof_transport_hops.setter
    def xst_ring_nof_transport_hops(self, nof_hops):
        response = self.client.write("xst_ring_nof_transport_hops", nof_hops)
        print_result("xst_ring_nof_transport_hops", response)

    @property
    def xst_ring_rx_clear_total_counts(self):
        return self.client.read("xst_ring_rx_clear_total_counts")

    @xst_ring_rx_clear_total_counts.setter
    def xst_ring_rx_clear_total_counts(self, clear):
        response = self.client.write("xst_ring_rx_clear_total_counts", clear)
        print_result("xst_ring_rx_clear_total_counts", response)

    @property
    def xst_rx_align_stream_enable(self):
        return self.client.read("xst_rx_align_stream_enable")

    @xst_rx_align_stream_enable.setter
    def xst_rx_align_stream_enable(self, enable):
        response = self.client.write("xst_rx_align_stream_enable", enable)
        print_result("xst_rx_align_stream_enable", response)

    # == OPC-UA MP for Ring in XSub ==
    @property
    def xst_ring_rx_total_nof_packets_received(self):
        return self.client.read("xst_ring_rx_total_nof_packets_received")

    @property
    def xst_ring_rx_total_nof_packets_discarded(self):
        return self.client.read("xst_ring_rx_total_nof_packets_discarded")

    @property
    def xst_ring_rx_total_nof_sync_received(self):
        return self.client.read("xst_ring_rx_total_nof_sync_received")

    @property
    def xst_ring_rx_total_nof_sync_discarded(self):
        return self.client.read("xst_ring_rx_total_nof_sync_discarded")

    @property
    def xst_ring_rx_bsn(self):
        return self.client.read("xst_ring_rx_bsn")

    @property
    def xst_ring_rx_nof_packets(self):
        return self.client.read("xst_ring_rx_nof_packets")

    @property
    def xst_ring_rx_nof_valid(self):
        return self.client.read("xst_ring_rx_nof_valid")

    @property
    def xst_ring_rx_latency(self):
        return self.client.read("xst_ring_rx_latency")

    @property
    def xst_rx_align_bsn(self):
        return self.client.read("xst_rx_align_bsn")

    @property
    def xst_rx_align_nof_packets(self):
        return self.client.read("xst_rx_align_nof_packets")

    @property
    def xst_rx_align_nof_valid(self):
        return self.client.read("xst_rx_align_nof_valid")

    @property
    def xst_rx_align_latency(self):
        return self.client.read("xst_rx_align_latency")

    @property
    def xst_rx_align_nof_replaced_packets(self):
        return self.client.read("xst_rx_align_nof_replaced_packets")

    @property
    def xst_aligned_bsn(self):
        return self.client.read("xst_aligned_bsn")

    @property
    def xst_aligned_nof_packets(self):
        return self.client.read("xst_aligned_nof_packets")

    @property
    def xst_aligned_nof_valid(self):
        return self.client.read("xst_aligned_nof_valid")

    @property
    def xst_aligned_latency(self):
        return self.client.read("xst_aligned_latency")

    @property
    def xst_ring_tx_bsn(self):
        return self.client.read("xst_ring_tx_bsn")

    @property
    def xst_ring_tx_nof_packets(self):
        return self.client.read("xst_ring_tx_nof_packets")

    @property
    def xst_ring_tx_nof_valid(self):
        return self.client.read("xst_ring_tx_nof_valid")

    @property
    def xst_ring_tx_latency(self):
        return self.client.read("xst_ring_tx_latency")

    # == end off set/get properties ==

    def setup_lane(self):
        # Setup nof hops for XST lane, see [1, 2]
        response = True
        nof_hops = [self.nof_hops] * self.n_nodes
        response &= self.client.write("xst_ring_nof_transport_hops", nof_hops)
        print_result("setup_lane", response)

    def setup_offload_hdr(self, mac=None, ip=None, port=None):
        """Setup XST offload destination address (same to all n_nodes)."""
        # Use default or argument
        _mac = self.dest_mac if mac is None else mac
        _ip = self.dest_ip if ip is None else ip
        _port = self.dest_udp_port if port is None else port

        # Use list or same for all nodes
        _mac = _mac if isinstance(_mac, list) else [_mac] * self.n_nodes
        _ip = _ip if isinstance(_ip, list) else [_ip] * self.n_nodes
        _port = _port if isinstance(_port, list) else [_port] * self.n_nodes

        self.xst_offload_hdr_udp_destination_port = _port
        self.xst_offload_hdr_eth_destination_mac = _mac
        self.xst_offload_hdr_ip_destination_address = _ip
        print_result("setup_offload_hdr", True)

    def setup_sub_weights(self, gain=1.0):
        response = True
        """Write subband weights, default gain = unit, phase = 0 fixed, so im = 0)"""
        fxp_gain = int(gain * 2**C_SDP.W_sub_weight_fraction)
        self.logger.info(f"set subband weight: {gain} = {fxp_gain}")
        sub_gains = [fxp_gain] * self.n_nodes * C_SDP.S_pn * C_SDP.N_sub
        response &= self.client.write("subband_weights", (sub_gains))
        print_result("setup_sub_weights", response)

    def setup_stream(self, sb_select, int_interval, n_crosslets):
        """Setup XST offload parameters (same to all n_nodes)."""
        if not isinstance(sb_select, list):
            self.logger.error("sb_select must be a list of 8 values")
            print_result("setup_stream", False)
            return
        if len(sb_select) != 8:
            self.logger.error("sb_select must have 8 values")
            print_result("setup_stream", False)
            return

        self.xst_subband_select = sb_select * self.n_nodes
        self.xst_integration_interval = [float(int_interval)] * self.n_nodes
        self.xst_offload_nof_crosslets = [int(n_crosslets)] * self.n_nodes
        print_result("setup_stream", True)

    def xst_processing_restart(self):
        # No need to toggle xst_processing_enable to restart all n_nodes
        self.xst_processing_enable = [True] * self.n_nodes
        time.sleep(2.0)  # see [3]
        print_result("xst_processing_restart", True)

    def bsn_monitor(self, mtime):
        """mtime: monitor runtime in seconds"""
        ntimes = 0
        start = time.time()
        while ntimes < mtime:
            while start > time.time():
                time.sleep(0.001)
            start += 1.0
            ntimes += 1
            self.logger.info(f"XST bsn nof_packets: {self.xst_offload_nof_packets}")
            self.logger.info(f"    bsn nof_valid  : {self.xst_offload_nof_valid}")
        print_result("bsn_monitor", True)

    def start_stream_reader(self):
        self.stream_reader = StreamReader(self.dest_udp_port, "XST", n_workers=1)
        self.stream_control = StreamControl(self.client, self.stream_reader)
        self.logger.debug("start stream-reader")
        self.stream_reader.start()
        print_result("start_stream_reader", True)

    def stop_stream_reader(self, timeout=60):
        if self.stream_reader:
            print_result("stop_stream_reader", True)
            # Wait until SDPFW has stopped stream output
            timeout_time = time.time() + timeout
            while time.time() < timeout_time and not self.stream_reader.done():
                time.sleep(0.0001)
            self.stream_reader.stop()

    def start_io(self):
        """Start stream_reader for input and start SDPFW output

        Start stream_reader before SDPFW statistics offload, to receive all statistics
        packets per integration interval.
        """
        self.start_stream_reader()
        self.stream_control.stream_on()
        self.logger.info("Started io")
        print_result("start_io", True)

    def stop_io(self):
        """Stop SDPFW output and stop stream_reader input

        Stop stream_reader after when SDPFW statistics offload has stopped, to receive all
        statistics packets per integration interval.
        """
        self.stream_control.stream_off()
        self.stop_stream_reader()
        self.logger.info("Stopped io")
        print_result("", True)

    def expected_nof_packets(self):
        # read current nof_crosslets to determine expect nof packets per integration interval
        # . use read from first node, assume all nodes have same settings
        nof_crosslets = self.xst_offload_nof_crosslets[0]
        nof_packets = self.n_nodes * self.p_sq * nof_crosslets
        return nof_packets, nof_crosslets

    def check_nof_valid_packets(self, n_valid_packets):
        # verify n_valid_packets should be integer multiple of nof_packets per integration interval
        nof_packets, nof_crosslets = self.expected_nof_packets()
        success = (n_valid_packets % nof_packets) == 0

        self.print_log(f"valid packets = {n_valid_packets} (= {self.n_nodes} * {self.p_sq} * {nof_crosslets} * {n_valid_packets / nof_packets})")
        print_result("check_nof_valid_packets", success)
        return success

    def print_missing_indices(self):
        # read current XST settings
        # . use read from first node, assume all nodes have same settings
        nof_packets, nof_crosslets = self.expected_nof_packets()
        sb_select = self.xst_subband_select[0:7]
        crosslets = sb_select[1:]
        # get BSN of all integration intervals
        bsns = self.stream_reader.data.get_all_bsn()
        self.print_log(f"Received bsn numbers {bsns}")
        # check expected xst indices per integration interval
        success = True
        for bsn in bsns[:-1]:  # ignore last bsn, may be incomplete due to stop reading ?
            indices = self.stream_reader.data.get_all_index(bsn)
            for xlet in range(nof_crosslets):
                sub = crosslets[xlet]
                for si_A in range(0, self.s_ant, C_SDP.S_pn):
                    for si_B in range(0, self.s_ant, C_SDP.S_pn):
                        # either the A*B or B*A (= conjugate) XST packet has to be there
                        data_id = self.stream_reader.data.xst_indices_to_data_id(
                            sub, si_A, si_B
                        )
                        conj_id = self.stream_reader.data.xst_indices_to_data_id(
                            sub, si_B, si_A
                        )
                        if data_id not in indices and conj_id not in indices:
                            self.logger.error(
                                f"- sub {sub}, A {si_A}, B {si_B} (= data_id {data_id}) not in bsn {bsn}"
                            )
                            success = False
        if not success:
            self.print_log("print_missing_indices")
        print_result("print_missing_indices", success)

    def print_xst_header(self):
        success = True
        bsn, index = self.stream_reader.data.get_packet_bsn_index(self.next_packet_nr)
        if bsn is not None and index is not None:
            hdr = self.stream_reader.data.get_header(bsn, index)
            recv_time = self.stream_reader.data.get_receive_time(bsn, index)
            print_header(hdr, stdout=self.stdout, recv_time=recv_time)
            self.next_packet_nr += 1
        else:
            success = False
            time.sleep(0.0001)
        print_result("print_xst_header", success)

    def plot(self):
        from base.plotting import Plotting

        s_pn = C_SDP.S_pn
        exp_nof_crosslets = 1
        exp_nof_index = C_SDP.X_sq * exp_nof_crosslets
        n_cols = 2  # One subplot for XST amplitude and one subplot for XST phase
        n_rows = 1
        s_ant = self.s_ant
        tick_labels = [i for i in range(s_ant)]
        fig = Plotting(fig_size=[15, 8], rows=n_rows, cols=n_cols)

        # XST phases at left
        fig.add_imshow(0, 0, (s_ant, s_ant), vmin=-180, vmax=180)
        fig.set_title(
            0, 0, "Angle values of complex crosslets statistics (deg)", fontsize=12
        )
        fig.set_yas_label(0, 0, "Signal B index", fontsize=10)
        fig.set_xas_label(0, 0, "Signal A index", fontsize=10)
        fig.set_xtick_labels(0, 0, tick_labels)
        fig.set_ytick_labels(0, 0, tick_labels)
        fig.rotate_xtick_label(0, 0, angle=45)

        # XST powers at right
        fig.add_imshow(0, 1, (s_ant, s_ant), vmin=0, vmax=1)
        fig.set_title(
            0,
            1,
            "Absolute values of complex crosslets statistics (in dB, normalized to 0..1)",
            fontsize=12,
        )
        fig.set_yas_label(0, 1, "Signal B index", fontsize=10)
        fig.set_xas_label(0, 1, "Signal A index", fontsize=10)
        fig.set_xtick_labels(0, 1, tick_labels)
        fig.set_ytick_labels(0, 1, tick_labels)
        fig.rotate_xtick_label(0, 1, angle=45)

        fig.draw()

        # Prepare xst_acm = XST array correlation matrix
        # Use NaN as default to detect missing XST values
        # Use np.ma module to automatically mask invalid values (nan, inf) with np.ma.masked_invalid(),
        # so that missing values can be plotted as empty (black)
        xst_acm = np.ma.zeros((s_ant, s_ant), dtype=complex)
        xst_acm[:] = np.nan  # set all to NaN, to detect missing values

        while True:
            # wait for complete XST integration interval received
            all_bsn = self.stream_reader.data.get_all_bsn()
            nof_bsn = len(all_bsn)
            if nof_bsn < 2:
                continue

            # use last complete XST integration interval received
            bsn = all_bsn[-2]

            # wait until all indices for bsn have been obtained, or timeout
            if not self.stream_reader.data.wait_for_all_indices(bsn, exp_nof_index):
                break

            #  fill xst_acm with received XST data
            all_index = self.stream_reader.data.get_all_index(bsn)
            for index in all_index:
                hdr = self.stream_reader.data.get_header(bsn, index)
                data = self.stream_reader.data.get_data(bsn, index)

                stats = np.ma.array(data)
                stats = np.ma.masked_invalid(stats)  # mask all invalid values
                stats = stats.reshape((s_pn, s_pn))

                # if all data zero, do not use it
                # if np.max(np.ma.abs(stats)) == 0:
                #    continue

                a_idx = hdr.signal_input_A_index
                b_idx = hdr.signal_input_B_index
                xst_acm[a_idx : a_idx + s_pn, b_idx : b_idx + s_pn] = stats

                # fill in missing
                if a_idx != b_idx:  # if a_index is b_index there is no missing
                    stats_missing = np.ma.conjugate(stats)
                    stats_missing = np.ma.transpose(stats_missing)
                    xst_acm[b_idx : b_idx + s_pn, a_idx : a_idx + s_pn] = stats_missing

            # Draw log plot
            # xst_acm = np.where(np.abs(xst_acm) != 0.0, xst_acm, (1 + 0j))
            xst_acm = np.ma.masked_invalid(xst_acm)
            xst_angles = np.ma.angle(xst_acm, deg=True)
            xst_powers = np.ma.abs(xst_acm)
            log_xst_powers = 10 * np.ma.log10(xst_powers)  # in dB

            # normalize value in range 0..1
            log_xst_max = np.max(log_xst_powers)
            log_xst_min = np.min(log_xst_powers)
            log_xst_powers = (log_xst_powers - log_xst_min) / (log_xst_max - log_xst_min)
            self.logger.info(f"XST max = {log_xst_max} dB, XST min = {log_xst_min} dB")

            fig.update_imshow(0, 0, xst_angles)
            fig.update_imshow(0, 1, log_xst_powers)

            fig.update_graph()
            break

        return True

    def test_cp(self):
        """
        https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface+presentation
        see 4.6.1
        see 4.5.1.2
        """
        success = True

        # Test lane default nof hops
        self.logger.info("test xst lane default nof hops")
        t_n_hops = [self.nof_hops] * self.n_nodes
        if self.xst_ring_nof_transport_hops != t_n_hops:
            self.logger.error(
                f"xst_ring_nof_transport_hops={self.xst_ring_nof_transport_hops}"
            )
            success = False

        # Test offload destination mac, ip, port
        self.logger.info("test xst offload header settings")
        t_port = [5115] * self.n_nodes
        t_mac = ["00:11:22:33:44:55"] * self.n_nodes
        t_ip = ["10.20.30.40"] * self.n_nodes

        self.xst_offload_hdr_udp_destination_port = t_port
        self.xst_offload_hdr_eth_destination_mac = t_mac
        self.xst_offload_hdr_ip_destination_address = t_ip
        # self.setup_offload_hdr(t_mac, t_ip, t_port)

        if self.xst_offload_hdr_udp_destination_port != t_port:
            self.logger.error(
                f"xst_offload_hdr_udp_destination_port={self.xst_offload_hdr_udp_destination_port}"
            )
            success = False
        if self.xst_offload_hdr_eth_destination_mac != t_mac:
            self.logger.error(
                f"xst_offload_hdr_eth_destination_mac={self.xst_offload_hdr_eth_destination_mac}"
            )
            success = False
        if self.xst_offload_hdr_ip_destination_address != t_ip:
            self.logger.error(
                f"xst_offload_hdr_ip_destination_address={self.xst_offload_hdr_ip_destination_address}"
            )
            success = False

        self.logger.info("setup offload header given by command line arguments")
        self.setup_offload_hdr()

        self.logger.info("test xst processing settings")
        # Remember current xst_processing_enable
        store_xst_processing_enable = self.xst_processing_enable

        # write and check xst stream settings
        t_sb_select = [7, 1, 2, 3, 4, 5, 6, 7]
        t_int_interval = 2.0
        t_n_crosslets = 7

        self.setup_stream(t_sb_select, t_int_interval, t_n_crosslets)
        self.xst_processing_restart()
        t_xst_proc_enable = True

        if self.xst_subband_select != t_sb_select * self.n_nodes:
            self.logger.error(f"xst_subband_select={self.xst_subband_select}")
            success = False
        if self.xst_integration_interval != [t_int_interval] * self.n_nodes:
            self.logger.error(
                f"xst_integration_interval={self.xst_integration_interval}"
            )
            success = False
        if self.xst_offload_nof_crosslets != [t_n_crosslets] * self.n_nodes:
            self.logger.error(
                f"xst_offload_nof_crosslets={self.xst_offload_nof_crosslets}"
            )
            success = False
        if self.xst_processing_enable != [t_xst_proc_enable] * self.n_nodes:
            self.logger.error(f"xst_processing_enable={self.xst_processing_enable}")
            success = False

        self.logger.info("test xst start time")
        self.xst_processing_enable = [False] * self.n_nodes
        start_time = time.time() + 10.0
        self.logger.info(f"set start time = {start_time}")
        self.xst_start_time = [start_time] * self.n_nodes
        self.xst_processing_enable = [True] * self.n_nodes
        while time.time() < (start_time - 0.1):
            if self.xst_processing_enable != [False] * self.n_nodes:
                self.logger.error(f"xst_processing_enable={self.xst_processing_enable} on time {time.time()}")
                success = False
            time.sleep(0.1)

        time.sleep(1.0)
        if self.xst_processing_enable != [True] * self.n_nodes:
            self.logger.error(f"xst_processing_enable={self.xst_processing_enable} on time {time.time()}")
            success = False

        self.logger.info("setup xst processing given by command line arguments")
        self.setup_stream(args.sb_select, args.interval, args.nof_crosslets)
        self.xst_processing_enable = store_xst_processing_enable
        print_result("test_cp", success)

    def test_mp(self):
        """
        https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface+presentation
        see 4.6.2
        """

        def make_check_mask(type):
            # make mask -1 is fixed, 0 is random
            base = [-1] * (self.n_nodes // 2) + [0] * (self.n_nodes // 2)
            if type == "tx":
                base = [0] + base[:-1]
            mask = base
            for _ in range(self.n_nodes - 1):
                base = [base[-1]] + base[:-1]
                mask += base

            self.logger.info(f"{type}: mask = {mask}")
            return mask

        def check_ring_nof_packets(arr, type, valid):
            success = True
            mask = make_check_mask(type)
            # mask value -1 must be same as arr, mask value 0, check if arr in valid
            for nr in range(len(mask)):
                if mask[nr] == -1:
                    if arr[nr] != -1:
                        self.logger.error(f"arr[{nr}]={arr[nr]} != -1")
                        success = False
                else:
                    if arr[nr] not in valid:
                        self.logger.error(f"arr[{nr}]={arr[nr]} not in {valid}")
                        success = False
            return success

        def check_nof_packets(arr, valid):
            success = True
            for i in arr:
                if i not in valid:
                    success = False
            return success

        self.logger.info("test xst offload_nof_packets")
        valid_nof_offload_packets = [5, 9]  # 5 for hba, 9 for lba
        valid_nof_packets = [195312, 195313]  # valid for 1 sec integration time

        success = True
        if not check_nof_packets(
            self.xst_offload_nof_packets, valid_nof_offload_packets
        ):
            self.logger.error(f"xst_offload_nof_packets {self.xst_offload_nof_packets}")
            success = False
        if not check_ring_nof_packets(
            self.xst_ring_tx_nof_packets, "tx", valid_nof_packets
        ):
            self.logger.error(f"xst_ring_tx_nof_packets={self.xst_ring_tx_nof_packets}")
            success = False
        if not check_ring_nof_packets(
            self.xst_ring_rx_nof_packets, "rx", valid_nof_packets
        ):
            self.logger.error(f"xst_ring_rx_nof_packets={self.xst_ring_rx_nof_packets}")
            success = False
        if not check_nof_packets(self.xst_rx_align_nof_packets, valid_nof_packets):
            self.logger.error(
                f"xst_rx_align_nof_packets={self.xst_rx_align_nof_packets}"
            )
            success = False
        if not check_nof_packets(self.xst_aligned_nof_packets, valid_nof_packets):
            self.logger.error(f"xst_aligned_nof_packets={self.xst_aligned_nof_packets}")
            success = False
        print_result("test_mp", success)

    def test_header(self, mtime):
        """
        test header
        """
        from sdp_info import SdpInfo

        sdp_info = SdpInfo(self.client)

        # save settings to restore later
        _antenna_field_index = sdp_info.antenna_field_index
        _station_id = sdp_info.station_id
        _observation_id = sdp_info.observation_id
        _antenna_band_index = sdp_info.antenna_band_index
        _nyquist_sampling_zone_index = sdp_info.nyquist_sampling_zone_index

        # set some values
        sdp_info.antenna_field_index = [3] * self.n_nodes
        sdp_info.station_id = [33] * self.n_nodes
        sdp_info.observation_id = [121] * self.n_nodes
        sdp_info.antenna_band_index = [1] * self.n_nodes
        sdp_info.nyquist_sampling_zone_index = [1] * self.n_nodes
        # wait to ensure new sdp_info settings will appear in statitics headers
        time.sleep(1.0)

        # header checks, keyname and expected value
        tests = {
            "marker": [88],
            "version_id": [5],
            "observation_id": sdp_info.observation_id,
            "antenna_field_index": sdp_info.antenna_field_index,
            "station_id": sdp_info.station_id,
            "antenna_band_index": sdp_info.antenna_band_index,
            "nyquist_zone_index": sdp_info.nyquist_sampling_zone_index,
            "f_adc": sdp_info.f_adc,
            "fsub_type": sdp_info.fsub_type,
            "payload_error": [0],
            "beam_repositioning_flag": [0],
            "subband_calibrated_flag": [0, 1],
            "gn_index": list(range(self.o_rn, self.o_rn + self.n_nodes)),
            "integration_interval": [195312, 195313],  # valid for 1sec integration
            "subband_index": list(range(512)),
            "signal_input_A_index": list(range(0, 192, 12)),
            "signal_input_B_index": list(range(0, 192, 12)),
            "nof_signal_inputs": [12],
            "nof_bytes_per_statistics": [8],
            "nof_statistics_per_packet": [288],
            "block_period": [5120],
            "bsn": [0, 1, 195313],  # valid for 1sec integration, XST processing started
            # in even (0, 195313) or odd sync second (1, 195313)
        }

        time.sleep(5.0)
        success = False
        if self.stream_reader:
            # first read in the data.
            record_time = 2.0 if mtime < 2.0 else mtime + 1.0
            self.stream_control.record_time(record_time)
            self.logger.info("Received %d BSN's", self.stream_reader.data.nof_received_bsn())

            # now analyse the data
            success = True
            self.next_packet_nr = 0
            n_valid_packets = 0
            bsn_sub_lo = 195312
            bsn_sub_hi = 195313
            bsn_sub_x2 = bsn_sub_lo + bsn_sub_hi

            try:
                for bsn in self.stream_reader.data.get_all_bsn()[:-1]:  # do not test last bsn
                    for index in self.stream_reader.data.get_all_index(bsn=bsn):
                        hdr = self.stream_reader.data.get_header(bsn, index)
                        valid_header = True
                        for key, exp_value in tests.items():
                            value = hdr[key]
                            if key == "bsn":
                                if value % bsn_sub_x2 not in exp_value:
                                    self.logger.error(
                                        f"{key} {value} not on expected PPS grid {value % bsn_sub_x2}"
                                    )  # see [1]
                                    valid_header = False
                                    break
                            elif value not in exp_value:
                                self.logger.error(f"{bsn}-{index}: {key} {value} not expected {exp_value}")
                                valid_header = False
                                break

                        if valid_header:
                            n_valid_packets += 1
                        else:
                            success = False
            except BaseException as err:
                if handle_exception(err) == 1:
                    return

            if n_valid_packets == 0:
                success = False

        if not self.check_nof_valid_packets(n_valid_packets):
            success = False

        self.print_log("")
        self.print_log(f"self.n_nodes = {self.n_nodes}")
        self.print_log(f"self.n_rn = {self.n_rn}")
        self.print_log(f"self.o_rn = {self.o_rn}")
        self.print_log(f"self.last_rn = {self.last_rn}")
        self.print_log("")

        # restore saved settings
        sdp_info.antenna_field_index = _antenna_field_index
        sdp_info.station_id = _station_id
        sdp_info.observation_id = _observation_id
        sdp_info.antenna_band_index = _antenna_band_index
        sdp_info.nyquist_sampling_zone_index = _nyquist_sampling_zone_index
        print_result("test_header", success)

    def test_data(self, interval, nof_crosslets, wg_apf, sub_gain):
        """
        test data
        """
        success = True
        s_pn = C_SDP.S_pn
        s_ant = self.s_ant

        # Determine maximum expected auto power XST level = SST level at WG frequency subband
        wg_ampl = wg_apf["ampl"][0]  # use ampl of first signal input in list
        wg_sub = int(wg_apf["freq"][0] / C_SDP.f_sub + 0.5)  # use sub of first signal input in list
        si_full_scale = 2**(C_SDP.W_adc - 1)
        si_ampl = wg_ampl * si_full_scale  # WG ampl in LSbit units
        g_subband_sine = C_SDP.G_subband_sine * sub_gain
        sub_ampl = g_subband_sine * si_ampl  # expected subband[wg_sub] amplitude
        sub_power = sub_ampl**2  # subband is complex, so no divide by 2
        # . average number of crosslet subband samples per integration interval
        N_int_sub = interval * C_SDP.f_sub
        max_sub_sst = sub_power * N_int_sub  # expected SST[wg_sub] statistic value
        # . limit XST value considered still to be almost zero for sub != wg_sub, factor found by trial
        power_margin_no_wg = 0.0001 * N_int_sub
        self.logger.info(f"max_sub_sst = {max_sub_sst:e}")
        self.logger.info(f"power_margin_no_wg = {power_margin_no_wg:e}")

        # Determine expected xst_acm (array correlation matrix, square 0:s_ant-1 by 0:s_ant-1
        # . xst_acm expected angles
        #   - all 0 degrees at diagonal from [0][0] left upper corner to [191][191] lower right corner
        #   - upper triangle < 0 degrees from 0 at [i][i] downto -360 at [i][191], but wrap -180:-360 to +180:0
        #   - lower triangle > 0 degrees from 0 at [i][i] to +360 at [i][191], but wrap +180:+360 to -180:0
        wg_phases = np.degrees(np.asarray(wg_apf["phase"]))
        exp_angles = np.zeros((s_ant, s_ant))
        for a_idx in range(s_ant):
            for b_idx in range(s_ant):
                diff_phase = wg_phases[a_idx] - wg_phases[b_idx]
                # get diff_phase in -+ 180 degrees range
                if diff_phase < -180:
                    diff_phase += 360
                if diff_phase > 180:
                    diff_phase -= 360
                exp_angles[a_idx, b_idx] = diff_phase

        # . xst_acm expected powers
        ampls = np.asarray(wg_apf["ampl"])
        si_ampls = ampls * si_full_scale  # WG ampl in LSbit units
        sub_ampls = g_subband_sine * si_ampls  # expected subband[wg_sub] amplitude
        exp_powers = np.outer(sub_ampls, sub_ampls) * N_int_sub  # expected XST[wg_sub] ACM power values
        exp_powers_max = np.max(exp_powers)
        exp_powers_min = np.min(exp_powers)

        # Prepare xst_acm = XST array correlation matrix
        xst_acm = np.zeros((s_ant, s_ant), dtype=complex)

        # first read in the data.
        self.stream_control.record_n_bsn(n_bsn=3)
        self.logger.info("Received %d BSN's:", self.stream_reader.data.nof_received_bsn())
        bsn = self.stream_reader.data.get_all_bsn()[1]

        # Process the received XST data:
        # . all_index contains the data_id's of all received packets in the bsn integration interval.
        # . for wg_sub fill xst_acm
        # . for other sub check that XST data is almost zero (occurs when nof_crosslets > 1).
        # . Using nof_crosslets > 1 yields at most one crosslet with WG data and others with almost
        #   zero data. This verifies that the crosslet index (data_id) in the header matches the
        #   crosslet data in the payload.
        all_index = self.stream_reader.data.get_all_index(bsn)
        wg_sub_present = False
        other_subs = []
        for index in all_index:
            hdr = self.stream_reader.data.get_header(bsn, index)
            data = self.stream_reader.data.get_data(bsn, index)

            stats = np.array(data)
            stats = stats.reshape((s_pn, s_pn))

            sub = hdr.subband_index
            a_idx = hdr.signal_input_A_index
            b_idx = hdr.signal_input_B_index

            # verify hdr payload_error bit
            if hdr.payload_error != 0:
                self.logger.error(f"sub-{sub}, A-{a_idx}, B-{b_idx} XST packet payload error")
                success = False
                pass

            # verify hdr integration_interval, due to fraction rounding it can differ by 1 from expected
            hdr_int_sub = hdr.integration_interval
            if abs(N_int_sub - hdr_int_sub) > 1:
                self.logger.error(f"sub-{sub}, A-{a_idx}, B-{b_idx} XST integration interval {hdr_int_sub} != {N_int_sub}")
                success = False
                pass

            if sub == wg_sub:
                wg_sub_present = True

                # fill xst_acm for WG sub
                xst_acm[a_idx : a_idx + s_pn, b_idx : b_idx + s_pn] = stats

                # fill in missing
                if a_idx != b_idx:  # if a_index is b_index there is no missing
                    stats_missing = np.conjugate(stats)
                    stats_missing = np.transpose(stats_missing)
                    xst_acm[b_idx : b_idx + s_pn, a_idx : a_idx + s_pn] = stats_missing
            else:
                other_subs.append(sub)
                # verify that any other sub has almost zero power
                stats_powers = np.abs(stats)
                stats_powers_max = np.max(stats_powers)
                self.logger.debug(f"sub-{sub}, A-{a_idx}, B-{b_idx} XST value max = {np.max(stats_powers)}")
                if len(stats_powers[stats_powers > power_margin_no_wg]) > 0:
                    self.logger.error(f"sub-{sub}, A-{a_idx}, B-{b_idx} XST value(s) differ too much from zero " +
                                      f"({stats_powers_max:e} > {power_margin_no_wg:e})")
                    success = False

        # All packets contained same hdr_int_sub if test passed
        self.logger.info(
            f"BSN-{bsn} hdr integration interval = {hdr_int_sub}, expected N_int_sub = {N_int_sub}"
        )

        # Report other sub present
        other_subs = list(
            set(other_subs)
        )  # use set() to remove duplicates from the list
        if len(other_subs) == 0:
            self.logger.info(f"No other subs present in BSN-{bsn} integration interval")
        else:
            self.logger.info(f"Other subs-{other_subs} present in BSN-{bsn} integration interval")

        # Report WG sub present
        if wg_sub_present is False:
            self.logger.info(f"No WG sub present in BSN-{bsn} integration interval")
        else:
            # Verify xst_acm only for wg_sub
            self.logger.info(f"WG sub-{wg_sub} present in BSN-{bsn} integration interval")
            # received xst_acm
            xst_angles = np.angle(xst_acm, deg=True)
            xst_powers = np.abs(xst_acm)
            xst_powers_max = np.max(xst_powers)
            xst_powers_min = np.min(xst_powers)
            self.logger.info(f"Measured XST powers in range {xst_powers_min} to {xst_powers_max}")
            self.logger.info(f"Expected XST powers in range {exp_powers_min} to {exp_powers_max}")

            # verify expected angles
            diff_angles = exp_angles - xst_angles
            # get diff_angles in -+ 180 degrees range
            diff_angles = np.where(diff_angles < 180, diff_angles + 360, diff_angles)
            diff_angles = np.where(diff_angles > 180, diff_angles - 360, diff_angles)

            # with np.printoptions(threshold=np.inf):
            #    print(diff_angles)  # print large array without truncation dots ...

            diff_angles_min = np.min(diff_angles)
            diff_angles_max = np.max(diff_angles)
            phase_step = 360 / s_ant  # WG phase step in mode xstwide
            angle_margin = 1.0 * phase_step  # factor found by trial, depends on limited accuracy of WG phase
            self.logger.info(f"XST angle margin is {angle_margin} degrees")
            self.logger.info(
                f"WG sub-{wg_sub} XST angles are within {diff_angles_min} and {diff_angles_max} degrees of expected"
            )

            # ofs_a = 185
            # ofs_b = 0
            # nof = 4
            # rng_a = range(ofs_a, ofs_a + nof)
            # rng_b = range(ofs_b, ofs_b + nof)
            # for a in rng_a:
            #    print(exp_angles[a][rng_b])
            # print()
            # for a in rng_a:
            #    print(xst_angles[a][rng_b])
            # print()

            abs_diff_angles = np.abs(diff_angles)
            abs_diff_angles_max = np.max(abs_diff_angles)

            if abs_diff_angles_max > angle_margin > 0:
                self.logger.error(f"WG sub-{wg_sub} XST angle(s) differ too much from expected ({abs_diff_angles_max} > {angle_margin})")
                success = False

            # verify expected powers
            abs_diff_powers = np.abs(exp_powers - xst_powers)
            abs_diff_powers_max = np.max(abs_diff_powers)
            relative_diff_powers = abs_diff_powers / exp_powers
            relative_diff_powers_max = np.max(relative_diff_powers)
            # check offset (absolute) margin and relative margin. The offset margin is
            power_margin_offset = max_sub_sst * 0.01  # factor found by trial
            power_margin_relative = 0.01  # factor found by trial
            self.logger.info(f"XST power margin offset is {power_margin_offset}")
            self.logger.info(f"XST power margin relative is {power_margin_relative}")
            self.logger.info(f"WG sub-{wg_sub} XST powers are within offset {abs_diff_powers_max} of expected")
            self.logger.info(f"WG sub-{wg_sub} XST powers are within factor {relative_diff_powers_max} of expected")

            if abs_diff_powers_max > power_margin_offset:
                self.logger.error(f"WG sub-{wg_sub} XST power(s) differ too much (offset {abs_diff_powers_max} > {power_margin_offset})")
                success = False

            if relative_diff_powers_max > power_margin_relative:
                self.logger.error(f"WG sub-{wg_sub} XST power(s) differ too much (factor {relative_diff_powers_max} > {power_margin_relative})")
                success = False
        print_result("test_data", success)


# functions used, if this script is called direct
def start_wg(client, ampl=0.25, sub=102, mode="xstwide", ampl_min=0):
    wg = WaveformGenerator(client)
    wg.disable()
    time.sleep(0.1)
    wg_apf = wg.set_xst_mode(ampl, 360, sub * C_SDP.f_sub, mode, ampl_min)
    wg.enable()
    return wg_apf  # dict of a_mpl, p_hase, and f_req lists


def stop_wg(client):
    wg = WaveformGenerator(client)
    wg.disable()


def run_setup_lane(_setup_lane, _args, xst_stat_stream):
    if _setup_lane:
        xst_stat_stream.setup_lane()


def run_setup_xst(_setup_xst, _args, xst_stat_stream):
    if _setup_xst:
        xst_stat_stream.setup_sub_weights()
        xst_stat_stream.setup_stream(_args.sb_select, _args.interval, _args.nof_crosslets)


def run_xst_proc(_xst_proc, _args, client, xst_stat_stream):
    if _xst_proc:
        if _xst_proc == "ON":
            xst_stat_stream.xst_processing_restart()
        elif _xst_proc == "OFF":
            xst_stat_stream.xst_processing_enable = [False] * client.n_nodes


def run_stream(_stream, _args, xst_stat_stream):
    if _stream:
        if _stream == "ON":
            xst_stat_stream.stream_control.stream_on()
        elif _stream == "OFF":
            xst_stat_stream.stream_control.stream_off()


def run_monitor(_monitor, _args, xst_stat_stream):
    if _monitor:
        xst_stat_stream.bsn_monitor(_args.mtime)


def run_headers(_headers, _args, xst_stat_stream):
    if _headers:
        xst_stat_stream.start_io()
        stop_time = time.time() + _args.mtime
        while True:
            try:
                xst_stat_stream.print_xst_header()
                if time.time() > stop_time:
                    break

            except BaseException as err:
                exit_state = handle_exception(err)
                if exit_state == 1:
                    break
        xst_stat_stream.stop_io()
        xst_stat_stream.print_missing_indices()


def run_plots(_plots, _args, client, xst_stat_stream):
    if _plots:
        # Use default settings
        xst_stat_stream.setup_sub_weights()
        xst_stat_stream.setup_stream(_args.sb_select, _args.interval, _args.nof_crosslets)  # = --setup_xst
        xst_stat_stream.xst_processing_restart()  # = --xst_proc ON
        if not _args.no_wg:
            # Default use max WG ampl without crosslet overflow
            start_wg(client, ampl=0.25, sub=102, mode="xstwide")

            # Try crosslet overflow
            # start_wg(client, ampl=1.0, sub=102, mode="xstwide")

            # Use mode xst to show individual correlator cells
            # start_wg(client, ampl=0.25, sub=102, mode="xst")

        xst_stat_stream.start_io()  # = --stream ON

        # plot XST once, rerun command to plot again
        try:
            xst_stat_stream.plot()
        except BaseException as err:
            handle_exception(err)

        xst_stat_stream.stop_io()  # = --stream OFF
        if not _args.no_wg:
            stop_wg(client)

        _ = input("== press Enter to stop plot ==")


def run_test_cp(_test_cp, _args, xst_stat_stream):
    if _test_cp:
        xst_stat_stream.test_cp()


def run_test_mp(_test_mp, _args, xst_stat_stream):
    if _test_mp:
        xst_stat_stream.start_io()
        time.sleep(4)  # wait for some integration intervals of 1 s
        xst_stat_stream.test_mp()
        xst_stat_stream.stop_io()


def run_test_header(_test_header, _args, xst_stat_stream):
    if _test_header:
        xst_stat_stream.start_stream_reader()
        xst_stat_stream.test_header(_args.mtime)
        xst_stat_stream.stop_stream_reader()
        xst_stat_stream.print_missing_indices()


def run_test_data(_test_data, _args, client, xst_stat_stream):
    if _test_data:
        # Comment line to try different WG amplitudes, default use 0.25
        # for maximum crosslet level without overflow
        wg_ampl = 1.0
        wg_ampl = 0.1
        wg_ampl = 0.0125
        wg_ampl = 0.025
        wg_apf = start_wg(client, ampl=wg_ampl, sub=102, mode="xstwide", ampl_min=0.01)
        time.sleep(10.0)

        # Reduce sub_gain to avoid crosslet overflow
        sub_gain = 1.0
        if wg_ampl > 0.25:
            sub_gain = 0.25 / wg_ampl
        xst_stat_stream.setup_sub_weights(sub_gain)
        xst_stat_stream.setup_stream(_args.sb_select, _args.interval, _args.nof_crosslets)  # = --setup_xst
        xst_stat_stream.xst_processing_restart()  # = --xst_proc ON
        xst_stat_stream.start_stream_reader()  # = --stream ON
        xst_stat_stream.test_data(_args.interval, _args.nof_crosslets, wg_apf, sub_gain)
        xst_stat_stream.stop_stream_reader()  # = --stream OFF
        stop_wg(client)


def print_result(info, result):
    if result is True:
        logger.info(f"{info}: PASSED")
    else:
        logger.info(f"{info}: FAILED")


def main():
    exit_state = 0

    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    xst_stat_stream = XstStatisticStream(client, stream_mac, stream_ip, args.udpport)

    # The if statements order determines the order in which the command line options are handled
    try:
        # Ensure correct destination mac, ip, udp port
        xst_stat_stream.setup_offload_hdr()

        run_setup_lane(args.setup_lane, args, xst_stat_stream)

        run_setup_xst(args.setup_xst, args, xst_stat_stream)

        # If necessary start SDP processing
        start_processing(client)

        run_xst_proc(args.xst_proc, args, client, xst_stat_stream)

        run_stream(args.stream, args, xst_stat_stream)

        run_monitor(args.bsn_monitor, args, xst_stat_stream)

        run_headers(args.headers, args, xst_stat_stream)

        run_plots(args.plots, args, client, xst_stat_stream)

        run_test_cp(args.test_cp, args, xst_stat_stream)

        run_test_mp(args.test_mp, args, xst_stat_stream)

        run_test_header(args.test_header, args, xst_stat_stream)

        run_test_data(args.test_data, args, client, xst_stat_stream)

    except BaseException as err:
        exit_state = handle_exception(err)

    stop_all_streams(client)
    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Defaults
    # . use first crosslet 102 (= 19.921875 MHz) because that is often used for WG
    # . use different crosslets to avoid duplicate packets
    # . use crosslet 109 instead of crosslet 106, because with WG at 102 the 103, 104, 105,
    #   107, 108 and 109 have stats_powers_max < power_margin_no_wg, but 106 has about
    #   factor 1e4 more spurious power. It also occurs with crosslet 110. This is probably
    #   an rounding artefact. Still the power of crosslet 106, 110 is about factor 1e6
    #   lower than max_sub_sst, so sufficiently small.
    default_sb_select = [0, 102, 103, 104, 105, 109, 107, 108]

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(
            textwrap.dedent(
                """\
            opcua client command line argument parser:
            0) Start XST ring processing after reboot:
               sdp_firmware.py --host 10.99.0.250 --port 4842 --image USER --reboot
               sdp_rw.py --host 10.99.0.250 --port 4842 -r firmware_version

            1) Setup ring (do this once)
               common_ring.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup
               # manually check that:
               sdp_rw.py --host 10.99.0.250 --port 4842 -r sdp_config_first_fpga_nr
               sdp_rw.py --host 10.99.0.250 --port 4842 -r sdp_config_nof_fpgas
               # should match:
               sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_node_offset
               sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_nof_nodes
               sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_use_cable_to_next_rn
               sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_use_cable_to_previous_rn

               stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup-lane

            2) --setup-xst with (use defaults or do this once):
                   --sb-select takes affect after --xst-proc
                   --interval takes affect after --xst-proc
                   --nof_crosslets takes affect in next integration interval
               # manually check:
               sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_subband_select
               sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_integration_interval
               sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_nof_crosslets

            3) --xst-proc to (re)start (= ON) or stop (= OFF) the XST processing (do this once or
               when setup-xst has changes)
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --xst-proc ON
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --xst-proc OFF
              sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_processing_enable

              # check crosslets on ring lane
              sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_ring_rx_latency

            4) --stream to enable/disable the XST offload
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream ON
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream OFF
              sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_enable

            5) --bsn-monitor of XST offload
              # use --stream to enable offload or in other terminal use sdp_rw.py
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream ON --bsn-monitor --mtime 3 -vv
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --bsn-monitor --mtime 3 -vv
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream OFF --bsn-monitor --mtime 100 -vv
              sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_offload_enable
              sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_offload_enable [True]*16

            6) --test-cp test some CP values

            7) --test-mp test some MP values

            8) --headers --> prints headers during mtime
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --headers --mtime 3

            9) --test-header --> valid packets = n_nodes * X_sq * nof_crosslets * mtime
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-header --mtime 3

            10) --test-data --> verifies expected XST for WG data
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-data -vv
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-data --nof_crosslets 3 -vv
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-data --nof_crosslets 3 --interval 0.1 -vv
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-data --nof_crosslets 3 --interval 10 -vv

            11) --plots --> plots XST using WG with range of decreasing ampl and 360 degrees phases
              stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --plots
              # use return in terminal to stop plots\n"""
            )
        ),
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "--host", type=str, default="localhost", help="host to connect to"
    )
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument(
        "-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG"
    )

    parser.add_argument("--ip", type=str, help="ip of destination machine")
    parser.add_argument("--mac", type=str, help="mac of destination machine")
    parser.add_argument(
        "--udpport",
        type=int,
        default=5003,
        help="udp port of destination machine to use for stream",
    )
    parser.add_argument("--setup-lane", action="store_true", help="setup lane")
    parser.add_argument("--setup-xst", action="store_true", help="setup XST")
    parser.add_argument(
        "--interval", type=float, default=1.0, help="set integration interval for XST"
    )
    parser.add_argument(
        "--sb-select",
        nargs=8,
        type=int,
        default=default_sb_select,
        help="select step and 7 subbands for XST, default %s" % default_sb_select,
    )
    parser.add_argument(
        "--nof_crosslets",
        type=int,
        default=1,
        help="set number of crosslets for XST offload",
    )
    parser.add_argument(
        "--xst-proc",
        type=str,
        choices=["OFF", "ON"],
        help="turn off/on XST processing to apply new settings",
    )
    parser.add_argument(
        "--stream",
        type=str,
        choices=["OFF", "ON"],
        help="turn off/on XST offload stream",
    )
    parser.add_argument(
        "--bsn-monitor", action="store_true", help="monitor bsn XST stream"
    )
    parser.add_argument(
        "--mtime",
        type=int,
        default=1,
        help="set time to monitor bsn XST stream, --mtime in seconds, default is 1 time",
    )
    parser.add_argument(
        "--headers", action="store_true", help="print SST stream headers"
    )
    parser.add_argument("--plots", action="store_true", help="print SST plots")
    parser.add_argument(
        "--no_wg",
        action="store_true",
        help="default use WG signal input for --plots, else use RCU2 inputs",
    )
    parser.add_argument("--test-cp", action="store_true", help="check XST CP")
    parser.add_argument("--test-mp", action="store_true", help="check XST MP")
    parser.add_argument("--test-header", action="store_true", help="check XST header")
    parser.add_argument("--test-data", action="store_true", help="check XST data")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    verbosity_nr = 0 if args.v is None else args.v
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))
    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    stream_mac, stream_ip = parse_stream_destination(args.mac, args.ip, "1G")
    if not stream_ip or not stream_mac:
        logger.error("need ip and mac for this test")
        sys.exit(1)

    sys.exit(main())
