#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . test write/read bf_weigts speed
# Description:
# . run ./sdp_speedtest.py -h for help
# ##########################################################################

import statistics
import time
import argparse
import logging
import sys

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class SpeedTest:
    S_PN = 12
    N_POL = 2
    A_PN = 6
    N_BF_SUB = 488

    def __init__(self, client):
        self.client = client
        self.n_nodes = self.client.read("sdp_config_nof_fpgas")
        self.n_beamsets = self.client.read("sdp_config_nof_beamsets")

    def get_pc_time(self):
        pc_time = time.time()
        print(f"pc time       = {pc_time} sec")
        return pc_time

    def get_sdptr_time(self):
        sdptr_tod = self.client.read("tod")
        sdptr_time = sdptr_tod[0] + (sdptr_tod[1] / 1e9)
        print(f"sdptr time    = {sdptr_time} sec")
        return sdptr_time

    def get_tod_pps_delta(self):
        sdptr_tod_pps_delta = self.client.read("tod_pps_delta")
        print(f"tod_pps_delta = {sdptr_tod_pps_delta} sec")
        return sdptr_tod_pps_delta

    def test1(self, nodelist, ttime):
        data = list(range(self.N_BF_SUB)) * self.n_beamsets * self.N_POL * self.A_PN * self.n_nodes
        times = []
        print(f"sending bf_weights_xx_yy to fpga {nodelist}")
        sdptr_time = self.get_sdptr_time()
        tod_pps_delta = self.get_tod_pps_delta()
        run_time = 1.0 + sdptr_time - tod_pps_delta + 0.4
        print(f"first runtime = {run_time} sec")
        for i in range(ttime):
            while time.time() < run_time:
                time.sleep(0.000001)
            run_time += 1.0
            t0 = time.time()
            self.client.write("bf_weights_xx_yy", data)
            t1 = time.time()
            times.append(t1 - t0)
            print(f"run {i} : took {t1 - t0} seconds")

        print(f"mean time = {statistics.mean(times)} sec")
        print(f"max time  = {max(times)} sec")
        print(f"min time  = {min(times)} sec")

    def test2(self, nodelist, ttime):
        data = list(range(self.N_BF_SUB)) * self.n_beamsets * self.N_POL * self.A_PN * self.n_nodes
        times = []
        print(f"sending bf_weights_xx_yy to fpga {nodelist} and reading back")
        sdptr_time = self.get_sdptr_time()
        tod_pps_delta = self.get_tod_pps_delta()
        run_time = 1.0 + sdptr_time - tod_pps_delta + 0.5
        print(f"first runtime = {run_time} sec")
        for i in range(ttime):
            while time.time() < run_time:
                time.sleep(0.000001)
            run_time += 1.0
            t0 = time.time()
            self.client.write("bf_weights_xx_yy", data)
            self.client.read("bf_weights_xx_yy")
            t1 = time.time()
            times.append(t1 - t0)
            print(f"run {i} : took {t1 - t0} seconds")

        print(f"mean time = {statistics.mean(times)} sec")
        print(f"max time  = {max(times)} sec")
        print(f"min time  = {min(times)} sec")

    def test3(self, nodelist, ttime):
        times = []
        print(f"reading bf_weights_xx_yy from fpga {nodelist}")
        sdptr_time = self.get_sdptr_time()
        tod_pps_delta = self.get_tod_pps_delta()
        run_time = 1.0 + sdptr_time - tod_pps_delta + 0.4
        print(f"first runtime = {run_time} sec")
        for i in range(ttime):
            while time.time() < run_time:
                time.sleep(0.000001)
            run_time += 1.0
            t0 = time.time()
            self.client.read("bf_weights_xx_yy")
            t1 = time.time()
            times.append(t1 - t0)
            print(f"run {i} : took {t1 - t0} seconds")

        print(f"mean time = {statistics.mean(times)} sec")
        print(f"max time  = {max(times)} sec")
        print(f"min time  = {min(times)} sec")

    def test4(self, nodelist, ttime):
        test_points = [
            "time_since_last_pps",
            "pps_capture_cnt",
            "signal_input_bsn",
            "signal_input_nof_blocks",
            "signal_input_nof_samples",
            "signal_input_mean",
            "signal_input_rms",
            "xst_input_bsn_at_sync",
            "xst_output_sync_bsn",
            "jesd204b_csr_rbd_count",
            "jesd204b_csr_dev_syncn",
            "jesd204b_rx_err0",
            "jesd204b_rx_err1",
            "sst_offload_nof_packets",
            "sst_offload_nof_valid",
            "sst_offload_bsn",
            "bst_offload_nof_packets",
            "bst_offload_nof_valid",
            "bst_offload_bsn",
            "xst_offload_nof_packets",
            "xst_offload_nof_valid",
            "xst_offload_bsn",
            "beamlet_output_nof_packets",
            "beamlet_output_nof_valid",
            "beamlet_output_bsn",
            "xst_ring_rx_bsn",
            "xst_ring_rx_nof_packets",
            "xst_ring_rx_nof_valid",
            "xst_ring_rx_latency",
            "xst_rx_align_bsn",
            "xst_rx_align_nof_packets",
            "xst_rx_align_nof_valid",
            "xst_rx_align_latency",
            "xst_aligned_bsn",
            "xst_aligned_nof_packets",
            "xst_aligned_nof_valid",
            "xst_aligned_latency",
            "xst_ring_tx_bsn",
            "xst_ring_tx_nof_packets",
            "xst_ring_tx_nof_valid",
            "xst_ring_tx_latency",
            "xst_rx_align_nof_replaced_packets",
            "bf_ring_nof_transport_hops",
            "bf_ring_rx_clear_total_counts",
            "bf_rx_align_stream_enable",
            "bf_ring_rx_total_nof_packets_received",
            "bf_ring_rx_total_nof_packets_discarded",
            "bf_ring_rx_total_nof_sync_received",
            "bf_ring_rx_total_nof_sync_discarded",
            "bf_ring_rx_bsn",
            "bf_ring_rx_nof_packets",
            "bf_ring_rx_nof_valid",
            "bf_ring_rx_latency",
            "bf_rx_align_bsn",
            "bf_rx_align_nof_packets",
            "bf_rx_align_nof_valid",
            "bf_rx_align_latency",
            "bf_rx_align_nof_replaced_packets",
            "bf_aligned_bsn",
            "bf_aligned_nof_packets",
            "bf_aligned_nof_valid",
            "bf_aligned_latency",
            "bf_ring_tx_bsn",
            "bf_ring_tx_nof_packets",
            "bf_ring_tx_nof_valid",
            "bf_ring_tx_latency",
        ]
        n_test_points = len(test_points)
        print(f"testing {n_test_points} points")

        run_time = 1.0 + time.time()
        sleep_cnt = 0
        n_points_to_test = n_test_points
        for i in range(ttime):
            sleep_cnt = 0
            while time.time() < run_time:
                time.sleep(0.000001)
                sleep_cnt += 1
            run_time += 1.0
            if sleep_cnt > 500:
                n_points_to_test += 5  # 5 to go faster to the max, could also be 1
            if sleep_cnt < 100:
                n_points_to_test -= 1

            tp = 0
            for j in range(n_points_to_test):
                self.client.read(test_points[tp])
                tp += 1
                if tp == n_test_points:
                    tp = 0
            print(f"sleep_cnt={sleep_cnt}, points={n_points_to_test}")

    def test5(self, nodelist, ttime):
        test_points = [
            "time_since_last_pps",
            "pps_capture_cnt",
            "signal_input_bsn",
            "signal_input_nof_blocks",
            "signal_input_nof_samples",
            "signal_input_mean",
            "signal_input_rms",
            "xst_input_bsn_at_sync",
            "xst_output_sync_bsn",
            "jesd204b_csr_rbd_count",
            "jesd204b_csr_dev_syncn",
            "jesd204b_rx_err0",
            "jesd204b_rx_err1",
            "sst_offload_nof_packets",
            "sst_offload_bsn",
            "bst_offload_nof_packets",
            "bst_offload_bsn",
            "xst_offload_nof_packets",
            "xst_offload_bsn",
            "beamlet_output_nof_packets",
            "beamlet_output_bsn",
            "xst_ring_rx_bsn",
            "xst_ring_rx_nof_packets",
            "xst_ring_rx_latency",
            "xst_rx_align_bsn",
            "xst_rx_align_nof_packets",
            "xst_rx_align_latency",
            "xst_aligned_bsn",
            "xst_aligned_nof_packets",
            "xst_aligned_latency",
            "xst_ring_tx_bsn",
            "xst_ring_tx_nof_packets",
            "xst_ring_tx_latency",
            "xst_rx_align_nof_replaced_packets",
            "bf_ring_nof_transport_hops",
            "bf_ring_rx_clear_total_counts",
            "bf_rx_align_stream_enable",
            "bf_ring_rx_total_nof_packets_received",
            "bf_ring_rx_total_nof_packets_discarded",
            "bf_ring_rx_total_nof_sync_received",
            "bf_ring_rx_total_nof_sync_discarded",
            "bf_ring_rx_bsn",
            "bf_ring_rx_nof_packets",
            "bf_ring_rx_latency",
            "bf_rx_align_bsn",
            "bf_rx_align_nof_packets",
            "bf_rx_align_latency",
            "bf_rx_align_nof_replaced_packets",
            "bf_aligned_bsn",
            "bf_aligned_nof_packets",
            "bf_aligned_latency",
            "bf_ring_tx_bsn",
            "bf_ring_tx_nof_packets",
            "bf_ring_tx_latency",
            "bf_weights_xy_yx",
            "bf_weights_xx_yy",
        ]
        n_test_points = len(test_points)
        print(f"testing {n_test_points} points")

        for i in range(ttime):
            start = time.time()
            for tp in test_points:
                self.client.read(tp)
            stop = time.time()
            print(f"reading {n_test_points} point in {stop - start} seconds")
            while time.time() < (start + 1.0):
                time.sleep(0.000001)

    def test6(self, nodelist, ttime):
        test_points = ["bf_weights_xy_yx", "bf_weights_xx_yy"]
        n_test_points = len(test_points)

        run_time = 1.0 + time.time()
        sleep_cnt = 0
        n_points_to_test = 25
        for i in range(ttime):
            sleep_cnt = 0
            while time.time() < run_time:
                time.sleep(0.000001)
                sleep_cnt += 1
            run_time += 1.0
            if sleep_cnt > 200:
                n_points_to_test += 1
            if sleep_cnt < 100:
                n_points_to_test -= 1

            tp = 0
            for j in range(n_points_to_test):
                self.client.read(test_points[tp])
                tp += 1
                if tp == n_test_points:
                    tp = 0
            print(f"sleep_cnt={sleep_cnt}, points={n_points_to_test}")


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    speed_test = SpeedTest(client)

    while True:
        try:
            if args.test == 1:
                speed_test.test1(node_list, args.ttime)
            elif args.test == 2:
                speed_test.test2(node_list, args.ttime)
            elif args.test == 3:
                speed_test.test3(node_list, args.ttime)
            elif args.test == 4:
                speed_test.test4(node_list, args.ttime)
            elif args.test == 5:
                speed_test.test5(node_list, args.ttime)
            elif args.test == 6:
                speed_test.test6(node_list, args.ttime)
            break
        except BaseException as err:
            exit_state = handle_exception(err)
            if exit_state == 1:
                break
    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    p = argparse.ArgumentParser(description="opcua client command line argument parser")
    p.add_argument("--host", dest="host", type=str, default="dop36", help="host to connect to")
    p.add_argument("--port", dest="port", type=int, default=4840, help="port to use")
    p.add_argument("-n", "--nodes", dest="nodes", type=str, help="nodes to use")
    p.add_argument(
        "-v", action="count", default=0, help="verbosity 'WARNING', 'INFO', 'DEBUG' -v, -vv, -vvv"
    )

    p.add_argument(
        "--test",
        type=int,
        default=1,
        help="""
           test speed:
           1) bf_weigts write
           2) bf_weigts write/read
           3) bf_weigts read
           4) points per second 1 sec points
           5) time needed for all 1 sec points.
           6) points per second (bf_weights)""",
    )
    p.add_argument(
        "--ttime", type=int, default=10, help="test time in seconds (every second 1 test)"
    )

    args = p.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
