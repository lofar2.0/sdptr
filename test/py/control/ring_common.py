#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . read or write common ring points
# .
# Description:
# . run ./ring_common.py -h for help
#   ./ring_common.py --host 10.99.0.252 --node_offset         # read node_offset
#   ./ring_common.py --host 10.99.0.252 --node_offset [1]*16  # write node_offset
#   ./ring_common.py --host 10.99.0.252 --setup  # write client default ring nodes and cables
#
# References:
# [1] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Ring
# ##########################################################################


import sys
sys.path.insert(0, "..")

import logging

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list
from base.constants import C_SDP


class RingCommon(object):
    def __init__(self, client):
        self.logger = logging.getLogger("RingCommon")
        self.client = client
        self.n_nodes = self.client.n_nodes
        self.fpga_mask = self.client.get_mask()
        self.actives_fpgas = [not i for i in self.client.read("fpga_communication_error")]

    def make_next_rn(self, O_rn, N_rn):
        Orn = O_rn % self.n_nodes
        Nrn = N_rn
        next_rn = [False] * self.n_nodes
        for i in range(self.n_nodes):
            if (i + 1) % C_SDP.fpgas_per_board == 0:
                if (i + 1) > Orn and (i + 1) <= (Orn + Nrn):
                    next_rn[i] = True
            if (i + 1) == (Orn + Nrn):
                next_rn[i] = True
        return next_rn

    def make_prev_rn(self, O_rn, N_rn):
        Orn = O_rn % self.n_nodes
        Nrn = N_rn
        prev_rn = [False] * self.n_nodes
        for i in range(self.n_nodes):
            if i % C_SDP.fpgas_per_board == 0:
                if i >= Orn and i < (Orn + Nrn):
                    prev_rn[i] = True
            if i == Orn:
                prev_rn[i] = True
        return prev_rn

    @property
    def node_offset(self):
        return self.client.read("ring_node_offset")

    @node_offset.setter
    def node_offset(self, data):
        return self.client.write("ring_node_offset", data)

    @property
    def nof_nodes(self):
        return self.client.read("ring_nof_nodes")

    @nof_nodes.setter
    def nof_nodes(self, data):
        return self.client.write("ring_nof_nodes", data)

    @property
    def use_cable_to_next_rn(self):
        return self.client.read("ring_use_cable_to_next_rn")

    @use_cable_to_next_rn.setter
    def use_cable_to_next_rn(self, data):
        return self.client.write("ring_use_cable_to_next_rn", data)

    @property
    def use_cable_to_previous_rn(self):
        return self.client.read("ring_use_cable_to_previous_rn")

    @use_cable_to_previous_rn.setter
    def use_cable_to_previous_rn(self, data):
        return self.client.write("ring_use_cable_to_previous_rn", data)

    def setup(self, O_rn, N_rn):
        """Setup ring nodes and cables, that is common for all lanes, see [1]"""
        O_rn_list = [O_rn] * self.n_nodes
        N_rn_list = [N_rn] * self.n_nodes
        next_rn_list = self.make_next_rn(O_rn, N_rn)
        prev_rn_list = self.make_prev_rn(O_rn, N_rn)

        self.node_offset = O_rn_list
        self.nof_nodes = N_rn_list
        self.use_cable_to_next_rn = next_rn_list
        self.use_cable_to_previous_rn = prev_rn_list
        self.logger.info("ring_common.setup: PASSED")

    # next functions for testing
    def compare_lists(self, list1, list2):
        same = True
        for nr, fpga_masked in enumerate(self.fpga_mask):
            if fpga_masked:
                if self.actives_fpgas[nr] is True:
                    if list1[nr] != list2[nr]:
                        same &= False
        return same

    def test(self):
        succes = True
        node_offset = self.node_offset
        new_node_offset = [i + 1 for i in node_offset]
        self.node_offset = new_node_offset
        if self.compare_lists(self.node_offset, new_node_offset) is False:
            self.logger.error("test failed writing node_offset")
            succes &= False
        self.node_offset(node_offset)

        nof_nodes = self.nof_nodes
        new_nof_nodes = [max(i - 1, 0) for i in nof_nodes]
        self.nof_nodes = new_nof_nodes
        if self.compare_lists(self.nof_nodes, new_nof_nodes) is False:
            self.logger.error("test failed writing nof_nodes")
            succes &= False
        self.nof_nodes = nof_nodes

        next_rn = self.use_cable_to_next_rn
        new_next_rn = [not i for i in next_rn]
        self.use_cable_to_next_rn = new_next_rn
        if self.compare_lists(self.use_cable_to_next_rn, new_next_rn) is False:
            self.logger.error("test failed writing use_cable_to_next_rn")
            succes &= False
        self.use_cable_to_next_rn = next_rn

        previous_rn = self.use_cable_to_previous_rn
        new_previous_rn = [not i for i in previous_rn]
        self.use_cable_to_previous_rn = new_previous_rn
        if self.compare_lists(self.use_cable_to_previous_rn, new_previous_rn) is False:
            self.logger.error("test failed writing use_cable_to_previous_rn")
            succes &= False
        self.use_cable_to_previous_rn = previous_rn
        return succes


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    ring_common = RingCommon(client)

    try:
        if args.node_offset is not None:
            if args.node_offset == []:
                print(ring_common.node_offset)
            else:
                ring_common.node_offset = eval(args.node_offset[0])

        if args.nof_nodes is not None:
            if args.nof_nodes == []:
                print(ring_common.nof_nodes)
            else:
                ring_common.nof_nodes = eval(args.nof_nodes[0])

        if args.next_rn is not None:
            if args.next_rn == []:
                print(ring_common.use_cable_to_next_rn)
            else:
                ring_common.use_cable_to_next_rn = eval(args.next_rn[0])

        if args.previous_rn is not None:
            if args.previous_rn == []:
                print(ring_common.use_cable_to_previous_rn)
            else:
                ring_common.use_cable_to_previous_rn = eval(args.previous_rn[0])

        if args.test:
            test_result = "PASSED" if ring_common.test() is True else "FAILED"
            print(f"common ring test: {test_result}")

        if args.setup:
            ring_common.setup(client.gn_first_fpga, client.n_nodes)

    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument("--node-offset", nargs="*", type=str, help="set/get ring_node_offset")
    parser.add_argument("--nof-nodes", nargs="*", type=str, help="set/get ring_nof_nodes")
    parser.add_argument("--next-rn", nargs="*", type=str, help="set/get ring_use_cable_to_next_rn")
    parser.add_argument(
        "--previous-rn", nargs="*", type=str, help="set/get ring_use_cable_to_previous_rn"
    )

    parser.add_argument("--test", action="store_true", help="test writing all common ring points")
    parser.add_argument("--setup", action="store_true", help="setup ring nodes and cables")
    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
