#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Plot data from signal_input_data_buffer (RAM_DIAG_DATA_BUFFER_BSN)
# Description:
# . run ./plot_input.py -h for help
# ##########################################################################


import sys

sys.path.insert(0, "..")

import logging
import argparse

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class SignalInputData:
    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger("SignalInputData")
        # get number of nodes from server
        self.n_nodes = self.client.read("sdp_config_nof_fpgas")
        # get number of nodes from server
        self.first_node_nr = self.client.read("sdp_config_first_fpga_nr")
        self.active_nodes = list(range(self.first_node_nr, self.first_node_nr + self.n_nodes))
        self.data = None

    def read(self):
        self.data = self.client.read("signal_input_data_buffer")

    def plot(self, nodes, inputs):
        import numpy as np

        # import matplotlib
        # matplotlib.use('Agg')
        import matplotlib.pyplot as plt

        nodes = self.active_nodes if nodes is None else nodes

        if self.data is None:
            self.read()

        np_data = np.array(self.data)
        S_PN = 12
        N_SIGNALS = 1024
        # for i in range(N_NODES):
        for i in list(nodes):
            for j in list(inputs):
                s1 = (i * S_PN * N_SIGNALS) + (j * N_SIGNALS)
                s2 = s1 + N_SIGNALS
                _data = np_data[s1:s2]
                plt.plot(range(N_SIGNALS), _data)
                plt.tight_layout()
                plt.show()
        return True


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    # read back nodes set.
    logger.info("fpga mask={}".format(client.get_mask()))

    signal_input_data = SignalInputData(client)

    try:
        signal_input_data.read()
        signal_input_data.plot(node_list, input_list)
    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument("-i", "--inputs", type=str, help="inputs to use")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None
    input_list = arg_str_to_list(args.inputs) if args.inputs else range(12)

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
