#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Control, monitor and setup BST statistics stream
# Description:
# . run ./stat_stream_bst.py -h for help
# References:
# [1] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Beamformer
# [2] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Ring
# [3] https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+SC+-+SDP+OPC-UA+interface
# ##########################################################################

import sys
import logging
import time
import numpy as np

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list

from base.constants import C_SDP
from base.sdp_functions import parse_stream_destination, start_processing, stop_all_streams
from base.stream_reader import StreamReader, print_header
from base.stream_control import StreamControl


class BstStatisticStream(object):
    def __init__(self, client, dest_mac, dest_ip, dest_udp_port):
        self.logger = logging.getLogger("BstStatisticsStream")
        self.client = client
        self.n_nodes = self.client.n_nodes  # get number of nodes from server
        self.n_rn = self.client.n_nodes  # all nodes used in ring
        self.o_rn = self.client.gn_first_fpga
        self.last_rn = self.n_rn - 1  # last node in ring relative to o_rn
        self.n_beamsets = self.client.read("sdp_config_nof_beamsets")
        self.stream_enable = self.client.read("bst_offload_enable")
        self.dest_mac = dest_mac
        self.dest_ip = dest_ip
        self.dest_udp_port = dest_udp_port
        self.stream_reader = None
        self.stream_control = None
        self.next_packet_nr = 0
        self.stdout = self.logger.getEffectiveLevel() > logging.INFO
        stop_all_streams(self.client)

    def print_log(self, fstr):
        print(fstr) if self.stdout else self.logger.info(fstr)

    # == OPC-UA CP for BF ==
    @property
    def bst_offload_hdr_eth_destination_mac(self):
        return self.client.read("bst_offload_hdr_eth_destination_mac")

    @bst_offload_hdr_eth_destination_mac.setter
    def bst_offload_hdr_eth_destination_mac(self, mac):
        return self.client.write("bst_offload_hdr_eth_destination_mac", mac)

    @property
    def bst_offload_hdr_ip_destination_address(self):
        return self.client.read("bst_offload_hdr_ip_destination_address")

    @bst_offload_hdr_ip_destination_address.setter
    def bst_offload_hdr_ip_destination_address(self, ip):
        return self.client.write("bst_offload_hdr_ip_destination_address", ip)

    @property
    def bst_offload_hdr_udp_destination_port(self):
        return self.client.read("bst_offload_hdr_udp_destination_port")

    @bst_offload_hdr_udp_destination_port.setter
    def bst_offload_hdr_udp_destination_port(self, port):
        return self.client.write("bst_offload_hdr_udp_destination_port", port)

    #  == OPC-UA MP for BF ==
    @property
    def bst_offload_nof_packets(self):
        return self.client.read("bst_offload_nof_packets")

    @property
    def bst_offload_nof_valid(self):
        return self.client.read("bst_offload_nof_valid")

    # == OPC-UA CP for Ring in BF ==
    @property
    def bf_ring_nof_transport_hops(self):
        return self.client.read("bf_ring_nof_transport_hops")

    @bf_ring_nof_transport_hops.setter
    def bf_ring_nof_transport_hops(self, nof_hops):
        return self.client.write("bf_ring_nof_transport_hops", nof_hops)

    # == OPC-UA MP for Ring in BF ==
    @property
    def bf_ring_rx_nof_packets(self):
        return self.client.read("bf_ring_rx_nof_packets")

    @property
    def bf_ring_rx_nof_valid(self):
        return self.client.read("bf_ring_rx_nof_valid")

    @property
    def bf_ring_rx_latency(self):
        return self.client.read("bf_ring_rx_latency")

    @property
    def bf_rx_align_bsn(self):
        return self.client.read("bf_rx_align_bsn")

    @property
    def bf_rx_align_nof_packets(self):
        return self.client.read("bf_rx_align_nof_packets")

    @property
    def bf_rx_align_nof_valid(self):
        return self.client.read("bf_rx_align_nof_valid")

    @property
    def bf_rx_align_latency(self):
        return self.client.read("bf_rx_align_latency")

    @property
    def bf_rx_align_nof_replaced_packets(self):
        return self.client.read("bf_rx_align_nof_replaced_packets")

    @property
    def bf_aligned_bsn(self):
        return self.client.read("bf_aligned_bsn")

    @property
    def bf_aligned_nof_packets(self):
        return self.client.read("bf_aligned_nof_packets")

    @property
    def bf_aligned_nof_valid(self):
        return self.client.read("bf_aligned_nof_valid")

    @property
    def bf_aligned_latency(self):
        return self.client.read("bf_aligned_latency")

    @property
    def bf_ring_tx_bsn(self):
        return self.client.read("bf_ring_tx_bsn")

    @property
    def bf_ring_tx_nof_packets(self):
        return self.client.read("bf_ring_tx_nof_packets")

    @property
    def bf_ring_tx_nof_valid(self):
        return self.client.read("bf_ring_tx_nof_valid")

    @property
    def bf_ring_tx_latency(self):
        return self.client.read("bf_ring_tx_latency")

    # == end off set/get properties ==

    def setup_lane(self):
        # Setup nof hops for beamlets lane, see [1, 2]
        response = True
        nof_hops = [1] * self.n_nodes
        nof_hops[self.last_rn] = 0  # stop at last RN
        response &= self.client.write("bf_ring_nof_transport_hops", nof_hops)
        print_result("setup_lane", response)

    def setup_offload_hdr(self, mac=None, ip=None, port=None):
        """Setup BST offload destination address (same to all n_nodes)."""
        # Use default or argument
        _mac = self.dest_mac if mac is None else mac
        _ip = self.dest_ip if ip is None else ip
        _port = self.dest_udp_port if port is None else port

        # Use list or same for all nodes
        _mac = _mac if isinstance(_mac, list) else [_mac] * self.n_nodes
        _ip = _ip if isinstance(_ip, list) else [_ip] * self.n_nodes
        _port = _port if isinstance(_port, list) else [_port] * self.n_nodes

        self.bst_offload_hdr_udp_destination_port = _port
        self.bst_offload_hdr_eth_destination_mac = _mac
        self.bst_offload_hdr_ip_destination_address = _ip

    def setup_bf_weights(
        self,
        xx=2**C_SDP.W_bf_weight_fraction,
        yy=2**C_SDP.W_bf_weight_fraction,
        xy=0,
        yx=0,
    ):
        # Write bf weights
        response = True
        bf_weights_xx = [xx] * C_SDP.S_sub_bf * C_SDP.P_pfb * self.n_nodes
        bf_weights_yy = [yy] * C_SDP.S_sub_bf * C_SDP.P_pfb * self.n_nodes
        bf_weights_xy = [xy] * C_SDP.S_sub_bf * C_SDP.P_pfb * self.n_nodes
        bf_weights_yx = [yx] * C_SDP.S_sub_bf * C_SDP.P_pfb * self.n_nodes

        response &= self.client.write("bf_weights_xx", bf_weights_xx)
        response &= self.client.write("bf_weights_xy", bf_weights_xy)
        response &= self.client.write("bf_weights_yx", bf_weights_yx)
        response &= self.client.write("bf_weights_yy", bf_weights_yy)
        print_result("setup_bf_weights", response)

    def setup_sub_weights(self, gain=1.0):
        """Write subband weights, default gain = unit, phase = 0 fixed, so im = 0)"""
        response = True
        fxp_gain = int(gain * 2**C_SDP.W_sub_weight_fraction)
        self.logger.info(f"set subband weight: {gain} = {fxp_gain}")
        sub_gains = [fxp_gain] * self.n_nodes * C_SDP.S_pn * C_SDP.N_sub
        response &= self.client.write("subband_weights", (sub_gains))
        print_result("setup_sub_weights", response)

    def setup_stream(self, port=None):
        response = True
        self.setup_offload_hdr(port=port)
        self.setup_sub_weights()
        self.setup_bf_weights()
        response &= self.client.write("spectral_inversion", ([False] * C_SDP.S_pn * self.n_nodes))
        # BF subband selection
        beamlet_selects_single = list(np.arange(0, C_SDP.S_sub_bf))
        beamlet_selects = beamlet_selects_single * self.n_nodes * C_SDP.A_pn * C_SDP.N_pol
        response &= self.client.write("beamlet_subband_select", beamlet_selects)
        print_result("setup_stream", response)

    def bsn_monitor(self, mtime):
        """mtime: monitor runtime in seconds"""
        ntimes = 0
        start = time.time()
        while ntimes < mtime:
            while start > time.time():
                time.sleep(0.001)
            start += 1.0
            ntimes += 1
            nof_packets = self.client.read("bst_offload_nof_packets")
            nof_valid = self.client.read("bst_offload_nof_valid")
            self.print_log(f"BST bsn nof_packets: {nof_packets}")
            self.print_log(f"    bsn nof_valid  : {nof_valid}")

    def start_stream_reader(self):
        self.stream_reader = StreamReader(self.dest_udp_port, "BST", n_workers=1)
        self.stream_control = StreamControl(self.client, self.stream_reader)
        self.logger.debug("start stream-reader")
        self.stream_reader.start()

    def stop_stream_reader(self, timeout=60):
        if self.stream_reader:
            print_result("stop_stream_reader", True)
            # Wait until SDPFW has stopped stream output
            timeout_time = time.time() + timeout
            while time.time() < timeout_time and not self.stream_reader.done():
                time.sleep(0.0001)
            self.stream_reader.stop()

    def start_io(self):
        """Start stream_reader for input and start SDPFW output

        Start stream_reader before SDPFW statistics offload, to receive all statistics
        packets per integration interval.
        """
        self.start_stream_reader()
        self.stream_control.stream_on()
        self.logger.info("Started io")

    def stop_io(self):
        """Stop SDPFW output and stop stream_reader input

        Stop stream_reader after when SDPFW statistics offload has stopped, to receive all
        statistics packets per integration interval.
        """
        self.stream_control.stream_off()
        self.stop_stream_reader()
        self.logger.info("Stopped io")

    def print_header(self):
        bsn, index = self.stream_reader.data.get_packet_bsn_index(self.next_packet_nr)
        if bsn is not None and index is not None:
            hdr = self.stream_reader.data.get_header(bsn, index)
            recv_time = self.stream_reader.data.get_receive_time(bsn, index)

            print_header(hdr, stdout=self.stdout, recv_time=recv_time)
            self.next_packet_nr += 1
            return True

    def plot(self):
        # For WG signal input use wg.py to setup and enable WG
        from base.plotting import Plotting

        SAMPLE_FREQ = C_SDP.f_adc_MHz
        UPPER_FREQ = SAMPLE_FREQ / 2

        n_cols = 1
        n_rows = 1
        n_lines = self.n_nodes
        fig = Plotting(fig_size=[9, 6], rows=n_rows, cols=n_cols)
        for row in range(n_rows):
            for col in range(n_cols):
                fig.add_plot(row, col, n_lines=n_lines)
                fig.set_title(
                    row, col, f"BST plot gn_index {row * n_cols + col}", fontsize=12
                )
                fig.set_yas_label(
                    row, col, "Beamlet statistics value (dB)", fontsize=10
                )
                fig.set_xas_label(row, col, "Frequency (MHz)", fontsize=10)
                fig.set_legend(row, col, [f"{i}" for i in range(n_lines)])
                fig.set_limits(row, col, (0, UPPER_FREQ), (0, 1))
                fig.set_grid(row, col)
        fig.draw()

        n_stats_per_packet = 976
        xdata = np.linspace(0.0, UPPER_FREQ, num=n_stats_per_packet)
        stats = np.ma.zeros((self.n_nodes, n_stats_per_packet))
        # update first time to draw figure on the screen
        for _row in range(n_rows):
            for _col in range(n_cols):
                fig.update_plot(0, 0, xdata, stats, auto_ylimit=True)
        fig.update_graph()

        while True:
            stop_time = time.time() + 2.0
            while time.time() < stop_time:
                bsn, index = self.stream_reader.data.get_packet_bsn_index(
                    self.next_packet_nr
                )

                if bsn is not None and index is not None:
                    hdr = self.stream_reader.data.get_header(bsn, index)
                    data = self.stream_reader.data.get_data(bsn, index)
                    self.next_packet_nr += 1
                else:
                    continue

                stats[hdr.gn_index] = np.ma.array(data)

            stats = np.ma.masked_invalid(stats)  # mask all invalid values

            # Draw log plot
            stats_abs = np.ma.abs(stats)
            stats_median = max(1.0, np.ma.median(stats_abs))

            stats_offs = np.ma.zeros(stats_abs.shape)
            for i in range(stats_abs.shape[0]):
                stats_offs[i] = stats_abs[i] / stats_median  # remove offset

            stats_norm = np.ma.where(stats_offs != 0.0, stats_offs, 1.0)
            ydata = 20 * np.ma.log10(stats_norm)

            fig.update_plot(0, 0, xdata, ydata, auto_ylimit=True)
            fig.update_graph()

    def test_cp(self):
        success = True

        # Test lane default nof hops
        self.logger.info("test beamlets lane default nof hops")
        t_n_hops = [1] * self.n_nodes
        t_n_hops[self.last_rn] = 0  # stop at last RN
        if self.bf_ring_nof_transport_hops != t_n_hops:
            self.logger.error(
                f"bf_ring_nof_transport_hops={self.bf_ring_nof_transport_hops}"
            )
            success = False

        # Test offload destination mac, ip, port
        self.logger.info("test bst offload header settings")
        t_port = [5115] * self.n_nodes
        t_mac = ["00:11:22:33:44:55"] * self.n_nodes
        t_ip = ["10.20.30.40"] * self.n_nodes

        self.bst_offload_hdr_udp_destination_port = t_port
        self.bst_offload_hdr_eth_destination_mac = t_mac
        self.bst_offload_hdr_ip_destination_address = t_ip

        if self.bst_offload_hdr_udp_destination_port != t_port:
            self.logger.error(
                f"bst_offload_hdr_udp_destination_port={self.bst_offload_hdr_udp_destination_port}"
            )
            success = False
        if self.bst_offload_hdr_eth_destination_mac != t_mac:
            self.logger.error(
                f"bst_offload_hdr_eth_destination_mac={self.bst_offload_hdr_eth_destination_mac}"
            )
            success = False
        if self.bst_offload_hdr_ip_destination_address != t_ip:
            self.logger.error(
                f"bst_offload_hdr_ip_destination_address={self.bst_offload_hdr_ip_destination_address}"
            )
            success = False

        self.logger.info("setup offload header given by command line arguments")
        self.setup_offload_hdr()
        print_result("test_cp", success)

    def test_mp(self):
        def make_check_mask(type, align=False, offload=False):
            # make mask -1 is fixed, 0 is random
            base = [0] * self.n_nodes
            if type == "tx":
                mask = base[:-1] + [-1]
            else:
                mask = [-1] + base[1:]
            if offload:
                mask = [-1] * self.n_nodes
                mask = mask[:-1] + [0]
            if align:
                mask = [mask[i // 2] for i in range(len(mask) * 2)]
                mask[0] = 0

            self.logger.info(f"{type}: mask = {mask}, align = {align}, offload = {offload}")
            return mask

        def check_ring_nof_packets(arr, type, valid, align=False, offload=False):
            success = True
            mask = make_check_mask(type, align, offload)
            # mask value -1 must be same as arr, mask value 0, check if arr in valid
            for nr in range(len(mask)):
                if mask[nr] == -1:
                    if arr[nr] != -1:
                        self.logger.error(f"arr[{nr}]={arr[nr]} != -1")
                        success = False
                else:
                    if arr[nr] not in valid:
                        self.logger.error(f"arr[{nr}]={arr[nr]} not in {valid}")
                        success = False
            return success

        def check_nof_packets(arr, valid):
            success = True
            for i in arr:
                if i not in valid:
                    success = False
            return success

        self.logger.info("test bst offload_nof_packets")
        valid_nof_offload_packets = [1]
        valid_nof_packets = [195312, 195313]

        success = True
        if not check_ring_nof_packets(
            self.bst_offload_nof_packets, "tx", valid_nof_offload_packets, False, True
        ):
            self.logger.error(f"bst_offload_nof_packets {self.bst_offload_nof_packets}")
            success = False
        if not check_ring_nof_packets(
            self.bf_ring_tx_nof_packets, "tx", valid_nof_packets
        ):
            self.logger.error(f"bf_ring_tx_nof_packets={self.bf_ring_tx_nof_packets}")
            success = False
        if not check_ring_nof_packets(
            self.bf_ring_rx_nof_packets, "rx", valid_nof_packets
        ):
            self.logger.error(f"bf_ring_rx_nof_packets={self.bf_ring_rx_nof_packets}")
            success = False
        if not check_ring_nof_packets(
            self.bf_rx_align_nof_packets, "rx", valid_nof_packets, True
        ):
            self.logger.error(f"bf_rx_align_nof_packets={self.bf_rx_align_nof_packets}")
            success = False
        if not check_nof_packets(self.bf_aligned_nof_packets, valid_nof_packets):
            self.logger.error(f"bf_aligned_nof_packets={self.bf_aligned_nof_packets}")
            success = False
        print_result("test_mp", success)

    def test_header(self, recv_time):
        from sdp_info import SdpInfo

        sdp_info = SdpInfo(self.client)

        # save settings to restore later
        _antenna_field_index = sdp_info.antenna_field_index
        _station_id = sdp_info.station_id
        _observation_id = sdp_info.observation_id
        _antenna_band_index = sdp_info.antenna_band_index
        _nyquist_sampling_zone_index = sdp_info.nyquist_sampling_zone_index

        # set some values
        sdp_info.antenna_field_index = [3] * self.n_nodes
        sdp_info.station_id = [33] * self.n_nodes
        sdp_info.observation_id = [121] * self.n_nodes
        sdp_info.antenna_band_index = [1] * self.n_nodes
        sdp_info.nyquist_sampling_zone_index = [1] * self.n_nodes
        # wait to ensure new sdp_info settings will appear in statitics headers
        time.sleep(1.0)

        # header checks, keyname and expected value
        tests = {
            "marker": [66],
            "version_id": [5],
            "observation_id": sdp_info.observation_id,
            "antenna_field_index": sdp_info.antenna_field_index,
            "station_id": sdp_info.station_id,
            "antenna_band_index": sdp_info.antenna_band_index,
            "nyquist_zone_index": sdp_info.nyquist_sampling_zone_index,
            "f_adc": sdp_info.f_adc,
            "fsub_type": sdp_info.fsub_type,
            "payload_error": [0],
            "beam_repositioning_flag": [0],
            "subband_calibrated_flag": [1],
            "gn_index": list(range(self.o_rn, self.o_rn + self.n_nodes)),
            "integration_interval": [195312, 195313],
            "beamlet_index": [0, 488],
            "nof_bytes_per_statistics": [8],
            "nof_statistics_per_packet": [976],
            "block_period": [5120],
            "bsn": [0, 195313],
        }

        time.sleep(5.0)
        success = False
        if self.stream_reader:
            # first read in the data.
            record_time = 2.0 if recv_time < 2.0 else recv_time + 1.0
            self.stream_control.record_time(record_time)
            self.logger.info("Received %d BSN's:", self.stream_reader.data.nof_received_bsn())
            
            # now analyse the data
            success = True
            self.next_packet_nr = 0
            n_valid_packets = 0
            bsn_sub_lo = 195312
            bsn_sub_hi = 195313
            bsn_sub_x2 = bsn_sub_lo + bsn_sub_hi

            try:
                for bsn in self.stream_reader.data.get_all_bsn()[:-1]:  # do not test last bsn
                    for index in self.stream_reader.data.get_all_index(bsn=bsn):
                        hdr = self.stream_reader.data.get_header(bsn, index)

                        valid_header = True
                        for key, exp_value in tests.items():
                            value = hdr[key]
                            if key == "bsn":
                                if value % bsn_sub_x2 not in exp_value:
                                    self.logger.error(
                                        f"{key} {value} not on expected PPS grid"
                                    )  # see [1]
                                    valid_header = False
                                    break
                            elif value not in exp_value:
                                self.logger.error(f"{key} {value} not expected {exp_value}")
                                valid_header = False
                                break

                        if valid_header:
                            n_valid_packets += 1
                        else:
                            success = False
            except BaseException as err:
                if handle_exception(err) == 1:
                    return
            
            if n_valid_packets == 0:
                success = False
            
        self.print_log(f"Number of valid packets = {n_valid_packets}")

        self.print_log("")
        self.print_log(f"self.n_nodes = {self.n_nodes}")
        self.print_log(f"self.n_rn = {self.n_rn}")
        self.print_log(f"self.o_rn = {self.o_rn}")
        self.print_log(f"self.last_rn = {self.last_rn}")
        self.print_log("")

        # restore saved settings
        sdp_info.antenna_field_index = _antenna_field_index
        sdp_info.station_id = _station_id
        sdp_info.observation_id = _observation_id
        sdp_info.antenna_band_index = _antenna_band_index
        sdp_info.nyquist_sampling_zone_index = _nyquist_sampling_zone_index
        print_result("test_header", success)

    def test_data(self):
        from wg import WaveformGenerator

        wg = WaveformGenerator(self.client)
        wg.disable()

        si_full_scale = 2 ** (C_SDP.W_adc - 1)
        wg_sub = 102  # WG sinus subband index in range(N_sub=512)
        T_int_sub = (
            C_SDP.f_sub
        )  # number of subband samples per integration interval of 1 s

        # Test procedure:
        # . test a range of wg amplitudes
        # . each time after wg is enabled, wait some seconds before taking statistics samples
        # . use list of all_bsn to select an integration interval for statistics verification
        # . --mtime is not used

        # BST = SST in this case as we set the BF weigths to 1 / nof_antenna_inputs
        
        success = True
        for wg_ampl in [1.0, 0.5, 0.1]:  # Not using smaller ampl as that will cause a too small signal after applying the BF weights.
            wg_phase = 0.0
            wg_freq = wg_sub * C_SDP.f_sub  # WG sinus subband frequency in Hz
            nof_antenna_inputs = (
                self.n_nodes * C_SDP.A_pn
            )  # Multiply with A_pn as X-pol and Y-pol are beamformed seperately.
            bmlt_gain = (
                1.0 / nof_antenna_inputs
            )  # Devide by nof antenna inputs to get the same BST value as SST for 1 antenna input and prevent clipping
            bmlt_weight = int(bmlt_gain * 2**C_SDP.W_bf_weight_fraction)
            c_subband_phase_offset = (
                -90.0
            )  # WG with zero phase sinus yields subband with -90 degrees phase (negative Im, zero Re)

            # Write bf weights
            self.setup_bf_weights(bmlt_weight, bmlt_weight)

            # Determine expect BST level at WG frequency subband.
            si_ampl = wg_ampl * si_full_scale  # WG ampl in LSbit units
            sub_ampl = C_SDP.G_subband_sine * si_ampl  # expected subband[wg_sub] amplitude
            bmlt_ampl = sub_ampl * bmlt_gain
            bmlt_phase = wg_phase + c_subband_phase_offset
            bmlt = bmlt_ampl * np.exp(1j * np.deg2rad(bmlt_phase))
            sum_bmlt = nof_antenna_inputs * bmlt
            bmlt_power = (
                sum_bmlt * sum_bmlt.conjugate()
            )  # beamlet is complex, so no divide by 2
            exp_sub_bst = int(
                bmlt_power.real * T_int_sub
            )  # expected BST[wg_sub] statistic value

            self.logger.info(f"set wg signal in sb={wg_sub}")
            wg.disable()
            time.sleep(0.1)
            wg.set_sinus(wg_ampl, wg_phase, wg_freq)
            wg.enable()
            time.sleep(7.0)
            
            # first read in the data.
            record_time = 3.0
            self.stream_control.record_time(record_time)
            self.logger.info("Received %d BSN's:", self.stream_reader.data.nof_received_bsn())

            # now analyse the data
            bsn = self.stream_reader.data.get_all_bsn()[1]
            self.logger.info(f"Test BST at BSN {bsn} for WG ampl {wg_ampl}")
            # get list of all s_ant signal input indices of rx packets in the integration interval
            all_index = list(self.stream_reader.data.get_all_index(bsn))
            if len(all_index) != self.n_beamsets:
                self.logger.error(f"Received {len(all_index)} BST packets, expected {self.n_beamsets}")
                success = False
            for beamset in all_index:
                # for each signal input (si) in the integration interval get the rx packet data
                data = self.stream_reader.data.get_data(bsn, beamset)
                for sub, bst_value in enumerate(data):
                    # for each subband index (sub) in the rx packet data, check SST level, should be exp_sub_sst
                    # at WG subband index and near zero for all other subbands
                    bst_relative = bst_value / exp_sub_bst
                    if (
                        sub == C_SDP.N_pol_bf * wg_sub
                        or sub == C_SDP.N_pol_bf * wg_sub + 1
                    ):
                        if bst_relative < 0.99 or bst_relative > 1.01:
                            self.logger.error(
                                f"si-{beamset}, sub-{sub} BST value={bst_value} /= {exp_sub_bst} at WG freq differs too much from expected (bst_relative = {bst_relative})"
                            )
                            success = False
                    else:
                        if bst_relative > 1e-9 / wg_ampl**2:
                            self.logger.error(
                                f"si-{beamset}, sub-{sub} BST value={bst_value} /= {exp_sub_bst} differs too much from zero (bst_relative = {bst_relative})"
                            )
                            success = False
            
        wg.disable()
        self.stream_control.stream_off()
        print_result("test_data", success)


def run_setup_lane(_setup_lane, _args, bst_stat_stream):
    if _setup_lane:
        bst_stat_stream.setup_lane()


def run_setup_bst(_setup_bst, _args, bst_stat_stream):
    if _setup_bst:
        bst_stat_stream.setup_stream(_args.udpport)


def run_stream(_stream, _args, bst_stat_stream):
    if _stream:
        if _stream == "ON":
            bst_stat_stream.stream_control.stream_on()
        elif _stream == "OFF":
            bst_stat_stream.stream_control.stream_off()


def run_bsn_monitor(_bsn_monitor, _args, bst_stat_stream):
    if _bsn_monitor:
        bst_stat_stream.bsn_monitor(args.mtime)
        return True


def run_headers(_headers, _args, bst_stat_stream):
    if _headers:
        bst_stat_stream.start_io()
        stop_time = time.time() + _args.mtime
        while True:
            try:
                bst_stat_stream.print_header()
                if time.time() > stop_time:
                    break
            except BaseException as err:
                if handle_exception(err) == 1:
                    break
        bst_stat_stream.stop_io()
    return True


def run_plots(_plots, _args, bst_stat_stream):
    if _plots:
        bst_stat_stream.start_stream_reader()
        while True:
            try:
                bst_stat_stream.plot()
            except BaseException as err:
                if handle_exception(err) == 1:
                    break
        bst_stat_stream.stop_stream_reader()
        return True


def run_test_cp(_test_cp, _args, bst_stat_stream):
    if _test_cp:
        bst_stat_stream.test_cp()


def run_test_mp(_test_mp, _args, bst_stat_stream):
    if _test_mp:
        bst_stat_stream.start_io()
        time.sleep(2)  # wait for some sync intervals of 1 s
        bst_stat_stream.test_mp()
        bst_stat_stream.stop_io()


def run_test_header(_test_header, _args, bst_stat_stream):
    if _test_header:
        bst_stat_stream.setup_stream(_args.udpport)
        bst_stat_stream.start_stream_reader()
        bst_stat_stream.test_header(_args.mtime)
        bst_stat_stream.stop_stream_reader()


def run_test_data(_test_data, _args, bst_stat_stream):
    if _test_data:
        bst_stat_stream.setup_stream(_args.udpport)
        bst_stat_stream.start_stream_reader()
        bst_stat_stream.test_data()
        bst_stat_stream.stop_stream_reader()


def print_result(info, result):
    if result is True:
        logger.info(f"{info}: PASSED")
    else:
        logger.info(f"{info}: FAILED")


def main():
    exit_state = 0

    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    bst_stat_stream = BstStatisticStream(client, stream_mac, stream_ip, args.udpport)

    try:
        # Ensure correct destination mac, ip, udp port
        bst_stat_stream.setup_offload_hdr()

        run_setup_lane(args.setup_lane, args, bst_stat_stream)

        run_setup_bst(args.setup_bst, args, bst_stat_stream)

        # If necessary start SDP processing
        start_processing(client)

        run_stream(args.stream, args, bst_stat_stream)

        run_bsn_monitor(args.bsn_monitor, args, bst_stat_stream)

        run_headers(args.headers, args, bst_stat_stream)

        run_plots(args.plots, args, bst_stat_stream)

        run_test_cp(args.test_cp, args, bst_stat_stream)

        run_test_mp(args.test_mp, args, bst_stat_stream)

        run_test_header(args.test_header, args, bst_stat_stream)

        run_test_data(args.test_data, args, bst_stat_stream)

    except BaseException as err:
        exit_state = handle_exception(err)

    stop_all_streams(client)
    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(
            textwrap.dedent(
                """\
            opcua client command line argument parser:
            0) Start BST offload after reboot:
               sdp_firmware.py --host 10.99.0.250 --port 4842 --image USER --reboot
               sdp_rw.py --host 10.99.0.250 --port 4842 -r firmware_version

            1) Setup ring (do this once)
               common_ring.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup
               # manually check that:
               sdp_rw.py --host 10.99.0.250 --port 4842 -r sdp_config_first_fpga_nr
               sdp_rw.py --host 10.99.0.250 --port 4842 -r sdp_config_nof_fpgas
               # should match:
               sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_node_offset
               sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_nof_nodes
               sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_use_cable_to_next_rn
               sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_use_cable_to_previous_rn

               stat_stream_xst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup-lane

               # check beamlets on ring lane
               sdp_rw.py --host 10.99.0.250 --port 4842 -r bst_ring_rx_latency

            2) --stream to enable/disable the BST offload
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream ON
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream OFF
              sdp_rw.py --host 10.99.0.250 --port 4842 -r bst_offload_enable

            3) --bsn-monitor of BST offload
              # use --stream to enable offload or in other terminal use sdp_rw.py
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream ON --bsn-monitor --mtime 3 -vv
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --bsn-monitor --mtime 3 -vv
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream OFF --bsn-monitor --mtime 100 -vv
              sdp_rw.py --host 10.99.0.250 --port 4842 -r bst_offload_enable

            4) --test-cp test some CP values

            5) --test-mp test some MP values

            6) --test-header --> valid packets = mtime * 1 packets, stream on last PN
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-header --mtime 30

            7) --test-data --> verifies expected BST for several amplitudes
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --setup-bst
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-data

            8) --headers --> prints headers during mtime, stream on last PN
              stat_stream_bst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --headers --mtime 3

            9) --plots --> plots BST
              # use return in terminal to stop plots\n"""
            )
        ),
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "--host", type=str, default="localhost", help="host to connect to"
    )
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument(
        "-v",
        action="count",
        default=0,
        help="verbosity 'WARNING', 'INFO', 'DEBUG' -v, -vv, -vvv",
    )

    parser.add_argument("--ip", type=str, help="ip of destination machine")
    parser.add_argument("--mac", type=str, help="mac of destination machine")
    parser.add_argument(
        "--udpport", type=int, default=5002, help="udp port to use for stream"
    )
    parser.add_argument(
        "--setup-lane", action="store_true", help="setup lane for beamformer"
    )
    parser.add_argument(
        "--setup-bst", action="store_true", help="setup offload for BST stream"
    )
    parser.add_argument(
        "--stream",
        type=str,
        choices=["OFF", "ON"],
        help="turn off/on BST offload stream",
    )
    parser.add_argument(
        "--bsn-monitor", action="store_true", help="monitor bsn BST stream"
    )
    parser.add_argument(
        "--mtime",
        type=int,
        default=1,
        help="set time to monitor bsn BST stream, --mtime in seconds, default is 1 time",
    )
    parser.add_argument(
        "--headers", action="store_true", help="print BST stream headers"
    )
    parser.add_argument("--plots", action="store_true", help="print BST plots")
    parser.add_argument("--test-cp", action="store_true", help="check BST CP")
    parser.add_argument("--test-mp", action="store_true", help="check BST MP")
    parser.add_argument("--test-header", action="store_true", help="check BST header")
    parser.add_argument("--test-data", action="store_true", help="check BST data")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))
    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    stream_mac, stream_ip = parse_stream_destination(args.mac, args.ip, "1G")
    if not stream_ip or not stream_mac:
        logger.error("need ip and mac for this request")
        sys.exit(1)

    sys.exit(main())
