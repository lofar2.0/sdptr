#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
#
# Purpose:
# . Control, monitor and setup SST statistics stream
#
# Description:
# . run ./stat_stream_sst.py -h for help
#
# References:
# [1] https://support.astron.nl/confluence/display/L2M/L2+STAT+Decision%3A+Timing+in+Station
# [2] https://support.astron.nl/confluence/display/L2M/L5+SDPFW+Design+Document%3A+Subband+filterbank
#
# ##########################################################################

import sys
import logging
import time
import numpy as np

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list

from base.constants import C_SDP
from base.sdp_functions import parse_stream_destination, start_processing, stop_all_streams
from base.stream_reader import StreamReader, print_header
from base.stream_control import StreamControl
from subband_filterbank import SubbandFilterbank


class SstStatisticStream:
    def __init__(self, client, dest_mac, dest_ip, dest_udp_port):
        self.logger = logging.getLogger("SstStatisticStream")
        self.client = client
        self.n_nodes = client.n_nodes  # get number of nodes from server
        self.first_gn = client.gn_first_fpga
        self.s_ant = self.n_nodes * C_SDP.S_pn  # assume signal input 0 is on first node
        self.subband_filterbank = SubbandFilterbank(client)
        self.dest_mac = dest_mac
        self.dest_ip = dest_ip
        self.dest_udp_port = dest_udp_port
        self.stream_reader = None
        self.stream_control = None
        self.next_packet_nr = 0
        self.stdout = self.logger.getEffectiveLevel() > logging.INFO
        stop_all_streams(self.client)

    def print_log(self, fstr):
        print(fstr) if self.stdout else self.logger.info(fstr)

    def setup_offload_hdr(self):
        self.subband_filterbank.udp_destination_port = [
            self.dest_udp_port
        ] * self.n_nodes
        self.subband_filterbank.eth_destination_mac = [self.dest_mac] * self.n_nodes
        self.subband_filterbank.ip_destination_address = [self.dest_ip] * self.n_nodes
        print_result("setup_offload_hdr", True)

    def setup_stream(self, weighted_subbands):
        self.subband_filterbank.weighted_subbands = [weighted_subbands] * self.n_nodes
        print_result("setup_stream", True)

    def bsn_monitor(self, mtime):
        """mtime: monitor runtime in seconds"""
        ntimes = 0
        start = time.time()
        while ntimes < mtime:
            while start > time.time():
                time.sleep(0.001)
            start += 1.0
            ntimes += 1
            self.logger.info(
                f"SST bsn nof_packets: {self.subband_filterbank.nof_packets}"
            )
            self.logger.info(
                f"    bsn nof_valid  : {self.subband_filterbank.nof_valid}"
            )
        return True

    def start_stream_reader(self):
        self.stream_reader = StreamReader(self.dest_udp_port, "SST", n_workers=1)
        self.stream_control = StreamControl(self.client, self.stream_reader)
        self.stream_reader.start()
        print_result("start_stream_reader", True)

    def stop_stream_reader(self, timeout=60):
        if self.stream_reader:
            print_result("stop_stream_reader", True)
            # Wait until SDPFW has stopped stream output
            timeout_time = time.time() + timeout
            while time.time() < timeout_time and not self.stream_reader.done():
                time.sleep(0.0001)
            self.stream_reader.stop()

    def start_io(self):
        """Start stream_reader for input and start SDPFW output

        Start stream_reader before SDPFW statistics offload, to receive all statistics
        packets per integration interval.
        """
        self.start_stream_reader()
        self.stream_control.stream_on()
        self.logger.info("Started io")
        print_result("start_io", True)

    def stop_io(self):
        """Stop SDPFW output and stop stream_reader input

        Stop stream_reader after when SDPFW statistics offload has stopped, to receive all
        statistics packets per integration interval.
        """
        self.stream_control.stream_off()
        self.stop_stream_reader()
        self.logger.info("Stopped io")
        print_result("stop_io", True)

    def print_missing_indices(self):
        bsns = self.stream_reader.data.get_all_bsn()
        self.print_log(f"Received bsn numbers {bsns}")
        success = True
        for bsn in bsns[:-1]:  # ignore last bsn, may be incomplete due to stop reading ?
            indices = self.stream_reader.data.get_all_index(bsn)
            for i in range(self.s_ant):
                if i not in indices:
                    self.logger.error(f"- signal_input_index {i} not in bsn {bsn}")
                    success = False
        if not success:
            self.print_log("print_missing_indices")
        print_result("print_missing_indices", success)

    def print_sst_header(self):
        success = True
        bsn, index = self.stream_reader.data.get_packet_bsn_index(self.next_packet_nr)

        if bsn is not None and index is not None:
            hdr = self.stream_reader.data.get_header(bsn, index)
            recv_time = self.stream_reader.data.get_receive_time(bsn, index)

            print_header(hdr, stdout=self.stdout, recv_time=recv_time)
            self.next_packet_nr += 1
        else:
            success = False
            time.sleep(0.0001)
        print_result("print_sst_header", success)

    def plot(self):
        # For WG signal input use wg.py to setup and enable WG
        from base.plotting import Plotting

        SAMPLE_FREQ = C_SDP.f_adc_MHz
        UPPER_FREQ = SAMPLE_FREQ / 2

        n_cols = 4
        n_rows = self.n_nodes // n_cols
        s_pn = C_SDP.S_pn

        fig = Plotting(fig_size=[18, 12], rows=n_rows, cols=n_cols)
        for row in range(n_rows):
            for col in range(n_cols):
                fig.add_plot(row, col, n_lines=s_pn)
                fig.set_title(
                    row, col, f"SST plot gn_index {row * n_cols + col}", fontsize=12
                )
                fig.set_yas_label(
                    row, col, "Subband statistics value (dB)", fontsize=10
                )
                fig.set_xas_label(row, col, "Frequency (MHz)", fontsize=10)
                fig.set_legend(row, col, [f"{i}" for i in range(s_pn)])
                fig.set_limits(row, col, (0, UPPER_FREQ), (0, 1))
                fig.set_grid(row, col)
        fig.draw()

        n_stats_per_input = 512
        xdata = np.linspace(0.0, UPPER_FREQ, num=n_stats_per_input)
        stats = np.ma.zeros((self.n_nodes, s_pn, n_stats_per_input))
        # update first time to draw figure on the screen
        for row in range(n_rows):
            for col in range(n_cols):
                fig.update_plot(row, col, xdata, stats[row * col], auto_ylimit=True)
        fig.update_graph()

        while True:
            stop_time = time.time() + 2.0
            while time.time() < stop_time:
                bsn, index = self.stream_reader.data.get_packet_bsn_index(self.next_packet_nr)

                if bsn is not None and index is not None:
                    hdr = self.stream_reader.data.get_header(bsn, index)
                    data = self.stream_reader.data.get_data(bsn, index)
                    self.next_packet_nr += 1
                else:
                    continue

                signal_input_nr = hdr.signal_input_index % s_pn
                stats[hdr.gn_index, signal_input_nr, :] = np.ma.array(data)

            stats = np.ma.masked_invalid(stats)  # mask all invalid values

            # Draw log plot
            stats_abs = np.ma.abs(stats)
            stats_median = max(1.0, np.ma.median(stats_abs))

            stats_offs = np.ma.zeros(stats_abs.shape)
            for i in range(stats_abs.shape[0]):
                stats_offs[i] = stats_abs[i] / stats_median  # remove offset

            stats_norm = np.ma.where(stats_offs != 0.0, stats_offs, 1.0)
            ydata = 20 * np.ma.log10(stats_norm)

            for row in range(n_rows):
                for col in range(n_cols):
                    fig.update_plot(
                        row, col, xdata, ydata[row * n_cols + col], auto_ylimit=True
                    )
            fig.update_graph()

    def test_header(self, recv_time, weighted):
        from sdp_info import SdpInfo

        sdp_info = SdpInfo(self.client)

        # save settings to restore later
        _antenna_field_index = sdp_info.antenna_field_index
        _station_id = sdp_info.station_id
        _observation_id = sdp_info.observation_id
        _antenna_band_index = sdp_info.antenna_band_index
        _nyquist_sampling_zone_index = sdp_info.nyquist_sampling_zone_index

        # set some values
        sdp_info.antenna_field_index = [3] * self.n_nodes
        sdp_info.station_id = [33] * self.n_nodes
        sdp_info.observation_id = [121] * self.n_nodes
        sdp_info.antenna_band_index = [1] * self.n_nodes
        sdp_info.nyquist_sampling_zone_index = [1] * self.n_nodes
        # wait to ensure new sdp_info settings will appear in statitics headers
        time.sleep(1.0)

        # header checks, keyname and expected value
        tests = {
            "marker": [83],
            "version_id": [5],
            "observation_id": sdp_info.observation_id,
            "antenna_field_index": sdp_info.antenna_field_index,
            "station_id": sdp_info.station_id,
            "antenna_band_index": sdp_info.antenna_band_index,
            "nyquist_zone_index": sdp_info.nyquist_sampling_zone_index,
            "f_adc": sdp_info.f_adc,
            "fsub_type": sdp_info.fsub_type,
            "payload_error": [0],
            "beam_repositioning_flag": [0],
            "subband_calibrated_flag": [1] if weighted else [0],
            "gn_index": list(range(self.first_gn, self.first_gn + self.n_nodes)),
            "integration_interval": [195312, 195313],
            "signal_input_index": list(range(192)),
            "nof_signal_inputs": [1],
            "nof_bytes_per_statistics": [8],
            "nof_statistics_per_packet": [512],
            "block_period": [5120],
            "bsn": [0, 195313],
        }

        time.sleep(5.0)
        success = False
        if self.stream_reader:
            # first read in the data.
            record_time = 2.0 if recv_time < 2.0 else recv_time + 1.0
            self.stream_control.record_time(record_time)
            self.logger.info("Received %d BSN's", self.stream_reader.data.nof_received_bsn())

            # now analyse the data
            success = True
            self.next_packet_nr = 0
            n_valid_packets = 0
            bsn_sub_lo = 195312
            bsn_sub_hi = 195313
            bsn_sub_x2 = bsn_sub_lo + bsn_sub_hi

            try:
                for bsn in self.stream_reader.data.get_all_bsn()[:-1]:  # do not test last bsn
                    for index in self.stream_reader.data.get_all_index(bsn=bsn):

                        hdr = self.stream_reader.data.get_header(bsn, index)

                        valid_header = True
                        for key, exp_value in tests.items():
                            value = hdr[key]
                            if key == "bsn":
                                if value % bsn_sub_x2 not in exp_value:
                                    self.logger.error(
                                        f"{key} {value} not on expected PPS grid"
                                    )  # see [1]
                                    valid_header = False
                                    break
                            elif value not in exp_value:
                                self.logger.error(f"{key} {value} not expected {exp_value}")
                                valid_header = False
                                break

                        if valid_header:
                            n_valid_packets += 1
                        else:
                            success = False
            except BaseException as err:
                if handle_exception(err) == 1:
                    return

            if n_valid_packets == 0:
                success = False

        self.print_log(f"Number of valid packets = {n_valid_packets} (= {self.s_ant} * {n_valid_packets / self.s_ant})")

        self.print_log("")
        self.print_log(f"self.n_nodes = {self.n_nodes}")
        self.print_log(f"self.first_gn = {self.first_gn}")
        self.print_log("")

        # restore saved settings
        sdp_info.antenna_field_index = _antenna_field_index
        sdp_info.station_id = _station_id
        sdp_info.observation_id = _observation_id
        sdp_info.antenna_band_index = _antenna_band_index
        sdp_info.nyquist_sampling_zone_index = _nyquist_sampling_zone_index
        print_result("test_header", success)

    def test_data(self):
        from wg import WaveformGenerator

        wg = WaveformGenerator(self.client)

        si_full_scale = 2**(C_SDP.W_adc - 1)
        wg_sub = 102  # WG sinus subband index in range(N_sub=512)
        T_int_sub = C_SDP.f_sub  # number of subband samples per integration interval of 1 s

        # Test procedure:
        # . test a range of wg amplitudes
        # . each time after wg is enabled, wait some seconds before taking statistics samples
        # . use list of all_bsn to select an integration interval for statistics verification
        # . --mtime is not used

        # wg_ampl = 0.001   # SST SB-102 = 825978677
        # wg_ampl = 0.01    # SST SB-102 = 82823420784
        # wg_ampl = 0.1     # SST SB-102 = 8300194881257
        # wg_ampl = 1.0     # SST SB-102 = 830172237735488

        # Maximum difference in measured and expected SST value at subband wg_sub
        c_wg_sst_margin = 0.01

        # For smallest wg_ampl = 0.001 the exact SST level depends more on the WG phase, due
        # to quantization noise. For wg_phase = 0.0 the c_wg_sst_margin = 0.01 is still
        # suitable, provided that the WG phase is adjusted for the shiftram latency to
        # effectively keep wg_phase = 0.0.
        # If the c_wg_phase_offset = 0, then c_wg_sst_margin = 0.02 would be needed to verify
        # the SST result when wg_ampl = 0.001.
        c_sdp_shiftram_latency = 3.0
        c_wg_phase_offset = 360.0 * c_sdp_shiftram_latency * (wg_sub / C_SDP.N_fft)

        success = True
        for wg_ampl in [1.0, 0.1, 0.01, 0.001]:
            wg_phase = 0.0 + c_wg_phase_offset
            wg_freq = wg_sub * C_SDP.f_sub  # WG sinus subband frequency in Hz

            # Determine expect SST level at WG frequency subband
            si_ampl = wg_ampl * si_full_scale  # WG ampl in LSbit units
            sub_ampl = C_SDP.G_subband_sine * si_ampl  # expected subband[wg_sub] amplitude
            sub_power = sub_ampl**2  # subband is complex, so no divide by 2
            exp_sub_sst = sub_power * T_int_sub  # expected SST[wg_sub] statistic value

            self.logger.info(f"set wg signal in sb={wg_sub}")
            wg.disable()
            time.sleep(0.1)
            wg.set_sinus(wg_ampl, wg_phase, wg_freq)
            wg.enable()
            time.sleep(7.0)

            # first read in the data.
            if self.stream_control.record_n_bsn(n_bsn=3):
                self.logger.info("Received %d BSN's:", self.stream_reader.data.nof_received_bsn())
            else:
                self.logger.error("Received no BSN's")
                success = False
                continue

            # now analyse the data
            bsn = self.stream_reader.data.get_all_bsn()[1]

            self.logger.info(f"Test SST at BSN {bsn} for WG ampl {wg_ampl}")
            # get list of all s_ant signal input indices of rx packets in the integration interval
            all_index = list(self.stream_reader.data.get_all_index(bsn))
            if len(all_index) != self.s_ant:
                self.logger.error(f"Received {len(all_index)} SST packets, expected {self.s_ant}")
                success = False
            for si in all_index:
                # for each signal input (si) in the integration interval get the rx packet data
                data = self.stream_reader.data.get_data(bsn, si)
                for sub, sst_value in enumerate(data):
                    # for each subband index (sub) in the rx packet data, check SST level, should be exp_sub_sst
                    # at WG subband index and near zero for all other subbands
                    sst_relative = sst_value / exp_sub_sst
                    if sub == wg_sub:
                        if sst_relative < 1.0 - c_wg_sst_margin or sst_relative > 1.0 + c_wg_sst_margin:
                            self.logger.error(
                                f"si-{si}, sub-{sub} SST value={sst_value} at WG freq differs too much from expected (sst_relative = {sst_relative})"
                            )
                            success = False
                    else:
                        if sst_relative > 1e-9 / wg_ampl**2:
                            self.logger.error(
                                f"si-{si}, sub-{sub} SST value={sst_value} differs too much from zero (sst_relative = {sst_relative})"
                            )
                            success = False

        wg.disable()
        self.stream_control.stream_off()
        print_result("test_data", success)


def run_setup(_setup, _args, sst_stat_stream):
    if args.setup:
        sst_stat_stream.setup_stream(_args.sst_weighted)


def run_stream(_stream, _args, sst_stat_stream):
    if _stream:
        if _stream == "ON":
            sst_stat_stream.stream_control.stream_on()
        elif _stream == "OFF":
            sst_stat_stream.stream_control.stream_off()


def run_bsn_monitor(_bsn_monitor, _args, sst_stat_stream):
    if _bsn_monitor:
        sst_stat_stream.bsn_monitor(_args.mtime)


def run_headers(_headers, _args, sst_stat_stream):
    if _headers:
        sst_stat_stream.start_io()
        stop_time = time.time() + _args.mtime
        while True:
            try:
                sst_stat_stream.print_sst_header()
                if time.time() > stop_time:
                    break

            except BaseException as err:
                if handle_exception(err) == 1:
                    break
        sst_stat_stream.stop_io()
        sst_stat_stream.print_missing_indices()


def run_plots(_plots, _args, sst_stat_stream):
    if _plots:
        sst_stat_stream.start_io()
        while True:
            try:
                sst_stat_stream.plot()
            except BaseException as err:
                if handle_exception(err) == 1:
                    break
        sst_stat_stream.stop_io()


def run_test_header(_test_header, _args, sst_stat_stream):
    if _test_header:
        sst_stat_stream.setup_stream(_args.sst_weighted)
        sst_stat_stream.start_stream_reader()
        sst_stat_stream.test_header(_args.mtime, _args.sst_weighted)
        sst_stat_stream.stop_stream_reader()
        sst_stat_stream.print_missing_indices()


def run_test_data(_test_data, _args, sst_stat_stream):
    if _test_data:
        sst_stat_stream.setup_stream(_args.sst_weighted)
        sst_stat_stream.start_stream_reader()
        sst_stat_stream.test_data()
        sst_stat_stream.stop_stream_reader()
        sst_stat_stream.print_missing_indices()


def print_result(info, result):
    if result is True:
        logger.info(f"{info}: PASSED")
    else:
        logger.info(f"{info}: FAILED")


# functions used, if this script is called direct
def main():
    exit_state = 0

    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    sst_stat_stream = SstStatisticStream(client, stream_mac, stream_ip, args.udpport)

    # The if statements order determines the order in which the command line options are handled
    try:
        # Ensure correct offload destination mac, ip, udp port
        sst_stat_stream.setup_offload_hdr()

        run_setup(args.setup, args, sst_stat_stream)

        # If necessary start SDP processing
        start_processing(client)

        run_stream(args.stream, args, sst_stat_stream)

        run_bsn_monitor(args.bsn_monitor, args, sst_stat_stream)

        run_headers(args.headers, args, sst_stat_stream)

        run_plots(args.plots, args, sst_stat_stream)

        run_test_header(args.test_header, args, sst_stat_stream)

        run_test_data(args.test_data, args, sst_stat_stream)

    except BaseException as err:
        exit_state = handle_exception(err)

    stop_all_streams(client)
    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(
            textwrap.dedent(
                """\
            opcua client command line argument parser:
            1) --bsn-monitor of SST offload
              # use --stream to enable offload or in other terminal use sdp_rw.py
              stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream ON --bsn-monitor --mtime 3 -vv
              stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --stream OFF --bsn-monitor --mtime 3 -vv
              sdp_rw.py --host 10.99.0.250 --port 4842 -r sst_offload_enable
              sdp_rw.py --host 10.99.0.250 --port 4842 -w sst_offload_enable [True]*16

            2) --headers --> print headers during mtime
              stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --headers --mtime 3

            3) --test-header --> valid packets = mtime * 192 packets
              stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-header --mtime 3

            4) --test-data --> verifies expected SST for ampl in [1.0, 0.1, 0.01, 0.001]
              stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --test-data

            5) --plots
              # use ctrl-C in terminal to stop plots
              # use wg.py in other terminal to control WG
              stat_stream_sst.py --host 10.99.0.250 --port 4842 --ip dop386 --mac dop386 --plots
              wg.py  --host 10.99.0.250 --port 4842 --setphase 0 --setfreq 19921875 --setampl 0.5 --enable
              wg.py  --host 10.99.0.250 --port 4842 --disable\n"""
            )
        ),
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "--host", type=str, default="localhost", help="host to connect to"
    )
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument(
        "-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG"
    )

    parser.add_argument("--ip", type=str, help="ip of destination machine")
    parser.add_argument("--mac", type=str, help="mac of destination machine")
    parser.add_argument(
        "--udpport",
        type=int,
        default=5001,
        help="udp port of destination machine to use for stream",
    )
    parser.add_argument("--setup", action="store_true", help="setup SST")
    parser.add_argument(
        "--stream",
        type=str,
        choices=["OFF", "ON"],
        help="turn off/on SST offload stream",
    )
    parser.add_argument(
        "--sst-weighted", action="store_true", help="use weighted subbands for sst"
    )
    parser.add_argument(
        "--bsn-monitor", action="store_true", help="monitor bsn SST stream"
    )
    parser.add_argument(
        "--mtime",
        type=int,
        default=1,
        help="set time to monitor bsn SST stream, --mtime in seconds, default is 1 time",
    )
    parser.add_argument(
        "--headers", action="store_true", help="print SST stream headers"
    )
    parser.add_argument("--plots", action="store_true", help="print SST plots")
    parser.add_argument("--test-header", action="store_true", help="check SST header")
    parser.add_argument("--test-data", action="store_true", help="check SST data")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))
    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    stream_mac, stream_ip = parse_stream_destination(args.mac, args.ip, "1G")
    if not stream_ip or not stream_mac:
        logger.error("need ip and mac for this test")
        sys.exit(1)

    sys.exit(main())
