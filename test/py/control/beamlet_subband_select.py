#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Set or get subband select
# Description:
# . run ./subband_select.py -h for help
# ##########################################################################


import sys

import logging
import argparse
import pprint

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class BeamletSubbandSelect:
    """ Beamlet subband select """
    S_PN = 12
    N_POL = 2
    A_PN = 6
    N_BF_SUB = 488

    def __init__(self, client):
        self.client = client
        self.n_nodes = self.client.read("sdp_config_nof_fpgas")
        self.n_beamsets = self.client.read("sdp_config_nof_beamsets")
        self.rd_data = None
        self.wr_data = None

    def read(self):
        """ Read subband selection """
        self.rd_data = self.client.read("beamlet_subband_select")
        return self.rd_data

    def write(self, select=None):
        """ write subband selection """
        if select is not None:
            if not isinstance(select, list):
                _select = list(select)
            expected_size = self.n_nodes * self.A_PN * self.N_POL * self.n_beamsets * self.N_BF_SUB
            multiply = expected_size // len(_select)
            if expected_size % len(_select) > 0:
                multiply += 1
            self.wr_data = _select * multiply
        else:
            print("write all data")
            self.wr_data = (
                list(range(488 * self.n_beamsets)) * self.N_POL * self.A_PN * self.n_nodes
            )
        self.client.write("beamlet_subband_select", (self.wr_data))

    def print_screen(self):
        """ Print read data to screen """
        pprint.pprint(self.rd_data, compact=True)

    def test(self, nodelist):
        """ Test """
        _nodes = list(range(self.n_nodes)) if nodelist is None else nodelist
        # fill test data all C_S_sub_bf data starts with a header 511 0 0 0 0 511
        # [N_pn][A_pn][N_pol][N_beamsets][C_S_sub_bf]
        data = []
        header = [0] * 6
        for node in range(self.n_nodes):
            header[0] = 510  # start header
            header[1] = node  # add node number to header
            for ant in range(self.A_PN):
                header[2] = ant  # add antenneset number to header
                for pol in range(self.N_POL):
                    header[3] = pol  # add pol number to header
                    for beamset in range(self.n_beamsets):
                        header[4] = beamset  # add beamset number to header
                        header[5] = 511  # stop header
                        data.extend(header)
                        for blet in range(18, 500, 1):
                            data.append(blet)

        self.wr_data = data
        self.write()
        self.read()

        node_size = self.A_PN * self.N_POL * self.n_beamsets * self.N_BF_SUB
        for node in _nodes:
            s1 = node * node_size
            s2 = s1 + node_size
            # print(f'node {node}: s1={s1}, s2={s2}')
            if self.rd_data[s1:s2] == self.wr_data[s1:s2]:
                print(f"Node {node}: WR/RD succes")
            else:
                print(f"node {node}, expected:")
                pprint.pprint(self.wr_data[s1:s2], compact=True)
                print(f"node {node}, received:")
                pprint.pprint(self.rd_data[s1:s2], compact=True)
                print(f"node {node}: WR/RD failed")


def main():
    """ Main function when direct called """
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    beamlet_subband_select = BeamletSubbandSelect(client)

    try:
        if args.read:
            beamlet_subband_select.read()
            beamlet_subband_select.print_screen()
        if args.write is not None:  # --write active with or whithout selection
            _select = None
            if args.write != "":
                _select = eval(args.write)
            print(_select)
            beamlet_subband_select.write(_select)
        if args.test:
            beamlet_subband_select.test(node_list)
    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument("--write", type=str, help="set subband select")
    parser.add_argument("--read", action="store_true", help="get subband select")
    parser.add_argument("--test", action="store_true", help="test subband select")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
