#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Set or get subband weights
# Description:
# . run ./subband_weights.py -h for help
# ##########################################################################


import sys
import logging
import argparse
import pprint

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class SubbandWeights:
    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger("SubbandWeights")
        self.n_nodes = self.client.read("sdp_config_nof_fpgas")
        self.rd_data = None
        self.wr_data = None

    def read(self):
        self.rd_data = self.client.read("subband_weights")
        return self.rd_data

    def write(self, weights):
        N_SUB = 512
        S_PN = 12
        if not isinstance(weights, list):
            _weights = [weights]
        if len(_weights) == 1:
            self.wr_data = (
                [weights] * N_SUB * self.n_nodes * S_PN
            )  # set weights for subbands to default
        elif len(_weights) == 12:
            self.wr_data = [weights] * N_SUB * self.n_nodes  # set weights for subbands to default
        elif len(_weights) == (N_SUB * self.n_nodes * S_PN):
            self.wr_data = _weights
        else:
            self.logger.error("weigts size can not be parsed")
            return
        self.client.write("subband_weights", (self.wr_data))

    def print_screen(self):
        pprint.pprint(self.rd_data, compact=True)


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    sb_weights = SubbandWeights(client)

    while True:
        try:
            if args.get_weights:
                sb_weights.read()
                sb_weights.print_screen()
            if args.set_weights:
                _weights = eval(args.set_weights)
                sb_weights.write(_weights)
            break
        except BaseException as err:
            exit_state = handle_exception(err)
            if exit_state == 1:
                break

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument("--set-weights", type=str, help="set subband weights")
    parser.add_argument("--get-weights", action="store_true", help="get subband weights")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
