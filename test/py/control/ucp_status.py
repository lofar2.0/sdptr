#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . read ucp communication status
# .
# Description:
# . run ./ucp_status.py -h for help
# ##########################################################################


import sys
sys.path.insert(0, "..")

import logging

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class UcpStatus(object):
    def __init__(self, client):
        self.logger = logging.getLogger("UcpStatus")
        self.client = client
        self.n_nodes = self.client.n_nodes
        self.fpga_mask = self.client.get_mask()
        self.actives_fpgas = [not i for i in self.client.read("fpga_communication_error")]

    def reset(self):
        self.client.write("ucp_reset_counters", [True] * self.client.n_nodes)
        return True

    def status(self):
        # ucp_reads ucp_read_retries ucp_read_fails
        # ucp_writes ucp_write_retries ucp_write_fails

        reads = self.client.read("ucp_nof_reads")
        read_retries = self.client.read("ucp_nof_read_retries")
        read_fails = self.client.read("ucp_nof_read_failures")
        writes = self.client.read("ucp_nof_writes")
        write_retries = self.client.read("ucp_nof_write_retries")
        write_fails = self.client.read("ucp_nof_write_failures")

        self.logger.info("           reads        r_retries    r_fails      writes       w_retries    w_fails")
        self.logger.info("          ------------ ------------ ------------ ------------ ------------ ------------")
        for i in range(self.client.n_nodes):
            self.logger.info(f"node-{i:<2d}: {reads[i]:>12d} {read_retries[i]:>12d} {read_fails[i]:>12d} {writes[i]:>12d} {write_retries[i]:>12d} {write_fails[i]:>12d} ")
        return True


def print_result(info, success):
    if success:
        logger.info(f"{info}: PASSED")
        print(f"{info}: PASSED")
    else:
        logger.info(f"{info}: FAILED")
        print(f"{info}: FAILED")


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.debug("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    ucp_status = UcpStatus(client)

    try:
        if args.status:
            result = ucp_status.status()
            print_result("ucp_status", result)
        if args.reset:
            result = ucp_status.reset()
            print_result("ucp_reset_counters", result)

    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Parse command line arguments
    parser = argparse.ArgumentParser(description="".join(
        textwrap.dedent("""
                ucp_status.py
                Get or reset communication status of the ucp protocol.
                - The total number of read and write actions are shown.
                - The number of retries and failures are shown.

                usage:
                ucp_status.py --host localhost --port 4842 --reset
                ucp_status.py --host localhost --port 4842 --status
            """)
        ), formatter_class=argparse.RawTextHelpFormatter
    )
    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument("--reset", action="store_true", help="reset all ucp status counter")
    parser.add_argument("--status", action="store_true", help="show ucp status")
    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 2 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.debug("parsed arguments: {}".format(args))

    sys.exit(main())
