#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . test write/read bf_weigts speed
# Description:
# . run ./sdp_temp_test.py -h for help
# ##########################################################################


import sys
import logging
import argparse
import time

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list
from base.terminal import run_cmd

"""
ssh sdptr-lts 'sudo iftop -n -t -s 10
/opt/vc/bin/vcgencmd measure_temp

"""


class CpuTest:
    """ Run CPU test """
    def __init__(self, client):
        self.client = client
        self.n_nodes = self.client.read("sdp_config_nof_fpgas")
        self.n_beamsets = self.client.read("sdp_config_nof_beamsets")
        self.cpulist_last = None

    def get_cpu_temp(self, host="localhost"):
        """ Get cpu temperature """
        host = "localhost" if host is None else "pi@" + host
        cmd = f"ssh {host} '/opt/vc/bin/vcgencmd measure_temp'"
        temp = run_cmd(cmd)
        return float(temp.split("=")[1].split("'")[0])

    def get_cpu_load(self, host="localhost"):
        """ get cpu load """
        host = "localhost" if host is None else "pi@" + host
        cmd = f"ssh {host} 'cat /proc/stat'"
        load = run_cmd(cmd)
        load = load.splitlines()
        columns = load[0].replace("cpu", "").strip().split(" ")
        cpulist_now = list(map(int, filter(None, columns)))
        cpu_percent = 0.0
        if self.cpulist_last is not None:
            dt = list(
                (t2 - t1) for t1, t2 in zip(self.cpulist_last, cpulist_now)
            )  # delta since last call
            idle_time = float(dt[3])
            total_time = sum(dt)
            cpu_percent = ((total_time - idle_time) / total_time) * 100
        self.cpulist_last = cpulist_now
        return cpu_percent


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    cpu_test = CpuTest(client)

    try:
        time_cnt = 0
        start_time = time.time() + 1.0
        while time_cnt < args.ttime:
            while time.time() < start_time:
                time.sleep(0.001)
            start_time += args.interval
            time_cnt += 1
            if args.temp:
                temp = cpu_test.get_cpu_temp(args.host)
                print(f"cpu temp: {temp:5.1f} 'C")
            if args.load:
                load = cpu_test.get_cpu_load(args.host)
                print(f"cpu load: {load:5.1f} %")
    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument(
        "--host", dest="host", type=str, default="dop36", help="opcua host to connect to"
    )
    parser.add_argument("--port", dest="port", type=int, default=4840, help="opcua port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument("--temp", action="store_true", help="get pi temp")
    parser.add_argument("--load", action="store_true", help="get pi load")
    parser.add_argument("--ttime", type=int, default=60, help="test time in seconds, default 60sec")
    parser.add_argument(
        "--interval", type=int, default=10, help="test interval in seconds, default 10sec"
    )

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
