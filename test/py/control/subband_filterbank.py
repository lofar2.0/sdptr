#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Control and monitor the Subband Filterbankand (Fsub)
# Description:
# . run ./subband_filterbank.py -h  (for help)
# ##########################################################################

import sys
import logging

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class SubbandFilterbank:
    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger("SubbandFilterbank")

    @property
    def spectral_inversion(self):
        return self.client.read("spectral_inversion")

    @spectral_inversion.setter
    def spectral_inversion(self, state):
        self.client.write("spectral_inversion", state)

    @property
    def fir_filter_coefficients(self):
        return self.client.read("subband_fir_filter_coefficients")

    @fir_filter_coefficients.setter
    def fir_filter_coefficients(self, fir_filter_coef):
        self.client.write("subband_fir_filter_coefficients", fir_filter_coef)

    @property
    def weights(self):
        return self.client.read("subband_weights")

    @weights.setter
    def weights(self, weights):
        self.client.write("subband_weights", weights)

    @property
    def udp_destination_port(self):
        return self.client.read("sst_offload_hdr_udp_destination_port")

    @udp_destination_port.setter
    def udp_destination_port(self, udp_port_list):
        self.client.write("sst_offload_hdr_udp_destination_port", udp_port_list)

    @property
    def eth_destination_mac(self):
        return self.client.read("sst_offload_hdr_eth_destination_mac")

    @eth_destination_mac.setter
    def eth_destination_mac(self, eth_mac_list):
        self.client.write("sst_offload_hdr_eth_destination_mac", eth_mac_list)

    @property
    def ip_destination_address(self):
        return self.client.read("sst_offload_hdr_ip_destination_address")

    @ip_destination_address.setter
    def ip_destination_address(self, ip_addr_list):
        self.client.write("sst_offload_hdr_ip_destination_address", ip_addr_list)

    @property
    def weighted_subbands(self):
        return self.client.read("sst_offload_weighted_subbands")

    @weighted_subbands.setter
    def weighted_subbands(self, state_list):
        self.client.write("sst_offload_weighted_subbands", state_list)

    @property
    def enable(self):
        return self.client.read("sst_offload_enable")

    @enable.setter
    def enable(self, state_list):
        self.client.write("sst_offload_enable", state_list)

    @property
    def bsn(self):
        return self.client.read("sst_offload_bsn")

    @property
    def nof_packets(self):
        return self.client.read("sst_offload_nof_packets")

    @property
    def nof_valid(self):
        return self.client.read("sst_offload_nof_valid")

    def test_control(self, nodelist):
        nodelist = self.client.node_list if nodelist is None else nodelist
        # save actual values
        _spectral_inversion = self.spectral_inversion
        # _fir_filter_coefficients = self.fir_filter_coefficients
        _weights = self.weights
        _eth_destination_mac = self.eth_destination_mac
        _ip_destination_address = self.ip_destination_address
        _udp_destination_port = self.udp_destination_port
        _weighted_subbands = self.weighted_subbands
        _enable = self.enable

        # do the test
        def valid_response(val, t_val):
            # check for valid response if node is selected
            sz_per_node = len(t_val) // self.client.n_fpgas
            for node in self.client.node_list:
                if node in nodelist:
                    node_nr = node % self.client.n_fpgas
                    val_begin = node_nr * sz_per_node
                    val_end = val_begin + sz_per_node
                    if t_val[val_begin:val_end] != val[val_begin:val_end]:
                        return False
            return True

        succes = True
        t_spectral_inversion = [not i for i in _spectral_inversion]
        self.spectral_inversion = t_spectral_inversion
        if not valid_response(self.spectral_inversion, t_spectral_inversion):
            self.logger.error("test subband_filterbank.spectral_inversion failed")
            succes = False

        t_weights = list(range(512)) * 12 * self.client.n_nodes
        self.weights = t_weights
        if self.weights != t_weights:
            self.logger.error("test subband_filterbank.weights failed")
            succes = False

        t_eth_destination_mac = [f"11:22:33:44:55:66:{i:02d}" for i in range(self.client.n_nodes)]
        self.eth_destination_mac = t_eth_destination_mac
        if not valid_response(self.eth_destination_mac, t_eth_destination_mac):
            self.logger.error("test subband_filterbank.eth_destination_mac failed")
            succes = False

        t_ip_destination_address = [f"11.22.33.{i:02d}" for i in range(self.client.n_nodes)]
        self.ip_destination_address = t_ip_destination_address
        if not valid_response(self.ip_destination_address, t_ip_destination_address):
            self.logger.error("test subband_filterbank.ip_destination_address failed")
            succes = False

        t_udp_destination_port = [6000 + i for i in range(self.client.n_nodes)]
        self.udp_destination_port = t_udp_destination_port
        if not valid_response(self.udp_destination_port, t_udp_destination_port):
            self.logger.error("test subband_filterbank.udp_destination_port failed")
            succes = False

        t_weighted_subbands = [not i for i in _weighted_subbands]
        self.weighted_subbands = t_weighted_subbands
        if not valid_response(self.weighted_subbands, t_weighted_subbands):
            self.logger.error("test subband_filterbank.weighted_subbands failed")
            succes = False

        t_enable = [not i for i in _enable]
        self.enable = t_enable
        if not valid_response(self.enable, t_enable):
            self.logger.error("test subband_filterbank.enable failed")
            succes = False

        # set back saved values
        self.spectral_inversion = _spectral_inversion
        # self.fir_filter_coefficients = _fir_filter_coefficients
        self.weights = _weights
        self.eth_destination_mac = _eth_destination_mac
        self.ip_destination_address = _ip_destination_address
        self.udp_destination_port = _udp_destination_port
        self.weighted_subbands = _weighted_subbands
        self.enable = _enable
        return succes


def main():
    exit_state = 0

    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    subband_filterbank = SubbandFilterbank(client)

    try:
        if args.spec_inv == "R":
            print(f"Spectral inversion: {subband_filterbank.spectral_inversion}")
        elif args.spec_inv is not None:
            subband_filterbank.spectral_inversion = eval(args.spec_inv)

        # if args.fir_filter == "R":
        #     print(f"Fir Filter Coefficients: {subband_filterbank.fir_filter_coefficients}")
        # elif args.fir_filter is not None:
        #     subband_filterbank.fir_filter_coefficients = eval(args.fir_filter)

        if args.weights == "R":
            print(f"Subband Weights: {subband_filterbank.weights}")
        elif args.weights is not None:
            subband_filterbank.weights = eval(args.weights)

        if args.ip == "R":
            print(f"SST offload destination IP addres: {subband_filterbank.ip_destination_address}")
        elif args.ip is not None:
            subband_filterbank.ip_destination_address = eval(args.ip)

        if args.mac == "R":
            print(f"SST offload destination MAC addres: {subband_filterbank.eth_destination_mac}")
        elif args.mac is not None:
            subband_filterbank.eth_destination_mac = eval(args.mac)

        if args.udpport == "R":
            print(f"SST offload UDP port: {subband_filterbank.udp_destination_port}")
        elif args.udpport is not None:
            subband_filterbank.udp_destination_port = eval(args.udpport)

        if args.enable == "R":
            print(f"SST offload enable: {subband_filterbank.enable}")
        elif args.enable is not None:
            subband_filterbank.enable = eval(args.enable)

        if args.weighted == "R":
            print(f"SST offload weighted subbands: {subband_filterbank.weighted_subbands}")
        elif args.weighted is not None:
            subband_filterbank.weighted_subbands = eval(args.weighted)

        if args.test_control:
            if subband_filterbank.test_control(node_list):
                print("SUCCES")
            else:
                print("FAILURE")

    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument(
        "--spec-inv",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="reverse order subbands, 0=normal 1=reverse",
    )
    # parser.add_argument(
    #     "--fir-filter",
    #     nargs="?",
    #     type=str,
    #     const="R",
    #     default=None,
    #     help="subband fir filter coefficients = N_tab * N_fft = 16384 coeffiecients",
    # )
    parser.add_argument(
        "--weights",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="subband weights",
    )
    parser.add_argument(
        "--ip",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="ip of destination machine",
    )
    parser.add_argument(
        "--mac",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="mac of destination machine",
    )
    parser.add_argument(
        "--udpport",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="udp port to use for stream",
    )
    parser.add_argument(
        "--enable",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="turn off/on SST offload stream",
    )
    parser.add_argument(
        "--weighted",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="use weighted subbands for sst",
    )
    parser.add_argument("--test_control", action="store_true", help="check SST control points")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))
    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
