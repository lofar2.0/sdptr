#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Reinier vd Walle
# Purpose:
# . Control, monitor and test JESD interface on SDP firmware
# Description:
# . run ./jesd.py -h for help
# . See ADCfunctions for how the clock_cw_statistics are determined from the
#   known common RCU input CW.
# ##########################################################################

import sys
import logging
import time
import numpy as np

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list
from base.constants import C_SDP
from base.adc_functions import AdcFunctions


class Jesd:
    """Jesd class"""

    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger("Jesd")
    
        self.cw_period = 16  # nof 200MHz samples per CW sinus period (12.5 MHz)
        self.rst_testcycle_duration = 3  # Each test cycle takes rstTestCycleDuration seconds, so the test will do about round_up(--mtime / rstTestCycleDuration) cycles.
        self.ber_testcycle_duration = 60  # Each test cycle takes berTestCycleDuration seconds, so the test will do about round_up(--mtime / berTestCycleDuration) cycles.

    def jesd_ber_test(self, global_sp_list, mtime):
        """Executes JESD Bit Error Rate test. Captures err0 and err1
        registers every berTestCycleDuration seconds for a total time
        of mtime seconds. Finally calculates the BER."""

        jesd_stats = {
            "csr_rbd_count": [],
            "csr_dev_syncn": [],
            "rx_err0": [],
            "rx_err1": [],
        }
        cycle_index = 0
        start_time = time.time()
        start = time.time()
        elapsed_time = 0.0
        bit_err_cnt = 0  # Counts all bit errors, fatal or non-fatal.
        fatal_bit_err_cnt = 0  # Counts only fatal bit errors.

        err0_list = []
        err1_list = []

        ber = 0  # Bit Error Rate including fatal and non-fatal bit errors.
        fatal_ber = 0  # Bit Error Rate including the fatal bit errors only.
        try:
            while elapsed_time < mtime:  # Run for mtime seconds
                while start > time.time():  # Sleep until berTestCycleDuration seconds are passed.
                    time.sleep(0.001)
                start = start + self.ber_testcycle_duration
                cycle_index = cycle_index + 1

                # Reset when encountered error
                if (np.count_nonzero(err0_list) + np.count_nonzero(err1_list)) > 0:
                    self.logger.error(
                        "Error detected at cycle %d. err0 = %s, err1 = %s",
                        cycle_index - 1,
                        str(err0_list),
                        str(err1_list),
                    )
                    was_bit_error = [
                        ((err & 0x300) >> 8) != 0 for err in err1_list
                    ]  # ECC or Fatal ECC error detected
                    was_corrected = [
                        ((err & 0x100) >> 8) != 0 for err in err1_list
                    ]  # No Fatal ECC error detected, only corrected ECC.
                    if was_bit_error:
                        self.logger.error(
                            "Error included %s recoverable bit error!",
                            ["UN", ""][int(was_corrected)],
                        )
                        bit_err_cnt += 1
                        fatal_bit_err_cnt = (
                            (fatal_bit_err_cnt + 1) if not was_corrected else fatal_bit_err_cnt
                        )
                    self.client.write(
                        "processing_enable", [True] * self.client.n_nodes
                    )  # use processing_enable = True to reset JESD IP.
                    time.sleep(2)

                # Read JESD stats
                jesd_stats["csr_rbd_count"].append(self.client.read("jesd204b_csr_rbd_count"))
                jesd_stats["csr_dev_syncn"].append(self.client.read("jesd204b_csr_dev_syncn"))
                jesd_stats["rx_err0"].append(self.client.read("jesd204b_rx_err0"))
                jesd_stats["rx_err1"].append(self.client.read("jesd204b_rx_err1"))

                err0_list = []
                err1_list = []
                for _sp in global_sp_list:
                    err0_list.append(jesd_stats["rx_err0"][-1][_sp])
                    err1_list.append(jesd_stats["rx_err1"][-1][_sp])

                elapsed_time = time.time() - start_time

        except KeyboardInterrupt:
            print(" user hit ctrl-c")

        # Validate
        err0_list = []
        err1_list = []
        for _sp in global_sp_list:
            err0_list.append([err[_sp] for err in jesd_stats["rx_err0"]])
            err1_list.append([err[_sp] for err in jesd_stats["rx_err1"]])

        err0_cnt = np.count_nonzero(err0_list)
        err1_cnt = np.count_nonzero(err1_list)
        self.logger.info("Done validating JESD: %d cycles", cycle_index)
        self.logger.info("amount of err0: %d, amount of err1: %d", err0_cnt, err1_cnt)

        ber = bit_err_cnt / (C_SDP.W_adc * C_SDP.f_adc_MHz * 10**6 * elapsed_time)
        fatal_ber = fatal_bit_err_cnt / (C_SDP.W_adc * C_SDP.f_adc_MHz * 10**6 * elapsed_time)
        self.logger.info("BER = %f", ber)
        self.logger.info("Fatal BER = %f", fatal_ber)

        result = (err0_cnt + err1_cnt) == 0
        if result:
            self.logger.info("PASSED")
        else:
            self.logger.error("FAILED")
            self.logger.info("err0 list = %s\nerr1 list = %s", str(err0_list), str(err1_list))

        return result

    def jesd_reset_test(self, node_list, global_sp_list, mtime):
        """Executes JESD Reset test. Captures databuffer data after
        every reset then calculates and verifies the statistics.
        executed every rstTestCycleDuration seconds for a total time
        of mtime seconds. Finally plots the  clock_cw_statistics
        (based on CW phase in databuffer) and the JESD statistics
        (from JESD IP registers). All RCU inputs have a common CW
        input with cwPeriod."""

        adc_h = AdcFunctions()
        adc_h.set_nof_bits(C_SDP.W_adc)
        adc_h.set_fs(C_SDP.f_adc_MHz)

        adc_h.plot_init()

        jesd_stats = {
            "csr_rbd_count": [],
            "csr_dev_syncn": [],
            "rx_err0": [],
            "rx_err1": [],
        }
        cycle_index = 0
        start_time = time.time()
        start = time.time()
        elapsed_time = 0.0
        bsn_running = True
        try:
            while elapsed_time < mtime:
                while start > time.time():
                    time.sleep(0.001)
                start = start + self.rst_testcycle_duration
                cycle_index = cycle_index + 1
                previous_bsns = self.client.read("signal_input_bsn")[
                    node_list[0] : node_list[-1] + 1
                ]

                # Processing on / reset
                self.client.write("processing_enable", [True] * self.client.n_nodes)
                time.sleep(2)

                # Wait for JESD dev_syncn (max 20s)
                timeout_time = 20.0 + time.time()
                while time.time() < timeout_time:
                    csr_dev_syncn = self.client.read("jesd204b_csr_dev_syncn")
                    sel_csr_dev_syncn = [csr_dev_syncn[sp] for sp in global_sp_list]
                    if np.count_nonzero(sel_csr_dev_syncn) < len(sel_csr_dev_syncn):
                        time.sleep(0.5)  # All sp don't have dev_syncn = 1 yet.
                    else:
                        break  # All sp have dev_syncn = 1

                # Check if BSN is increasing
                current_bsns = self.client.read("signal_input_bsn")[
                    node_list[0] : node_list[-1] + 1
                ]
                bsn_running = all([c > p for (c, p) in zip(current_bsns, previous_bsns)])

                # Read JESD stats
                jesd_stats["csr_rbd_count"].append(self.client.read("jesd204b_csr_rbd_count"))
                jesd_stats["csr_dev_syncn"].append(self.client.read("jesd204b_csr_dev_syncn"))
                jesd_stats["rx_err0"].append(self.client.read("jesd204b_rx_err0"))
                jesd_stats["rx_err1"].append(self.client.read("jesd204b_rx_err1"))

                # Read JESD DB
                samples = self.client.read("signal_input_data_buffer")
                selected_samples = []
                for (
                    _sp
                ) in (
                    global_sp_list
                ):  # Select samples for chosen ADC inputs, can be any global signal input index.
                    s1 = (_sp + 1) * C_SDP.V_si_db
                    s2 = _sp * C_SDP.V_si_db
                    selected_samples.append(samples[s2:s1])

                # Process ADC samples
                adc_h.jesd_words_2_samples(selected_samples)

                # determine clock-CW statistics (phases, amplitudes and SNRs) of the SP
                adc_h.add_clock_cw_statistics(self.cw_period)

                # Report clock-CW statistics (phases, amplitudes and SNRs) of all SP for all repeats
                adc_h.log_clock_cw_statistics(stat_str="dcs")
                adc_h.log_clock_cw_statistics(stat_str="phases")
                adc_h.log_clock_cw_statistics(stat_str="amplitudes")
                adc_h.log_clock_cw_statistics(stat_str="noisepeaks")
                adc_h.log_clock_cw_statistics(stat_str="snrs")

                elapsed_time = time.time() - start_time

        except KeyboardInterrupt:
            print(" user hit ctrl-c")

        # Plot
        self.logger.info("plot results")
        adc_h.plot_clock_cw_statistics(cycle_index, fig_nr=2, stat_str="dcs")
        adc_h.plot_clock_cw_statistics(
            cycle_index, fig_nr=3, stat_str="phases", cw_period=self.cw_period
        )
        adc_h.plot_clock_cw_statistics(cycle_index, fig_nr=4, stat_str="amplitudes")
        adc_h.plot_clock_cw_statistics(cycle_index, fig_nr=5, stat_str="noisepeaks")
        adc_h.plot_clock_cw_statistics(cycle_index, fig_nr=6, stat_str="snrs")

        # Validate
        err0_list = []
        err1_list = []
        for _sp in global_sp_list:
            err0_list.append([err[_sp] for err in jesd_stats["rx_err0"]])
            err1_list.append([err[_sp] for err in jesd_stats["rx_err1"]])

        err0_cnt = np.count_nonzero(err0_list)
        err1_cnt = np.count_nonzero(err1_list)
        self.logger.info("Done validating JESD: %d resets", cycle_index)
        self.logger.info("amount of err0: %d, amount of err1: %d", err0_cnt, err1_cnt)

        max_phases = [
            np.amax(np.transpose(adc_h.clk_cw_phases)[si]) for si in range(len(global_sp_list))
        ]
        min_phases = [
            np.amin(np.transpose(adc_h.clk_cw_phases)[si]) for si in range(len(global_sp_list))
        ]
        diff_phases = [max_p - min_p for (max_p, min_p) in zip(max_phases, min_phases)]
        self.logger.info("Difference between max/min phase offset for each channel: %s", str(diff_phases))

        correct_phases = [d < 0.01 for d in diff_phases]
        result = bsn_running and all(correct_phases) and ((err0_cnt + err1_cnt) == 0)
        if result:
            self.logger.info("PASSED")
        else:
            self.logger.error("FAILED")
            self.logger.info("err0 list = %s\nerr1 list = %s", str(err0_list), str(err1_list))
            self.logger.info("BSN running = %d", bsn_running)

        return result

    def no_wg_jesd_reset_test(self, node_list, global_sp_list, mtime):
        """Executes JESD Reset test. Captures the JESD error registers after
        every reset. Executed every rstTestCycleDuration seconds for a total time
        of mtime seconds. Same as jesd_reset_test() but without reading the data
        buffers and the clock_cw_phase checks. This no_wg_jesd_reset_test() can
        be used without the need for a common CW input signal at the receiver
        inputs."""

        jesd_stats = {
            "csr_rbd_count": [],
            "csr_dev_syncn": [],
            "rx_err0": [],
            "rx_err1": [],
        }
        cycle_index = 0
        start_time = time.time()
        start = time.time()
        elapsed_time = 0.0
        bsn_running = True
        try:
            while elapsed_time < mtime:
                while start > time.time():
                    time.sleep(0.001)
                start = start + self.rst_testcycle_duration
                cycle_index = cycle_index + 1
                previous_bsns = self.client.read("signal_input_bsn")[
                    node_list[0] : node_list[-1] + 1
                ]

                # Processing on / reset
                self.client.write("processing_enable", [True] * self.client.n_nodes)
                time.sleep(2)

                # Wait for JESD dev_sync (max 20s)
                timeout_time = 20.0 + time.time()
                while time.time() < timeout_time:
                    csr_dev_syncn = self.client.read("jesd204b_csr_dev_syncn")
                    sel_csr_dev_syncn = [csr_dev_syncn[sp] for sp in global_sp_list]
                    if np.count_nonzero(sel_csr_dev_syncn) < len(sel_csr_dev_syncn):
                        time.sleep(0.5)
                    else:
                        break

                # Check if BSN is increasing
                current_bsns = self.client.read("signal_input_bsn")[
                    node_list[0] : node_list[-1] + 1
                ]
                bsn_running = all([c > p for (c, p) in zip(current_bsns, previous_bsns)])

                # Read JESD stats
                csr_rbd_count = self.client.read("jesd204b_csr_rbd_count")
                csr_dev_syncn = self.client.read("jesd204b_csr_dev_syncn")
                rx_err0 = self.client.read("jesd204b_rx_err0")
                rx_err1 = self.client.read("jesd204b_rx_err1")

                jesd_stats["csr_rbd_count"].append(csr_rbd_count)
                jesd_stats["csr_dev_syncn"].append(csr_dev_syncn)
                jesd_stats["rx_err0"].append(rx_err0)
                jesd_stats["rx_err1"].append(rx_err1)

                # Define Error conditions
                sel_err0 = [rx_err0[sp] for sp in global_sp_list]
                sel_err1 = [rx_err1[sp] for sp in global_sp_list]
                sel_csr_dev_syncn = [csr_dev_syncn[sp] for sp in global_sp_list]
                sel_csr_rbd_count = [csr_rbd_count[sp] for sp in global_sp_list]
                csr_rbd_count_too_high = [int(n > 8) for n in sel_csr_rbd_count]

                # Log Errors
                if np.count_nonzero(sel_err0) > 0:
                    self.logger.error(f"Encountered non-zero value in rx_err0:\n{sel_err0}")
                if np.count_nonzero(sel_err1) > 0:
                    self.logger.error(f"Encountered non-zero value in rx_err1:\n{sel_err1}")
                if np.count_nonzero(csr_rbd_count_too_high) > 0:
                    self.logger.error(f"Encountered csr_rbd_count > 8:\n{sel_csr_rbd_count}")
                if np.count_nonzero(sel_csr_dev_syncn) < len(sel_csr_dev_syncn):
                    self.logger.error(
                        f"Encountered dev_sync = 0 for a selected signal input:\n{sel_csr_dev_syncn}"
                    )

                elapsed_time = time.time() - start_time

        except KeyboardInterrupt:
            print(" user hit ctrl-c")

        # Validate
        err0_list = []
        err1_list = []
        for sp in global_sp_list:
            err0_list.append([err[sp] for err in jesd_stats["rx_err0"]])
            err1_list.append([err[sp] for err in jesd_stats["rx_err1"]])

        err0_cnt = np.count_nonzero(err0_list)
        err1_cnt = np.count_nonzero(err1_list)
        self.logger.info(f"Done validating JESD: {cycle_index} resets")
        self.logger.info(f"amount of err0: {err0_cnt}, amount of err1: {err1_cnt}")

        result = bsn_running and ((err0_cnt + err1_cnt) == 0)
        if result:
            self.logger.info("PASSED")
        else:
            self.logger.error("FAILED")
            self.logger.info(f"err0 list = {err0_list}\nerr1 list = {err1_list}")
            self.logger.info(f"BSN running = {bsn_running}")

        return result


def main():
    exit_state = 0

    client = OpcuaClient(args.host, args.port)
    client.connect()
    node_list = args_node_list
    if node_list is None:
        node_list = client.node_list  # if no --nodes in arguments, select all.

    global_sp_list = args_global_sp_list
    if global_sp_list is None:
        global_sp_list = [
            C_SDP.S_pn * n + ch for n in node_list for ch in range(C_SDP.S_pn)
        ]  # if no --sp in arguments, select all.

    client.set_mask(node_list)
    logger.info("fpga mask = {}".format(client.get_mask()))  # read back nodes set.

    jesd = Jesd(client)
    try:
        if args.validate_jesd_reset:
            result = jesd.jesd_reset_test(node_list, global_sp_list, args.mtime)
            # Convert False->1 and True->0
            exit_state = int(not result)
        if args.validate_no_wg_jesd_reset:
            result = jesd.no_wg_jesd_reset_test(node_list, global_sp_list, args.mtime)
            # Convert False->1 and True->0
            exit_state = int(not result)
        if args.validate_jesd_ber:
            result = jesd.jesd_ber_test(global_sp_list, args.mtime)
            # Convert False->1 and True->0
            exit_state = int(not result)

    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(
            textwrap.dedent(
                """\
            opcua client command line argument parser:
            # Example for validating JESD on nodes 5:7 using channels 0:2 on each of the selected nodes
            jesd.py --host 10.99.0.250 -n 5:7 -r 0:2 --validate-jesd-reset --mtime 3600 -vv\n
            Alternatively, you could use --globalsp to select the ADC inputs using the global index, max(0:192).
            Note that only the corresponding nodes will be reset when using --gloablsp. \n"""
            )
        ),
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument(
        "-v", action="count", help="verbosity -v = WARNING, -vv = INFO, -vvv = DEBUG"
    )

    parser.add_argument(
        "-r",
        "--localsp",
        type=str,
        help="channel inputs on each of the selected nodes can be max 0:11",
    )
    parser.add_argument(
        "-g",
        "--globalsp",
        type=str,
        help="global signal inputs to use instead of -n/-r, this has priority over -n and -r. Can be max 0:192",
    )
    parser.add_argument(
        "--mtime",
        type=int,
        default=1,
        help=textwrap.dedent(
            """\
                set duration of validation, in seconds, default = 1"""
        ),
    )

    parser.add_argument(
        "--validate-jesd-reset",
        action="store_true",
        help="validate the jesd interface after reset repeatedly for --mtime seconds",
    )
    parser.add_argument(
        "--validate-no-wg-jesd-reset",
        action="store_true",
        help="validate the jesd interface after reset repeatedly for --mtime seconds using only the JESD status registers",
    )
    parser.add_argument(
        "--validate-jesd-ber",
        action="store_true",
        help="validate the jesd interface measuring bit error rate --mtime seconds",
    )

    args = parser.parse_args()

    args_node_list = arg_str_to_list(args.nodes) if args.nodes else None
    args_ch_list = arg_str_to_list(args.localsp) if args.localsp else None
    args_global_sp_list = arg_str_to_list(args.globalsp) if args.globalsp else None

    if args_global_sp_list is not None:
        args_node_list = list(np.sort(np.unique([i // 12 for i in args_global_sp_list])))
    elif args_ch_list is not None:
        args_global_sp_list = [C_SDP.S_pn * n + ch for n in args_node_list for ch in args_ch_list]
    else:  # select all sp for all selected nodes
        args_global_sp_list = [
            C_SDP.S_pn * n + ch for n in args_node_list for ch in range(C_SDP.S_pn)
        ]

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")
    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
