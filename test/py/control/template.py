#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . This is the template script to read/write opc-ua server points
# . Is can be also used as bases for new scripts.
#   . Make copy of this script and rename it.
#   . Add needed arguments in the parser.
#   . Add test code under "while ClientRunning".
# Description:
# . run ./template.py -h for help
# ##########################################################################


import sys
import logging
import argparse

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class ClassName:
    def __init__(self, client):
        self.client = client
    
    def func(self):
        pass


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    class_name = ClassName(client)
    class_name.func()

    while True:
        try:
            #
            # add test here
            #
            pass
        except BaseException as err:
            exit_state = handle_exception(err)
            if exit_state == 1:
                break

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument("--host", dest="host", type=str, default="dop36", help="host to connect to")
    parser.add_argument("--port", dest="port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", dest="nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")
    #
    # add needed parse arguments
    #

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
