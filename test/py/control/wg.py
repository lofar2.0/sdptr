#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . set Waveform generator
# Description:
# . run ./wg.py -h for help
#
# ##########################################################################

import sys
import logging
import time
import pprint
import math

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list
from base.constants import C_SDP


class WaveformGenerator:
    def __init__(self, client):
        self.client = client
        self.logger = logging.getLogger("WaveformGenerator")
        self.n_nodes = self.client.read(
            "sdp_config_nof_fpgas"
        )  # get number of nodes from server
        self.s_ant = C_SDP.S_pn * self.n_nodes
        self._ampl = None
        self._phase = None
        self._freq = None

    def set_ampl(self, value, mode=None, ampl_min=0):
        """ Set WG amplitude = value, 1.0 is full scale.

        The value is maximum amplitude in case of range of WG amplitudes down to ampl_min.
        Minimum amplitude > 0 is ampl_min = 1 / 2**(C_SDP.W_adc - 1) = 1/(2**13) = 0.000122.
        """
        ampl_max = value
        if ampl_max < ampl_min:
            ampl_max = ampl_min
            self.logger.warning(f"set wg amplitude is too small, force ampl_max = ampl_min = {ampl_max}")
        ampl_span = ampl_max - ampl_min
        if ampl_span > 0:
            self.logger.info(f"set wg amplitude range: {ampl_max} downto {ampl_min}")
        else:
            self.logger.info(f"set wg amplitude: {ampl_max}")
        mode = "sinus" if mode is None else mode.lower()
        if mode == "xstwide":
            # decrement amplitude from ampl_max to ampl_min) over all s_ant
            val_range = range(self.s_ant)
            val_list = [ampl_span * (1 - (i / (self.s_ant))) + ampl_min for i in val_range]
            self.client.write("wg_amplitude", val_list)
        elif mode == "xst":
            # decrement amplitude from ampl_max to ampl_min over S_pn per node, so same range for each node
            val_range = range(C_SDP.S_pn)
            val_list = [ampl_span * (1 - (i / (C_SDP.S_pn))) + ampl_min for i in val_range] * self.n_nodes
            self.client.write("wg_amplitude", val_list)
        else:
            # use same amplitude = ampl_max for all s_ant
            val_list = [ampl_max] * self.s_ant
            self.client.write("wg_amplitude", val_list)
        return val_list

    def get_ampl(self):
        self._ampl = self.client.read("wg_amplitude")
        return self._ampl

    def set_phase(self, value, mode=None):
        """Set WG phase in degrees. Maximum phase in case of range of WG phases down to almost 0."""
        phase_rad = value / (360 / (2 * math.pi))
        self.logger.info(f"set wg phase: {phase_rad} rad")
        mode = "sinus" if mode is None else mode.lower()
        if mode == "xstwide":
            # increment phase from 0 to almost 360 over all s_ant
            val_range = range(self.s_ant)
            val_list = [i * (phase_rad / (self.s_ant)) for i in val_range]
            self.client.write("wg_phase", val_list)
        elif mode == "xst":
            # increment phase from 0 to almost 360 over S_pn per node, so same range for each node
            val_range = range(C_SDP.S_pn)
            val_list = [i * (phase_rad / C_SDP.S_pn) for i in val_range] * self.n_nodes
            self.client.write("wg_phase", val_list)
        else:
            # use same phase for all s_ant
            val_list = [phase_rad] * self.s_ant
            self.client.write("wg_phase", val_list)
        return val_list  # phases in rad

    def get_phase(self):
        self._phase = self.client.read("wg_phase")
        return [i * (360 / (2 * math.pi)) for i in self._phase]

    def set_freq(self, value, mode=None):
        """Set WG freq in Hz. First freq in case of range of WG frequencies that increment by f_sub.

        Frequencies f_adc/2 < freq < f_adc alias to f_adc/2 - freq.
        Frequencies freq + n * f-adc alias to freq.
        """
        freq = value
        self.logger.info(f"set wg freq: {freq} Hz")
        mode = "sinus" if mode is None else mode.lower()
        if mode == "sstwide":
            # increment frequency by f_sub over all s_ant, starting at freq
            val_range = range(self.s_ant)
            val_list = [freq + i * C_SDP.f_sub for i in val_range]
            self.client.write("wg_frequency", val_list)
        elif mode == "sst":
            # increment frequency by f_sub over all S_pn per node, starting at freq, so same range for each node
            val_range = range(C_SDP.S_pn)
            val_list = [freq + i * C_SDP.f_sub for i in val_range] * self.n_nodes
            self.client.write("wg_frequency", val_list)
        else:
            # use same freq for all s_ant
            val_list = [freq] * self.s_ant
            self.client.write("wg_frequency", val_list)
        # pprint.pprint(val_list)
        return val_list

    def get_freq(self):
        self._freq = self.client.read("wg_frequency")
        return self._freq

    def set_sinus(self, ampl, phase, freq):
        self.logger.info("set sinus mode")
        self.set_ampl(ampl, "sinus")
        self.set_phase(phase, "sinus")
        self.set_freq(freq, "sinus")

    def set_xst_mode(self, ampl, phase, freq, widemode=None, ampl_min=0):
        # Write WG configuration with phases from 0 - 360 and 1 to 1/12 amplitudes
        mode = "xst" if widemode is None else widemode
        self.logger.info("set xst mode")
        ampl_list = self.set_ampl(ampl, mode, ampl_min)
        phase_list = self.set_phase(phase, mode)
        freq_list = self.set_freq(freq, mode)
        return {"ampl": ampl_list, "phase": phase_list, "freq": freq_list}

    def enable(self):
        if self.client.write("wg_enable", ([True] * self.s_ant)):
            return True
        return False

    def disable(self):
        if self.client.write("wg_enable", ([False] * self.s_ant)):
            return True
        return False


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    wg = WaveformGenerator(client)

    while True:
        try:
            if args.toggle or args.disable:
                print("turn off wg signal")
                wg.disable()

            if args.setfreq:
                wg.set_freq(args.setfreq, args.mode)
            if args.setampl:
                wg.set_ampl(args.setampl, args.mode)
            if args.setphase:
                wg.set_phase(args.setphase, args.mode)

            if args.getfreq:
                pprint.pprint(wg.get_freq(), width=100, compact=True)
            if args.getampl:
                pprint.pprint(wg.get_ampl(), width=100, compact=True)
            if args.getphase:
                pprint.pprint(wg.get_phase(), width=100, compact=True)

            if args.toggle or args.enable:
                print("turn on wg signal")
                wg.enable()
                print("wait until wg active again")
                time.sleep(2.0)  # wain until active
            break
        except BaseException as err:
            exit_state = handle_exception(err)
            if exit_state == 1:
                break

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(
            textwrap.dedent(
                """\
            opcua client command line argument parser:
            * command line examples:
              wg.py --host 10.99.0.250 --port 4842 --setphase 0 --setfreq 19921875 --setampl 0.5 --enable
              wg.py --host 10.99.0.250 --port 4842 --mode sst --setfreq 19921875 --setampl 1.0 --setphase 0 --enable
              wg.py --host 10.99.0.250 --port 4842 --disable
            * mode
              . frequency range: starting at setfreq and incrementing by f_sub
              . amplitude range: starting at setampl and decrementing to ampl_min = 1 LSbit = 1/(2**13) = 0.000122
              . phase range: 360 degrees in s_pn or s_ant steps
              sst     : range of subband frequencies, incrementing over s_pn, same range for each node
              sstwide : range of subband frequencies incrementing over all s_ant
              xst     : range of decreasing ampl and 360 degrees phase over s_pn, same range for each node
              xstwide : range of decreasing ampl and 360 degrees phase over all s_ant
              sinus   : same ampl, phase, freq for all s_ant
            * To set WG stimuli for XST plot with range of ampl and phases:
              # . setfreq 19921875, for subband 102 * f_sub = 102 * 195312.5 = 19921875 Hz
              # . setampl 0.25, for maximum amplitude of FS / 4 (FS = full scale) for crosslets
              wg.py --host 10.99.0.250 --port 4842 --mode xstwide --setfreq 19921875 --setampl 0.25 --setphase 0
              wg.py --host 10.99.0.250 --port 4842 --toggle"""
            )
        ),
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "--host", type=str, default="localhost", help="host to connect to"
    )
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument(
        "-v", action="count", help="verbosity 'WARNING','INFO','DEBUG' -v,-vv,-vvv"
    )

    parser.add_argument(
        "--mode",
        type=str,
        choices=["sst", "sstwide", "xst", "xstwide", "sinus"],
        help="select wg mode",
    )
    parser.add_argument("--setfreq", type=float, help="set wg freq")
    parser.add_argument("--setampl", type=float, help="set wg ampl")
    parser.add_argument("--setphase", type=float, help="set wg phase in degrees")
    parser.add_argument("--getfreq", action="store_true", help="get wg freq")
    parser.add_argument("--getampl", action="store_true", help="get wg ampl")
    parser.add_argument(
        "--getphase", action="store_true", help="get wg phase in degrees"
    )
    parser.add_argument("--enable", action="store_true", help="turn on wg signal")
    parser.add_argument("--disable", action="store_true", help="turn off wg signal")
    parser.add_argument(
        "--toggle", action="store_true", help="turn the wg signal off and on again"
    )

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
