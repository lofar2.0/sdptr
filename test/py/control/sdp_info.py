#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . read / write sdp info settings
# Description:
# . run ./sdp_rw.py -h for help
#   . ./sdp_info.py --host dop36 --help  # print help for all available arguments
# ##########################################################################


import sys
import logging
import argparse

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list


class SdpInfo:
    def __init__(self, client):
        self.client = client

    @property
    def station_id(self):
        return self.client.read("sdp_info_station_id")

    @station_id.setter
    def station_id(self, id):
        if max(id) < 2**10:  # < uint16
            self.client.write("sdp_info_station_id", id)
        else:
            logger.error("Station id (%d) out of range", max(id))

    @property
    def antenna_field_index(self):
        return self.client.read("sdp_info_antenna_field_index")

    @antenna_field_index.setter
    def antenna_field_index(self, id):
        if max(id) < 2**6:  # < uint16
            self.client.write("sdp_info_antenna_field_index", id)
        else:
            logger.error("Antenna field index (%d) out of range", max(id))

    @property
    def observation_id(self):
        return self.client.read("sdp_info_observation_id")

    @observation_id.setter
    def observation_id(self, id):
        if max(id) < 2**32:  # < uint32
            self.client.write("sdp_info_observation_id", id)
        else:
            logger.error("Observation id (%d) out of range", max(id))

    @property
    def nyquist_sampling_zone_index(self):
        return self.client.read("sdp_info_nyquist_sampling_zone_index")

    @nyquist_sampling_zone_index.setter
    def nyquist_sampling_zone_index(self, index):
        valid_index = [0, 1, 2]
        if min(index) in valid_index and max(index) in valid_index:
            self.client.write("sdp_info_nyquist_sampling_zone_index", index)
        else:
            logger.error("Nyquist sampling zone index (%d) out of range", max(index))

    @property
    def antenna_band_index(self):
        return self.client.read("sdp_info_antenna_band_index")

    @antenna_band_index.setter
    def antenna_band_index(self, index):
        valid_index = [0, 1]
        if min(index) in valid_index and max(index) in valid_index:
            self.client.write("sdp_info_antenna_band_index", index)
        else:
            logger.error("Antenna band index (%d) out of range", max(index))

    @property
    def f_adc(self):
        return self.client.read("sdp_info_f_adc")

    @property
    def fsub_type(self):
        return self.client.read("sdp_info_fsub_type")

    @property
    def block_period(self):
        return self.client.read("sdp_info_block_period")

    def test_control(self, nodelist):
        nodelist = self.client.node_list if nodelist is None else nodelist
        # save actual values
        _station_id = self.station_id
        _observation_id = self.observation_id
        _nyquist_sampling_zone_index = self.nyquist_sampling_zone_index
        _antenna_band_index = self.antenna_band_index

        # do the test
        def valid_response(val, t_val):
            # check for valid response if node is selected
            sz_per_node = len(t_val) // self.client.n_fpgas
            for node in self.client.node_list:
                if node in nodelist:
                    node_nr = node % self.client.n_fpgas
                    val_begin = node_nr * sz_per_node
                    val_end = val_begin + sz_per_node
                    if t_val[val_begin:val_end] != val[val_begin:val_end]:
                        return False
            return True

        succes = True

        t_station_id = [10101] * self.client.n_nodes
        self.station_id = t_station_id
        if not valid_response(self.station_id, t_station_id):
            logger.error("test sdp_info.station_id failed")
            succes = False

        t_observation_id = [110011] * self.client.n_nodes
        self.observation_id = t_observation_id
        if not valid_response(self.observation_id, t_observation_id):
            logger.error("test sdp_info.observation_id failed")
            succes = False

        t_nyquist_sampling_zone_index = [int(not i) for i in _nyquist_sampling_zone_index]
        self.nyquist_sampling_zone_index = t_nyquist_sampling_zone_index
        if not valid_response(self.nyquist_sampling_zone_index, t_nyquist_sampling_zone_index):
            logger.error("test sdp_info.nyquist_sampling_zone_index failed")
            succes = False

        t_antenna_band_index = [int(not i) for i in _antenna_band_index]
        self.antenna_band_index = t_antenna_band_index
        if not valid_response(self.antenna_band_index, t_antenna_band_index):
            logger.error("test sdp_info.antenna_band_index failed")
            succes = False

        # set back saved values
        self.station_id = _station_id
        self.observation_id = _observation_id
        self.nyquist_sampling_zone_index = _nyquist_sampling_zone_index
        self.antenna_band_index = _antenna_band_index
        return succes


def main():
    exit_state = 0
    client = OpcuaClient(args.host, args.port)
    client.connect()

    client.set_mask(node_list)
    logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

    sdp_info = SdpInfo(client)

    try:
        if args.station_id == "R":
            print(f"Station-ID: {sdp_info.station_id}")
        elif args.station_id is not None:
            sdp_info.station_id = eval(args.station_id)

        if args.antenna_field_index == "R":
            print(f"Antenna field index: {sdp_info.antenna_field_index}")
        elif args.antenna_field_index is not None:
            sdp_info.antenna_field_index = eval(args.antenna_field_index)

        if args.observation_id == "R":
            print(f"Observation-ID: {sdp_info.observation_id}")
        elif args.observation_id is not None:
            sdp_info.observation_id = eval(args.observation_id)

        if args.nyquist_idx == "R":
            print(f"Nyquist sampling zone index: {sdp_info.nyquist_sampling_zone_index}")
        elif args.nyquist_idx is not None:
            sdp_info.nyquist_sampling_zone_index = eval(args.nyquist_idx)

        if args.antenna_band_index == "R":
            print(f"Antenna band index: {sdp_info.antenna_band_index}")
        elif args.antenna_band_index is not None:
            sdp_info.antenna_band_index = eval(args.antenna_band_index)

        if args.f_adc:
            print(f"F adc: {sdp_info.f_adc}")

        if args.fsub_type:
            print(f"fsub type: {sdp_info.fsub_type}")

        if args.block_period:
            print(f"Block period: {sdp_info.block_period}")

        if args.test_control:
            if sdp_info.test_control(node_list):
                print("SUCCES")
            else:
                print("FAILURE")

    except BaseException as err:
        exit_state = handle_exception(err)

    client.disconnect()
    return exit_state


if __name__ == "__main__":
    # Parse command line arguments
    parser = argparse.ArgumentParser(description="opcua client command line argument parser")
    parser.add_argument("--host", dest="host", type=str, default="dop36", help="host to connect to")
    parser.add_argument("--port", dest="port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", dest="nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument(
        "--antenna-field-index",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="get or set antenna-field-index for all nodes",
    )
    parser.add_argument(
        "--station-id",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="get or set station-id for all nodes",
    )
    parser.add_argument(
        "--observation-id",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="get or set observation-id for all nodes",
    )
    parser.add_argument(
        "--nyquist-idx",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="get or set nyquist-sampling-zone-index for all nodes",
    )
    parser.add_argument(
        "--antenna-band-index",
        nargs="?",
        type=str,
        const="R",
        default=None,
        help="get or set antenna-band-index for all nodes",
    )

    parser.add_argument("--f-adc", action="store_true", help="get f-adc for all nodes")
    parser.add_argument("--fsub-type", action="store_true", help="get fsub-type for all nodes")
    parser.add_argument(
        "--block-period", action="store_true", help="get block-period for all nodes"
    )
    parser.add_argument(
        "--test-control", action="store_true", help="test control points for all nodes"
    )

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    sys.exit(main())
