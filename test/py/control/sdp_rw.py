#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . read / write actions to all available opc-ua points
# Description:
# . run ./sdp_rw.py -h for help
#   . ./sdp_rw .py --host dop36 --list  # print all available opc-ua points
#   . ./sdp_rw .py --host dop36 --read wg_enable  # read wg_enable status
#   . ./sdp_rw .py --host dop36 --write wg_enable [True]*(12*16)  # write wg_enable = True to all wg
# ##########################################################################


import sys
import logging
import math

from base.handle_exceptions import handle_exception
from base.opcua_client import OpcuaClient
from base.base_tools import arg_str_to_list

clientRunning = True


def print_result(info, success):
    if success:
        logger.info(f"{info}: PASSED")
        print(f"{info}: PASSED")
    else:
        logger.info(f"{info}: FAILED")
        print(f"{info}: FAILED")


def main():
    exit_state = 0
    with OpcuaClient(args.host, args.port) as client:

        # get available nodes, make fpga_mask, and set it.
        nodelist = node_list
        if nodelist is None:
            nodelist = client.node_list  # if no --nodes in arguments select all

        client.set_mask(nodelist)  # write fpga_mask for write actions
        logger.info("fpga mask={}".format(client.get_mask()))  # read back nodes set.

        try:
            if args.list is not None:
                search_str = None if args.list == [] else args.list[0]
                cols = 2

                write_names = sorted(list(client.valid_write_names()))
                read_names = sorted(list(client.valid_read_names()))
                readonly_names = [n for n in read_names if n not in write_names]

                if search_str:
                    print('=== search for points with "{}" in the name ===\n'.format(search_str))
                    cols = 1
                    rw_points = [n for n in write_names if search_str in n]
                    ro_points = [n for n in readonly_names if search_str in n]
                else:
                    rw_points = write_names
                    ro_points = readonly_names

                groups = [rw_points, ro_points]
                group_headers = [
                    "=== Available OPC-ua read-write points ===",
                    "=== Available OPC-ua read-only points ===",
                ]

                for group, groupheader in zip(groups, group_headers):
                    rows = math.ceil(len(group) / cols)
                    print(groupheader)
                    for i in range(rows):
                        for j in range(cols):
                            try:
                                name = group[i * cols + j]
                                dim = client.get_dim(name)
                                dtype = client.get_datatype(name).lower()
                                info = name + " = " + str(dim) + "(" + dtype + ")"
                                print("  {:60s}".format(info), end="")
                            except IndexError:
                                pass
                        print("")
                    print("")

            if args.read:
                data = client.read(args.read[0])
                success = False if data is None else True
                if not success:
                    print(f"opcua.read {args.read[0]} not available")
                else:
                    if not isinstance(data, list):
                        data = [data]
                    if len(data) == 1:
                        # TR point
                        fstr = f"read {args.read[0]}: {str(data[0]):4s}"
                        print(fstr)
                        logger.info(fstr)

                    elif len(data) < client.n_nodes:
                        # TR point
                        fstr = f"read {args.read[0]}: {str(data)}"
                        print(fstr)
                        logger.info(fstr)
                    else:
                        # FPGA point
                        fstr = f"read {args.read[0]}:"
                        print(fstr)
                        logger.info(fstr)
                        cols = len(data) // client.n_nodes
                        line = []
                        col = 0
                        node = client.gn_first_fpga
                        for d in data:
                            if node not in nodelist:
                                node += 1
                                continue
                            line.append(f"{str(d):4s}")
                            col += 1
                            if col == cols:
                                fstr = f"node {node:2d}:  {' '.join(line)}"
                                print(fstr)
                                logger.info(fstr)
                                col = 0
                                line = []
                                node += 1
                print_result(f"opcua.read {args.read[0]}", success)
                exit_state = int(not success)

            if args.write:
                print(f"write {args.write[0]}:")
                data = list(eval(args.write[1]))
                cols = len(data) // client.n_nodes
                line = []
                col = 0
                node = client.gn_first_fpga
                for d in data:
                    line.append(f"{str(d):4s}")
                    col += 1
                    if col == cols:
                        print(f'node {node:2d}:  {" ".join(line)}')
                        col = 0
                        line = []
                        node += 1
                success = client.write(args.write[0], data)
                print_result(f"opcua.write {args.write[0]}", success)
                exit_state = int(not success)

        except BaseException as err:
            exit_state = handle_exception(err)

    return exit_state


if __name__ == "__main__":
    import argparse
    import textwrap

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(textwrap.dedent("""\
            opcua client command line argument parser, examples:

            * sdp_rw.py -h
            * sdp_rw.py --host 10.99.0.250 --port 4842 -l
            * sdp_rw.py --host 10.99.0.250 --port 4842 -l xst
            * sdp_rw.py --host 10.99.0.250 --port 4842 -r beamlet_output_hdr_eth_source_mac
            * sdp_rw.py --host 10.99.0.250 --port 4842 -w beamlet_output_hdr_eth_source_mac "['12:34:56:78:9A:BC']*16"
            * sdp_rw.py --host 10.99.0.250 --port 4842 -r beamlet_output_hdr_ip_destination_address
            * sdp_rw.py --host 10.99.0.250 --port 4842 -w beamlet_output_hdr_ip_destination_address "['1.2.3.4']*16"
            * sdp_rw.py --host 10.99.0.250 --port 4842 -r xst_subband_select
            * sdp_rw.py --host 10.99.0.250 --port 4842 -w xst_subband_select [0,1,2,3,4,5,6,7]*16
            * sdp_rw.py --host 10.99.0.250 --port 4842 -w bf_ring_rx_clear_total_counts [True]*16
            * sdp_rw.py --host 10.99.0.250 --port 4842 -r bf_ring_nof_transport_hops
            * sdp_rw.py --host 10.99.0.250 --port 4842 -w bf_ring_nof_transport_hops [0]*16
            * sdp_rw.py --host 10.99.0.250 --port 4842 -r ring_use_cable_to_previous_rn
            * sdp_rw.py --host 10.99.0.250 --port 4842 -w ring_use_cable_to_previous_rn [False,False,False,False,False,False,False,True,False,False,False,True,False,False,False,True]
            \n""")),
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("--host", type=str, default="localhost", help="host to connect to")
    parser.add_argument("--port", type=int, default=4840, help="port to use")
    parser.add_argument("-n", "--nodes", type=str, help="nodes to use")
    parser.add_argument("-v", action="count", help="verbosity -v=WARNING, -vv=INFO, -vvv=DEBUG")

    parser.add_argument(
        "-l",
        "--list",
        nargs="*",
        type=str,
        help="list all available opc-ua point names, also search if part of name is given",
    )
    parser.add_argument("-r", "--read", nargs=1, type=str, help="read point_name")
    parser.add_argument("-w", "--write", nargs=2, type=str, help="write point_name value")

    args = parser.parse_args()

    node_list = arg_str_to_list(args.nodes) if args.nodes else None

    verbosity_nr = 0 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info("parsed arguments: {}".format(args))

    exit_state = main()
    sys.exit(exit_state)
