### Python SDP control using OPC-ua.

Python scripts to test the "SDP Translator" (OPC-ua to SDP).

Source init_sdptr.sh in git/sdptr to get access to all script everywhere.
```
cd git/sdptr
. ./init_sdptr.sh
```

#### test/py  directory.

This directory holds test/control scripts to write/read all OPC-ua points.

```
sdptr/test/py    # main directory, where this file is located
./base           # directory with basic functionality
./control        # all specific scripts with a class inside to control opcua points.
./sdp_rw.py      # universal script to read/write all available OPC-ua points.
```

In the control directory there is also a *template.py* file for new specific scripts. New scripts based on this template can be run on the command line, but the class inside can also be used in other scripts.

#### Make new specific script using the template.

1. Make a copy of template.py and rename it.
2. Add the specific class (rename ClassName) and all necessary functions.
3. Add your test or control under "while clientRunning" using the class made by 2.
4. Add needed arguments to the parser.

#### Using "sdp_rw.py".

sdp_rw.py can be used to direct read/write OPC-ua points.

List all available read/write and read-only points.
`./sdp_rw.py --host dop369 --list`  

Print wg_enable status.
`./sdp_rw.py --host dop369 --read wg_enable`   

Write list to wg_enable.
`./sdp_rw.py --host dop369 --write wg_enable [True]*16`   

#### Using a specific script(class) in other python scripts.

Minimal code to control, as example the waveform generator class is used. 

```
from opcua_ctl.control import *

client = base.OpcuaClient(args.host, args.port)
client.connect()

client.set_mask([1,3,5])   # only fpga 1,3 and 5 are selected to be writen
print("fpga mask={}".format(client.get_mask()))  # read back nodes set.

wg = WaveformGenerator(client)
wg.set_ampl(0.1, mode='sinus')
wg.set_phase(0.0, mode='sinus')
wg.set_freq(1e6, mode='sinus')
wg.enable()
...
wg.disable()
client.disconnect()
```

#### Test scripts in the control dir.

```
beamlet_stream.py     # control beamlet stream.
statistics_stream.py  # control statistics streams (SST, BST and XST).
subband_weights.py    # set/get beamlet weigts.
wg.py                 # control waveform generator.
plot_input_data.py    # receive input data and make a plot from it.
plot_histogram.py     # receive histogram data and make a plot from it.
firmware.py           # reboot, read, write, verify an image.
```

#### Handy one-liners

close hanging scripts
`kill $(ps -aux | grep 'git/sdptr' | grep -v grep | awk '{print $2}')`