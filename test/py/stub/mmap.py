# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# Read mmap, convert to zmmap for usage in ROM_SYSTEM_INFO.
# ##########################################################################

import logging
import zlib


class MMap:
    """ MMap class
    This class reads the *.mmap file, and makes a zipped version that can be used
    in the ROM_SYSTEM_INFO register, that is be send to the client (sdptr)
    """
    def __init__(self):
        self.logger = logging.getLogger("RegMap")
        self._filename = None
        self._mmap = None
        self._zmmap = None

    @property
    def mmap(self):
        return self._mmap

    @property
    def zmmap(self):
        if self._zmmap is None:
            self.zip_mmap()
        return self._zmmap

    def read_mmap(self, filename):
        """read_mmap()
        - read *.mmap to get register info
        """
        self.logger.info(f"read register map from {filename}")
        self._filename = filename
        self._mmap = ""
        with open(self._filename, "r") as fd:
            self._mmap = fd.read()  # .encode("utf-8")

    def zip_mmap(self):
        self.logger.info("make zipped mmap for ROM_SYSTEM_INFO")
        if self._mmap:
            data = []
            # delete comment en spaces first.
            for line in self._mmap.splitlines():
                if line.startswith("#"):
                    continue
                line = line.replace("b[", "")
                line = line.replace("]", "")
                line = [f"{i.strip()}" for i in line.split()]
                data.append(" ".join(line))
            mmap = "\n".join(data)
            self.logger.debug(f"{len(mmap)} bytes to zip")
            # print(f"{mmap}")
            self._zmmap = zlib.compress(mmap.encode("utf-8"), -1)
