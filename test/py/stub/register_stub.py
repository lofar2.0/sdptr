# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# Emulate all registers.
# . Register class, used as storage of values.
#
# ##########################################################################

import logging
import struct
from array import array
from copy import deepcopy


class RegisterStub:
    """Register class
    This class emulates all registers from the mmap and can store a flash image.
    Some registers are filled with some data to check.
    """

    def __init__(self, mmap, gn_id):
        self.logger = logging.getLogger("Register")
        self.mmap = mmap  # regmap is class
        self.gn_id = gn_id
        # self.reg and self.name, dict with register info
        self.reg = {}
        self.name = {}
        # flash info: 320 sectors, 1024 pages/sector, 64 words/page
        self.flash_mem = array("L")
        self.flash_mem.fromlist([0xFFFFFFFF] * (320 * 1024 * 64))
        self.flash_addr = 0
        self.parse_mmap()
        self.preset_reg_info()

    def parse_mmap(self):
        """
        parse mmap and get the setting needed to write/read all register addresses
        info in: self.regmap and self.name
        """
        mmap = self.mmap.mmap

        last_port_name = ""
        port_name = ""
        n_perph = 0
        n_ports = 0
        port_type = ""
        field_name = ""
        address = 0
        n_fields = 0
        access_mode = ""
        radix = ""
        peripheral_span = 0
        mm_port_span = 0

        for line in mmap.splitlines():
            if line.startswith("#"):
                continue
            if "fpga_name" in line:
                continue
            if "number_of_columns" in line:
                continue

            line_list = line.strip().split()

            port_name = port_name if line_list[0] == "-" else line_list[0]
            n_perph = n_perph if line_list[1] == "-" else line_list[1]
            n_ports = n_ports if line_list[2] == "-" else line_list[2]
            port_type = port_type if line_list[3] == "-" else line_list[3]
            field_name = field_name if line_list[4] == "-" else line_list[4]
            address = address if line_list[5] == "-" else int(line_list[5], base=16)
            n_fields = n_fields if line_list[6] == "-" else int(line_list[6])
            access_mode = access_mode if line_list[7] == "-" else line_list[7]
            radix = line_list[8]
            if port_name == last_port_name:
                peripheral_span = peripheral_span if line_list[11] == "-" else int(line_list[11])
                mm_port_span = mm_port_span if line_list[12] == "-" else int(line_list[12])
            else:
                peripheral_span = 1 if line_list[11] == "-" else int(line_list[11])
                mm_port_span = 1 if line_list[12] == "-" else int(line_list[12])
            last_port_name = port_name
            if radix == "-":
                continue

            for i in range(int(n_perph)):
                for j in range(int(n_ports)):
                    addr = address + i * peripheral_span + j * mm_port_span  # in mm words
                    addr = addr * 4  # now addr in bytes
                    if addr in self.reg:
                        continue

                    if radix == "char8":
                        n_regs = n_fields // 4
                    elif radix in ["uint64", "int64"]:
                        n_regs = n_fields * 2
                    elif radix == "cint64":
                        n_regs = n_fields * 4
                    else:
                        n_regs = n_fields

                    data = array("L")
                    data.fromlist([0] * n_regs)

                    self.reg[addr] = {
                        "port_name": port_name,
                        "inst_nr": i,
                        "port_nr": j,
                        "port_type": port_type,
                        "field_name": field_name,
                        "access_mode": access_mode,
                        "radix": radix,
                        "n_fields": n_fields,
                        "n_regs": n_regs,
                        "data": deepcopy(data),
                    }
                    name = f"{port_name}-{field_name}"
                    self.name[name] = addr

    def find_base_addr(self, addr):
        """If data size is to big to send/receive in one message, an offset is used.
        This functions returns the base address of addr
        """
        for base in self.reg:
            if addr == base:
                return base
            if addr > base:
                end = base + self.reg[base]["n_regs"] * 4
                if addr < end:
                    return base
        return None

    def name2addr(self, portname, fieldname):
        try:
            name = f"{portname}-{fieldname}"
            addr = self.name[name]
            return addr
        except IndexError:
            return None

    def bytestr2u32(self, nvalues, bytestr, rev=None):
        rev = True if rev is None else rev
        u32 = []
        n_zeros_to_add = 4 - (len(bytestr) % 4)
        for _ in range(n_zeros_to_add):
            bytestr += b"\0"
        n_bytes = len(bytestr)
        for i in range(n_bytes // 4):
            begin = i * 4
            end = begin + 4
            ch4 = bytestr[begin:end]
            rev_ch4 = ch4[::-1]
            if rev:
                u32.append(struct.unpack("I", rev_ch4)[0])
            else:
                u32.append(struct.unpack("I", ch4)[0])
        return u32

    #
    # functions to change the data stored in RO registers
    #

    def set_reg_mmap(self, zmmap):
        addr = self.name2addr("ROM_SYSTEM_INFO", "data")
        nvals = self.reg[addr]["n_regs"]
        data = self.bytestr2u32(nvals, zmmap)
        d = array("L")
        d.fromlist(data)
        self.reg[addr]["data"][0 : len(d)] = d  # regmap

    def set_reg_system_info(self):
        addr = self.name2addr("PIO_SYSTEM_INFO", "info")
        data = self.gn_id  # gn index
        data |= 1 << 8  # hw version 1=unb2b, 2=unb2c
        data |= 0 << 10  # cs info
        data |= 0 << 16  # firmware major
        data |= 1 << 20  # firmware minor
        data |= 3 << 24  # rom version
        data |= 0 << 27  # info technology
        self.reg[addr]["data"][0] = data

    def set_reg_design_name(self, name):
        addr = self.name2addr("PIO_SYSTEM_INFO", "design_name")
        nvals = self.reg[addr]["n_regs"]
        data = self.bytestr2u32(nvals, name.encode("utf-8"), rev=False)
        arr = array("L")
        arr.fromlist(data)
        self.reg[addr]["data"][0 : len(arr)] = arr

    def set_reg_date(self, date):
        addr = self.name2addr("PIO_SYSTEM_INFO", "stamp_date")
        self.reg[addr]["data"][0] = date

    def set_reg_time(self, time):
        addr = self.name2addr("PIO_SYSTEM_INFO", "stamp_time")
        self.reg[addr]["data"][0] = time

    def set_reg_commit(self, commit):
        addr = self.name2addr("PIO_SYSTEM_INFO", "stamp_commit")
        nvals = self.reg[addr]["n_regs"]
        data = self.bytestr2u32(nvals, commit.encode("utf-8"), rev=False)
        arr = array("L")
        arr.fromlist(data)
        self.reg[addr]["data"][0 : len(arr)] = arr

    def set_reg_temp(self, temp):
        addr = self.name2addr("REG_FPGA_TEMP_SENS", "temp")
        # see: https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/ug_alttemp_sense.pdf
        val = round(((temp + 265.0) * 1024.0) / 693.0)
        self.reg[addr]["data"][0] = val

    def set_reg_pps(self, capture, expected, offset):
        addr = self.name2addr("PIO_PPS", "capture_cnt")
        val = int(capture)
        self.reg[addr]["data"][0] = val

        addr = self.name2addr("PIO_PPS", "expected_cnt")
        val = int(expected)
        self.reg[addr]["data"][0] = val

        addr = self.name2addr("PIO_PPS", "offset_cnt")
        val = int(offset)
        self.reg[addr]["data"][0] = val

    def preset_reg_info(self):
        self.set_reg_mmap(self.mmap.zmmap)
        self.set_reg_system_info()
        self.set_reg_design_name("uniboard_stub")
        self.set_reg_date(19630629)
        self.set_reg_time(101112)
        self.set_reg_commit(":git-hash:")
        self.set_reg_temp(25.0)
        self.set_reg_pps(200000000, 200000000, 2000000)

    #
    # functions to read/write/flash all RW registers
    #

    def write(self, addr, nregs, data):
        base = self.find_base_addr(addr)
        if base is None:
            self.logger.warning(f"{addr} not in regmap")
            return False

        if self.reg[base]["access_mode"] not in ["WO", "RW"]:
            self.logger.warning(f"{addr} not writable")
            return False

        if nregs > self.reg[base]["n_regs"]:
            self.logger.warning(f"0x{base:08x}, has not enough regs available to write {nregs}")
            return False

        if self.reg[base]["port_name"] in ["REG_EPCS", "REG_DPMM_CTRL", "REG_MMDP_CTRL"]:
            self.flash(base, nregs, data)
            return True

        d = array("L")
        d.fromlist(data)
        begin = (addr - base) // 4
        end = begin + nregs
        self.reg[base]["data"][begin:end] = d
        return True

    def read(self, addr, nregs):

        base = self.find_base_addr(addr)
        if base is None:
            self.logger.warning(f"{addr} not in regmap")
            return False, []

        if self.reg[base]["access_mode"] not in ["RO", "RW"]:
            self.logger.warning(f"{addr} not readable")
            return False, []

        if nregs > self.reg[base]["n_regs"]:
            self.logger.warning(f"0x{base:08x}, has not enough regs available to read {nregs}")
            return False, []

        if self.reg[base]["port_name"] in ["REG_EPCS", "REG_DPMM_CTRL", "REG_MMDP_CTRL"]:
            return True, self.flash(base, nregs, [])

        begin = (addr - base) // 4
        end = begin + nregs
        return True, self.reg[base]["data"][begin:end].tolist()

    def flash(self, addr, nregs, data):
        if self._reginfo[addr]["port_name"] == "REG_EPCS":
            if self._reginfo[addr]["field_name"] == "addr":
                self.flash_addr = int(data)
            elif self._reginfo[addr]["field_name"] == "sector_erase":
                self.erase_flash()
            elif self._reginfo[addr]["field_name"] == "busy":
                return [0]  # 0=done, 1=busy
        if self._reginfo[addr]["port_name"] == "REG_DPMM_CTRL":
            return [64]  # return usewd=64

    def write_flash(self, data):
        d = array("L")
        d.fromlist(data)
        begin = self.flash_addr
        end = begin + 64
        self.flash_mem[begin:end] = d

    def read_flash(self):
        begin = self.flash_addr
        end = begin + 64
        return self.flash_mem[begin:end].tolist()

    def erase_flash(self):
        data = [0xFFFFFFFF] * (1024 * 64)
        begin = self.flash_addr
        end = begin + (1024 * 64)
        d = array("L")
        d.fromlist(data)
        self.flash_mem[begin:end] = d
