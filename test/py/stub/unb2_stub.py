#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . Simple stub to emulate uniboard communication for testing.
#   - The writen register value is saved and retured on reading the same register.
#   - "REG_FPGA_TEMP_SENS.temp" has a random changing value.
#
#   * based on uniboard udp protocol V1.1
#
# Usage:
# . open terminal and start the stub.
# cd git/sdptr
# . ./init_sdptr.sh
# unb_stub.py -f 16 -g 0
#
# -f: number of fpgas
# -g: first fpga numner (gn)
# -m: mmap file to use (generated while making the fpga image)
#     default unb2_stub.mmap is used.
#
# . open new terminal, start sdptr with argument -s (stub-mode)
# cd git/sdptr
# . ./init_sdptr.sh
# sdptr -f 16 -g 0 -d -s
#
# Now OPC-ua on 'localhost', port 4840 can be used for testing.
# ##########################################################################

import sys
from multiprocessing import Process
import traceback
import logging
import struct
from random import random
from time import sleep

from base.udp import UdpServer
from mmap import MMap
from register_stub import RegisterStub


class FpgaCommStub(Process):
    """ Fpga class
    wait for a request on the given udp_port, handle the request using
    ucp_protocol and return response.
    """
    def __init__(self, node_id, udp_port, mmap):
        super().__init__()
        self.id = node_id
        self.udp_port = udp_port
        self.mmap = mmap
        self.name = f"fpga-{self.id:02d}"
        self.logger = logging.getLogger(f"{self.name}")
        self.logger.debug(f"init UNB node stub for {self.name}")
        self.eth = UdpServer(self.udp_port)
        self.ucp = UcpProt(self.id, self.name, self.mmap)
        self.keep_running = True

    def stop(self):
        self.keep_running = False

    def unpackPayload(self, data):
        try:
            fmt = "I" * (len(data) // 4)
            dd = struct.unpack(fmt, data)
            return dd
        except struct.error:
            self.logger.error(f"struct.error: {fmt} {len(data)//4}")
            return []

    def packPayload(self, data):
        fmt = "I" * len(data)
        packed_data = struct.pack(fmt, *data)
        return packed_data

    def run(self):
        self.logger.debug("start waiting for data")
        while self.keep_running:
            try:
                # get new message and unpack byte-data to uint32
                data = self.eth.recv_from()
                if data is None:
                    continue
                data_u32 = self.unpackPayload(data)
                # set data, process message and get back data
                self.ucp.payload = data_u32
                self.ucp.process()
                data_u32 = self.ucp.payload
                # pack uint32 to byte-data before sending back response
                data = self.packPayload(data_u32)
                self.eth.send(data)
            except BaseException:
                print("")
                print("-" * 80)
                traceback.print_exc(limit=2, file=sys.stdout)
                print("-" * 80)

                self.logger.error(f"Aborting Fpga-{self.id} NOW")
                self.keep_running = False


class UcpProt:
    """ UcpProt class
    Only the used opcodes from the uniboard protocol are implemented.
    memory_read/write
    flash_read/write

    The FpgaRegMap class (regmap) holds the parsed mmap file.
    The Register class is called to store and return the requested value.
    """
    def __init__(self, id, name, mmap):
        self.id = id
        self.name = name
        self.mmap = mmap
        self.logger = logging.getLogger(f"ucp-prot-{self.name}")

        self.reg = RegisterStub(self.mmap, self.id)

        self.in_data = []
        self.in_data_nr = 0
        self.out_data = []

        # opcode to function LUT
        #   opcode    :     function to call
        self.opcode2func = {
            0x00000001: self.memory_read,
            0x00000002: self.memory_write,
            0x00000009: self.fifo_read,
            0x0000000A: self.fifo_write,
        }

        self.memory_opcodes = [0x00000001, 0x00000002]
        self.fifo_opcodes = [0x00000009, 0x0000000A]

    @property
    def payload(self):
        return self.out_data

    @payload.setter
    def payload(self, payload):
        self.in_data = payload
        self.in_data_nr = 0

    def get_next_uint32(self):
        try:
            data = self.in_data[self.in_data_nr]
            self.in_data_nr += 1
            return data
        except IndexError:
            self.logger.error("no in_data available")

    def get_remaining_uint32(self):
        try:
            data = []
            if self.in_data_nr < len(self.in_data):
                data = list(self.in_data[self.in_data_nr:])
                self.in_data_nr = len(self.in_data)
            return data
        except IndexError:
            self.logger.error("no in_data available")
            return []

    def memory_read(self):
        self.logger.debug("memory read")
        self.out_data.append(self.address)
        status, data = self.reg.read(self.address, self.n_words)
        if status:
            self.out_data.extend(data)

    def memory_write(self):
        self.logger.debug("memory write)")
        self.out_data.append(self.address)

        self.reg.write(self.address, self.n_words, self.get_remaining_uint32())

    def fifo_read(self):
        self.logger.debug("fifo read")
        self.out_data.append(self.address)
        self.out_data.extend(self.reg.read_flash(self.n_words, self.address))

    def fifo_write(self):
        self.logger.debug("fifo write")
        self.out_data.append(self.address)
        self.reg.write_flash(self.get_remaining_uint32())

    def change_reg_value(self):
        # change some values
        self.reg.set_reg_temp(25.0 + random() * 5.0)

    def process(self):
        self.out_data = []
        psn = self.get_next_uint32()
        opcode = self.get_next_uint32()
        self.logger.debug(f"opcode={opcode}")
        self.out_data.append(psn)
        self.change_reg_value()

        if opcode in self.opcode2func:
            # set size and address
            self.n_words = self.get_next_uint32()
            self.address = self.get_next_uint32()  # addr in bytes
            self.logger.debug(
                "memory n_words = %d, start address = 0x%08x in words",
                self.n_words,
                self.address // 4,
            )
            # get function to call for this opcode
            self.opcode2func[opcode]()
        else:
            self.logger.error(f"Unknown opcode '{opcode}'")


def main(args):
    fpga_map = []
    fpga_list = [i for i in range(args.first_gn, args.first_gn + args.fpgas, 1)]
    port_list = [5000 + i for i in fpga_list]
    mmap = MMap()
    mmap.read_mmap(args.mmap)
    mmap.zip_mmap()

    for fpga_nr, port_nr in zip(fpga_list, port_list):
        fpga = FpgaCommStub(fpga_nr, port_nr, mmap)
        fpga_map.append(fpga)
        fpga.start()

    logger.info("all nodes started")

    while True:
        try:
            sleep(0.1)
        except KeyboardInterrupt:
            logger.info("interupt, user pressed Ctrl-C")
            break

    for fpga in fpga_map:
        fpga.stop()
        fpga.join()

    logger.info("all nodes stopped")

    return 0


if __name__ == "__main__":
    import os
    import argparse
    import textwrap

    # set default mmap file
    # cwd = __file__[:__file__.rfind('/')]
    stub_dir = os.getenv("STUB_DIR")
    default_mmap_file = os.path.join(stub_dir, "unb2_stub.mmap")

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description="".join(
            textwrap.dedent(
                """\
            Uniboard stub command line argument parser:
            """
            )
        ),
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument("-f", "--fpgas", type=int, help="number of fpgas")
    parser.add_argument("-g", "--first_gn", type=int, help="number of first fpga")
    parser.add_argument("-m", "--mmap", type=str, default=default_mmap_file, help="mmap file to use")
    parser.add_argument("-v", action="count", help="verbosity 'WARNING','INFO','DEBUG' -v,-vv,-vvv")

    args = parser.parse_args()

    verbosity_nr = 2 if args.v is None else args.v
    LOGLEVEL = ["ERROR", "WARNING", "INFO", "DEBUG"]
    log_level = eval("logging.{}".format(LOGLEVEL[verbosity_nr]))

    logging.basicConfig(level=log_level)
    logger = logging.getLogger("main")

    logger.info(f"parsed arguments: {args}")

    ret = main(args)
    sys.exit(ret)
