#!/usr/bin/env python
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# -------------------------------------------------------------------------------
# Name:        udp_class.py
# Purpose:     udp server and client class
#
# Author:      donker (ASTRON)
#
# Created:     16-12-2011
# Updated:     mrt 2021
# -------------------------------------------------------------------------------

import logging
import socket as sck

sck.setdefaulttimeout(3.0)


class UdpServer:
    """
    UDP server class waiting for data on opened port
    """
    def __init__(self, port=5000):
        """ Open UDP socket and start listening on given 'port' """
        self.logger = logging.getLogger("UDP-Server")
        self.port = port
        self.address = (None, None)
        self.sock = sck.socket(sck.AF_INET, sck.SOCK_DGRAM)
        self.sock.setsockopt(sck.SOL_SOCKET, sck.SO_REUSEADDR, 1)
        self.sock.settimeout(1.0)
        self.sock.bind(('', self.port))
        self.logger.debug("udp server opened")
        self.n_bytes = 1024

    def __del__(self):
        self.sock.close()
        self.logger.debug("udp server closed")

    def close(self):
        """ close UDP socket """
        self.sock.close()
        self.logger.debug("udp server closed")

    def expected_n_bytes(self, n_bytes):
        """ number of expected bytes to receive """
        self.n_bytes = n_bytes

    def send(self, data):
        """ send data on this socket """
        self.sock.sendto(data, self.address)

    def recv_from(self):
        """ read max. n_bytes from UDP socket and save address of sender """
        try:
            self.sock.settimeout(3.0)
            data, address = self.sock.recvfrom(self.n_bytes)
            self.address = address
            self.logger.debug(self.address)
            return data
        except sck.timeout:
            self.logger.debug("socket.timeout")
            return None

    def recv(self):
        """ read max. n_bytes from UDP socket """
        try:
            # self.sock.settimeout(3.0)
            return self.sock.recv(self.n_bytes)
        except sck.timeout:
            self.logger.debug("socket.timeout")
            return None

    def flush(self):
        """ clear input buffer """
        self.sock.settimeout(0.1)
        while True:
            try:
                _ = self.sock.recv(1)
            except sck.timeout:
                return

    def get_address(self):
        """ if used recv_from() return address of sender """
        return self.address[0]


class UdpClient:
    """
    UDP client class for connecting to UDP server
    """
    ST_OK      = 0
    ST_TIMEOUT = 2

    def __init__(self, host='localhost', port=8881):
        """ Make client UDP socket and use host, port for sending messages. """
        self.logger = logging.getLogger("UDP-Client")
        self.port = port
        self.host = host
        self.sock = sck.socket(sck.AF_INET, sck.SOCK_DGRAM)
        self.sock.settimeout(1.0)

    def __del__(self):
        self.sock.close()

    def send(self, data):
        """ Send data to (host, port) """
        self.sock.sendto(data, (self.host, self.port))

    def recv(self):
        """ Receive data from (host, port) """
        status = self.ST_OK
        try:
            self.sock.settimeout(2.0)
            data = self.sock.recv(4096)
        except sck.timeout:
            self.logger.debug("socket.timeout")
            status = self.ST_TIMEOUT
            data = ""
        except BaseException:
            raise
        return (status, data)
