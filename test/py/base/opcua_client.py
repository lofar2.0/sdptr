# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . class to communicate with the opc-ua server
#
# https://opcua-asyncio.readthedocs.io/en/latest/
# ##########################################################################

import sys

import traceback
import logging
from time import sleep

from asyncua.sync import Client
from asyncua import ua
import asyncua.ua.uaerrors as uaerrors

from .constants import C_SDP

VARIANTTYPE = {'VariantType.Boolean': ua.VariantType.Boolean,
               'VariantType.SByte'  : ua.VariantType.SByte,
               'VariantType.Byte'   : ua.VariantType.Byte,
               'VariantType.Int16'  : ua.VariantType.Int16,
               'VariantType.UInt16' : ua.VariantType.UInt16,
               'VariantType.Int32'  : ua.VariantType.Int32,
               'VariantType.UInt32' : ua.VariantType.UInt32,
               'VariantType.Int64'  : ua.VariantType.Int64,
               'VariantType.UInt64' : ua.VariantType.UInt64,
               'VariantType.Float'  : ua.VariantType.Float,
               'VariantType.Double' : ua.VariantType.Double,
               'VariantType.String' : ua.VariantType.String}

OPCUA_FIXED_N_NODES = 16


class SubHandler:
    """
    Subscription Handler. To receive events from server for a subscription
    data_change and event methods are called directly from receiving thread.
    Do not do expensive, slow or network operation there. Create another
    thread if you need to do such a thing
    """
    def datachange_notification(self, node, val, data):
        print("New data change event", node, val)

    def event_notification(self, event):
        print("New event", event)


class OpcuaClient:
    """ OpcuaClient used to communicate with the opc-ua server (sdptr)
    """
    def __init__(self, host='localhost', port=4840):
        # OPCUA always has a fixed array size, this means n_opcua_fpgas=16 even if n_fpgas=4.
        self.n_opcua_fpgas = OPCUA_FIXED_N_NODES
        self.logger = logging.getLogger('OpcuaClient')
        self.host = str(host)
        self.port = int(port)
        self.client = None
        self.connected = False
        self.n_boards = None
        self.n_fpgas = None
        self.gn_first_fpga = None
        self.fpga_gn_list = []
        self.browse_name_R = {}
        self.browse_name_RW = {}
        self.browse_name_WO = {}
        self.child_dimension = {}
        self.child_datatype = {}

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        if self.connected:
            self.disconnect()

    def connect(self, host=None, port=None):
        """ connect to 'host' on 'port' """
        if self.connected:
            self.logger.info("client already connected")
            return

        self.host = self.host if host is None else str(host)
        self.port = self.port if port is None else int(port)

        self.url = "opc.tcp://{}:{}/".format(self.host, self.port)
        self.client = Client(url=self.url, timeout=100)
        logging.getLogger('asyncua').setLevel(logging.CRITICAL)

        self.logger.info("Connected to OPCua server add %s:%d", self.host, self.port)
        try:
            self.client.connect()
            self.connected = True
        except ConnectionRefusedError:
            self.logger.error("Connection with {} not possible".format(self.url))
            self.client.disconnect()
            self.client = None
            return

        self.n_fpgas = self.client.nodes.objects.get_child("2:TR_sdp_config_nof_fpgas_R").read_value()
        self.n_boards = self.n_fpgas // C_SDP.fpgas_per_board
        self.gn_first_fpga = self.client.nodes.objects.get_child("2:TR_sdp_config_first_fpga_nr_R").read_value()
        self.fpga_gn_list = list(range(self.gn_first_fpga, self.gn_first_fpga + self.n_fpgas))

        self.client.load_data_type_definitions()
        self.get_all_child_info()

    @property
    def node_list(self):
        """ return list with all global-node numbers handled by the connected server """
        return self.fpga_gn_list

    @property
    def n_nodes(self):
        """ return number of nodes(fpgas) handled by the connected server """
        return self.n_fpgas

    def disconnect(self):
        """ disconnect from the server """
        if self.connected:
            self.logger.info("Disconnect from OPCua server")
            self.client.disconnect()
            self.connected = False
            self.client = None
            self.logger.info("Connection with OPCua server closed")

    def get_all_child_info(self):
        """ get all child info (register points) from the server
        and detect if it is _RW, _WO or _R point.
        """
        if not self.connected:
            self.logger.error("client not connected to server")
            return

        children = self.client.nodes.objects.get_children()
        for child in children:
            try:
                browse_name = child.read_browse_name().to_string()
                if browse_name.startswith("2:"):
                    short_name = browse_name[browse_name.find("_") + 1:browse_name.rfind("_")]
                    if browse_name.endswith("_RW"):
                        self.browse_name_RW[short_name] = browse_name
                    elif browse_name.endswith("_WO"):
                        self.browse_name_WO[short_name] = browse_name
                    elif browse_name.endswith("_R"):
                        self.browse_name_R[short_name] = browse_name
                    else:
                        self.logger.error(f"unknown type {browse_name}")

                    # get child dimension, always 1 dim size is n_nodes * n_data_one_node
                    # if dim is None, it is not an array, but a single value (only TR_ points).
                    dim = child.read_array_dimensions()
                    if dim is None:
                        self.child_dimension[short_name] = [1, None]
                    elif "FPGA_" in browse_name or "TR_fpga_" in browse_name or "TR_ucp_" in browse_name:
                        dim_one_fpga = max(1, dim[0] // self.n_opcua_fpgas)
                        self.child_dimension[short_name] = [self.n_fpgas, dim_one_fpga]
                    else:
                        self.child_dimension[short_name] = [1, dim[0]]
                    self.child_datatype[short_name]  = str(child.read_data_type_as_variant_type())
            except uaerrors.UaError:
                pass
            except BaseException:
                print('Caught %s', str(sys.exc_info()[0]))
                print(str(sys.exc_info()[1]))
                print('TRACEBACK:\n%s', traceback.format_exc())

    def valid_names(self):
        """ return list with all valid datapoint names """
        names = []
        names.extend(self.browse_name_R.keys())
        names.extend(self.browse_name_WO.keys())
        return names

    def valid_read_names(self):
        """ return list with all valid read-only datapoint names """
        return self.browse_name_R.keys()

    def valid_write_names(self):
        """ return list with all valid write datapoint names """
        names = []
        names.extend(self.browse_name_RW.keys())
        names.extend(self.browse_name_WO.keys())
        return names

    def is_valid_name(self, name):
        """ check if 'name' is a valid datapoint name and return True or False """
        if name in self.browse_name_R or name in self.browse_name_RW or name in self.browse_name_WO:
            return True
        return False

    def _check_name(self, name):
        """ check if 'name' is a valid datapoint name and return True or False and print a error message on False """
        if name in self.browse_name_R or name in self.browse_name_RW or name in self.browse_name_WO:
            return True
        self.logger.error("'%s' is not a valid node name", name)
        return False

    def get_dim(self, name):
        """ return the array dimmensions of datapoint 'name'
        return list with dimension if an array
        return [1] if a scalar
        return None if 'name' not excists
        """
        if self._check_name(name) is True:
            dim = self.child_dimension[name]
            return [1] if None in dim else dim
        return None

    def get_datatype(self, name):
        """ return datatype of datapoint 'name'
        if datapoint 'name' not excists return None
        """
        if self._check_name(name) is True:
            return str(self.child_datatype[name]).split(".")[1]
        return None

    def read(self, name):
        """ read datapoint 'name'
        return None if not connected.
        return None if datapoint 'name' not excists
        """
        if not self.connected:
            self.logger.error("client not connected to server")
            return None

        if name in self.browse_name_R:
            browse_name = self.browse_name_R[name]
            var = self.client.nodes.objects.get_child(browse_name)
            value = var.read_value()
            if self.child_dimension[name][1] is None:
                return value
            last_val = self.n_fpgas * self.child_dimension[name][1]
            return value[:last_val]
        return None

    def write(self, name, value, check=None):
        """ write 'value' to datapoint 'name'
        if 'check' is True, the value is read back and checked.
        """
        if not self.connected:
            self.logger.error("client not connected to server")
            return False

        check = False if check is None else check

        if self._check_name(name) is True:
            dim = self.child_dimension[name]
            if dim[1] is not None:
                n_val = dim[0] * dim[1]
            else:
                n_val = dim[0]
            if len(value) != n_val:
                if dim[0] == 1:
                    self.logger.error("'%s' wrong dimensions for value, need [%d], got [%d]", name, dim[0], len(value))
                else:
                    self.logger.error("'%s' wrong dimensions for value, need (%dx%d)=[%d], got [%d]", name, dim[0], dim[1], n_val, len(value))
                return False

            if (self.n_opcua_fpgas // dim[0]) > 1:
                if name == "fpga_mask":
                    value += [False] * (self.n_opcua_fpgas - dim[0])
                else:
                    value = list(value) * (self.n_opcua_fpgas // dim[0])

            if name in self.browse_name_RW:
                browse_name = self.browse_name_RW[name]
            elif name in self.browse_name_WO:
                browse_name = self.browse_name_WO[name]
            varianttype = VARIANTTYPE[self.child_datatype[name]]
            var = self.client.nodes.objects.get_child(browse_name)
            var.write_value(ua.Variant(list(value), varianttype))
            if check:
                sleep(0.1)
                rval = self.read(name)
                if value != rval:
                    self.logger.info("register '%s'", str(name))
                    self.logger.info("write val='%s'", str(value))
                    self.logger.info("read  val='%s'", str(rval))
            return True
        self.logger.error("write '%s' not a valid name", name)
        return False

    def get_mask(self):
        """ read active write masker from the server
        return active write masker """
        return self.read('fpga_mask')

    def set_mask(self, mask):
        """ write write-masker to the server
        return True on success els False

        mask is list with nodes to set, [0, 2, 8]
        if None, all available nodes are selected
        """
        if self.fpga_gn_list:
            nodelist = self.fpga_gn_list if mask is None else mask
            return self.write('fpga_mask', [True if i in nodelist else False for i in self.fpga_gn_list])  # write fpga_mask for write actions
        self.logger.error("no nodes")
        return False
