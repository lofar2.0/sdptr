#! /usr/bin/env python3

# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Reinier vd Walle
# Purpose:
# . ADC functions
# Description:
# . Based on $UPE_GEAR/base/adc_functions.py
# ##########################################################################
# System imports

import array
import math
import logging
import pylab as pl
import numpy as np
from .constants import C_SDP


class AdcFunctions:
    """ADC functions class"""

    c_max_channels = 18
    c_max_word_lines = 16

    def __init__(self):
        self.logger = logging.getLogger("ADC_functions")
        self.data = array.array("b")
        self.data_label = []
        self.nof_channels = []
        self.nof_samples = []
        self.nof_data_words = []
        self.data_words = array.array("b")
        self.bits = C_SDP.W_adc
        self.sample_freq = C_SDP.f_adc_MHz
        self.clk_cw_dcs = []
        self.clk_cw_phases = []
        self.clk_cw_amplitudes = []
        self.clk_cw_noise_peaks = []
        self.clk_cw_snrs = []

    def set_nof_bits(self, bits):
        """Changing the number of bits in the class"""
        self.bits = bits

    def set_fs(self, sample_freq):
        """Changing the sample frequency in the class"""
        self.sample_freq = sample_freq

    @classmethod
    def plot_init(cls):
        """set plotting interactive"""
        pl.ion()

    # Function for translating samples
    def jesd_words_2_samples(self, rd_data):
        """jesd_words_2_samples
        Import the JESD ADC samples rd_data (1 dim array) into the class array self.data
        note: this function was added later for LOFAR20
        """
        self.data = rd_data
        self.nof_data_words = len(rd_data)
        self.nof_channels = len(self.data)

        for channel_nr in range(self.nof_channels):
            label = "CH" + str(channel_nr)
            self.data_label.append(label)

    def add_clock_cw_statistics(self, cw_period):
        """
        1) Determine and subtract DC offset from signal

        2) Calculate the clock phase and
        3) signal amplitude for each signal path:
        . Define cwPeriod = T, omega= 2*pi/T, phase = phi and amplitude = A
        . The sampled signal path is a CW(t) = A*sin(omega*t + phi)
        . Multiply the CW(t) in self.data by I(t) = sin(omega*t) and Q(t) = cos(omega*t) and sum of an integer
          number of periods.
        . Using gonio formula for sin*sin and cos*sin and omitting the term with 2*omega, because it cancels yields:

          sumSin = T * A/2 * cos(phi)
          sumCos = T * A/2 * sin(phi)

          ==>

          phi = atan2(sumCos, sumSin)  [rad]  --> * 1/omega to get it in sample period units
          A   = 2/T * sqrt(sumSin^2 + sumCos^2)

        4) Determine the maximum noise peak in the signal. If the noise peak >~ 2 then that is an indication for
           bit errors in the ADC sampling

        5) Calculate the SNR of the each signal path using a reference CW with the estimated phi and A

          sumNoisePower = sum[ (CW(t) - si(t))^2 ]
          noisePower = sumNoisePower / nof samples
          cwPower = (A^2)/2

          ==>

          SNR = 10 * log10(cwPower / noisePower)
        """

        # Create reference I and Q carrier wave with same cwPeriod same as the sampled RF signal period in self.data
        ref_sin = []
        ref_cos = []
        omega = 2 * math.pi / cw_period
        for t in range(cw_period):
            ref_sin.append(math.sin(omega * t))
            ref_cos.append(math.cos(omega * t))
        # Determine dc, phase, amplitude, noise peak and SNR per signal path (= channel)
        data = self.data
        nof_samples = len(data[0])
        # print "add_clock_cw_statistics nof_samples=",nof_samples
        clk_cw_dcs = []
        clk_cw_phases = []
        clk_cw_amplitudes = []
        clk_cw_noise_peaks = []
        clk_cw_snrs = []
        for si in data:
            # >>> Subtract the DC of the sampled CW
            _dc = 0.0
            for t in range(nof_samples):
                _dc = _dc + si[t]
            _dc = 1.0 * _dc / nof_samples
            clk_cw_dcs.append(_dc)
            _sw = []
            for t in range(nof_samples):
                _sw.append(si[t] - _dc)

            # >>> Determine the I and Q sum of the correlation per signal path (= channel)
            sum_sin = 0.0
            sum_cos = 0.0
            for t in range(nof_samples):
                tt = t % cw_period
                sum_sin = sum_sin + ref_sin[tt] * _sw[t]
                sum_cos = sum_cos + ref_cos[tt] * _sw[t]
            # print sum_sin, sum_cos

            # >>> Determine the phase in units of the sample period
            phs = -1.0 * math.atan2(sum_cos, sum_sin) / omega
            if phs < 0:
                phs = phs + cw_period
            clk_cw_phases.append(phs)

            # >>> Determine the amplitude of the sampled CW
            ampl = math.sqrt(sum_sin**2 + sum_cos**2) * 2 / nof_samples
            clk_cw_amplitudes.append(ampl)

            # >>> Determine the SNR of the sampled CW
            # . Create estimated CW with ampl and phs for sw
            ref_cw = []
            for t in range(cw_period):
                ref_cw.append(ampl * math.sin(omega * (t - phs)))
            # . Calculate the noise power and the noise peak
            sum_noise_power = 0.0
            noise_peak = 0.0
            for t in range(nof_samples):
                tt = t % cw_period
                noise = ref_cw[tt] - _sw[t]
                sum_noise_power = sum_noise_power + noise**2
                if abs(noise) > noise_peak:
                    noise_peak = abs(noise)
            clk_cw_noise_peaks.append(noise_peak)
            # . Calculate the SNR using the maximum ADC CW input for the ADC
            noise_power = sum_noise_power / nof_samples
            ampl_max = 2.0 ** (self.bits - 1) - 0.5
            cw_power = (ampl_max**2) / 2
            if ampl != 0.0:
                snr = 10 * math.log10(cw_power / noise_power)
            else:
                # Force undefined snr to 0 for unconnected floating inputs (and also to avoid divide by 0)
                snr = 0.0
            clk_cw_snrs.append(snr)

            # print dc, phs, ampl, noisePeak, snr
        self.clk_cw_dcs.append(clk_cw_dcs)
        self.clk_cw_phases.append(clk_cw_phases)
        self.clk_cw_amplitudes.append(clk_cw_amplitudes)
        self.clk_cw_noise_peaks.append(clk_cw_noise_peaks)
        self.clk_cw_snrs.append(clk_cw_snrs)

    def log_clock_cw_statistics(self, stat_str=None, nof_sigma=None):
        """
        Log the clkCw* statitics for statStr = 'dcs', 'phases', 'amplitudes', 'noisepeaks' or 'snrs'
        Transpose the 2-dim list of clkCw* from [repeats][channel phases] to [channel phases][repeats]
        """
        stat_str = "phases" if stat_str is None else stat_str
        nof_sigma = [1, 2, 3, 4, 5] if nof_sigma is None else nof_sigma

        if stat_str == "dcs":
            clk_cw_stats = np.transpose(self.clk_cw_dcs)
            self.logger.info(">>> Clock-CW DCs:")
            sigma_type = "S"
        elif stat_str == "phases":
            clk_cw_stats = np.transpose(self.clk_cw_phases)
            self.logger.info(">>> Clock-CW phases:")
            sigma_type = "U"
        elif stat_str == "amplitudes":
            clk_cw_stats = np.transpose(self.clk_cw_amplitudes)
            self.logger.info(">>> Clock-CW amplitudes:")
            sigma_type = "S"
        elif stat_str == "noisepeaks":
            clk_cw_stats = np.transpose(self.clk_cw_noise_peaks)
            self.logger.info(">>> Clock-CW noise peaks:")
            sigma_type = "S"
        elif stat_str == "snrs":
            clk_cw_stats = np.transpose(self.clk_cw_snrs)
            self.logger.info(">>> Clock-CW SNRs:")
            sigma_type = "S"
        else:
            raise ValueError

        title_str = "  Channel : Mean      Std       Max       Min       Diff        "
        for _ns in nof_sigma:
            title_str = title_str + "Nof>%d%s   " % (_ns, sigma_type)
        self.logger.info(title_str)
        for _si in range(self.nof_channels):
            stat_mean = np.mean(clk_cw_stats[_si])
            stat_std = np.std(clk_cw_stats[_si])
            stat_max = np.amax(clk_cw_stats[_si])
            stat_min = np.amin(clk_cw_stats[_si])
            stat_diff = stat_max - stat_min
            ch_stat_str = "  %-7s : %8.5f  %8.5f  %8.5f  %8.5f  %8.5f  " % (
                self.data_label[_si],
                stat_mean,
                stat_std,
                stat_max,
                stat_min,
                stat_diff,
            )
            sigma = 1
            if sigma_type == "S":
                sigma = stat_std
            for _ns in nof_sigma:
                nof_larger = 0
                nof_smaller = 0
                delta = _ns * sigma
                if sigma_type == "S":
                    for rep in clk_cw_stats[_si]:
                        if rep < stat_mean - delta:
                            nof_larger = nof_larger + 1
                        if rep > stat_mean + delta:
                            nof_larger = nof_larger + 1
                else:
                    # For sigmaType == 'U' count with respect to min or max instead of mean
                    # to ensure single +1U or single -1U delta is not lost in count as +-0.5U
                    for rep in clk_cw_stats[_si]:
                        if rep > stat_min + delta:
                            nof_larger = nof_larger + 1
                        if rep < stat_max - delta:
                            nof_smaller = nof_smaller + 1
                    if nof_smaller < nof_larger:
                        nof_larger = nof_smaller
                if nof_larger != 0:
                    ch_stat_str = ch_stat_str + "%8d " % nof_larger
                else:
                    break
            self.logger.info(ch_stat_str)

    def plot_clock_cw_statistics(
        self, nof_runs, fig_nr=None, stat_str=None, cw_period=None
    ):
        """Plot the clkCw* statitics for statStr='dcs', 'phases', 'amplitudes', 'noisepeaks' or 'snrs'"""
        fig_nr = 2 if fig_nr is None else fig_nr
        stat_str = "phases" if stat_str is None else stat_str
        cw_period = 8 if cw_period is None else cw_period

        pl.figure(fig_nr, figsize=(11, 6))
        # Transpose the 2-dim list of clkCw* from [repeats][channel phases] to [channel phases][repeats]
        if stat_str == "dcs":
            clk_cw_stats = np.transpose(self.clk_cw_dcs)
            fig_file_name = "clock_cw_dcs"
            pl.title("Clock CW DC")
            pl.ylabel("DC offset")
            # pl.ylim([-2, 2])
        elif stat_str == "phases":
            clk_cw_stats = np.transpose(self.clk_cw_phases)
            fig_file_name = "clock_cw_phases"
            pl.title("Clock CW phase")
            pl.ylabel("Sample phase")
            # pl.ylim([0, cwPeriod])
        elif stat_str == "amplitudes":
            clk_cw_stats = np.transpose(self.clk_cw_amplitudes)
            fig_file_name = "clock_cw_amplitudes"
            pl.title("Clock CW amplitude")
            pl.ylabel("Amplitude")
            # ampl_min = 0
            # ampl_min = 10* int(np.amin(clkCwStats)/10)
            # ampl_max = 10 * (int(np.amax(clk_cw_stats) / 10) + 1)
            # ampl_max = 2**(self.bits-1)
            # pl.ylim([0, ampl_max])
        elif stat_str == "noisepeaks":
            clk_cw_stats = np.transpose(self.clk_cw_noise_peaks)
            fig_file_name = "clock_cw_noise_peaks"
            pl.title("Clock CW noise peaks")
            pl.ylabel("Noise peak")
            # peak_max = 5 * (int(np.amax(clk_cw_stats) / 5) + 1)
            # pl.ylim([0, peak_max])
        elif stat_str == "snrs":
            clk_cw_stats = np.transpose(self.clk_cw_snrs)
            fig_file_name = "clock_cw_snrs"
            pl.title("Clock CW SNR")
            pl.ylabel("SNR")
            # snr_adc = 1.76 + 6.02 * self.bits
            # snr_max = 10 * (int(snr_adc / 10) + 1) + 15
            # pl.ylim([0, snr_max])
        else:
            raise ValueError

        # print(clk_cw_stats)
        data_label = self.data_label
        for si in range(self.nof_channels):
            pl.plot(clk_cw_stats[si], label=data_label[si])
        pl.grid(True)
        pl.xlabel("Repeat")
        pl.xlim([0, 1.2 * nof_runs - 1])  # create space for legend at right of plot
        pl.legend()
        pl.show()
        # pl.draw()
        # Save clkCw* plot in file with parameters, result and sof info (via tc.gpString) in file name
        # Use insertSP = False to omit SP in file name to avoid too long file name for when the picture
        # needs to be viewed in Windows
        fig_file_name = fig_file_name + "_repeat_%d" % nof_runs
        fig_file_name = fig_file_name + ".png"
        pl.savefig(fig_file_name)
