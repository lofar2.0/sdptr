###############################################################################
#
# Copyright (C) 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Author:  P.Donker
# Date  : dec 2023 created

import time
import logging


class StreamControl:
    """ StreamControl
    This class controls receiving a stream.
    It must be called with a active client (OpcuaClient) and stream_reader(StreamReader)

    function to call for collecting data:
    - record(state)               : turn on/off recording, True|False
    - record_time(rec_time)       : start recording wait 'rec_time' and stop recording
    - record_n_bsn(n_bsn)         : start recording wait for 'n_bsn' and stop recording
    - record_n_packets(n_packets) : start recording wait for 'n_packets' incoming packets and stop recording
    """
    def __init__(self, client, stream_reader):
        self.client = client
        self.stream_reader = stream_reader
        self.data_type = self.stream_reader.data_type
        self.logger = logging.getLogger("StreamControl")
        self.start_record_time = 0
        self.stop_record_time = 0
        self.recording_time = 0
    
    def print_result(self, info, result):
        if result is True:
            self.logger.info(f"{info}: PASSED")
        else:
            self.logger.info(f"{info}: FAILED")

    def stream_on(self):
        """ turn stream on, use data_type to selct next function """
        if self.data_type == "SST":
            self._sst_on()
        elif self.data_type == "BST":
            self._bst_on()
        elif self.data_type == "XST":
            self._xst_on()
        elif self.data_type == "BEAMLET":
            self._beamlet_on()

    def stream_off(self):
        """ turn stream off, use data_type to selct next function """
        if self.data_type == "SST":
            self._sst_off()
        elif self.data_type == "BST":
            self._bst_off()
        elif self.data_type == "XST":
            self._xst_off()
        elif self.data_type == "BEAMLET":
            self._beamlet_off()

    def _sst_on(self):
        response = self.client.write("sst_offload_enable", [True] * self.client.n_nodes)
        self.print_result("sst_stream_on", response)

    def _sst_off(self):
        response = self.client.write("sst_offload_enable", [False] * self.client.n_nodes)
        self.print_result("sst_stream_off", response)

    def _bst_on(self):
        n_rn = self.client.n_nodes  # all nodes used in ring
        last_rn = n_rn - 1  # last node in ring relative to o_rn
        enable = [False] * self.client.n_nodes
        enable[last_rn] = True
        response = self.client.write("bst_offload_enable", enable)
        self.print_result("bst_stream_on", response)

    def _bst_off(self):
        response = self.client.write("bst_offload_enable", ([False] * self.client.n_nodes))
        self.print_result("bst_stream_off", response)

    def _xst_on(self):
        response = self.client.write("xst_offload_enable", [True] * self.client.n_nodes)
        self.print_result("xst_stream_on", response)

    def _xst_off(self):
        response = self.client.write("xst_offload_enable", [False] * self.client.n_nodes)
        self.print_result("xst_stream_off", response)

    def _beamlet_on(self):
        """ Enable beamlet output for one node and all selected beamsets, boolean FPGA_beamlet_output_enable_RW[N_pn][N_beamsets_ctrl].
        Node in range(N_rn). Default enable output on last node in ring.
        Beamset in range(N_beamsets_ctrl). Default enable output for all beamsets selected by N_beamsets_ctrl.
        See definition of Ring common in ICD SC-SDP.
        """
        n_beamsets = self.client.read("sdp_config_nof_beamsets")
        n_rn = self.client.n_nodes  # all nodes used in ring
        last_rn = n_rn - 1  # last node in ring relative to o_rn
        enable = [False] * self.client.n_nodes * n_beamsets
        enabled_rn = last_rn
        for i in range(n_beamsets):
            enable[n_beamsets * enabled_rn + i] = True  # order is [N_pn][N_beamsets_ctrl]
        response = self.client.write("beamlet_output_enable", enable)
        self.print_result("beamlet_stream_on", response)

    def _beamlet_off(self):
        """ Disable beamlet output for all nodes and beamsets, boolean FPGA_beamlet_output_enable_RW[N_pn][N_beamsets_ctrl]."""
        n_beamsets = self.client.read("sdp_config_nof_beamsets")
        response = self.client.write("beamlet_output_enable", ([False] * self.client.n_nodes * n_beamsets))
        self.print_result("beamlet_stream_off", response)

    def start_timer(self):
        self.start_record_time = time.time()
        
    def stop_timer(self):
        self.stop_record_time = time.time()
        self.recording_time = self.stop_record_time - self.start_record_time
        
    def record(self, state):
        """ enable recording data state is True(on) or False(off) """
        if state is True:
            self.stream_reader.data.reset_data()
            self.stream_on()
        else:
            self.stream_off()

    def record_time(self, rec_time):
        """ record time
        receive 'rec_time' data, and wait until processed
        return True if done
        """
        self.record(True)
        self.start_timer()
        time.sleep(rec_time)
        self.record(False)
        while not self.stream_reader.done():
            time.sleep(0.1)
        self.stop_timer()
        self.logger.info(f"recording and processing {rec_time} seconds done in {self.recording_time:5.3f} seconds")
        return True

    def record_n_bsn(self, n_bsn, wait_all_processed=None):
        """ record n bsn
        record minimal 'n_bsn' numbers.
        n_bsn: number of bsn numbers in data dictonary to wait for.
        return True if succes else False
        """
        wait_all_processed = False if wait_all_processed is None else wait_all_processed
        self.stream_reader.set_max_packets(0)
        self.record(True)
        self.start_timer()
        result = self.stream_reader.data.wait_for_nof_bsn(n_bsn, timeout=30)
        self.record(False)
        if wait_all_processed:
            while not self.stream_reader.done():
                time.sleep(0.1)
        self.stop_timer()
        self.logger.info(f"recording and processing {n_bsn} bsn's done in {self.recording_time:5.3f} seconds")
        return result

    def record_n_packets(self, n_packets):
        """ record n packets
        record minimal 'n_packets' of incoming data.
        return True
        """
        self.stream_reader.set_max_packets(n_packets)
        self.record(True)
        self.start_timer()
        time.sleep(0.001)
        while not self.stream_reader.done():
            time.sleep(0.001)
        self.record(False)
        self.stop_timer()
        self.logger.info(f"recording and processing {n_packets} packets done in {self.recording_time:5.3f} seconds")
        self.stream_reader.set_max_packets(0)
        return True
