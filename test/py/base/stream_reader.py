###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Author:  P.Donker
# Date  : jun 2022 created

"""
  stream reader for Statistic and beamlet data

   --------------------------------------------------------------------------------------
  | Stream Reader class (used by user)                                                   |
  |                                                                                      |
  | - start/stop all processes and threads                                               |
  | - to get received data the StreamReader.data.functions() can be used                 |
   --------------------------------------------------------------------------------------
     [start]                           [start]                              [start]   ^
     [stop]                            [stop]                               [stop]    |
       |                              n_workers                                |      |
       |                                  |                                    |      |
       |                                  v                                    |      |
       |                              ---------                                |      |
       |                             | Unpack  |                               |      |
       v                         --->| Stream  |--->                           v  [functions]
   ---------                    |    | Process |    |                         ------------
  | Udp     |                   |     ---------     |                        | Stream     |
  | Reader  | --> [raw_data] --           :          --> [unpacket_data] --> | Data       |
  | Process |       queue       |     ---------     |        queue           | Thread     |
   ---------                    |    | Unpack  |    |                         ------------
                                 --->| Stream  |--->
                                     | Process |
                                      ---------

The StreamReader class is the class that is included by the user, it starts the needed processes
and threads. The received data can be accessed using functions of data.
example:
  stream_reader = StreamReader(port=5001, data_type="SST")
  stream_reader.start()

  received_bsn_numbers = steam_reader.data.get_all_bsn()
  received_bsn_indexes = stream_reader.data.get_all_index(received_bsn_numbers[0])

  header = stream_reader.data.get_header(bsn, index)
  data = stream_reader.data.get_data(bsn, index)

  stream_reader.stop()

It is live data, so the returned get_all_bsn() list is growing.

References:
[1] ICD SC-SDP statistics offload packet definition in
https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L2%20Interface%20Control%20Documents/SC%20to%20SDP%20ICD
[2] ICD STAT-SDP inbeamlet output packet definition in
https://plm.astron.nl/polarion/#/project/LOFAR2System/wiki/L1%20Interface%20Control%20Documents/STAT%20to%20CEP%20ICD

"""
import struct
import logging
import time
import datetime
import random
from multiprocessing import Process, RLock, active_children
from threading import Thread
from queue import Empty, Full
from faster_fifo import Queue

from .udp import UdpServer
from .handle_exceptions import handle_exception
from .attribute_dict import AttributeDict

MARKER_TYPES = {0x53: "SST", 0x42: "BST", 0x58: "XST", 0x62: "BEAMLET"}


def extract_antenna_field_index(station_info):
    """Extract antenna_field_index[5:0] from station_info[15:10]"""
    return (station_info & 0xFC00) >> 10


def extract_station_id(station_info):
    """Extract station_id[9:0] from station_info[9:0]"""
    return station_info & 0x3FF


def stat_header_data(hdr):
    """ get header data from hdr """
    data = {}
    data["marker"] = hdr[0]
    data["data_type"] = MARKER_TYPES.get(hdr[0], "??")
    data["version_id"] = hdr[1]
    data["observation_id"] = hdr[2]
    data["station_info"] = hdr[3]
    data["antenna_field_index"] = extract_antenna_field_index(hdr[3])
    data["station_id"] = extract_station_id(hdr[3])
    data["source_info"] = hdr[4]
    data["gn_index"] = hdr[4] & 0x00FF
    data["subband_calibrated_flag"] = (hdr[4] & 0x0100) >> 8
    data["beam_repositioning_flag"] = (hdr[4] & 0x0200) >> 9
    data["payload_error"] = (hdr[4] & 0x0400) >> 10
    data["fsub_type"] = (hdr[4] & 0x0800) >> 11
    data["f_adc"] = (hdr[4] & 0x1000) >> 12
    data["nyquist_zone_index"] = (hdr[4] & 0x6000) >> 13
    data["antenna_band_index"] = (hdr[4] & 0x8000) >> 15
    data["integration_interval"] = hdr[5] & 0x00FFFFFF
    data["data_id"] = hdr[6]
    data["signal_input_index"] = None if data["data_type"] != "SST" else (hdr[6] & 0x000000FF)
    data["beamlet_index"] = None if data["data_type"] != "BST" else (hdr[6] & 0x0000FFFF)
    data["subband_index"] = None if data["data_type"] != "XST" else (hdr[6] & 0x00FF0000) >> 16
    data["signal_input_A_index"] = None if data["data_type"] != "XST" else (hdr[6] & 0x0000FF00) >> 8
    data["signal_input_B_index"] = None if data["data_type"] != "XST" else (hdr[6] & 0x000000FF)
    data["nof_signal_inputs"] = hdr[7]
    data["nof_bytes_per_statistics"] = hdr[8]
    data["nof_statistics_per_packet"] = hdr[9]
    data["block_period"] = hdr[10]
    data["bsn"] = hdr[11]
    return data


def beamlet_header_data(hdr):
    """ get beamlet data from hdr """
    data = {}
    data["marker"] = hdr[0]
    data["data_type"] = MARKER_TYPES.get(hdr[0], "??")
    data["version_id"] = hdr[1]
    data["observation_id"] = hdr[2]
    data["station_info"] = hdr[3]
    data["antenna_field_index"] = extract_antenna_field_index(hdr[3])
    data["station_id"] = extract_station_id(hdr[3])
    data["source_info"] = hdr[4] << 8 + hdr[5]  # fmt 'HB' in big endian ">BBLHHBLHHBHHQ" to get 3 octets
    data["gn_index"] = hdr[5]
    data["beamlet_width"] = (hdr[4] & 0xF)
    data["beam_repositioning_flag"] = (hdr[4] & 0x010) >> 4
    data["payload_error"] = (hdr[4] & 0x020) >> 5
    data["fsub_type"] = (hdr[4] & 0x040) >> 6
    data["f_adc"] = (hdr[4] & 0x80) >> 7
    data["nyquist_zone_index"] = (hdr[4] & 0x300) >> 8
    data["antenna_band_index"] = (hdr[4] & 0x400) >> 10
    data["beamlet_scale"] = hdr[7]
    data["beamlet_index"] = hdr[8]
    data["nof_blocks_per_packet"] = hdr[9]
    data["nof_beamlets_per_block"] = hdr[10]
    data["block_period"] = hdr[11]
    data["bsn"] = hdr[12]
    data["n_beamlet"] = data["nof_blocks_per_packet"] * data["nof_beamlets_per_block"]
    return data


def print_header(hdr, stdout=None, recv_time=None):
    """
    print header info to:
    """
    str_list = []
    if hdr.data_type in ["SST", "BST", "XST"]:
        str_list.append(f"=== Statistics header {hdr.data_type}")
        if recv_time:
            str_list.append(f"=== {recv_time}")
        str_list.append(f"- marker                     = {hdr.marker}")
        str_list.append(f"- version_id                 = {hdr.version_id}")
        str_list.append(f"- observation_id             = {hdr.observation_id}")
        str_list.append(f"- antenna_field_index        = {extract_antenna_field_index(hdr.station_info)}")
        str_list.append(f"- station_id                 = {extract_station_id(hdr.station_info)}")
        str_list.append(f"- si.gn_index                = {hdr.gn_index}")
        str_list.append(f"- si.subband_calibrated_flag = {hdr.subband_calibrated_flag}")
        str_list.append(f"- si.beam_repositioning_flag = {hdr.beam_repositioning_flag}")
        str_list.append(f"- si.payload_error           = {hdr.payload_error}")
        str_list.append(f"- si.fsub_type               = {hdr.fsub_type}")
        str_list.append(f"- si.f_adc                   = {hdr.f_adc}")
        str_list.append(f"- si.nyquist_zone_index      = {hdr.nyquist_zone_index}")
        str_list.append(f"- si.antenna_band_index      = {hdr.antenna_band_index}")
        str_list.append(f"- integration_interval       = {hdr.integration_interval}")
        str_list.append(f"- di.signal_input_index      = {hdr.signal_input_index}")
        str_list.append(f"- di.beamlet_index           = {hdr.beamlet_index }")
        str_list.append(f"- di.subband_index           = {hdr.subband_index }")
        str_list.append(f"- di.signal_input_A_index    = {hdr.signal_input_A_index}")
        str_list.append(f"- di.signal_input_B_index    = {hdr.signal_input_B_index}")
        str_list.append(f"- nof_signal_inputs          = {hdr.nof_signal_inputs}")
        str_list.append(f"- nof_bytes_per_statistics   = {hdr.nof_bytes_per_statistics}")
        str_list.append(f"- nof_statistics_per_packet  = {hdr.nof_statistics_per_packet}")
        str_list.append(f"- block_period               = {hdr.block_period}")
        str_list.append(f"- bsn                        = {hdr.bsn}")
    else:
        if recv_time:
            str_list.append(f"\nbeamlet header: {recv_time}")
        else:
            str_list.append("\nbeamlet header")
        str_list.append(f"- marker                     = {hdr.marker}")
        str_list.append(f"- version_id                 = {hdr.version_id}")
        str_list.append(f"- observation_id             = {hdr.observation_id}")
        str_list.append(f"- antenna_field_index        = {extract_antenna_field_index(hdr.station_info)}")
        str_list.append(f"- station_id                 = {extract_station_id(hdr.station_info)}")
        str_list.append(f"- si.gn_index                = {hdr.gn_index}")
        str_list.append(f"- si.beamlet_width           = {hdr.beamlet_width}")
        str_list.append(f"- si.beam_repositioning_flag = {hdr.beam_repositioning_flag}")
        str_list.append(f"- si.payload_error           = {hdr.payload_error}")
        str_list.append(f"- si.fsub_type               = {hdr.fsub_type}")
        str_list.append(f"- si.f_adc                   = {hdr.f_adc}")
        str_list.append(f"- si.nyquist_zone_index      = {hdr.nyquist_zone_index}")
        str_list.append(f"- si.antenna_band_index      = {hdr.antenna_band_index}")
        str_list.append(f"- beamlet_scale              = {hdr.beamlet_scale}")
        str_list.append(f"- beamlet_index              = {hdr.beamlet_index}")
        str_list.append(f"- nof_blocks_per_packet      = {hdr.nof_blocks_per_packet}")
        str_list.append(f"- nof_beamlets_per_block     = {hdr.nof_beamlets_per_block}")
        str_list.append(f"- block_period               = {hdr.block_period}")
        str_list.append(f"- bsn                        = {hdr.bsn}")

    if stdout is True:
        print('\n'.join(str_list))

    logger = logging.getLogger("print_header")
    for _s in str_list:
        logger.info(_s)


class _StreamData(Thread):
    """
    This class hold the unpacked received data, and is a Thread so the data can be accessed.
    """
    def __init__(self, data_type, in_data):
        super().__init__()
        self.logger = logging.getLogger("StreamData")
        self.data_type = data_type
        self.in_data = in_data
        self.lock = RLock()
        self.received_data = {}
        self._n_packets_in = 0
        self._n_packets_valid = 0
        self._n_packets_duplicate = 0
        self._input_order = []
        self._last_new_data_time = time.time()
        self._keep_running = True

    def stop(self):
        """ stop """
        self._keep_running = False
        self.logger.info("=== Streaming data status %s", str(self.data_type))
        self.logger.info("- read %d unpacked packets from in_queue", self._n_packets_in)
        self.logger.info("- n_valid %d packets", self._n_packets_valid)
        self.logger.info("- n_duplicate %d packets", self._n_packets_duplicate)

    def run(self):
        """run """
        while self._keep_running:
            try:
                many_data = self.in_data.get_many_nowait(max_messages_to_get=4000)
                self.add_to_received_data(many_data)
            except Empty:
                time.sleep(0.001)
            except BaseException as err:
                if handle_exception(err) == 1:
                    self._keep_running = False
        self.in_data.close()

    def reset_data(self):
        with self.lock:
            self.received_data = {}
            self._n_packets_in = 0
            self._n_packets_valid = 0
            self._n_packets_duplicate = 0
            self._input_order = []

    def busy(self):
        """ busy function
        SDPFW sends all statistics data for an integration interval within 0.1 s, so after statistics
        output disable it may take at most 0.1 s for the stream to become idle.
        SDPFW continuously sends beamlet data packets, so after beamlet output disable it will take
        less than 1 ms for the stream to become idle.
        Use some more time margin to account for processing all recv data packets in the _data buffer.
        """
        return time.time() < (self._last_new_data_time + 0.5)

    def _get_index(self, hdr):
        if self.data_type == "BEAMLET":
            return hdr.beamlet_index
        if self.data_type == "SST":
            return hdr.signal_input_index
        if self.data_type == "BST":
            return hdr.beamlet_index
        if self.data_type == "XST":
            return hdr.data_id
            # return hdr.subband_index
        return None

    @classmethod
    def xst_data_id_to_indices(cls, data_id):
        """ xst_data_id_to_indices """
        sub = (data_id & 0x00FF0000) >> 16
        si_a = (data_id & 0x0000FF00) >> 8
        si_b = data_id & 0x000000FF
        return sub, si_a, si_b

    @classmethod
    def xst_indices_to_data_id(cls, sub, si_a, si_b):
        """ xst_indices_to_data_id """
        data_id = (sub << 16) + (si_a << 8) + si_b
        return data_id

    def add_to_received_data(self, many_data):
        """ add data from many_data into the received data dictonary """
        with self.lock:
            # self.logger.info(f"append {len(many_data)} unpacked packets")
            for recv_time, hdr, data in many_data:
                self._n_packets_in += 1
                header = AttributeDict(hdr)
                bsn = header.bsn
                index = self._get_index(header)
                if bsn in self.received_data and index in self.received_data[bsn]:
                    if self.data_type == "XST":
                        sub, si_a, si_b = self.xst_data_id_to_indices(index)
                        self.logger.error("%s, index (sub, A, B) %d, %d, %d is duplicate", str(bsn), sub, si_a, si_b)
                    else:
                        self.logger.error("%s, index %s is duplicate", str(bsn), str(index))
                    self._n_packets_duplicate += 1
                    return

                if bsn not in self.received_data:
                    self.received_data[bsn] = {}
                if index not in self.received_data[bsn]:
                    self.received_data[bsn][index] = {}

                self._input_order.append((bsn, index))
                self.received_data[bsn][index]["recv_time"] = recv_time
                self.received_data[bsn][index]["header"] = header
                self.received_data[bsn][index]["data"] = data
                self._n_packets_valid += 1
                self._last_new_data_time = time.time()

    def nof_received_indexes(self, bsn):
        """ Return current number of indices that has been received for bsn interval."""
        with self.lock:
            return len(self.received_data[bsn])

    def get_packet_bsn_index(self, _nr):
        """ get_packet_bsn_index """
        with self.lock:
            if _nr < self._n_packets_valid:
                return self._input_order[_nr]
            return (None, None)

    def get_all_bsn(self):
        """ get_all_bsn
        returns a list with all bsn numbers available in the received data dictonary
        """
        with self.lock:
            return list(sorted(self.received_data.keys()))

    def nof_received_bsn(self):
        """ Return number of bsn that has been recveived

        for statistics number of bsn counts the number of integration intervals
        for beamlets number of bsn counts the number of beamlet packets.
        """
        with self.lock:
            return len(self.received_data)

    def get_all_index(self, bsn):
        """ get_all_index
        returns a list with all index numbers available in the received data dictonary
        for requested bsn number.
        """
        with self.lock:
            return list(sorted(self.received_data[bsn].keys()))

    def is_available(self, bsn, index):
        """ is_available
        checks in received data dictonary if bsn is available and index is available in bsn.
        return True of available els False
        """
        with self.lock:
            if bsn in self.received_data:
                if index in self.received_data[bsn]:
                    return True
            return False

    def get_receive_time(self, bsn, index):
        """ get_receive_time
        return received-timestamp of packet with bsn and index number
        """
        with self.lock:
            return self.received_data[bsn][index]["recv_time"]

    def get_header(self, bsn, index):
        """ get_header
        return header of packet with bsn and index number
        """
        with self.lock:
            return self.received_data[bsn][index]["header"]

    def get_data(self, bsn, index):
        """ get_data
        return data of packet with bsn and index number
        """
        with self.lock:
            data = self.received_data[bsn][index]["data"]
            return data

    def wait_for_nof_bsn(self, exp_nof_bsn, timeout=2.0):
        """ Wait until expected number of bsn intervals has been received, or timeout.
        return True if exp_nof_bsn or more is received
        return False on timeout
        """
        # no need to use with self.lock because self.nof_received_bsn() does that
        ref_time = time.time()
        while True:
            wait_time = time.time() - ref_time
            nof_bsn = len(self.received_data)
            if nof_bsn >= exp_nof_bsn:
                self.logger.info("wait_for_nof_bsn(%d) obtained in %f s", exp_nof_bsn, wait_time)
                return True  # success
            if wait_time < timeout:
                time.sleep(0.000001)
            else:
                self.logger.error("wait_for_nof_bsn() timeout occured")
                return False  # timeout

    def wait_for_all_indices(self, bsn, exp_nof_index, timeout=12.0):
        """ Wait until expected number of indices has been received for bsn interval, or timeout.
        return True if exp_nof_index or more is received
        return False on timeout
        """
        # no need to use with self.lock because self.nof_received_indexes() does that
        ref_time = time.time()
        while True:
            wait_time = time.time() - ref_time
            nof_index = len(self.received_data[bsn])
            if nof_index >= exp_nof_index:
                self.logger.info("wait_for_all_indices() obtained in %f s", wait_time)
                return True  # success
            if wait_time < timeout:
                time.sleep(0.000001)
            else:
                self.logger.error("wait_for_all_indices() timeout occured (%d != %d)", nof_index, exp_nof_index)
                return False  # timeout


class _UdpReader(Process):
    """UdpReader
    This process reads on given udp port and stores all data into the given queue.
    The commands and response queue is used to communicate with this process.
    Commands that can be used:
    stop             : stop UDP reader
    clear-buffer     : clear input buffer (network)
    is-buffer-cleared: if input-buffer cleared returns True in response queue
    recv-on          : start receiving data
    recv-off         : stop receiving data
    received-packets : return number of received packets in response queue
    cleared-packets  : return number of cleared packets in response queue
    """

    def __init__(self, commands, response, data_type, port, out_data):
        super().__init__()
        self.logger = logging.getLogger("UdpReader")
        self.commands = commands
        self.response = response
        self.port = port
        self.data_type = data_type
        self.out_data = out_data
        self.data_type_packet_size = {
            "SST": 4128,
            "BST": 7840,
            "XST": 2336,
            "BEAMLET": 7840
        }
        self.packet_size = self.data_type_packet_size[self.data_type]
        self.stream = UdpServer(port=self.port)
        self.stream.expected_n_bytes(self.packet_size)
        self.keep_running = True
        self.recv = False
        self.clear_buffer = False
        self.max_packets = 0
        self.recv_packages_cnt = 0
        self.cleared_packages_cnt = 0

    def check_control_commands(self):
        try:
            # if the commands queue is empty it will throw a Empty exception
            cmd = self.commands.get_nowait()
            self.logger.debug("udp reader cmd = %s", cmd)
            if cmd == "stop":
                self.logger.debug("stop udp server")
                self.recv = False
                self.keep_running = False
            elif cmd == "clear-buffer":
                self.logger.debug("clear input buffer")
                self.recv = False
                self.clear_buffer = True
            elif cmd == "is-buffer-cleared":
                self.logger.debug("is buffer cleared")
                self.response.put(not self.clear_buffer)
            elif cmd == "recv-on":
                self.logger.debug("start receiving udp packages")
                self.recv = True
                self.clear_buffer = False
            elif cmd == "recv-off":
                self.logger.debug("stop receiving udp packages")
                self.recv = False
            elif "max-packets" in cmd:
                value = cmd.split("=")[1]
                self.max_packets = int(value)
            elif cmd == "received-packets":
                self.response.put(self.recv_packages_cnt)
            elif cmd == "cleared-packets":
                self.response.put(self.cleared_packages_cnt)
        except Empty:
            pass

    def run(self):
        self.logger.info("start udp server")
        
        while self.keep_running:
            # check for control commands
            self.check_control_commands()
            
            try:
                if self.recv:
                    # recv-on command is received.
                    for _ in range(100):
                        data = self.stream.recv()  # recv timeout is set to 1 sec
                        if data:
                            recv_time = datetime.datetime.now()
                            self.out_data.put((recv_time, data))
                            self.recv_packages_cnt += 1
                        else:
                            break
                    if self.max_packets and self.recv_packages_cnt >= self.max_packets:
                        self.recv = False
                    continue

                if self.clear_buffer:
                    # clear-buffer command is received.
                    for _ in range(200):
                        data = self.stream.recv()  # recv timeout is set to 1 sec
                        if not data:
                            self.clear_buffer = False
                            break
                        else:
                            self.cleared_packages_cnt += 1
                    continue

                if not self.recv or not self.clear_buffer:
                    time.sleep(0.001)

            except Full:
                self.logger.warning("raw_data queue full")
                self.recv = False

        # close udp_server (stream) and output queue
        self.stream.close()
        self.out_data.close()


class _UnpackPacket(Process):
    """
    This process listen to the in_data Queue and proccesses the received data (recvtime and rawdata)
    after processing it will put (recvtime, header, data) in the out_data Queue
    recvtime: time of receiving the packet in iso format
    header: simple class with all unpacked header info
    data: list with formatted data
    The commands queue is used to communicate with this process, used commands:
    stop  : stop this process
    unpack-on : start unpacking data from in_data and put unpacked in out_data
    unpack-off: stop unpacking data
    """

    def __init__(self, id, commands, data_type, in_data, out_data):
        super().__init__()
        self.logger = logging.getLogger("UnpackPacket")
        self.id = id
        self.commands = commands
        self.data_type = data_type
        self.in_data = in_data
        self.out_data = out_data
        self.header_size = 32
        self.type2func = {"BEAMLET": self.unpack_beamlet,
                          "SST": self.unpack_sst,
                          "BST": self.unpack_bst,
                          "XST": self.unpack_xst}

    def run(self):
        self.logger.info("start unpack")
        keep_running = True
        unpack = False
        many_cnt = []
        while keep_running:
            # check for commands
            try:
                cmd = self.commands.get_nowait()
                self.logger.debug("unpack cmd = %s", cmd)
                if cmd == "stop":
                    self.logger.debug("stop udpack")
                    unpack = False
                    keep_running = False
                elif cmd == "unpack-on":
                    self.logger.debug("start unpacking received udp packets")
                    many_cnt = []
                    unpack = True
                elif cmd == "unpack-off":
                    self.logger.debug("stop unpacking receiving udp packets")
                    self.logger.debug(f"id={self.id}: many_cnt={many_cnt}")
                    unpack = False
            except Empty:
                pass

            if unpack:
                try:
                    many_data = self.in_data.get_many_nowait(max_messages_to_get=2000)
                    many_cnt.append(len(many_data))
                    self.unpack(many_data)
                    time.sleep(random.uniform(0.00001, 0.01))
                except Empty:
                    time.sleep(random.uniform(0.0001, 0.1))
                except BaseException as err:
                    self.logger.error(err)
                    if handle_exception(err) == 1:
                        raise err
            else:
                time.sleep(0.01)

        # close input and output queue
        self.in_data.close()
        self.out_data.close()

    def unpack(self, many_data):
        """ unpack raw data and put data in the out_queue """
        for recv_time, data in many_data:
            try:
                packet_hdr, packet_data = self.split_header_data(data)
                unpacked = self.type2func[self.data_type](packet_hdr, packet_data)
                if unpacked is not None:
                    hdr = unpacked[0]
                    data = unpacked[1]
                    self.out_data.put((recv_time.isoformat(), hdr, data))
            except Full:
                self.logger.warning("unpacked_data queue full")
                time.sleep(0.001)
            except BaseException as err:
                self.logger.error(err)
                if handle_exception(err) == 1:
                    raise err

    def split_header_data(self, packet):
        """ split_header_data """
        # if len(packet) >= self.header_size:
        try:
            _header = packet[:self.header_size]
            _data = packet[self.header_size:]
            return _header, _data
        except IndexError:
            self.logger.error("Wrong packet size")
        return None, None

    def unpack_sst(self, packet_hdr, packet_data):
        """ Unpacking the sst header and data, see [1]

        https://docs.python.org/3/library/struct.html?highlight=struct#module-struct
        """
        hdr = None
        data = None
        try:
            fmt = ">BBLHHLLBBHHQ"
            hdr_list = struct.unpack(fmt, packet_hdr)
            hdr = stat_header_data(hdr_list)
        except struct.error:
            self.logger.error("unpacking sst header failed")
            return None

        if hdr["data_type"] != "SST":
            return None

        try:
            n_words = hdr["nof_statistics_per_packet"]
            fmt = ">" + "Q" * n_words  # > = big-endian, Q = 8byte unsigned long long
            data_list = list(struct.unpack(fmt, packet_data))

            # Purpose: Wrap an integer value to w bits
            data_w = 54
            wrap_sign = 2 ** (data_w - 1)
            wrap_mask = 2 ** (data_w - 1) - 1

            data = []
            for _data in data_list:
                if _data & wrap_sign:
                    data.append((_data & wrap_mask) - wrap_sign)
                else:
                    data.append(_data & wrap_mask)
        except struct.error:
            self.logger.error("unpacking sst data failed")
            return None
        return hdr, data

    def unpack_bst(self, packet_hdr, packet_data):
        """ Unpacking the bst header and data, see [1]

        https://docs.python.org/3/library/struct.html?highlight=struct#module-struct
        """
        hdr = None
        data = None
        try:
            fmt = ">BBLHHLLBBHHQ"
            hdr_list = struct.unpack(fmt, packet_hdr)
            hdr = stat_header_data(hdr_list)
        except struct.error:
            self.logger.error("unpacking bst header failed")
            return None

        if hdr["data_type"] != "BST":
            return None

        try:
            n_words = hdr["nof_statistics_per_packet"]
            fmt = ">" + "Q" * n_words  # > = big-endian, Q = 8byte unsigned long long
            data = list(struct.unpack(fmt, packet_data))
        except struct.error:
            self.logger.error("unpacking bst data failed")
            return None

        return hdr, data

    def unpack_xst(self, packet_hdr, packet_data):
        """ Unpacking the xst header and data, see [1]

        https://docs.python.org/3/library/struct.html?highlight=struct#module-struct
        """
        hdr = None
        data = None
        try:
            fmt = ">BBLHHLLBBHHQ"
            hdr_list = struct.unpack(fmt, packet_hdr)
            hdr = stat_header_data(hdr_list)
        except struct.error:
            self.logger.error("unpacking xst header failed")
            return None

        if hdr["data_type"] != "XST":
            return None

        try:
            n_words = hdr["nof_statistics_per_packet"]
            fmt = ">" + "q" * n_words  # > = big-endian, q = 8byte signed long long
            data_list = list(struct.unpack(fmt, packet_data))

            n_complex_stats = hdr["nof_statistics_per_packet"] // 2
            data = [
                # Python complex() function order is complex(Re, Im).
                # Payload data order index [N_complex] is 0 = Re, 1 = Im, so data_list
                # in order Re, Im, Re, Im, ... see [1]
                complex(float(data_list[2 * i]), float(data_list[2 * i + 1]))
                for i in range(n_complex_stats)
            ]

        except struct.error:
            self.logger.error("unpacking xst data failed")
            return None
        return hdr, data

    def unpack_beamlet(self, packet_hdr, packet_data):
        """ Unpacking the beamlet header and data, see [2]

        https://docs.python.org/3/library/struct.html?highlight=struct#module-struct
        """
        hdr = None
        data = None
        try:
            fmt = ">BBLHHBLHHBHHQ"
            hdr_list = struct.unpack(fmt, packet_hdr)
            hdr = beamlet_header_data(hdr_list)
        except struct.error:
            self.logger.error("unpacking beamlet header failed")
            return None

        if hdr["data_type"] != "BEAMLET":
            return None

        try:
            n_words = 4 * hdr["n_beamlet"]  # 4 = 2 * 2  = n_pol_bf * n_complex
            fmt = ">" + "b" * n_words  # > = big-endian, b = 8byte signed char
            data_list = list(struct.unpack(fmt, packet_data))
            # Python complex() function order is complex(Re, Im).
            # Data order in data_list: Re, Im, Re, Im ..., see [2]
            data = [
                complex(float(data_list[2 * i]), float(data_list[2 * i + 1]))
                for i in range(hdr["n_beamlet"] * 2)  # n_pol_bf = 2
            ]
        except struct.error:
            self.logger.error("unpacking beamlet data failed")
            return None
        return hdr, data


class StreamReader:
    """
    This is the only class the user should use.

    This StreamReader class can read:
    - Statistic data (SST, BST and XST)
    - Beamlet data (BEAMLET)
    The received data is unpacked and can be accessed from self.data
    calling start() starts receiving and unpacking data
    calling stop() stops receiving data and stops

    """

    def __init__(self, port, data_type, n_workers=None):
        """
        port: used udp port to receive data
        data_type: type of data to receive, can be one of [SST, BST, XST, BEAMLET]
        """
        self.logger = logging.getLogger("StreamReader")
        self.port = port
        self.data_type = data_type
        self._reader = None
        self._unpack_workers = []
        self._n_workers = 1 if n_workers is None else n_workers
        self._running = False

        self._raw_data = Queue(int(1e10))
        self._unpacked_data = Queue(int(1e8))
        self._reader_commands = Queue(256)
        self._reader_response = Queue(256)
        self._unpack_commands = [Queue(256) for i in range(self._n_workers)]
        self.data = _StreamData(self.data_type, self._unpacked_data)

    def __del__(self):
        """ on closing this class the data class and queue are closed """
        self.data.stop()
        self._raw_data.close()
        self._unpacked_data.close()
        self._running = False

    def done(self):
        """ checks if all received data is processed and return True if done else False """
        return (self._raw_data.qsize() == 0) and (self._unpacked_data.qsize() == 0) and not self.data.busy()

    def isactive(self):
        """ return True if Streamreader is active """
        return self._running

    def set_max_packets(self, max_packets):
        self._reader_commands.put(f"max-packets={max_packets}")

    def clear_reader_buffer(self):
        """ clear udp reader input buffer (network)
        return number of cleared packets
        """
        self._reader_commands.put("clear-buffer")
        while True:
            self._reader_commands.put("is-buffer-cleared")
            try:
                cleared = self._reader_response.get(timeout=2.0)
            except Empty:
                break
            if cleared:
                break
            time.sleep(0.1)
        self._reader_commands.put("cleared-packets")
        try:
            cleared_cnt = self._reader_response.get(timeout=2.0)
        except Empty:
            cleared_cnt = 0
        return cleared_cnt

    def empty_queue(self, q):
        """ empty_queue
        queues are used by more than one process, the queue is closed before
        closing the processes to prefent hanging.
        """
        while True:
            try:
                q.get_many_nowait()
            except Empty:
                break

    def start(self):
        """ start stream reader
        clear old udp network packets
        setup 1 data thread
        setup n unpack_workers processes
        setup 1 udp reader process
        start unpacking
        start receiving
        """
        self.logger.debug("== Start stream reader with %d workers ==", self._n_workers)
        self.data.start()
        # start unpack and reader process
        for worker_nr in range(self._n_workers):
            _p = _UnpackPacket(worker_nr, self._unpack_commands[worker_nr], self.data_type, self._raw_data, self._unpacked_data)
            self._unpack_workers.append(_p)
            _p.start()
        self._reader = _UdpReader(self._reader_commands, self._reader_response, self.data_type, self.port, self._raw_data)
        self._reader.start()

        cleared_cnt = self.clear_reader_buffer()
        self.logger.debug(f"start cleared {cleared_cnt} udp packets")

        # start unpacking and receiving
        for worker_nr in range(self._n_workers):
            (self._unpack_commands[worker_nr]).put("unpack-on")
        self._reader_commands.put("recv-on")
        self._running = True

    def stop(self):
        """ stop stream reader
        stop receiving
        clear udp network buffer
        stop unpacking
        log some statistics
        empty queues
        stop udp reader
        stop unpack workers
        """
        self.logger.info("== stop stream reader ==")

        # stop receiving udp data
        self.logger.debug("== stop receiving udp data ==")
        self._reader_commands.put("recv-off")

        cleared_cnt = self.clear_reader_buffer()
        self.logger.debug(f"stop cleared {cleared_cnt} udp packets")

        # stop receiving and unpacking
        self.logger.debug("== stop unpacking raw data ==")
        for worker_nr in range(self._n_workers):
            (self._unpack_commands[worker_nr]).put("unpack-off")

        time.sleep(0.5)
        # log received packages by reader
        self.logger.debug(f"not processed {self._raw_data.qsize()} raw packets")
        self.logger.debug(f"not processed {self._unpacked_data.qsize()} unpacked packets")

        self._reader_commands.put("received-packets")
        cnt = self._reader_response.get(timeout=2.0)
        self.logger.info(f"Received {cnt} udp packets")

        # empty the queue's
        self.logger.debug("empty raw queue")
        self.empty_queue(self._raw_data)
        self.logger.debug("empty unpacked queue")
        self.empty_queue(self._unpacked_data)

        # stop reader and  unpacking process
        self.logger.debug("== stop reader and workers ==")
        self._reader_commands.put("stop")
        for worker_nr in range(self._n_workers):
            (self._unpack_commands[worker_nr]).put("stop")
        time.sleep(0.5)

        # terminate reader and unpack processes if still running after stop command
        self.logger.debug("== Terminating children ==")
        for child in active_children():
            self.logger.debug(f"Terminating: {child}")
            child.terminate()
        time.sleep(0.5)
