# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . general constants
# ##########################################################################

from .attribute_dict import AttributeDict

""" SDP Constants attribute dict """
C_SDP = AttributeDict()

# fill dict with constant values
C_SDP.N_pn_max = 16
C_SDP.fpgas_per_board = 4
C_SDP.N_fft = 1024
C_SDP.N_sub = 512
C_SDP.N_pol = 2  # Number of antenna polarizations, X and Y.
C_SDP.N_pol_bf = 2  # Number of beam polarizations, X and Y.
C_SDP.A_pn = 6  # Number of dual polarization antennas per Processing Node (PN) FPGA.
C_SDP.S_pn = C_SDP.N_pol * C_SDP.A_pn  # n signals per processing node
C_SDP.S_sub_bf = 488
C_SDP.f_sub = 195312.5  # = f_adc_MHz / N_fft
C_SDP.f_adc_MHz = 200
C_SDP.W_adc = 14
C_SDP.V_si_db = 1024
C_SDP.N_crosslets_max = 7
C_SDP.P_pfb = 6
C_SDP.P_sq = (C_SDP.N_pn_max // 2) + 1
C_SDP.X_sq = C_SDP.S_pn * C_SDP.S_pn  # = 144
C_SDP.G_subband_sine = 0.994817 * 0.5 * 2**5 * 1.0  # ~= 16 for unit subband
C_SDP.W_sub_weight_fraction = 14
C_SDP.W_bf_weight_fraction = 14
C_SDP.W_beamlet_scale_fraction = 15
