"""
handel exception and print traceback
"""

import sys
import traceback


def handle_exception(err):
    """
    handle exception err
    on keyboard interrupt (Ctrl-C) return exit_code 1
    on other exception print traceback and return exit-code 1
    """
    exit_code = 0
    try:
        raise err
    except KeyboardInterrupt:
        print("\n user hit ctrl-c")
        exit_code = 1
    except BaseException:
        print("")
        print("-" * 80)
        traceback.print_exc(limit=2, file=sys.stdout)
        print("-" * 80)
        
        print("Aborting NOW")
        exit_code = 1
    return exit_code
