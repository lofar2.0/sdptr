#! /usr/bin/python3
###############################################################################
#
# Copyright (C) 2012
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

# Author:  P.Donker
# Date  : mrt 2022 created

"""
  PlotLines class
  this plot class is used to realtime update plots
"""

import matplotlib
matplotlib.use('TkAgg')
from matplotlib import pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np


class Plotting:
    def __init__(self, fig_size, rows, cols):
        """
        figsize: in inches
        subplots: tuple (rows, cols)
        """
        self.rows = rows
        self.cols = cols
        self.nplots = self.rows * self.cols

        self.fig, self.axs = plt.subplots(self.rows, self.cols, figsize=fig_size)
        self.fig.set_tight_layout(True)
        self.data = {}

    def get_axs(self, row, col):
        if self.rows == 1 and self.cols == 1:
            return self.axs
        if self.rows == 1:
            return self.axs[col]
        if self.cols == 1:
            return self.axs[row]
        return self.axs[row, col]

    def add_plot(self, row, col, n_lines):
        """
        row, col: which subplot to use
        """
        for line in range(n_lines):
            if self.rows * self.cols > 1:
                key = f'{row}_{col}_{line}'
                self.data[key], = self.axs[row, col].plot([], lw=1)
            else:
                key = f'{line}'
                self.data[key], = self.axs.plot([], lw=1)

    def update_plot(self, row, col, xdata, ydata, auto_xlimit=None, auto_ylimit=None):
        """ update data using this function """
        auto_xlimit = False if auto_xlimit is None else auto_xlimit
        auto_ylimit = False if auto_ylimit is None else auto_ylimit

        if auto_xlimit:
            xmin = np.min(xdata) * 0.95
            xmax = np.max(xdata) * 1.05
            if xmin == xmax:
                xmax = xmin + 1
            self.set_x_limit(row, col, (xmin, xmax))

        if auto_ylimit:
            ymin = np.min(ydata) * 0.95
            ymax = np.max(ydata) * 1.05
            if ymin == ymax:
                ymax = ymin + 1
            self.set_y_limit(row, col, (ymin, ymax))

        for line in range(ydata.shape[0]):
            if self.rows * self.cols > 1:
                key = f'{row}_{col}_{line}'
            else:
                key = f'{line}'
            self.data[key].set_data(xdata, ydata[line])

    def update_plot_line(self, row, col, line, xdata, ydata, auto_xlimit=None, auto_ylimit=None):
        """ update data using this function """
        auto_xlimit = False if auto_xlimit is None else auto_xlimit
        auto_ylimit = False if auto_ylimit is None else auto_ylimit
        plot_nr = row * col + col
        if auto_xlimit:
            xmin = np.min(xdata) * 0.95
            xmax = np.max(xdata) * 1.05
            if xmin == xmax:
                xmax = xmin + 1
            self.set_x_limit(plot_nr, (xmin, xmax))

        if auto_ylimit:
            ymin = np.min(ydata) * 0.95
            ymax = np.max(ydata) * 1.05
            if ymin == ymax:
                ymax = ymin + 1
            self.set_y_limit(plot_nr, (ymin, ymax))

        if self.rows * self.cols > 1:
            key = f'{row}_{col}_{line}'
        else:
            key = f'{line}'
        self.data[key].set_data(xdata, ydata)

    def add_imshow(self, row, col, im_size, vmin=None, vmax=None):
        vmin = 0 if vmin is None else vmin
        vmax = 0 if vmax is None else vmax
        key = f'{row}_{col}'
        data = np.zeros(im_size)
        self.data[key] = self.get_axs(row, col).imshow(data, cmap='jet', norm=colors.Normalize(vmin=vmin, vmax=vmax))
        # self.data[key] = self.get_axs(row, col).imshow(data, cmap='jet')

        # create an axes on the right side of ax. The width of cax will be 5%
        # of ax and the padding between cax and ax will be fixed at 0.05 inch.
        divider = make_axes_locatable(self.get_axs(row, col))
        cax = divider.append_axes("right", size="5%", pad=0.05)
        plt.colorbar(self.data[key], cax=cax)

    def update_imshow(self, row, col, data):
        key = f'{row}_{col}'
        self.data[key].set_data(data)

    def set_title(self, row, col, title, fontsize=14):
        self.get_axs(row, col).set_title(title, fontsize=fontsize)

    def set_xas_label(self, row, col, label, fontsize=12):
        self.get_axs(row, col).set_xlabel(label, fontsize=fontsize)

    def set_yas_label(self, row, col, label, fontsize=12):
        self.get_axs(row, col).set_ylabel(label, fontsize=fontsize)

    def set_xtick_labels(self, row, col, labels):
        self.get_axs(row, col).set_xticks(np.arange(len(labels)), labels)

    def set_ytick_labels(self, row, col, labels):
        self.get_axs(row, col).set_yticks(np.arange(len(labels)), labels)

    def rotate_xtick_label(self, row, col, angle):
        # Rotate the tick labels and set their alignment.
        plt.setp(self.get_axs(row, col).get_xticklabels(), rotation=angle, ha="right", rotation_mode="anchor")

    def rotate_ytick_label(self, row, col, angle):
        # Rotate the tick labels and set their alignment.
        plt.setp(self.get_axs(row, col).get_yticklabels(), rotation=angle, ha="right", rotation_mode="anchor")

    def set_grid(self, row, col):
        self.get_axs(row, col).grid()

    def set_limits(self, row, col, xlim, ylim):
        self.get_axs(row, col).set_xlim(xlim)
        self.get_axs(row, col).set_ylim(ylim)

    def set_x_limit(self, row, col, xlim):
        self.get_axs(row, col).set_xlim(xlim)

    def set_y_limit(self, row, col, ylim):
        self.get_axs(row, col).set_ylim(ylim)

    def set_legend(self, row, col, legend_list):
        self.get_axs(row, col).legend(legend_list, fontsize='xx-small', loc='upper right')

    def set_text(self, row, col, xpos, ypos, text, value):
        textcolors = ["w", "k"]
        text = self.get_axs(col, row).text(xpos, ypos, text, ha="center", va="center", color=textcolors[int(np.norm(value) > 0)])

    def draw(self):
        """ draw screen after setting it up """
        plt.show(block=False)
        self.update_graph()

    def update_graph(self):
        """ after updating all data call this function to update the plots """
        self.fig.gca().relim()
        self.fig.gca().autoscale_view()
        self.fig.canvas.draw()


# =================================================================================
# test part for PlotLines class
# run with: python3 ./plot_lines.py
if __name__ == "__main__":
    nrows = 4
    ncols = 4
    nlines = 12

    fig = Plotting(fig_size=(18, 12), rows=nrows, cols=ncols)

    for row in range(nrows):
        for col in range(ncols):
            fig.add_plot(row, col, nlines)
            fig.set_limits(row, col, (0, 50), (-1, 1))
            fig.set_legend(row, col, [f'p{i}' for i in range(nlines)])
            fig.set_title(row, col, f'{nlines} lines')
            fig.set_xas_label(row, col, 'xas')
            fig.set_yas_label(row, col, 'yas')
            fig.set_grid(row, col)

    # first setup figure then draw()
    fig.draw()

    # update data of figure
    xlen = 100
    ylen = 100

    xdata = np.zeros(xlen)
    ydata = np.zeros((nlines, ylen))
    xdata = np.linspace(0., 50., num=xlen)
    k = 0.0

    for i in range(1000):
        for i in range(nlines):
            ydata[i, :] = np.sin(xdata / 3. + k + i / 10)
        k += 0.11

        for row in range(nrows):
            for col in range(ncols):
                fig.update_plot(row, col, xdata, ydata)
        fig.update_graph()
