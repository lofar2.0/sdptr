# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . universal tools
# ##########################################################################

import time
import string
from functools import wraps


def arg_str_to_list(arg_str, sort=None):
    """
    = Convert argument string to (sorted)list =

    def arg_str_to_list(arg_str, [sort=True])
    - arg_str = argument string to convert
    - sort = True|False (optional)

    arg_str_to_list("0,1,2,3,10:13,9") ==> [0,1,2,3,9,10,11,12,13]
    arg_str_to_list("0,1,2,3,10:13,9", False) ==> [0,1,2,3,10,11,12,13,9]

    """
    sort = sort = True if sort is None else sort
    _arg_list = []
    _range = False
    _nr_str = ''
    _last_nr = 0
    
    if not arg_str:
        return []

    for _ch in arg_str:
        if _ch in string.digits or _ch == '-':
            _nr_str += _ch
        elif _ch == ',':
            _nr = int(_nr_str)
            if _range:
                for i in range(_last_nr, _nr + 1, 1):
                    _arg_list.append(i)
                _range = False
            else:
                _arg_list.append(_nr)
            _last_nr = _nr
            _nr_str = ''
        elif _ch == ':':
            _nr = int(_nr_str)
            _last_nr = _nr
            _nr_str = ''
            _range = True
        else:
            pass

    # process last number
    _nr = int(_nr_str)
    if _range:
        for i in range(_last_nr, _nr + 1, 1):
            _arg_list.append(i)
        _range = False
    else:
        _arg_list.append(_nr)

    # return sorted or unsorted
    if sort is True:
        return sorted(_arg_list)
    return _arg_list


def timing(_f):
    """
    timing decorator
    put @timing before the function to time
    """
    @wraps(_f)
    def wrap(*args, **kw):
        _ts = time.time()
        result = _f(*args, **kw)
        _te = time.time()
        print('func:{} args:[{}, {}] took: {:2.4f} sec'.format(_f.__name__, args, kw, _te - _ts))
        return result
    return wrap
