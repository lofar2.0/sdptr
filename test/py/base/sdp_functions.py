# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Eric Kooistra
# Purpose:
# . SDP functions
# ##########################################################################

import time


def parse_stream_destination(mac_str, ip_str, type):
    """ Parse mac, ip arguments

    Support mac,ip strings, or derive mac,ip strings from machine name string.
    The sdptr ignores illegal format strings.
    type: can be 1G or 10G
    """
    c_machines_1gbe = {  # 1GbE
        "dop36" : ["00:1B:21:71:76:B9", "10.99.0.254"],  # dop36, enp2s0
        "dop369" : ["00:15:17:be:d2:49", "10.99.0.253"],
        "dop386" : ["00:15:17:98:5f:bf", "10.99.0.253"],  # dop386, enp5s0
        "dop421" : ["00:15:17:aa:22:9c", "10.99.0.254"],  # dop421, enp67s0f1
    }

    c_machines_10gbe = {  # 10GbE
        "dop386" : ["00:07:43:06:c7:00", "192.168.0.1"],  # dop386, ens2
    }

    # If dop386 ens2 10GbE interface lost its IP or mtu settings (e.g. due
    # to PC reboot or cable reconnect), then to recover do:
    # > sudo ifconfig ens2 192.168.0.1 netmask 255.255.0.0
    # > sudo ip link set dev ens2 mtu 9000
    # > sudo ifconfig ens2 down
    # > sudo ifconfig ens2 up

    if type == "1G":
        c_machines = c_machines_1gbe
    elif type == "10G":
        c_machines = c_machines_10gbe
    else:
        c_machines = {}
        print(f"Unknown port type '{type}'")

    stream_mac = mac_str
    stream_ip = ip_str
    if mac_str in c_machines:
        stream_mac = c_machines[mac_str][0]
    if ip_str in c_machines:
        stream_ip = c_machines[ip_str][1]
    return stream_mac, stream_ip


def start_processing(client):
    """ Start SDP processing if not already started."""
    if False in client.read("processing_enable"):
        client.write("processing_enable", [True] * client.n_nodes)
        time.sleep(3.0)  # wait for SDP processing is on again


def stop_all_streams(client):
    """ Stop all streams if enabled."""
    if True in client.read("sst_offload_enable"):
        client.write("sst_offload_enable", [False] * client.n_nodes)
    if True in client.read("bst_offload_enable"):
        client.write("bst_offload_enable", [False] * client.n_nodes)
    if True in client.read("xst_offload_enable"):
        client.write("xst_offload_enable", [False] * client.n_nodes)
    if True in client.read("beamlet_output_enable"):
        client.write("beamlet_output_enable", [False] * client.n_nodes)
    time.sleep(2.0)  # wait for streams stopping
