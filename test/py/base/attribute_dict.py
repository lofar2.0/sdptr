# ##########################################################################
# Copyright 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ##########################################################################

# ##########################################################################
# Author:
# . Pieter Donker
# Purpose:
# . attribute dictonary
# ##########################################################################

from collections import defaultdict


class AttributeDict(defaultdict):
    """ make attribute from dict """
    def __init__(self, data=None):
        super(AttributeDict, self).__init__(AttributeDict)
        if data is not None:
            for k, v in data.items():
                self[k] = v

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            print(f"Error: key {key} not found")
            raise AttributeError(key)

    def __setattr__(self, key, value):
        self[key] = value

    def __delattr__(self, key):
        del self[key]
