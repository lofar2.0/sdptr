#!/usr/bin/env bash
# set -e
###############################################################################
#
# Copyright (C) 2023
# ASTRON (Netherlands Institute for Radio Astronomy) <http://www.astron.nl/>
# P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# $Id$
#
###############################################################################

#
# Initialisation script to setup the environment variables in this terminal
#

# only set variables if we didn't set them before
if [ "${jupyter_init_read:-not_set}" = "not_set" ]; then

# Make sure the script is sourced and not accidentally given execution rights and just executes it.
if [[ "$_" == "${0}" ]]; then
    echo "ERROR: Use this command with '. ' or 'source '"
    sleep 1
    exit
fi

echo "This Jupyter terminal will be setup for running python test scripts."

# Figure out where this script is located and set environment variables accordingly
SDPTR_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
export UNB2_IMAGES="${HOME}/notebooks/unb2_images"

source ${SDPTR_DIR}/generic.sh
source ${SDPTR_DIR}/init_sdptr_python.sh

export HL="sdptr-lba.service.consul"
export HH0="sdptr-hba0.service.consul"
export HH1="sdptr-hba1.service.consul"

echo "+=================================+"
echo "| Aliases set for use with --host |"
echo "|                                 |"
echo "| For LBA  use --host \$HL        |"
echo "|     HBA0 use --host \$HH0       |"
echo "|     HBA1 use --host \$HH1       |"
echo "+=================================+"
echo ""

# Mark the fact that we read this file and end the guarded part
jupyter_init_read="yes"
fi
